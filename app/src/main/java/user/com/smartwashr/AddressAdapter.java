package user.com.smartwashr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.activities.MapsActivity;
import user.com.smartwashr.activities.ZeplinProducts;
import user.com.smartwashr.adapter.AddressView;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.addressesResponse.Address;
import user.com.smartwashr.models.addressesResponse.AddressesResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by farazqureshi on 09/05/2018.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressView> implements ResponseHandler {

    Activity activity;
    ArrayList<Address> list;
    private String address, lat, lng;
    AlertDialog.Builder alertDialogBuilder;
    SessionManager sessionManager;
    TextView no_data;

    public AddressAdapter(Activity activity, ArrayList<Address> list, TextView no_data) {
        this.activity = activity;
        this.list = list;
        this.no_data = no_data;
    }

    @Override
    public AddressView onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.addres_view, viewGroup, false);
        sessionManager = new SessionManager(activity);
        return new AddressView(v);
    }

    @Override
    public void onBindViewHolder(AddressView holder, int i) {
        final Address item = list.get(i);

        holder.title.setText(item.getName());
//        holder.building.setText(item.getBuildingName());
        holder.address.setText(item.getAddressLine1());
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, MapsActivity.class);
                i.putExtra("lat", item.getLat());
                i.putExtra("lng", item.getLng());
                Constants.address = item;
                activity.startActivity(i);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogBuilder = new AlertDialog.Builder(activity);
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    alertDialogBuilder.setMessage("Do you want to delete this address?");
                    alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getYesLable(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            deleteAddress(item.getId() + "");
                        }
                    });
                    alertDialogBuilder.setNegativeButton(Constants.TRANSLATIONS.getNoLable(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                } else {
                    alertDialogBuilder.setMessage("تبغا تحذف موقعك؟");
                    alertDialogBuilder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            deleteAddress(item.getId() + "");
                        }
                    });
                    alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                address = item.getAddressLine1();
                if (Internet.isAvailable(activity)) {
                    lat = item.getLat();
                    lng = item.getLng();
                    new RestCaller(AddressAdapter.this, SmartWashr.getRestClient().nearest_laundry("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), item.getLat(), item.getLng()), 1);
//                    new RestCaller(activity, SmartWashr.getRestClient().nearest_laundry("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), Constants.SAUDI_LNG, Constants.SAUDI_LAT), 1);
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(activity, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(activity, false, "الرجاء الانتظار");
                    }
                }
            }
        });
    }

    private void deleteAddress(String id) {
        if (Internet.isAvailable(activity)) {
            new RestCaller(AddressAdapter.this, SmartWashr.getRestClient().deleteAddress("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), id), 2);
//                    new RestCaller(activity, SmartWashr.getRestClient().nearest_laundry("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), Constants.SAUDI_LNG, Constants.SAUDI_LAT), 1);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(activity, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(activity, false, "الرجاء الانتظار");
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 1) {
            GenericResponse genericResponse = (GenericResponse) response.body();
            if (genericResponse.getDriver_id() != null && genericResponse.getLaundry_id() != null) {
                sessionManager.put(Constants.DRIVER_ID, String.valueOf(genericResponse.getDriver_id()));
                sessionManager.put(Constants.LAUNDRY_ID, genericResponse.getLaundry_id());
                sessionManager.put(Constants.LANE1, address);
                sessionManager.put(Constants.LATITUDE, lat);
                sessionManager.put(Constants.LONGITUDE, lng);
                activity.startActivity(new Intent(activity, ZeplinProducts.class));
            }
        } else {
            list.clear();
            AddressesResponse res = (AddressesResponse) response.body();
            if (res.getSuccess()) {
                if (res.getAddresses().size() > 0) {
                    no_data.setVisibility(View.GONE);
                    list.addAll(res.getAddresses());
                    notifyDataSetChanged();
                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        no_data.setVisibility(View.VISIBLE);
                    } else {
                        no_data.setVisibility(View.VISIBLE);
                        no_data.setText("لا يوجد مواقع مسجل سابقاً");
                    }
                    notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(activity);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }



}
