package user.com.smartwashr.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import user.com.smartwashr.adapter.ProductsOrder;
import user.com.smartwashr.newModels.OrderProduct;

/**
 * Created by zeeshan on 6/12/17.
 */

public class ProductOrder implements Serializable {

    @SerializedName("post_code")
    @Expose
    private String postCode;
    @SerializedName("laundry_id")
    @Expose
    private String laundryId;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("address")
    @Expose
    private String addressOne;
    @SerializedName("driver_id")
    @Expose
    private Integer driverId;
    @SerializedName("address_two")
    @Expose
    private String addressTwo;
    @SerializedName("lat")
    @Expose
    private String latitude;
    @SerializedName("lng")
    @Expose
    private String longitude;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("delivery_instruction")
    @Expose
    private String deliveryInstruction;
    @SerializedName("pickup_time")
    @Expose
    private String collectionDateTime;
    @SerializedName("coupon_code")
    @Expose
    private String coupon_code;
    @SerializedName("discounted_price")
    @Expose
    private String discounted_price;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryDateTime;
    @SerializedName("collection_date_time_to")
    @Expose
    private String collection_date_time_to;
    @SerializedName("products_order")
    @Expose
    private List<ProductsOrder> productsOrder = null;
    @SerializedName("products")
    @Expose
    private List<OrderProduct> orderProducts = null;
    @SerializedName("payment_method")
    @Expose
    private String payment_method;
    @SerializedName("user_comments")
    @Expose
    private String user_comments;
    @SerializedName("coupon_id")
    @Expose
    private Integer couponId;


    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getLaundryId() {
        return laundryId;
    }

    public void setLaundryId(String laundryId) {
        this.laundryId = laundryId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getAddressTwo() {
        return addressTwo;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDeliveryInstruction() {
        return deliveryInstruction;
    }

    public void setDeliveryInstruction(String deliveryInstruction) {
        this.deliveryInstruction = deliveryInstruction;
    }

    public String getCollectionDateTime() {
        return collectionDateTime;
    }

    public void setCollectionDateTime(String collectionDateTime) {
        this.collectionDateTime = collectionDateTime;
    }

    public String getDeliveryDateTime() {
        return deliveryDateTime;
    }

    public void setDeliveryDateTime(String deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    public List<ProductsOrder> getProductsOrder() {
        return productsOrder;
    }

    public void setProductsOrder(List<ProductsOrder> productsOrder) {
        this.productsOrder = productsOrder;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCollection_date_time_to() {
        return collection_date_time_to;
    }

    public void setCollection_date_time_to(String collection_date_time_to) {
        this.collection_date_time_to = collection_date_time_to;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getDiscounted_price() {
        return discounted_price;
    }

    public void setDiscounted_price(String discounted_price) {
        this.discounted_price = discounted_price;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getUser_comments() {
        return user_comments;
    }

    public void setUser_comments(String user_comments) {
        this.user_comments = user_comments;
    }

    public List<OrderProduct> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<OrderProduct> orderProducts) {
        this.orderProducts = orderProducts;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }
}