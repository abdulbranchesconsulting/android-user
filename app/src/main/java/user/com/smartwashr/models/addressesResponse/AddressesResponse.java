package user.com.smartwashr.models.addressesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by farazqureshi on 09/05/2018.
 */

public class AddressesResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

}
