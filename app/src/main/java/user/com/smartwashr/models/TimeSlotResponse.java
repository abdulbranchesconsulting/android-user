package user.com.smartwashr.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TimeSlotResponse {

    @SerializedName("all_days_time")
    @Expose
    private ArrayList<String> allDaysTime = null;
    @SerializedName("current_day_time")
    @Expose
    private ArrayList<String> currentDayTime = null;
    @SerializedName("open_time")
    @Expose
    private Integer openTime;
    @SerializedName("close_time")
    @Expose
    private Integer closeTime;
    @SerializedName("delievery_gap_in_hr")
    @Expose
    private Integer delieveryGapInHr;

    public ArrayList<String> getAllDaysTime() {
        return allDaysTime;
    }

    public void setAllDaysTime(ArrayList<String> allDaysTime) {
        this.allDaysTime = allDaysTime;
    }

    public ArrayList<String> getCurrentDayTime() {
        return currentDayTime;
    }

    public void setCurrentDayTime(ArrayList<String> currentDayTime) {
        this.currentDayTime = currentDayTime;
    }

    public Integer getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public Integer getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getDelieveryGapInHr() {
        return delieveryGapInHr;
    }

    public void setDelieveryGapInHr(Integer delieveryGapInHr) {
        this.delieveryGapInHr = delieveryGapInHr;
    }

}
