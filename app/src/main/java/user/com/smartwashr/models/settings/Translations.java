package user.com.smartwashr.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Translations {

    @SerializedName("about_label")
    @Expose
    private String aboutLabel;
    @SerializedName("active_label")
    @Expose
    private String activeLabel;
    @SerializedName("add_label")
    @Expose
    private String addLabel;
    @SerializedName("search_lable")
    @Expose
    private String searchLabel;
    @SerializedName("add_new_address_label")
    @Expose
    private String addNewAddressLabel;
    @SerializedName("add_promo_code_label")
    @Expose
    private String addPromoCodeLabel;
    @SerializedName("allow_location_desription_label")
    @Expose
    private String allowLocationDesriptionLabel;
    @SerializedName("and_a_screenshot_want_label")
    @Expose
    private String andAScreenshotWantLabel;
    @SerializedName("attention_lable")
    @Expose
    private String attentionLable;
    @SerializedName("call_us_label")
    @Expose
    private String callUsLabel;
    @SerializedName("cancel_label")
    @Expose
    private String cancelLabel;
    @SerializedName("cancel_order_label")
    @Expose
    private String cancelOrderLabel;
    @SerializedName("carpet_curtains_description_label")
    @Expose
    private String carpetCurtainsDescriptionLabel;
    @SerializedName("carpet_curtains_label")
    @Expose
    private String carpetCurtainsLabel;
    @SerializedName("cash_on_delivery_label")
    @Expose
    private String cashOnDeliveryLabel;
    @SerializedName("change_label")
    @Expose
    private String changeLabel;
    @SerializedName("change_language_label")
    @Expose
    private String changeLanguageLabel;
    @SerializedName("change_password_label")
    @Expose
    private String changePasswordLabel;
    @SerializedName("completed_label")
    @Expose
    private String completedLabel;
    @SerializedName("confirm_label")
    @Expose
    private String confirmLabel;
    @SerializedName("confirm_password_can_not_be_empty_label")
    @Expose
    private String confirmPasswordCanNotBeEmptyLabel;
    @SerializedName("confirm_password_label")
    @Expose
    private String confirmPasswordLabel;
    @SerializedName("confirm_pickup_label")
    @Expose
    private String confirmPickupLabel;
    @SerializedName("contact_us_label")
    @Expose
    private String contactUsLabel;
    @SerializedName("continue_facebook")
    @Expose
    private String continueFacebook;
    @SerializedName("continue_lable")
    @Expose
    private String continueLable;
    @SerializedName("cost_label")
    @Expose
    private String costLabel;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("current_password_label")
    @Expose
    private String currentPasswordLabel;
    @SerializedName("deactivate_msg")
    @Expose
    private String deactivateMsg;
    @SerializedName("delete_account_label")
    @Expose
    private String deleteAccountLabel;
    @SerializedName("delete_lable")
    @Expose
    private String deleteLable;
    @SerializedName("delete_msg")
    @Expose
    private String deleteMsg;
    @SerializedName("delivered_to_label")
    @Expose
    private String deliveredToLabel;
    @SerializedName("delivery_charges_label")
    @Expose
    private String deliveryChargesLabel;
    @SerializedName("done_label")
    @Expose
    private String doneLabel;
    @SerializedName("dropoff_date_label")
    @Expose
    private String dropoffDateLabel;
    @SerializedName("email_can_not_be_empty_label")
    @Expose
    private String emailCanNotBeEmptyLabel;
    @SerializedName("email_label")
    @Expose
    private String emailLabel;
    @SerializedName("enable_location_description_label")
    @Expose
    private String enableLocationDescriptionLabel;
    @SerializedName("enter_code_label")
    @Expose
    private String enterCodeLabel;
    @SerializedName("error_label")
    @Expose
    private String errorLabel;
    @SerializedName("faq_help_label")
    @Expose
    private String faqHelpLabel;
    @SerializedName("forgot_password")
    @Expose
    private String forgotPassword;
    @SerializedName("forgot_password_label")
    @Expose
    private String forgotPasswordLabel;
    @SerializedName("getting_washed_label")
    @Expose
    private String gettingWashedLabel;
    @SerializedName("home_label")
    @Expose
    private String homeLabel;
    @SerializedName("items_label")
    @Expose
    private String itemsLabel;
    @SerializedName("labe_cancel")
    @Expose
    private String labeCancel;
    @SerializedName("label_choose_from_Library")
    @Expose
    private String labelChooseFromLibrary;
    @SerializedName("label_please_check_your_internet_connection")
    @Expose
    private String labelPleaseCheckYourInternetConnection;
    @SerializedName("label_please_enter_a_valid_email")
    @Expose
    private String labelPleaseEnterAValidEmail;
    @SerializedName("label_please_enter_your_full_name")
    @Expose
    private String labelPleaseEnterYourFullName;
    @SerializedName("label_please_fill_all_fields")
    @Expose
    private String labelPleaseFillAllFields;
    @SerializedName("label_please_wait")
    @Expose
    private String labelPleaseWait = "Please wait...";
    @SerializedName("label_select_file")
    @Expose
    private String labelSelectFile;
    @SerializedName("label_select_Image")
    @Expose
    private String labelSelectImage;
    @SerializedName("label_take_Photo")
    @Expose
    private String labelTakePhoto;
    @SerializedName("let_get_washing_label")
    @Expose
    private String letGetWashingLabel;
    @SerializedName("letusknow_what_issue_label")
    @Expose
    private String letusknowWhatIssueLabel;
    @SerializedName("location_denied_label")
    @Expose
    private String locationDeniedLabel;
    @SerializedName("location_map_lable")
    @Expose
    private String locationMapLable;
    @SerializedName("location_not_determined_label")
    @Expose
    private String locationNotDeterminedLabel;
    @SerializedName("location_restricted_label")
    @Expose
    private String locationRestrictedLabel;
    @SerializedName("location_service_label")
    @Expose
    private String locationServiceLabel;
    @SerializedName("login_label")
    @Expose
    private String loginLabel;
    @SerializedName("logout_label")
    @Expose
    private String logoutLabel;
    @SerializedName("logout_msg")
    @Expose
    private String logoutMsg;
    @SerializedName("missing_label")
    @Expose
    private String missingLabel;
    @SerializedName("my_order_label")
    @Expose
    private String myOrderLabel;
    @SerializedName("my_places_description_label")
    @Expose
    private String myPlacesDescriptionLabel;
    @SerializedName("my_places_label")
    @Expose
    private String myPlacesLabel;
    @SerializedName("name_can_not_be_empty_label")
    @Expose
    private String nameCanNotBeEmptyLabel;
    @SerializedName("name_label")
    @Expose
    private String nameLabel;
    @SerializedName("new_password_can_not_be_empty_label")
    @Expose
    private String newPasswordCanNotBeEmptyLabel;
    @SerializedName("new_password_label")
    @Expose
    private String newPasswordLabel;
    @SerializedName("new_pasword_and_confirm_password_same_label")
    @Expose
    private String newPaswordAndConfirmPasswordSameLabel;
    @SerializedName("nickname_instructions")
    @Expose
    private String nicknameInstructions;
    @SerializedName("nickname_label")
    @Expose
    private String nicknameLabel;
    @SerializedName("No_Address_Found")
    @Expose
    private String noAddressFound;
    @SerializedName("no_address_found_label")
    @Expose
    private String noAddressFoundLabel;
    @SerializedName("no_lable")
    @Expose
    private String noLable;
    @SerializedName("no_order_found")
    @Expose
    private String noOrderFound;
    @SerializedName("notes_label")
    @Expose
    private String notesLabel;
    @SerializedName("ok_label")
    @Expose
    private String okLabel;
    @SerializedName("oops_label")
    @Expose
    private String oopsLabel;
    @SerializedName("optional_deleivery_notes_label")
    @Expose
    private String optionalDeleiveryNotesLabel;
    @SerializedName("optional_delivery_notes_instructions")
    @Expose
    private String optionalDeliveryNotesInstructions;
    @SerializedName("order_number_label")
    @Expose
    private String orderNumberLabel;
    @SerializedName("order_skip_button")
    @Expose
    private String orderSkipButton;
    @SerializedName("order_status_label")
    @Expose
    private String orderStatusLabel;
    @SerializedName("ordered_successfully_label")
    @Expose
    private String orderedSuccessfullyLabel;
    @SerializedName("paidwith_label")
    @Expose
    private String paidwithLabel;
    @SerializedName("password_and_confirm_password_must_same_label")
    @Expose
    private String passwordAndConfirmPasswordMustSameLabel;
    @SerializedName("password_can_not_be_empty_label")
    @Expose
    private String passwordCanNotBeEmptyLabel;
    @SerializedName("password_instructions")
    @Expose
    private String passwordInstructions;
    @SerializedName("password_label")
    @Expose
    private String passwordLabel;
    @SerializedName("payment_method_label")
    @Expose
    private String paymentMethodLabel;
    @SerializedName("phone_can_not_be_empty_label")
    @Expose
    private String phoneCanNotBeEmptyLabel;
    @SerializedName("phone_label")
    @Expose
    private String phoneLabel;
    @SerializedName("phone_number_instructions")
    @Expose
    private String phoneNumberInstructions;
    @SerializedName("phone_number_label")
    @Expose
    private String phoneNumberLabel;
    @SerializedName("pickone_of_saved_places_label")
    @Expose
    private String pickoneOfSavedPlacesLabel;
    @SerializedName("pickup_date_label")
    @Expose
    private String pickupDateLabel;
    @SerializedName("place_new_order_label")
    @Expose
    private String placeNewOrderLabel;
    @SerializedName("place_nickname_label")
    @Expose
    private String placeNicknameLabel;
    @SerializedName("please_select_at_least_1_product_label")
    @Expose
    private String pleaseSelectAtLeast1ProductLabel;
    @SerializedName("please_set_delivery_time_label")
    @Expose
    private String pleaseSetDeliveryTimeLabel;
    @SerializedName("please_set_pickup_time_label")
    @Expose
    private String pleaseSetPickupTimeLabel;
    @SerializedName("please_wait_lable")
    @Expose
    private String pleaseWaitLable;
    @SerializedName("pos_label")
    @Expose
    private String posLabel;
    @SerializedName("premium_care_description_label")
    @Expose
    private String premiumCareDescriptionLabel;
    @SerializedName("premium_care_label")
    @Expose
    private String premiumCareLabel;
    @SerializedName("price_list_label")
    @Expose
    private String priceListLabel;
    @SerializedName("profile_label")
    @Expose
    private String profileLabel;
    @SerializedName("promo_code_field_label")
    @Expose
    private String promoCodeFieldLabel;
    @SerializedName("prompt_order_skip_button")
    @Expose
    private String promptOrderSkipButton;
    @SerializedName("quantity_label")
    @Expose
    private String quantityLabel;
    @SerializedName("read_for_dropoff_label")
    @Expose
    private String readForDropoffLabel;
    @SerializedName("ready_for_pickup_label")
    @Expose
    private String readyForPickupLabel;
    @SerializedName("resend_confirmation_label")
    @Expose
    private String resendConfirmationLabel;
    @SerializedName("restart_app_label")
    @Expose
    private String restartAppLabel;
    @SerializedName("review_and_order_label")
    @Expose
    private String reviewAndOrderLabel;
    @SerializedName("save_label")
    @Expose
    private String saveLabel;
    @SerializedName("search_items_here_label")
    @Expose
    private String searchItemsHereLabel;
    @SerializedName("select_address_label")
    @Expose
    private String selectAddressLabel;
    @SerializedName("select_an_item_first_label")
    @Expose
    private String selectAnItemFirstLabel;
    @SerializedName("select_deliver_date_label")
    @Expose
    private String selectDeliverDateLabel;
    @SerializedName("select_item_label")
    @Expose
    private String selectItemLabel;
    @SerializedName("select_pickup_date_label")
    @Expose
    private String selectPickupDateLabel;
    @SerializedName("send_label")
    @Expose
    private String sendLabel;
    @SerializedName("setting_label")
    @Expose
    private String settingLabel;
    @SerializedName("setup_profile_instructions")
    @Expose
    private String setupProfileInstructions;
    @SerializedName("signup_label")
    @Expose
    private String signupLabel;
    @SerializedName("smart_washr_label")
    @Expose
    private String smartWashrLabel;
    @SerializedName("sorting_fee_label")
    @Expose
    private String sortingFeeLabel;
    @SerializedName("success_label")
    @Expose
    private String successLabel;
    @SerializedName("tell_a_fried_label")
    @Expose
    private String tellAFriedLabel;
    @SerializedName("terms_and_conditions_label")
    @Expose
    private String termsAndConditionsLabel;
    @SerializedName("total_label")
    @Expose
    private String totalLabel;
    @SerializedName("version_label")
    @Expose
    private String versionLabel;
    @SerializedName("wash_order_description_label")
    @Expose
    private String washOrderDescriptionLabel;
    @SerializedName("wash_order_label")
    @Expose
    private String washOrderLabel;
    @SerializedName("welcome_smartwashr_label")
    @Expose
    private String welcomeSmartwashrLabel;
    @SerializedName("yes_lable")
    @Expose
    private String yesLable;
    @SerializedName("your_active_order_label")
    @Expose
    private String yourActiveOrderLabel;
    @SerializedName("add_description")
    @Expose
    private String add_description;


    public String getAboutLabel() {
        return aboutLabel;
    }

    public void setAboutLabel(String aboutLabel) {
        this.aboutLabel = aboutLabel;
    }

    public String getActiveLabel() {
        return activeLabel;
    }

    public void setActiveLabel(String activeLabel) {
        this.activeLabel = activeLabel;
    }

    public String getAddLabel() {
        return addLabel;
    }

    public void setAddLabel(String addLabel) {
        this.addLabel = addLabel;
    }

    public String getAddNewAddressLabel() {
        return addNewAddressLabel;
    }

    public void setAddNewAddressLabel(String addNewAddressLabel) {
        this.addNewAddressLabel = addNewAddressLabel;
    }

    public String getAddPromoCodeLabel() {
        return addPromoCodeLabel;
    }

    public void setAddPromoCodeLabel(String addPromoCodeLabel) {
        this.addPromoCodeLabel = addPromoCodeLabel;
    }

    public String getAllowLocationDesriptionLabel() {
        return allowLocationDesriptionLabel;
    }

    public void setAllowLocationDesriptionLabel(String allowLocationDesriptionLabel) {
        this.allowLocationDesriptionLabel = allowLocationDesriptionLabel;
    }

    public String getAndAScreenshotWantLabel() {
        return andAScreenshotWantLabel;
    }

    public void setAndAScreenshotWantLabel(String andAScreenshotWantLabel) {
        this.andAScreenshotWantLabel = andAScreenshotWantLabel;
    }

    public String getCallUsLabel() {
        return callUsLabel;
    }

    public void setCallUsLabel(String callUsLabel) {
        this.callUsLabel = callUsLabel;
    }

    public String getCancelLabel() {
        return cancelLabel;
    }

    public void setCancelLabel(String cancelLabel) {
        this.cancelLabel = cancelLabel;
    }

    public String getCancelOrderLabel() {
        return cancelOrderLabel;
    }

    public void setCancelOrderLabel(String cancelOrderLabel) {
        this.cancelOrderLabel = cancelOrderLabel;
    }

    public String getCarpetCurtainsDescriptionLabel() {
        return carpetCurtainsDescriptionLabel;
    }

    public void setCarpetCurtainsDescriptionLabel(String carpetCurtainsDescriptionLabel) {
        this.carpetCurtainsDescriptionLabel = carpetCurtainsDescriptionLabel;
    }

    public String getCarpetCurtainsLabel() {
        return carpetCurtainsLabel;
    }

    public void setCarpetCurtainsLabel(String carpetCurtainsLabel) {
        this.carpetCurtainsLabel = carpetCurtainsLabel;
    }

    public String getCashOnDeliveryLabel() {
        return cashOnDeliveryLabel;
    }

    public void setCashOnDeliveryLabel(String cashOnDeliveryLabel) {
        this.cashOnDeliveryLabel = cashOnDeliveryLabel;
    }

    public String getChangeLabel() {
        return changeLabel;
    }

    public void setChangeLabel(String changeLabel) {
        this.changeLabel = changeLabel;
    }

    public String getChangeLanguageLabel() {
        return changeLanguageLabel;
    }

    public void setChangeLanguageLabel(String changeLanguageLabel) {
        this.changeLanguageLabel = changeLanguageLabel;
    }

    public String getChangePasswordLabel() {
        return changePasswordLabel;
    }

    public void setChangePasswordLabel(String changePasswordLabel) {
        this.changePasswordLabel = changePasswordLabel;
    }

    public String getCompletedLabel() {
        return completedLabel;
    }

    public void setCompletedLabel(String completedLabel) {
        this.completedLabel = completedLabel;
    }

    public String getConfirmLabel() {
        return confirmLabel;
    }

    public void setConfirmLabel(String confirmLabel) {
        this.confirmLabel = confirmLabel;
    }

    public String getConfirmPasswordCanNotBeEmptyLabel() {
        return confirmPasswordCanNotBeEmptyLabel;
    }

    public void setConfirmPasswordCanNotBeEmptyLabel(String confirmPasswordCanNotBeEmptyLabel) {
        this.confirmPasswordCanNotBeEmptyLabel = confirmPasswordCanNotBeEmptyLabel;
    }

    public String getConfirmPasswordLabel() {
        return confirmPasswordLabel;
    }

    public void setConfirmPasswordLabel(String confirmPasswordLabel) {
        this.confirmPasswordLabel = confirmPasswordLabel;
    }

    public String getConfirmPickupLabel() {
        return confirmPickupLabel;
    }

    public void setConfirmPickupLabel(String confirmPickupLabel) {
        this.confirmPickupLabel = confirmPickupLabel;
    }

    public String getContactUsLabel() {
        return contactUsLabel;
    }

    public void setContactUsLabel(String contactUsLabel) {
        this.contactUsLabel = contactUsLabel;
    }

    public String getContinueFacebook() {
        return continueFacebook;
    }

    public void setContinueFacebook(String continueFacebook) {
        this.continueFacebook = continueFacebook;
    }

    public String getCostLabel() {
        return costLabel;
    }

    public void setCostLabel(String costLabel) {
        this.costLabel = costLabel;
    }

    public String getCurrentPasswordLabel() {
        return currentPasswordLabel;
    }

    public void setCurrentPasswordLabel(String currentPasswordLabel) {
        this.currentPasswordLabel = currentPasswordLabel;
    }

    public String getDeleteAccountLabel() {
        return deleteAccountLabel;
    }

    public void setDeleteAccountLabel(String deleteAccountLabel) {
        this.deleteAccountLabel = deleteAccountLabel;
    }

    public String getDeliveredToLabel() {
        return deliveredToLabel;
    }

    public void setDeliveredToLabel(String deliveredToLabel) {
        this.deliveredToLabel = deliveredToLabel;
    }

    public String getDeliveryChargesLabel() {
        return deliveryChargesLabel;
    }

    public void setDeliveryChargesLabel(String deliveryChargesLabel) {
        this.deliveryChargesLabel = deliveryChargesLabel;
    }

    public String getDoneLabel() {
        return doneLabel;
    }

    public void setDoneLabel(String doneLabel) {
        this.doneLabel = doneLabel;
    }

    public String getDropoffDateLabel() {
        return dropoffDateLabel;
    }

    public void setDropoffDateLabel(String dropoffDateLabel) {
        this.dropoffDateLabel = dropoffDateLabel;
    }

    public String getEmailCanNotBeEmptyLabel() {
        return emailCanNotBeEmptyLabel;
    }

    public void setEmailCanNotBeEmptyLabel(String emailCanNotBeEmptyLabel) {
        this.emailCanNotBeEmptyLabel = emailCanNotBeEmptyLabel;
    }

    public String getEmailLabel() {
        return emailLabel;
    }

    public void setEmailLabel(String emailLabel) {
        this.emailLabel = emailLabel;
    }

    public String getEnableLocationDescriptionLabel() {
        return enableLocationDescriptionLabel;
    }

    public void setEnableLocationDescriptionLabel(String enableLocationDescriptionLabel) {
        this.enableLocationDescriptionLabel = enableLocationDescriptionLabel;
    }

    public String getEnterCodeLabel() {
        return enterCodeLabel;
    }

    public void setEnterCodeLabel(String enterCodeLabel) {
        this.enterCodeLabel = enterCodeLabel;
    }

    public String getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(String errorLabel) {
        this.errorLabel = errorLabel;
    }

    public String getFaqHelpLabel() {
        return faqHelpLabel;
    }

    public void setFaqHelpLabel(String faqHelpLabel) {
        this.faqHelpLabel = faqHelpLabel;
    }

    public String getForgotPassword() {
        return forgotPassword;
    }

    public void setForgotPassword(String forgotPassword) {
        this.forgotPassword = forgotPassword;
    }

    public String getForgotPasswordLabel() {
        return forgotPasswordLabel;
    }

    public void setForgotPasswordLabel(String forgotPasswordLabel) {
        this.forgotPasswordLabel = forgotPasswordLabel;
    }

    public String getGettingWashedLabel() {
        return gettingWashedLabel;
    }

    public void setGettingWashedLabel(String gettingWashedLabel) {
        this.gettingWashedLabel = gettingWashedLabel;
    }

    public String getHomeLabel() {
        return homeLabel;
    }

    public void setHomeLabel(String homeLabel) {
        this.homeLabel = homeLabel;
    }

    public String getLetGetWashingLabel() {
        return letGetWashingLabel;
    }

    public void setLetGetWashingLabel(String letGetWashingLabel) {
        this.letGetWashingLabel = letGetWashingLabel;
    }

    public String getLetusknowWhatIssueLabel() {
        return letusknowWhatIssueLabel;
    }

    public void setLetusknowWhatIssueLabel(String letusknowWhatIssueLabel) {
        this.letusknowWhatIssueLabel = letusknowWhatIssueLabel;
    }

    public String getLocationDeniedLabel() {
        return locationDeniedLabel;
    }

    public void setLocationDeniedLabel(String locationDeniedLabel) {
        this.locationDeniedLabel = locationDeniedLabel;
    }

    public String getLocationNotDeterminedLabel() {
        return locationNotDeterminedLabel;
    }

    public void setLocationNotDeterminedLabel(String locationNotDeterminedLabel) {
        this.locationNotDeterminedLabel = locationNotDeterminedLabel;
    }

    public String getLocationRestrictedLabel() {
        return locationRestrictedLabel;
    }

    public void setLocationRestrictedLabel(String locationRestrictedLabel) {
        this.locationRestrictedLabel = locationRestrictedLabel;
    }

    public String getLocationServiceLabel() {
        return locationServiceLabel;
    }

    public void setLocationServiceLabel(String locationServiceLabel) {
        this.locationServiceLabel = locationServiceLabel;
    }

    public String getLoginLabel() {
        return loginLabel;
    }

    public void setLoginLabel(String loginLabel) {
        this.loginLabel = loginLabel;
    }

    public String getLogoutLabel() {
        return logoutLabel;
    }

    public void setLogoutLabel(String logoutLabel) {
        this.logoutLabel = logoutLabel;
    }

    public String getMissingLabel() {
        return missingLabel;
    }

    public void setMissingLabel(String missingLabel) {
        this.missingLabel = missingLabel;
    }

    public String getMyOrderLabel() {
        return myOrderLabel;
    }

    public void setMyOrderLabel(String myOrderLabel) {
        this.myOrderLabel = myOrderLabel;
    }

    public String getMyPlacesDescriptionLabel() {
        return myPlacesDescriptionLabel;
    }

    public void setMyPlacesDescriptionLabel(String myPlacesDescriptionLabel) {
        this.myPlacesDescriptionLabel = myPlacesDescriptionLabel;
    }

    public String getMyPlacesLabel() {
        return myPlacesLabel;
    }

    public void setMyPlacesLabel(String myPlacesLabel) {
        this.myPlacesLabel = myPlacesLabel;
    }

    public String getNameCanNotBeEmptyLabel() {
        return nameCanNotBeEmptyLabel;
    }

    public void setNameCanNotBeEmptyLabel(String nameCanNotBeEmptyLabel) {
        this.nameCanNotBeEmptyLabel = nameCanNotBeEmptyLabel;
    }

    public String getNameLabel() {
        return nameLabel;
    }

    public void setNameLabel(String nameLabel) {
        this.nameLabel = nameLabel;
    }

    public String getNewPasswordCanNotBeEmptyLabel() {
        return newPasswordCanNotBeEmptyLabel;
    }

    public void setNewPasswordCanNotBeEmptyLabel(String newPasswordCanNotBeEmptyLabel) {
        this.newPasswordCanNotBeEmptyLabel = newPasswordCanNotBeEmptyLabel;
    }

    public String getNewPasswordLabel() {
        return newPasswordLabel;
    }

    public void setNewPasswordLabel(String newPasswordLabel) {
        this.newPasswordLabel = newPasswordLabel;
    }

    public String getNewPaswordAndConfirmPasswordSameLabel() {
        return newPaswordAndConfirmPasswordSameLabel;
    }

    public void setNewPaswordAndConfirmPasswordSameLabel(String newPaswordAndConfirmPasswordSameLabel) {
        this.newPaswordAndConfirmPasswordSameLabel = newPaswordAndConfirmPasswordSameLabel;
    }

    public String getNicknameInstructions() {
        return nicknameInstructions;
    }

    public void setNicknameInstructions(String nicknameInstructions) {
        this.nicknameInstructions = nicknameInstructions;
    }

    public String getNicknameLabel() {
        return nicknameLabel;
    }

    public void setNicknameLabel(String nicknameLabel) {
        this.nicknameLabel = nicknameLabel;
    }

    public String getNoAddressFound() {
        return noAddressFound;
    }

    public void setNoAddressFound(String noAddressFound) {
        this.noAddressFound = noAddressFound;
    }

    public String getNoAddressFoundLabel() {
        return noAddressFoundLabel;
    }

    public void setNoAddressFoundLabel(String noAddressFoundLabel) {
        this.noAddressFoundLabel = noAddressFoundLabel;
    }

    public String getNoOrderFound() {
        return noOrderFound;
    }

    public void setNoOrderFound(String noOrderFound) {
        this.noOrderFound = noOrderFound;
    }

    public String getNotesLabel() {
        return notesLabel;
    }

    public void setNotesLabel(String notesLabel) {
        this.notesLabel = notesLabel;
    }

    public String getOkLabel() {
        return okLabel;
    }

    public void setOkLabel(String okLabel) {
        this.okLabel = okLabel;
    }

    public String getOopsLabel() {
        return oopsLabel;
    }

    public void setOopsLabel(String oopsLabel) {
        this.oopsLabel = oopsLabel;
    }

    public String getOptionalDeleiveryNotesLabel() {
        return optionalDeleiveryNotesLabel;
    }

    public void setOptionalDeleiveryNotesLabel(String optionalDeleiveryNotesLabel) {
        this.optionalDeleiveryNotesLabel = optionalDeleiveryNotesLabel;
    }

    public String getOptionalDeliveryNotesInstructions() {
        return optionalDeliveryNotesInstructions;
    }

    public void setOptionalDeliveryNotesInstructions(String optionalDeliveryNotesInstructions) {
        this.optionalDeliveryNotesInstructions = optionalDeliveryNotesInstructions;
    }

    public String getOrderNumberLabel() {
        return orderNumberLabel;
    }

    public void setOrderNumberLabel(String orderNumberLabel) {
        this.orderNumberLabel = orderNumberLabel;
    }

    public String getOrderSkipButton() {
        return orderSkipButton;
    }

    public void setOrderSkipButton(String orderSkipButton) {
        this.orderSkipButton = orderSkipButton;
    }

    public String getOrderStatusLabel() {
        return orderStatusLabel;
    }

    public void setOrderStatusLabel(String orderStatusLabel) {
        this.orderStatusLabel = orderStatusLabel;
    }

    public String getOrderedSuccessfullyLabel() {
        return orderedSuccessfullyLabel;
    }

    public void setOrderedSuccessfullyLabel(String orderedSuccessfullyLabel) {
        this.orderedSuccessfullyLabel = orderedSuccessfullyLabel;
    }

    public String getPaidwithLabel() {
        return paidwithLabel;
    }

    public void setPaidwithLabel(String paidwithLabel) {
        this.paidwithLabel = paidwithLabel;
    }

    public String getPasswordAndConfirmPasswordMustSameLabel() {
        return passwordAndConfirmPasswordMustSameLabel;
    }

    public void setPasswordAndConfirmPasswordMustSameLabel(String passwordAndConfirmPasswordMustSameLabel) {
        this.passwordAndConfirmPasswordMustSameLabel = passwordAndConfirmPasswordMustSameLabel;
    }

    public String getPasswordCanNotBeEmptyLabel() {
        return passwordCanNotBeEmptyLabel;
    }

    public void setPasswordCanNotBeEmptyLabel(String passwordCanNotBeEmptyLabel) {
        this.passwordCanNotBeEmptyLabel = passwordCanNotBeEmptyLabel;
    }

    public String getPasswordInstructions() {
        return passwordInstructions;
    }

    public void setPasswordInstructions(String passwordInstructions) {
        this.passwordInstructions = passwordInstructions;
    }

    public String getPasswordLabel() {
        return passwordLabel;
    }

    public void setPasswordLabel(String passwordLabel) {
        this.passwordLabel = passwordLabel;
    }

    public String getPaymentMethodLabel() {
        return paymentMethodLabel;
    }

    public void setPaymentMethodLabel(String paymentMethodLabel) {
        this.paymentMethodLabel = paymentMethodLabel;
    }

    public String getPhoneCanNotBeEmptyLabel() {
        return phoneCanNotBeEmptyLabel;
    }

    public void setPhoneCanNotBeEmptyLabel(String phoneCanNotBeEmptyLabel) {
        this.phoneCanNotBeEmptyLabel = phoneCanNotBeEmptyLabel;
    }

    public String getPhoneLabel() {
        return phoneLabel;
    }

    public void setPhoneLabel(String phoneLabel) {
        this.phoneLabel = phoneLabel;
    }

    public String getPhoneNumberInstructions() {
        return phoneNumberInstructions;
    }

    public void setPhoneNumberInstructions(String phoneNumberInstructions) {
        this.phoneNumberInstructions = phoneNumberInstructions;
    }

    public String getPhoneNumberLabel() {
        return phoneNumberLabel;
    }

    public void setPhoneNumberLabel(String phoneNumberLabel) {
        this.phoneNumberLabel = phoneNumberLabel;
    }

    public String getPickoneOfSavedPlacesLabel() {
        return pickoneOfSavedPlacesLabel;
    }

    public void setPickoneOfSavedPlacesLabel(String pickoneOfSavedPlacesLabel) {
        this.pickoneOfSavedPlacesLabel = pickoneOfSavedPlacesLabel;
    }

    public String getPickupDateLabel() {
        return pickupDateLabel;
    }

    public void setPickupDateLabel(String pickupDateLabel) {
        this.pickupDateLabel = pickupDateLabel;
    }

    public String getPlaceNewOrderLabel() {
        return placeNewOrderLabel;
    }

    public void setPlaceNewOrderLabel(String placeNewOrderLabel) {
        this.placeNewOrderLabel = placeNewOrderLabel;
    }

    public String getPlaceNicknameLabel() {
        return placeNicknameLabel;
    }

    public void setPlaceNicknameLabel(String placeNicknameLabel) {
        this.placeNicknameLabel = placeNicknameLabel;
    }

    public String getPleaseSelectAtLeast1ProductLabel() {
        return pleaseSelectAtLeast1ProductLabel;
    }

    public void setPleaseSelectAtLeast1ProductLabel(String pleaseSelectAtLeast1ProductLabel) {
        this.pleaseSelectAtLeast1ProductLabel = pleaseSelectAtLeast1ProductLabel;
    }

    public String getPleaseSetDeliveryTimeLabel() {
        return pleaseSetDeliveryTimeLabel;
    }

    public void setPleaseSetDeliveryTimeLabel(String pleaseSetDeliveryTimeLabel) {
        this.pleaseSetDeliveryTimeLabel = pleaseSetDeliveryTimeLabel;
    }

    public String getPleaseSetPickupTimeLabel() {
        return pleaseSetPickupTimeLabel;
    }

    public void setPleaseSetPickupTimeLabel(String pleaseSetPickupTimeLabel) {
        this.pleaseSetPickupTimeLabel = pleaseSetPickupTimeLabel;
    }

    public String getPosLabel() {
        return posLabel;
    }

    public void setPosLabel(String posLabel) {
        this.posLabel = posLabel;
    }

    public String getPremiumCareDescriptionLabel() {
        return premiumCareDescriptionLabel;
    }

    public void setPremiumCareDescriptionLabel(String premiumCareDescriptionLabel) {
        this.premiumCareDescriptionLabel = premiumCareDescriptionLabel;
    }

    public String getPremiumCareLabel() {
        return premiumCareLabel;
    }

    public void setPremiumCareLabel(String premiumCareLabel) {
        this.premiumCareLabel = premiumCareLabel;
    }

    public String getPriceListLabel() {
        return priceListLabel;
    }

    public void setPriceListLabel(String priceListLabel) {
        this.priceListLabel = priceListLabel;
    }

    public String getProfileLabel() {
        return profileLabel;
    }

    public void setProfileLabel(String profileLabel) {
        this.profileLabel = profileLabel;
    }

    public String getPromoCodeFieldLabel() {
        return promoCodeFieldLabel;
    }

    public void setPromoCodeFieldLabel(String promoCodeFieldLabel) {
        this.promoCodeFieldLabel = promoCodeFieldLabel;
    }

    public String getPromptOrderSkipButton() {
        return promptOrderSkipButton;
    }

    public void setPromptOrderSkipButton(String promptOrderSkipButton) {
        this.promptOrderSkipButton = promptOrderSkipButton;
    }

    public String getQuantityLabel() {
        return quantityLabel;
    }

    public void setQuantityLabel(String quantityLabel) {
        this.quantityLabel = quantityLabel;
    }

    public String getReadForDropoffLabel() {
        return readForDropoffLabel;
    }

    public void setReadForDropoffLabel(String readForDropoffLabel) {
        this.readForDropoffLabel = readForDropoffLabel;
    }

    public String getReadyForPickupLabel() {
        return readyForPickupLabel;
    }

    public void setReadyForPickupLabel(String readyForPickupLabel) {
        this.readyForPickupLabel = readyForPickupLabel;
    }

    public String getResendConfirmationLabel() {
        return resendConfirmationLabel;
    }

    public void setResendConfirmationLabel(String resendConfirmationLabel) {
        this.resendConfirmationLabel = resendConfirmationLabel;
    }

    public String getRestartAppLabel() {
        return restartAppLabel;
    }

    public void setRestartAppLabel(String restartAppLabel) {
        this.restartAppLabel = restartAppLabel;
    }

    public String getReviewAndOrderLabel() {
        return reviewAndOrderLabel;
    }

    public void setReviewAndOrderLabel(String reviewAndOrderLabel) {
        this.reviewAndOrderLabel = reviewAndOrderLabel;
    }

    public String getSaveLabel() {
        return saveLabel;
    }

    public void setSaveLabel(String saveLabel) {
        this.saveLabel = saveLabel;
    }

    public String getSearchItemsHereLabel() {
        return searchItemsHereLabel;
    }

    public void setSearchItemsHereLabel(String searchItemsHereLabel) {
        this.searchItemsHereLabel = searchItemsHereLabel;
    }

    public String getSelectAddressLabel() {
        return selectAddressLabel;
    }

    public void setSelectAddressLabel(String selectAddressLabel) {
        this.selectAddressLabel = selectAddressLabel;
    }

    public String getSelectAnItemFirstLabel() {
        return selectAnItemFirstLabel;
    }

    public void setSelectAnItemFirstLabel(String selectAnItemFirstLabel) {
        this.selectAnItemFirstLabel = selectAnItemFirstLabel;
    }

    public String getSelectDeliverDateLabel() {
        return selectDeliverDateLabel;
    }

    public void setSelectDeliverDateLabel(String selectDeliverDateLabel) {
        this.selectDeliverDateLabel = selectDeliverDateLabel;
    }

    public String getSelectItemLabel() {
        return selectItemLabel;
    }

    public void setSelectItemLabel(String selectItemLabel) {
        this.selectItemLabel = selectItemLabel;
    }

    public String getSelectPickupDateLabel() {
        return selectPickupDateLabel;
    }

    public void setSelectPickupDateLabel(String selectPickupDateLabel) {
        this.selectPickupDateLabel = selectPickupDateLabel;
    }

    public String getSendLabel() {
        return sendLabel;
    }

    public void setSendLabel(String sendLabel) {
        this.sendLabel = sendLabel;
    }

    public String getSettingLabel() {
        return settingLabel;
    }

    public void setSettingLabel(String settingLabel) {
        this.settingLabel = settingLabel;
    }

    public String getSetupProfileInstructions() {
        return setupProfileInstructions;
    }

    public void setSetupProfileInstructions(String setupProfileInstructions) {
        this.setupProfileInstructions = setupProfileInstructions;
    }

    public String getSignupLabel() {
        return signupLabel;
    }

    public void setSignupLabel(String signupLabel) {
        this.signupLabel = signupLabel;
    }

    public String getSmartWashrLabel() {
        return smartWashrLabel;
    }

    public void setSmartWashrLabel(String smartWashrLabel) {
        this.smartWashrLabel = smartWashrLabel;
    }

    public String getSortingFeeLabel() {
        return sortingFeeLabel;
    }

    public void setSortingFeeLabel(String sortingFeeLabel) {
        this.sortingFeeLabel = sortingFeeLabel;
    }

    public String getSuccessLabel() {
        return successLabel;
    }

    public void setSuccessLabel(String successLabel) {
        this.successLabel = successLabel;
    }

    public String getTellAFriedLabel() {
        return tellAFriedLabel;
    }

    public void setTellAFriedLabel(String tellAFriedLabel) {
        this.tellAFriedLabel = tellAFriedLabel;
    }

    public String getTermsAndConditionsLabel() {
        return termsAndConditionsLabel;
    }

    public void setTermsAndConditionsLabel(String termsAndConditionsLabel) {
        this.termsAndConditionsLabel = termsAndConditionsLabel;
    }

    public String getTotalLabel() {
        return totalLabel;
    }

    public void setTotalLabel(String totalLabel) {
        this.totalLabel = totalLabel;
    }

    public String getVersionLabel() {
        return versionLabel;
    }

    public void setVersionLabel(String versionLabel) {
        this.versionLabel = versionLabel;
    }

    public String getWashOrderDescriptionLabel() {
        return washOrderDescriptionLabel;
    }

    public void setWashOrderDescriptionLabel(String washOrderDescriptionLabel) {
        this.washOrderDescriptionLabel = washOrderDescriptionLabel;
    }

    public String getWashOrderLabel() {
        return washOrderLabel;
    }

    public void setWashOrderLabel(String washOrderLabel) {
        this.washOrderLabel = washOrderLabel;
    }

    public String getWelcomeSmartwashrLabel() {
        return welcomeSmartwashrLabel;
    }

    public void setWelcomeSmartwashrLabel(String welcomeSmartwashrLabel) {
        this.welcomeSmartwashrLabel = welcomeSmartwashrLabel;
    }

    public String getYourActiveOrderLabel() {
        return yourActiveOrderLabel;
    }

    public void setYourActiveOrderLabel(String yourActiveOrderLabel) {
        this.yourActiveOrderLabel = yourActiveOrderLabel;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAttentionLable() {
        return attentionLable;
    }

    public void setAttentionLable(String attentionLable) {
        this.attentionLable = attentionLable;
    }

    public String getContinueLable() {
        return continueLable;
    }

    public void setContinueLable(String continueLable) {
        this.continueLable = continueLable;
    }

    public String getDeactivateMsg() {
        return deactivateMsg;
    }

    public void setDeactivateMsg(String deactivateMsg) {
        this.deactivateMsg = deactivateMsg;
    }

    public String getDeleteLable() {
        return deleteLable;
    }

    public void setDeleteLable(String deleteLable) {
        this.deleteLable = deleteLable;
    }

    public String getDeleteMsg() {
        return deleteMsg;
    }

    public void setDeleteMsg(String deleteMsg) {
        this.deleteMsg = deleteMsg;
    }

    public String getItemsLabel() {
        return itemsLabel;
    }

    public void setItemsLabel(String itemsLabel) {
        this.itemsLabel = itemsLabel;
    }

    public String getLabeCancel() {
        return labeCancel;
    }

    public void setLabeCancel(String labeCancel) {
        this.labeCancel = labeCancel;
    }

    public String getLabelChooseFromLibrary() {
        return labelChooseFromLibrary;
    }

    public void setLabelChooseFromLibrary(String labelChooseFromLibrary) {
        this.labelChooseFromLibrary = labelChooseFromLibrary;
    }

    public String getLabelPleaseCheckYourInternetConnection() {
        return labelPleaseCheckYourInternetConnection;
    }

    public void setLabelPleaseCheckYourInternetConnection(String labelPleaseCheckYourInternetConnection) {
        this.labelPleaseCheckYourInternetConnection = labelPleaseCheckYourInternetConnection;
    }

    public String getLabelPleaseEnterAValidEmail() {
        return labelPleaseEnterAValidEmail;
    }

    public void setLabelPleaseEnterAValidEmail(String labelPleaseEnterAValidEmail) {
        this.labelPleaseEnterAValidEmail = labelPleaseEnterAValidEmail;
    }

    public String getLabelPleaseEnterYourFullName() {
        return labelPleaseEnterYourFullName;
    }

    public void setLabelPleaseEnterYourFullName(String labelPleaseEnterYourFullName) {
        this.labelPleaseEnterYourFullName = labelPleaseEnterYourFullName;
    }

    public String getLabelPleaseFillAllFields() {
        return labelPleaseFillAllFields;
    }

    public void setLabelPleaseFillAllFields(String labelPleaseFillAllFields) {
        this.labelPleaseFillAllFields = labelPleaseFillAllFields;
    }

    public String getLabelPleaseWait() {
        return labelPleaseWait;
    }

    public void setLabelPleaseWait(String labelPleaseWait) {
        this.labelPleaseWait = labelPleaseWait;
    }

    public String getLabelSelectFile() {
        return labelSelectFile;
    }

    public void setLabelSelectFile(String labelSelectFile) {
        this.labelSelectFile = labelSelectFile;
    }

    public String getLabelSelectImage() {
        return labelSelectImage;
    }

    public void setLabelSelectImage(String labelSelectImage) {
        this.labelSelectImage = labelSelectImage;
    }

    public String getLabelTakePhoto() {
        return labelTakePhoto;
    }

    public void setLabelTakePhoto(String labelTakePhoto) {
        this.labelTakePhoto = labelTakePhoto;
    }

    public String getLocationMapLable() {
        return locationMapLable;
    }

    public void setLocationMapLable(String locationMapLable) {
        this.locationMapLable = locationMapLable;
    }

    public String getLogoutMsg() {
        return logoutMsg;
    }

    public void setLogoutMsg(String logoutMsg) {
        this.logoutMsg = logoutMsg;
    }

    public String getNoLable() {
        return noLable;
    }

    public void setNoLable(String noLable) {
        this.noLable = noLable;
    }

    public String getPleaseWaitLable() {
        return pleaseWaitLable;
    }

    public void setPleaseWaitLable(String pleaseWaitLable) {
        this.pleaseWaitLable = pleaseWaitLable;
    }

    public String getYesLable() {
        return yesLable;
    }

    public void setYesLable(String yesLable) {
        this.yesLable = yesLable;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public String getAdd_description() {
        return add_description;
    }

    public void setAdd_description(String add_description) {
        this.add_description = add_description;
    }
}
