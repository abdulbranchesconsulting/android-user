package user.com.smartwashr.models.orderresponse;

/**
 * Created by zeeshan on 6/19/17.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_order")
    @Expose
    private List<UserOrder> userOrder = null;
    @SerializedName("active_orders")
    @Expose
    private Integer activeorders;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserOrder> getUserOrder() {
        return userOrder;
    }

    public void setUserOrder(List<UserOrder> userOrder) {
        this.userOrder = userOrder;
    }

    public Integer getActiveorders() {
        return activeorders;
    }

    public void setActiveorders(Integer activeorders) {
        this.activeorders = activeorders;
    }
}