package user.com.smartwashr.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsResponse {

    @SerializedName("is_shop_close")
    @Expose
    private String isShopClose;
    @SerializedName("delievery_margin")
    @Expose
    private Integer delieveryMargin;
    @SerializedName("close_time")
    @Expose
    private Integer closeTime;
    @SerializedName("open_time")
    @Expose
    private Integer openTime;
    @SerializedName("delivery_charges")
    @Expose
    private Integer deliveryCharges;
    @SerializedName("sorting_fee")
    @Expose
    private Integer sortingFee;
    @SerializedName("translations")
    @Expose
    private Translations translations;

    public String getIsShopClose() {
        return isShopClose;
    }

    public void setIsShopClose(String isShopClose) {
        this.isShopClose = isShopClose;
    }

    public Integer getDelieveryMargin() {
        return delieveryMargin;
    }

    public void setDelieveryMargin(Integer delieveryMargin) {
        this.delieveryMargin = delieveryMargin;
    }

    public Integer getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public Integer getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(Integer deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public Integer getSortingFee() {
        return sortingFee;
    }

    public void setSortingFee(Integer sortingFee) {
        this.sortingFee = sortingFee;
    }

    public Translations getTranslations() {
        return translations;
    }

    public void setTranslations(Translations translations) {
        this.translations = translations;
    }
}
