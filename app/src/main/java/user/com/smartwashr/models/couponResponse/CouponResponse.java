package user.com.smartwashr.models.couponResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by farazqureshi on 07/05/2018.
 */

public class CouponResponse {

    @SerializedName("coupon_id")
    @Expose
    private Integer couponId;
    @SerializedName("discounted_price")
    @Expose
    private Integer discountedPrice;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("message_title")
    @Expose
    private String messageTitle;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Integer discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }
}
