package user.com.smartwashr.models.productsresponse;

/**
 * Created by zeeshan on 6/6/17.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot {

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}