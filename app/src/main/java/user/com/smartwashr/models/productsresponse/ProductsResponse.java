package user.com.smartwashr.models.productsresponse;

/**
 * Created by zeeshan on 6/6/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import user.com.smartwashr.models.Category;

public class ProductsResponse {

    @SerializedName("Categories")
    @Expose
    private List<Category> categories = null;

    @SerializedName("delivery_charges")
    @Expose
    private String deliveryCharges;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(String deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }
}