
package user.com.smartwashr.models.orderDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Orders {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("laundry_id")
    @Expose
    private Integer laundryId;
    @SerializedName("is_read_admin")
    @Expose
    private Integer isReadAdmin;
    @SerializedName("is_read_laundry")
    @Expose
    private Integer isReadLaundry;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("address_id")
    @Expose
    private Object addressId;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("payment_method")
    @Expose
    private Object paymentMethod;
    @SerializedName("driver_id")
    @Expose
    private Integer driverId;
    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;
    @SerializedName("sw_total_price")
    @Expose
    private Integer swTotalPrice;
    @SerializedName("delivery_charges")
    @Expose
    private Integer deliveryCharges;
    @SerializedName("commission")
    @Expose
    private Integer commission;
    @SerializedName("delivery_charges_commission")
    @Expose
    private Integer deliveryChargesCommission;
    @SerializedName("coupon_code")
    @Expose
    private Object couponCode;
    @SerializedName("sw_total_orignal_price")
    @Expose
    private Integer swTotalOrignalPrice;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("post_code")
    @Expose
    private String postCode;
    @SerializedName("address_one")
    @Expose
    private String addressOne;
    @SerializedName("address_two")
    @Expose
    private Object addressTwo;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("delivery_instruction")
    @Expose
    private Object deliveryInstruction;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("collection_date_time")
    @Expose
    private String collectionDateTime;
    @SerializedName("collection_date_time_to")
    @Expose
    private String collectionDateTimeTo;
    @SerializedName("delivery_date_time")
    @Expose
    private String deliveryDateTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("order_detail")
    @Expose
    private List<OrderDetail> orderDetail = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getLaundryId() {
        return laundryId;
    }

    public void setLaundryId(Integer laundryId) {
        this.laundryId = laundryId;
    }

    public Integer getIsReadAdmin() {
        return isReadAdmin;
    }

    public void setIsReadAdmin(Integer isReadAdmin) {
        this.isReadAdmin = isReadAdmin;
    }

    public Integer getIsReadLaundry() {
        return isReadLaundry;
    }

    public void setIsReadLaundry(Integer isReadLaundry) {
        this.isReadLaundry = isReadLaundry;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getAddressId() {
        return addressId;
    }

    public void setAddressId(Object addressId) {
        this.addressId = addressId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Object getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Object paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getSwTotalPrice() {
        return swTotalPrice;
    }

    public void setSwTotalPrice(Integer swTotalPrice) {
        this.swTotalPrice = swTotalPrice;
    }

    public Integer getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(Integer deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public Integer getDeliveryChargesCommission() {
        return deliveryChargesCommission;
    }

    public void setDeliveryChargesCommission(Integer deliveryChargesCommission) {
        this.deliveryChargesCommission = deliveryChargesCommission;
    }

    public Object getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(Object couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getSwTotalOrignalPrice() {
        return swTotalOrignalPrice;
    }

    public void setSwTotalOrignalPrice(Integer swTotalOrignalPrice) {
        this.swTotalOrignalPrice = swTotalOrignalPrice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public Object getAddressTwo() {
        return addressTwo;
    }

    public void setAddressTwo(Object addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Object getDeliveryInstruction() {
        return deliveryInstruction;
    }

    public void setDeliveryInstruction(Object deliveryInstruction) {
        this.deliveryInstruction = deliveryInstruction;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCollectionDateTime() {
        return collectionDateTime;
    }

    public void setCollectionDateTime(String collectionDateTime) {
        this.collectionDateTime = collectionDateTime;
    }

    public String getCollectionDateTimeTo() {
        return collectionDateTimeTo;
    }

    public void setCollectionDateTimeTo(String collectionDateTimeTo) {
        this.collectionDateTimeTo = collectionDateTimeTo;
    }

    public String getDeliveryDateTime() {
        return deliveryDateTime;
    }

    public void setDeliveryDateTime(String deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public List<OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

}
