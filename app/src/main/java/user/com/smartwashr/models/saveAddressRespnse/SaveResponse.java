package user.com.smartwashr.models.saveAddressRespnse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by farazqureshi on 11/05/2018.
 */

public class SaveResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("address_stored")
    @Expose
    private AddressStored addressStored;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AddressStored getAddressStored() {
        return addressStored;
    }

    public void setAddressStored(AddressStored addressStored) {
        this.addressStored = addressStored;
    }

}
