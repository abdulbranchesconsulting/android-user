package user.com.smartwashr.restiapis;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;

/**
 * Created by zeeshan on 6/2/17.
 */

public interface ResponseHandler {

    void onSuccess(Call call, Response response, int reqCode);

    void onFailure(Call call, GenericResponse error, int reqCode);

    void onError(Call call, ERRORSO error, int reqCode);

    void onApiCrash(Call call, Throwable t, int reqCode);
}