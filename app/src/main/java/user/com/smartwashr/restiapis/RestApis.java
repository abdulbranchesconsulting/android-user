package user.com.smartwashr.restiapis;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import user.com.smartwashr.fragments.GResponse;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.ProductOrder;
import user.com.smartwashr.models.TimeSlotResponse;
import user.com.smartwashr.models.UpdateResponse;
import user.com.smartwashr.models.addressesResponse.AddressesResponse;
import user.com.smartwashr.models.couponResponse.CouponResponse;
import user.com.smartwashr.models.loginresponse.LoginResponse;
import user.com.smartwashr.models.orderDetailResponse.DetailModel;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.models.productsresponse.ProductsResponse;
import user.com.smartwashr.models.saveAddressRespnse.SaveResponse;
import user.com.smartwashr.models.settings.SettingsResponse;
import user.com.smartwashr.newModels.HTMLResponse;
import user.com.smartwashr.newModels.address.AddressResponse;
import user.com.smartwashr.newModels.faq_respnose.FaqResponse;
import user.com.smartwashr.newModels.login.LoginResult;
import user.com.smartwashr.newModels.loginFB.NewLoginResponse;
import user.com.smartwashr.newModels.orderPlacement.OrderPlacementResponse;
import user.com.smartwashr.newModels.pricing.PricingResponse;
import user.com.smartwashr.newModels.saveAddress.SaveAddress;

/**
 * Created by zeeshan on 6/2/17.
 */

public interface RestApis {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("email") String email, @Field("password") String password, @Field("device_token") String token);

    @FormUrlEncoded
    @POST("user-social-login")
    Call<LoginResponse> login_with_fb(@Field("email") String email, @Field("name") String name, @Field("pic") String pic, @Field("device_token") String token);


    @FormUrlEncoded
    @POST("social-login")
    Call<NewLoginResponse> newFbLogin(@Field("email") String email, @Field("fb_id") String fb_id, @Field("full_name") String name, @Field("profile_pic") String pic, @Field("device_token") String token);

    @FormUrlEncoded
    @POST("register")
    Call<NewLoginResponse> newRegister(@Field("full_name") String full_name,
                                       @Field("email") String email,
                                       @Field("phone") String phone,
                                       @Field("password") String password,
                                       @Field("password_confirmation") String password_confirmation,
                                       @Field("device_token") String token);


    @Multipart
    @POST("register")
    Call<NewLoginResponse> newRegister(@Part("full_name") String full_name,
                                       @Part("email") String email,
                                       @Part("phone") String phone,
                                       @Part("password") String password,
                                       @Part("password_confirmation") String password_confirmation,
                                       @Part("device_token") String token,
                                       @Part MultipartBody.Part dp);


    @GET("price-list")
    Call<PricingResponse> newCategories();


    @GET("category")
    Call<PricingResponse> newProducts(@Header("Device-Type") String decive);


    @FormUrlEncoded
    @POST("login")
    Call<LoginResult> newLogin(@Field("email") String email, @Field("password") String password, @Field("device_token") String token);


    @FormUrlEncoded
    @POST("get-driver")
    Call<GenericResponse> nearest_laundry(@Header("Authorization") String token, @Field("lat") String lat, @Field("lng") String lng);

    @Multipart
    @POST("register")
    Call<LoginResponse> register(@Header("Content_Type") String app, @Part("name") String name, @Part("email") String email, @Part("password") String password, @Part("phone_number") String phone_number, @Part("device_token") String token, @Part MultipartBody.Part dp);

    @Multipart
    @POST("register")
    Call<LoginResponse> register(@Header("Content_Type") String app, @Part("name") String name, @Part("email") String email, @Part("password") String password, @Part("phone_number") String phone_number, @Part("device_token") String token);


    @Multipart
    @POST("order-complain")
    Call<GenericResponse> contact_upload(@Header("Authorization") String authHeader,
                                         @Header("X-localization") String xlocalization,
                                         @Part("description") String feed_back,
                                         @Part MultipartBody.Part[] files);


    @Multipart
    @POST("order-complain")
    Call<GResponse> complaint(@Header("Authorization") String authHeader,
                              @Part("order_id") String order_id,
                              @Part("complain_desc") String complain_desc,
                              @Part MultipartBody.Part[] files);

    @Multipart
    @POST("contact-complain")
    Call<GenericResponse> contact_upload(@Header("Authorization") String authHeader,
                                         @Header("X-localization") String xlocalization,
                                         @Part("description") String feed_back);

    @FormUrlEncoded
    @POST("password/email")
    Call<GenericResponse> forgetPass(@Field("email") String email);


    @FormUrlEncoded
    @POST("password/email")
    Call<GenericResponse> resendEmail(@Field("email") String email);

    @FormUrlEncoded
    @POST("change-password")
    Call<GenericResponse> changePassword(@Header("Authorization") String token, @Field("password") String oldPass, @Field("password_confirmation") String newPassword);


    @POST("order")
    Call<OrderResponse> order(@Header("Content-Type") String content,
                              @Header("Accept") String accept,
                              @Header("Authorization") String token,
                              @Body ProductOrder productOrder);


    @POST("order")
    Call<OrderPlacementResponse> order(@Header("Authorization") String token,
                                       @Header("Content-Type") String type,
                                       @Body ProductOrder productOrder);

    @POST("order")
    Call<OrderPlacementResponse> orderReview(@Header("Authorization") String token,
                                             @Header("X-localization") String xlocalization,
                                             @Header("Content-Type") String type,
                                             @Body ProductOrder productOrder);

    @FormUrlEncoded
    @POST("skip-order")
    Call<OrderPlacementResponse> skipOrder(@Header("Authorization") String token,
                                           @Header("X-localization") String xlocalization,
                                           @Header("Accept") String accept,
                                           @Field("lat") String lat,
                                           @Field("lng") String lng,
                                           @Field("driver_id") String driver_id,
                                           @Field("coupon_id") String coupon_id,
                                           @Field("address") String address,
                                           @Field("pickup_time") String pickup_time,
                                           @Field("delivery_time") String delivery_time,
                                           @Field("payment_method") String payment_method,
                                           @Field("user_comments") String user_comments);

    @FormUrlEncoded
    @POST("skip-order")
    Call<OrderPlacementResponse> skipOrder(@Header("Authorization") String token,
                                           @Header("X-localization") String xlocalization,
                                           @Header("Accept") String accept,
                                           @Field("lat") String lat,
                                           @Field("lng") String lng,
                                           @Field("driver_id") String driver_id,
                                           @Field("address") String address,
                                           @Field("pickup_time") String pickup_time,
                                           @Field("delivery_time") String delivery_time,
                                           @Field("payment_method") String payment_method,
                                           @Field("user_comments") String user_comments);

    @Multipart
    @POST("update-me")
    Call<LoginResult> updateUser(@Header("Authorization") String token,
                                 @Header("Accept") String accept,
                                 @Part("full_name") String name, @Part("phone") String phone_num, @Part MultipartBody.Part dp);

    @FormUrlEncoded
    @POST("update-me")
    Call<LoginResult> updateUserWithoutPic(@Header("Authorization") String token,
                                           @Header("Accept") String accept,
                                           @Field("full_name") String name,
                                           @Field("phone") String phone_number);

    @GET("product")
    Call<ProductsResponse> fetchProducts();


    @GET("settings")
    Call<SettingsResponse> fetchSettings(@Header("X-localization") String localization);

    @GET("client-orders")
    Call<OrderResponse> fetchOrders(@Header("Authorization") String token);

    @GET("delivered-orders")
    Call<OrderResponse> deliveredOrders(@Header("Authorization") String token);

    @GET("order/cancel")
    Call<OrderResponse> cancel(@Header("Authorization") String token, @Query("id") String id);


    @PUT("order/{id}")
    Call<OrderPlacementResponse> cancelOrder(@Header("Authorization") String token,
                                             @Header("Accept") String accept,
                                             @Path("id") String id,
                                             @Query("orderstatus_id") String orderStatus);

    @FormUrlEncoded
    @POST("coupon/validate")
    Call<CouponResponse> applyCoupon(@Header("Authorization") String token, @Header("Accept") String type, @Field("price") String price, @Field("coupon_code") String code);

    @FormUrlEncoded
    @POST("coupon/validate")
    Call<CouponResponse> applyCoupon1(@Header("Authorization") String token, @Header("X-localization") String localization, @Field("price") String price, @Field("coupon_code") String code);


    @FormUrlEncoded
    @POST("coupon/validate")
    Call<CouponResponse> applyCoupon(@Header("Authorization") String token, @Field("price") String price, @Field("coupon_code") String code);


//    @GET("order/get-detail/")
//    Call<DetailResponse> orderDetail(@Header("Authorization") String token, @Query("id") String id);

    @GET("order/get-detail/{id}")
    Call<DetailModel> orderDetail(@Header("Authorization") String token, @Path("id") String id);

    @FormUrlEncoded
    @POST("order/reschedule")
    Call<GenericResponse> orderUpdate(@Header("Authorization") String token, @Field("order_id") String order_id, @Field("delivery_date_time") String date_time);

    @DELETE("delete-user")
    Call<GenericResponse> delete(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("update_profile")
    Call<UpdateResponse> update_deviceToken(@Header("Authorization") String token, @Field("device_token") String devive_token);

    @FormUrlEncoded
    @POST("vote-location")
    Call<GenericResponse> voteArea(@Header("Authorization") String token, @Field("lat") String s1, @Field("lng") String s2, @Field("address") String address);

    @GET("user-addresses")
    Call<AddressesResponse> getAddresses(@Header("Authorization") String token, @Header("Content-Type") String type);

//    @FormUrlEncoded
//    @POST("user-addresses")
//    Call<SaveResponse> saveAddress(@Header("Authorization") String token,
//                                   @Header("Accept") String accept,
//                                   @Field("name") String name,
//                                   @Field("building_name") String flat,
//                                   @Field("address_line_1") String address,
//                                   @Field("address_line_2") String address1,
//                                   @Field("phone") String phone,
//                                   @Field("delivery_instructions") String del_ins,
//                                   @Field("lat") String lat,
//                                   @Field("lng") String lng);


    @FormUrlEncoded
    @POST("user-addresses-manual-edit")
    Call<SaveResponse> updateAddress(@Header("Authorization") String token,
                                     @Header("Accept") String accept,
                                     @Field("address_id") String address_id,
                                     @Field("name") String name,
                                     @Field("building_name") String flat,
                                     @Field("address_line_1") String address,
                                     @Field("phone") String phone,
                                     @Field("delivery_instructions") String del_ins,
                                     @Field("lat") String lat,
                                     @Field("lng") String lng);


    @GET("pages/1")
    Call<HTMLResponse> fetchTerms(@Header("X-localization") String localization);

    @GET("user-address-delete/{id}")
    Call<AddressesResponse> deleteAddress(@Header("Authorization") String token, @Path("id") String id);

    @GET("order")
    Call<ArrayList<user.com.smartwashr.newModels.orders.OrderResponse>> getOrders(@Header("Authorization") String token,
                                                                                  @Query("status") String status);


    @GET("addresses")
    Call<AddressResponse> getAddresses(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST("addresses")
    Call<SaveAddress> saveAddress(@Header("Authorization") String token,
                                  @Field("building_name") String name,
                                  @Field("lat") String lat,
                                  @Field("lng") String lng,
                                  @Field("address") String address,
                                  @Field("address2") String address1,
                                  @Field("city") String city,
                                  @Field("postal_code") String post,
                                  @Field("phone") String phone);


    @FormUrlEncoded
    @POST("addresses/{id}")
    Call<SaveAddress> editAddress(@Header("Authorization") String token,
                                  @Path("id") String address_id,
                                  @Field("building_name") String name,
                                  @Field("lat") String lat,
                                  @Field("lng") String lng,
                                  @Field("address") String address,
                                  @Field("address2") String address1,
                                  @Field("city") String city,
                                  @Field("postal_code") String post,
                                  @Field("phone") String phone);


    @FormUrlEncoded
    @POST("get-operationals-hr")
    Call<TimeSlotResponse> getTimeSlots(@Field("current_hr") int hour);

    @GET("faqs")
    Call<FaqResponse> getFaqs(
//            @Header("Authorization") String token
    );

    @DELETE("addresses/{id}")
    Call<GenericResponse> deleteNewAddress(@Header("Authorization") String token,
                                           @Header("X-localization") String xlocalization,
                                           @Path("id") String id);

}