package user.com.smartwashr.restiapis;

import android.graphics.Bitmap;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by zeeshan on 6/2/17.
 */

public interface iImageResponseHandler {
    void onDownloadCompleted(Bitmap image);

    void onDownloadError(String error);
}
