package user.com.smartwashr.restiapis;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import user.com.smartwashr.StringConverterFactory;
import user.com.smartwashr.utils.Constants;

/**
 * Created by zeeshan on 6/2/17.
 */

public class RetroClient {
    private Retrofit retrofit = null;
    private static RetroClient object;

    private RetroClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);

        httpClient.addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY));
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_IP)
                .addConverterFactory(StringConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        service = retrofit.create(RestApis.class);
    }

    public static RetroClient getRetroClient() {
        if (object == null) {
            object = new RetroClient();
        } else if (object != null && !object.getRetrofit().baseUrl().toString().equalsIgnoreCase(Constants.SERVER_IP)) {
            object = new RetroClient();
        }
        return object;
    }

    private RestApis service;

    public RestApis getApiServices() {
        return object.service;
    }

    public Retrofit getRetrofit() {
        return object.retrofit;
    }
}

