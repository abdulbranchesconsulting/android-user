package user.com.smartwashr.restiapis;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.utils.Loading;

/**
 * Created by zeeshan on 6/2/17.
 */

public class RestCaller {


    private int REQ_CODE = 0;
    ResponseHandler handler;
    iImageResponseHandler filehandler;

    public RestCaller(ResponseHandler context, Call caller, final int REQUEST_CODE) throws NumberFormatException {
        if (REQUEST_CODE <= 0) {
            NumberFormatException ex = new NumberFormatException();
            throw ex;
        }
        REQ_CODE = REQUEST_CODE;
        handler = context;
        ENQUE(caller);
    }

    public RestCaller(iImageResponseHandler context, Call caller, final int REQUEST_CODE) throws NumberFormatException {
        if (REQUEST_CODE <= 0) {
            NumberFormatException ex = new NumberFormatException();
            throw ex;
        }
        REQ_CODE = REQUEST_CODE;
        filehandler = context;
        ENQUEFIle(caller);
    }

    private void ENQUE(Call call) {
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200 | response.code() == 201) {
                    handler.onSuccess(call, response, REQ_CODE);
                } else if (response.code() == 403) {
                    Loading.cancel();
                    Toast.makeText(SmartWashr.getAppContext(), "SERVER is BROKEN", Toast.LENGTH_SHORT).show();
//                    handler.onApiCrash(call, new Throwable(response.raw().message()), REQ_CODE);
                } else if (response.code() == 422) {
                    ERRORSO error = null;
                    Converter<ResponseBody, ERRORSO> errorConverter =
                            SmartWashr.getRetrofit().responseBodyConverter(ERRORSO.class, new Annotation[0]);
                    try {
                        error = errorConverter.convert(response.errorBody());
                    } catch (IOException e) {
                    }
                    if (error != null) {
                        handler.onError(call, error, REQ_CODE);
                    }
                } else {
                    GenericResponse error = null;
                    Converter<ResponseBody, GenericResponse> errorConverter =
                            SmartWashr.getRetrofit().responseBodyConverter(GenericResponse.class, new Annotation[0]);
                    try {
                        error = errorConverter.convert(response.errorBody());
                    } catch (IOException e) {
                    }
                    if (error != null)
                        handler.onFailure(call, error, REQ_CODE);
                    else
                        handler.onApiCrash(call, new Throwable(response.raw().message()), REQ_CODE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (t instanceof UnknownHostException)
                    handler.onApiCrash(call, new Throwable("Unable to access server. Please check your connection."), REQ_CODE);
                else
                    handler.onApiCrash(call, t, REQ_CODE);
            }
        });
    }

    private void ENQUEFIle(Call call) {
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Bitmap bm = BitmapFactory.decodeStream(response.body().byteStream());
                        filehandler.onDownloadCompleted(bm);
                    } else {
                        filehandler.onDownloadError(response.message());
                    }
                } else {
                    filehandler.onDownloadError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                filehandler.onDownloadError(t != null ? t.getLocalizedMessage() : "Something went wrong");
            }
        });
    }
}
