package user.com.smartwashr;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 12/12/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private SessionManager sessionManager;

    @Override
    public void onTokenRefresh() {
        sessionManager = new SessionManager(MyFirebaseInstanceIDService.this);
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sessionManager.put(Constants.DEVICE_TOKEN, refreshedToken);


    }


}

