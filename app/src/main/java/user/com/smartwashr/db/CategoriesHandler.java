package user.com.smartwashr.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import user.com.smartwashr.models.Category;
import user.com.smartwashr.models.User;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.utils.Constants;

/**
 * Created by zeeshan on 6/6/17.
 */

public class CategoriesHandler extends SQLiteOpenHelper {

    private SQLiteDatabase database;

    protected final String CategoryTable = "category_table";
    protected final String D_ID = "category_id";
    protected final String D_CAT_NAME = "category_name";
    protected final String D_SERVER_ID = "categoryserver_id";
    protected final String D_JSON = "category_json";
    // Labels table name
    private static final String TABLE_LABELS = "labels";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    // Order table name
    protected final String OrderTable = "ordertable";
    protected final String O_ID = "order_id";
    protected final String O_JSON = "order_json";

    public CategoriesHandler(Context context) {
        super(context, Constants.DBName, null, Constants.DBVersion);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CategoryTable + " ( " +
                D_ID + " INTEGER PRIMARY KEY, " +
                D_CAT_NAME + " TEXT, " +
                D_SERVER_ID + " INTEGER NOT NULL, " +
                D_JSON + " TEXT NOT NULL " +
                " );");
        db.execSQL("CREATE TABLE " + OrderTable + " ( " +
                O_ID + " INTEGER PRIMARY KEY, " +
                O_JSON + " TEXT NOT NULL " +
                " );");
        String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_LABELS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT)";
        db.execSQL(CREATE_CATEGORIES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + CategoryTable);
        db.execSQL("DROP TABLE IF EXIST " + OrderTable);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_LABELS);
        onCreate(db);
    }

    public boolean insertCategory(Category model) {
        ContentValues cv = new ContentValues();
        database = this.getWritableDatabase();
        cv.put(D_SERVER_ID, model.getId());
        cv.put(D_CAT_NAME, model.getName());
        cv.put(D_JSON, new Gson().toJson(model));
        int res = (int) database.insert(CategoryTable, null, cv);
        database.close();
        if (res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean insertOrder(OrderResponse model) {
        ContentValues cv = new ContentValues();
        database = this.getWritableDatabase();
        cv.put(O_JSON, new Gson().toJson(model));
        int res = (int) database.insert(OrderTable, null, cv);
        database.close();
        if (res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void insertLabel(String label) {
        database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, label);

        // Inserting Row
        database.insert(TABLE_LABELS, null, values);
        database.close(); // Closing database connection
    }

    public List<String> getAllLabels() {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LABELS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }


    public void updateJobList(ArrayList<Category> lst) {
        for (Category category : lst) {
            try {
                insertCategory(category);
            } catch (Exception e) {
            }
        }
    }

    public boolean deleteCategory(Category model) {
        database = this.getWritableDatabase();
        int res = (int) database.delete(CategoryTable, D_SERVER_ID + "='" + model.getId() + "'", null);
        if (res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void deleteAllCategories() {
        database = this.getWritableDatabase();

        database.execSQL("delete from " + CategoryTable);
        database.close();
    }

    public void deleteAllOrders() {
        database = this.getWritableDatabase();

        database.execSQL("delete from " + OrderTable);
        database.close();
    }

    public void deleteLabels() {
        database = this.getWritableDatabase();
        database.execSQL("delete from " + TABLE_LABELS);
        database.close();
    }


    public User getCategory() {
        database = this.getReadableDatabase();
        Gson gson = new Gson();
        Cursor c = database.rawQuery("SELECT * FROM " + CategoryTable, null);
        if (c != null)
            c.moveToFirst();
        User obj = gson.fromJson(c.getString(c.getColumnIndex(D_JSON)), User.class);
        return obj;
    }

    public Collection<Category> getAllCategories() {
        database = this.getReadableDatabase();
        Gson gson = new Gson();
        ArrayList<Category> lst = new ArrayList<>();
        Cursor c = database.rawQuery("SELECT * FROM " + CategoryTable, null);
        if (c.moveToFirst()) {
            do {
                Category obj = gson.fromJson(c.getString(c.getColumnIndex(D_JSON)), Category.class);
                if (obj != null)
                    lst.add(obj);
            } while (c.moveToNext());
        }
        return lst;
    }

    public Collection<OrderResponse> getAllOrders() {
        database = this.getReadableDatabase();
        Gson gson = new Gson();
        ArrayList<OrderResponse> lst = new ArrayList<>();
        Cursor c = database.rawQuery("SELECT * FROM " + OrderTable + " ORDER BY " + O_ID + " DESC", null);
        if (c.moveToFirst()) {
            do {
                OrderResponse obj = gson.fromJson(c.getString(c.getColumnIndex(O_JSON)), OrderResponse.class);
                if (obj != null)
                    lst.add(obj);
            } while (c.moveToNext());
        }
        return lst;
    }

}