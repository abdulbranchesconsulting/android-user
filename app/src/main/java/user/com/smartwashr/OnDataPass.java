package user.com.smartwashr;

import user.com.smartwashr.adapter.ProductsOrder;

/**
 * Created by zeeshan on 11/27/17.
 */

public interface OnDataPass {

    public void onDataPass(ProductsOrder productsOrders);


    public void onDataPass(int product_id);
}
