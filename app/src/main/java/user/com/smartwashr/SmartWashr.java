package user.com.smartwashr;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import user.com.smartwashr.restiapis.RestApis;
import user.com.smartwashr.restiapis.RetroClient;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;


/**
 * Created by zeeshan on 6/2/17.
 */

public class SmartWashr extends Application {
    private static SmartWashr _instance;
    SessionManager sessionManager;


    public static SmartWashr getAppContext() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!Constants.isDevelopment) {
            Fabric.with(this, new Crashlytics());
        }
        _instance = this;
        sessionManager = new SessionManager(_instance);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/sf-ui-display-semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }


    public static synchronized SmartWashr getInstance() {
        return _instance;
    }

    public static RestApis getRestClient() {
        return RetroClient.getRetroClient().getApiServices();
    }


    public static Retrofit getRetrofit() {
        return RetroClient.getRetroClient().getRetrofit();
    }

}
