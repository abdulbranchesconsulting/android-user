
package user.com.smartwashr.newModels.orderPlacement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPlacementResponse {

    @SerializedName("order")
    @Expose
    private Order order;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("message_title")
    @Expose
    private String messageTitle;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

}
