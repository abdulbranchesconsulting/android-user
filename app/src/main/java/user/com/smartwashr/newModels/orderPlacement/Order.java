
package user.com.smartwashr.newModels.orderPlacement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("lat_lng")
    @Expose
    private LatLng latLng;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("invoice_num")
    @Expose
    private String invoiceNum;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("skip_by_client")
    @Expose
    private String skipByClient;
    @SerializedName("user_comments")
    @Expose
    private Object userComments;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("subtotal")
    @Expose
    private Integer subtotal;
    @SerializedName("delivery_charges")
    @Expose
    private Integer deliveryCharges;
    @SerializedName("sorting_fee")
    @Expose
    private Integer sortingFee;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private Integer id;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSkipByClient() {
        return skipByClient;
    }

    public void setSkipByClient(String skipByClient) {
        this.skipByClient = skipByClient;
    }

    public Object getUserComments() {
        return userComments;
    }

    public void setUserComments(Object userComments) {
        this.userComments = userComments;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(Integer deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public Integer getSortingFee() {
        return sortingFee;
    }

    public void setSortingFee(Integer sortingFee) {
        this.sortingFee = sortingFee;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
