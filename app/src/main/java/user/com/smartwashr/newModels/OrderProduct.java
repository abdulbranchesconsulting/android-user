package user.com.smartwashr.newModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProduct {

    @SerializedName("id")
    @Expose
    String productId;
    @SerializedName("qty")
    @Expose
    String productQty;
    @SerializedName("service_id")
    @Expose
    String serviceId;
    @SerializedName("price")
    @Expose
    Object servicePrice;

    String productName;


    public OrderProduct(String productId, String productName, String productQty, String serviceId, Object servicePrice) {
        this.productId = productId;
        this.productName = productName;
        this.productQty = productQty;
        this.serviceId = serviceId;
        this.servicePrice = servicePrice;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQty() {
        return productQty;
    }

    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Object getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }
}
