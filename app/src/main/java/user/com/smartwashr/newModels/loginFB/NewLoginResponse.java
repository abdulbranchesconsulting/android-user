package user.com.smartwashr.newModels.loginFB;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewLoginResponse {
    @SerializedName("data")
    @Expose
    private Dara data;

    public Dara getData() {
        return data;
    }

    public void setData(Dara data) {
        this.data = data;
    }

}
