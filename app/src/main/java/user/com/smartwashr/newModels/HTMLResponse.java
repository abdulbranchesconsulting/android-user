package user.com.smartwashr.newModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HTMLResponse {

    @SerializedName("data")
    @Expose
    private Datum data;

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

}
