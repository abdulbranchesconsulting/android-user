package user.com.smartwashr.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.DetailAdapter;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.orderDetailResponse.DetailModel;
import user.com.smartwashr.models.orderDetailResponse.OrderDetail;
import user.com.smartwashr.models.orderDetailResponse.Orders;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/25/17.
 */

public class OrderDetailActivity extends AppCompatActivity implements ResponseHandler,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    private ArrayList<OrderDetail> products;
    private DetailAdapter adapter;
    private RecyclerView recyclerView;
    private TextView sub_total, total, order_num, receipt, delivered_to, delivery_txt, del_price, sub_txt, tot_txt, complaint;
    private Button cancel_order;
    private AlertDialog alertDialog;
    private LinearLayout ar_delivery, ar_total, eng_del, eng_total;
    private TextView ar_final_total_price, ar_total_txt, ar_del_price, ar_delivery_txt;
    private TextView order_title, address, status_txt, time, items_pay;
    private ImageView back, bback, status_img;
    private ArrayList<OrderDetail> list_product = new ArrayList<>();
    private String list;
    private String address_field, time_field;
    private String tot;
    private String status;
    private String order_id;
    private SessionManager sessionManager;
    private String changed_time, changed_date;
    private String[] selected_date, selected_time;
    private String[] date_time;
    private String current_date, current_month, current_year, current_hour, current_min;
    private TextView call_us;
    LinearLayout call;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail);

        sessionManager = new SessionManager(OrderDetailActivity.this);

        order_title = (TextView) findViewById(R.id.order_title);

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recylerView);
        sub_total = (TextView) findViewById(R.id.total_price);
        receipt = (TextView) findViewById(R.id.receipt);
        del_price = (TextView) findViewById(R.id.del_price);
        delivery_txt = (TextView) findViewById(R.id.delivery_txt);
        delivered_to = (TextView) findViewById(R.id.delivered_to);
        total = (TextView) findViewById(R.id.final_total_price);
        sub_txt = (TextView) findViewById(R.id.sub_txt);
        tot_txt = (TextView) findViewById(R.id.total_txt);
        order_num = (TextView) findViewById(R.id.order_num);
        complaint = findViewById(R.id.complaint);
        cancel_order = (Button) findViewById(R.id.cancel_order);

        status_img = (ImageView) findViewById(R.id.status_img);

        address = (TextView) findViewById(R.id.address);
        status_txt = (TextView) findViewById(R.id.status_to);
        time = (TextView) findViewById(R.id.time_left);

        ar_delivery = (LinearLayout) findViewById(R.id.ar_delivery_layout);
        ar_total = (LinearLayout) findViewById(R.id.ar_total_layout);
        eng_del = (LinearLayout) findViewById(R.id.eng_del_layout);
        eng_total = (LinearLayout) findViewById(R.id.eng_tot_layout);

        ar_delivery_txt = (TextView) findViewById(R.id.ar_delivery_txt);
        ar_del_price = (TextView) findViewById(R.id.ar_del_price);
        ar_final_total_price = (TextView) findViewById(R.id.ar_final_total_price);
        ar_total_txt = (TextView) findViewById(R.id.ar_total_txt);

        items_pay = (TextView) findViewById(R.id.items_pay);

        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        call = (LinearLayout) findViewById(R.id.call);

        call_us = (TextView) findViewById(R.id.call_us);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            order_id = extras.getString("order_id");
        } else {
            order_id = sessionManager.get(Constants.ORDER_ID);
        }
        makeCall(order_id);

        recyclerView.setLayoutManager(new LinearLayoutManager(OrderDetailActivity.this));

        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Internet.isAvailable(OrderDetailActivity.this)) {
                    new RestCaller(OrderDetailActivity.this, SmartWashr.getRestClient().cancel("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), order_id), 1);
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(OrderDetailActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(OrderDetailActivity.this, false, "الرجاء الانتظار");
                    }
                }
            }
        });
        products = new ArrayList<>();
        adapter = new DetailAdapter(OrderDetailActivity.this, products);
        recyclerView.setAdapter(adapter);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (time.getText().toString().equalsIgnoreCase("RESCHEDULE")) {
                    Calendar now = Calendar.getInstance();
//                    t = 1;
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            OrderDetailActivity.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DATE)
                    );
                    dpd.setMinDate(new GregorianCalendar(Integer.parseInt(current_year), Integer.parseInt(current_month) - 1, Integer.parseInt(current_date)));
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                        dpd.setTitle(getResources().getString(R.string.time_date_msg));
                    else
                        dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 1 ظهرا - 11 مساءا ]");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCallBtnClick();
            }
        });

        complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OrderDetailActivity.this, ComplaintActivity.class);
                i.putExtra("orderId", order_id);
                startActivity(i);
            }
        });

    }

    private void makeCall(String order_id) {
        if (Internet.isAvailable(OrderDetailActivity.this)) {
            new RestCaller(OrderDetailActivity.this, SmartWashr.getRestClient().orderDetail("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), order_id), 2);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(OrderDetailActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(OrderDetailActivity.this, false, "الرجاء الانتظار");
            }

        } else {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Toast.makeText(OrderDetailActivity.this, "No internet available", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(OrderDetailActivity.this, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        products.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(Call calli, Response response, int reqCode) {
        Loading.cancel();
        Gson gson = new Gson();
        String json = gson.toJson(response.body());
        if (reqCode == 1) {
            OrderResponse orderResponse = (OrderResponse) response.body();
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                alertDialog = new AlertDialog.Builder(
                        OrderDetailActivity.this).create();
                alertDialog.setTitle("Cancelled");
                alertDialog.setMessage(orderResponse.getMessage());
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        startActivity(new Intent(OrderDetailActivity.this, HomeActivity.class));
                        OrderDetailActivity.this.finish();
                    }
                });
                alertDialog.show();
            } else {
                alertDialog = new AlertDialog.Builder(
                        OrderDetailActivity.this).create();
                alertDialog.setTitle(" ملغي");
                alertDialog.setMessage(orderResponse.getMessage());
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        startActivity(new Intent(OrderDetailActivity.this, HomeActivity.class));
                        OrderDetailActivity.this.finish();
                    }
                });
                alertDialog.show();
            }

            CategoriesHandler ordersHandler = new CategoriesHandler(OrderDetailActivity.this);
            if (ordersHandler.getAllOrders().size() > 0) {
                ordersHandler.deleteAllOrders();
            }
            sessionManager.put(Constants.ACTIVE_ORDER, orderResponse.getActiveorders());
            ordersHandler.insertOrder(orderResponse);
        } else if (reqCode == 3) {
            GenericResponse genericResponse = (GenericResponse) response.body();
            if (genericResponse.getSuccess().equalsIgnoreCase("true")) {
                Toast.makeText(this, "Successfully updated", Toast.LENGTH_SHORT).show();
            }
        } else if (reqCode == 2) {

            DetailModel detailModel = (DetailModel) response.body();
            Orders orders = (Orders) detailModel.getOrders();

            for (int i = 0; i < orders.getOrderDetail().size(); i++) {
                OrderDetail orderDetail = (OrderDetail) orders.getOrderDetail().get(i);
                list_product.add(orderDetail);
            }

            list = gson.toJson(list_product);
            tot = orders.getSwTotalPrice() + "";
            status = orders.getStatus() + "";
            address_field = detailModel.getOrders().getAddressOne();


            JsonParser parser = new JsonParser();
            JsonElement mJson = parser.parse(list);
            List<OrderDetail> productList = Arrays.asList(gson.fromJson(mJson,
                    OrderDetail[].class));
            for (int i = 0; i < productList.size(); i++) {
                OrderDetail product = productList.get(i);
                products.add(product);
            }
            adapter.notifyDataSetChanged();

            time_field = detailModel.getOrders().getDeliveryDateTime();

            date_time = time_field.split("\\s+");

            selected_date = date_time[0].split("-");
            selected_time = date_time[1].split(":");
            current_year = selected_date[0];
            current_month = selected_date[1];
            current_date = selected_date[2];

            current_hour = selected_time[0];
            current_min = selected_time[1];

            Log.d("tv", current_year + " " + current_month + " " + current_date + " " + current_hour + " " + current_min);

            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {

                back.setVisibility(View.VISIBLE);
                bback.setVisibility(View.GONE);

                FontUtils.setRobotoBold(order_title);
                FontUtils.setRobotoBold(receipt);
                FontUtils.setRobotoBold(status_txt);
                FontUtils.setRobotoBold(time);
                FontUtils.setRobotoBold(delivered_to);
                FontUtils.setRobotoBold(address);
                FontUtils.setRobotoBold(order_num);
                FontUtils.setFont(items_pay);
                FontUtils.setRobotoBold(delivery_txt);
                FontUtils.setRobotoMediumFont(del_price);
                FontUtils.setRobotoBold(tot_txt);
                FontUtils.setRobotoMediumFont(total);
                FontUtils.setRobotoMediumFont(call_us);
                FontUtils.setRobotoBold(complaint);
                time.setText(formatTime(time_field));
                address.setText(address_field);
                order_num.setText("Order Number\n" + order_id);
                sub_total.setText("SAR " + (Double.parseDouble(tot) - 10));
                total.setText("SAR " + tot);
                delivered_to.setText("Address");

                ar_delivery.setVisibility(View.GONE);
                ar_total.setVisibility(View.GONE);

                eng_total.setVisibility(View.VISIBLE);
                eng_del.setVisibility(View.VISIBLE);

                if (status.equalsIgnoreCase("0")) {
                    cancel_order.setVisibility(View.VISIBLE);
                    status_txt.setText("Ready for Pick Up");
                    status_img.setImageResource(R.drawable.ic_ready4pickup);
                    time.setText(formatTime(detailModel.getOrders().getCollectionDateTimeTo()));
                } else if (status.equalsIgnoreCase("1")) {
                    status_txt.setText("Getting Picked up");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_gettingpickup);
                } else if (status.equalsIgnoreCase("2")) {
                    status_txt.setText("Getting Washed");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_gettingwashed);
                } else if (status.equalsIgnoreCase("3")) {
                    status_txt.setText("Ready For Delivery");
                    time.setText("RESCHEDULE");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_ready4delivery);
                } else if (status.equalsIgnoreCase("4")) {
                    status_txt.setText("Delivered");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_delivered_new);
                    call.setVisibility(View.GONE);
                    complaint.setVisibility(View.VISIBLE);
                } else if (status.equalsIgnoreCase("5")) {
                    status_txt.setText("Cancelled");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_cancelled);
                } else if (status.equalsIgnoreCase("6")) {
                    status_txt.setText("Getting Dropped Off");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_gettingdroped);
                }
            } else {

                if (status.equalsIgnoreCase("0")) {
                    cancel_order.setVisibility(View.VISIBLE);
                    status_txt.setText("جاهز للاستلام ");
                    status_img.setImageResource(R.drawable.ic_ready4pickup);
                    time.setText(formatTime(detailModel.getOrders().getCollectionDateTimeTo()));
                } else if (status.equalsIgnoreCase("1")) {
                    cancel_order.setVisibility(View.GONE);
                    status_txt.setText("جاري الإستلام");
                    status_img.setImageResource(R.drawable.ic_gettingpickup);
                } else if (status.equalsIgnoreCase("2")) {
                    status_txt.setText("جاري الغسيل");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_gettingwashed);
                } else if (status.equalsIgnoreCase("3")) {
                    status_txt.setText("جاهز للتسليم");
                    time.setText("أعادة جدوله");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_ready4delivery);
                } else if (status.equalsIgnoreCase("4")) {
                    status_txt.setText("تم التوصيل");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_delivered_new);
                    call.setVisibility(View.GONE);
                    complaint.setVisibility(View.VISIBLE);
                } else if (status.equalsIgnoreCase("5")) {
                    status_txt.setText("Cancelled");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_cancelled);
                } else if (status.equalsIgnoreCase("6")) {
                    status_txt.setText("جاري التوصيل");
                    cancel_order.setVisibility(View.GONE);
                    status_img.setImageResource(R.drawable.ic_gettingdroped);
                }

                back.setVisibility(View.GONE);
                bback.setVisibility(View.VISIBLE);

                ar_delivery.setVisibility(View.VISIBLE);
                ar_total.setVisibility(View.VISIBLE);

                eng_total.setVisibility(View.GONE);
                eng_del.setVisibility(View.GONE);

                order_title.setText("حالة الطلب");
                time.setText(time_field);
                address.setText(address_field);
                cancel_order.setText("الغاء الطلب");
                order_num.setText(" رقم الطلب" + " \n" + order_id);
                items_pay.setText("الأصناف و الدفع");
                ar_delivery_txt.setText("رسوم التوصيل");
                ar_del_price.setText("10" + " ريال ");
                receipt.setText("الحالة");
                ar_total_txt.setText("المجموع");

                call_us.setText("اتصل بنا");
                ar_final_total_price.setText(tot + " ريال ");

                delivered_to.setText("العنوان");
                complaint.setText("ارفع شكوى");

                FontUtils.setArabic(order_title);
                FontUtils.setArabic(status_txt);
                FontUtils.setArabic(cancel_order);
                FontUtils.setArabic(order_num);
                FontUtils.setArabic(items_pay);
                FontUtils.setArabic(ar_del_price);
                FontUtils.setArabic(ar_delivery_txt);
                FontUtils.setArabic(receipt);
                FontUtils.setArabic(ar_total_txt);
                FontUtils.setArabic(ar_total);
                FontUtils.setArabic(ar_final_total_price);
                FontUtils.setArabic(delivered_to);
                FontUtils.setArabic(call_us);
                FontUtils.setArabic(complaint);

            }
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        changed_date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                OrderDetailActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        dpd.setMinTime(
                Integer.parseInt(current_hour), Integer.parseInt(current_min), 0);
        dpd.setMaxTime(23, 0, 0);
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
            dpd.setTitle(getResources().getString(R.string.time_date_msg));
        else
            dpd.setTitle("نحن منفتحون 7 أيام في الأسبوع 1pm - 11pm");
        dpd.show(getFragmentManager(), "Datepickerdialog");

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        changed_time = hourOfDay + ":" + minute + ":" + "00";
        time.setText(changed_date + " " + changed_time);
        if (Internet.isAvailable(OrderDetailActivity.this)) {
            new RestCaller(OrderDetailActivity.this, SmartWashr.getRestClient().orderUpdate("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), order_id, time.getText().toString()), 3);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(OrderDetailActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(OrderDetailActivity.this, false, "الرجاء الانتظار");
            }
        }

    }

    private void onCallBtnClick() {
        if (Build.VERSION.SDK_INT < 23) {
            phoneCall();
        } else {

            if (ActivityCompat.checkSelfPermission(OrderDetailActivity.this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                phoneCall();
            } else {
                final String[] PERMISSIONS_STORAGE = {Manifest.permission.CALL_PHONE};
                //Asking request Permissions
                ActivityCompat.requestPermissions(OrderDetailActivity.this, PERMISSIONS_STORAGE, 9);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionGranted = false;
        switch (requestCode) {
            case 9:
                permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (permissionGranted) {
            phoneCall();
        } else {
            Toast.makeText(OrderDetailActivity.this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    private void phoneCall() {
        if (ActivityCompat.checkSelfPermission(OrderDetailActivity.this,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:+966547771807"));
            startActivity(callIntent);
        } else {
            Toast.makeText(OrderDetailActivity.this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private String formatTime(String time) {

        String start_dt = time;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = (Date) formatter.parse(start_dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat newFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm");
        String finalString = newFormat.format(date);

        return finalString;
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderDetailActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}

