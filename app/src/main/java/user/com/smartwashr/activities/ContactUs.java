package user.com.smartwashr.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.newAdapters.ImageAdapter;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.SessionManager;

public class ContactUs extends AppCompatActivity implements ResponseHandler {

    private static final int SELECT_FILE = 0;
    private static final int REQUEST_CAMERA = 1;
    private PermissionManager permissionsManager;
    ArrayList<String> files; //These are the uris for the files to be uploaded
    MediaType mediaType = MediaType.parse("");

    ArrayList<String> list = new ArrayList<>();
    private String img_path;
    private String userChoosenTask;

    private ImageAdapter adapter;

    private RelativeLayout top;
    private ImageView cancel;
    private TextView send;
    private EditText description;
    private RecyclerView imgRecycler;
    private RelativeLayout call;
    private TextView callUs, title, text;

    private void findViews() {
        top = (RelativeLayout) findViewById(R.id.top);
        cancel = (ImageView) findViewById(R.id.cancel);
        send = (TextView) findViewById(R.id.send);
        description = (EditText) findViewById(R.id.description);
        imgRecycler = (RecyclerView) findViewById(R.id.img_recycler);
        call = findViewById(R.id.call);
        callUs = (TextView) findViewById(R.id.tv_call);
        title = findViewById(R.id.title);
        text = findViewById(R.id.text);

        send.setText(Constants.TRANSLATIONS.getSendLabel());
        title.setText(Constants.TRANSLATIONS.getContactUsLabel());
        description.setHint(Constants.TRANSLATIONS.getLetusknowWhatIssueLabel());
        text.setText(Constants.TRANSLATIONS.getAndAScreenshotWantLabel());
        callUs.setText(Constants.TRANSLATIONS.getCallUsLabel());


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        permissionsManager = PermissionManager.getInstance(ContactUs.this);
        permissionsManager.getPermissionifNotAvailble(new String[]{Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.READ_EXTERNAL_STORAGE
                , Manifest.permission.CALL_PHONE}, 111);


        findViews();

        list.add("");

        adapter = new ImageAdapter(this, list);

        imgRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        imgRecycler.setAdapter(adapter);

        files = new ArrayList<>();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (description.getText().length() > 0) {
                    if (Internet.isAvailable(ContactUs.this)) {
                        if (files.size() > 0) {
                            final MultipartBody.Part[] fileParts = new MultipartBody.Part[files.size()];
                            for (int i = 0; i < files.size(); i++) {
                                File file = new File(files.get(i));
                                RequestBody fileBody = RequestBody.create(mediaType, file);
                                //Setting the file name as an empty string here causes the same issue, which is sending the request successfully without saving the files in the backend, so don't neglect the file name parameter.
                                fileParts[i] = MultipartBody.Part.createFormData(String.format(Locale.ENGLISH, "complain_images[%d]", i), file.getName(), fileBody);
                            }
                            new RestCaller(ContactUs.this, SmartWashr.getRestClient().contact_upload(SessionManager.get(Constants.ACCESS_TOKEN),SessionManager.get(Constants.LANG), description.getText().toString(), fileParts), 1);

                            Loading.show(ContactUs.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                        } else {
                            new RestCaller(ContactUs.this, SmartWashr.getRestClient().contact_upload(SessionManager.get(Constants.ACCESS_TOKEN), SessionManager.get(Constants.LANG),description.getText().toString()), 1);

                            Loading.show(ContactUs.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                        }
                    } else {
                        Toast.makeText(ContactUs.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ContactUs.this, "Please enter description", Toast.LENGTH_SHORT).show();

                }
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+966547771807"));
                if (android.support.v4.app.ActivityCompat.checkSelfPermission(ContactUs.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            }
        });


    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ContactUs.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            img_path = saveImage(bm, 100);
            Log.d("PATH", img_path);

            if (img_path != null) {
                list.add(0, img_path);
                files.add(img_path);
                adapter.notifyDataSetChanged();

                imgRecycler.smoothScrollToPosition(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            img_path = saveImage(bm, 100);
            //list.set(currentPosition, actualPath);
            Log.d("PATH", "" + img_path);

            if (img_path != null) {
                list.add(0, img_path);
                files.add(img_path);
                adapter.notifyDataSetChanged();

                imgRecycler.smoothScrollToPosition(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        GenericResponse generic = (GenericResponse) response.body();
//        if (generic.isEmail_sent()) {
//            Toast.makeText(this, "Sent", Toast.LENGTH_SHORT).show();
//            finish();
//        } else {
//            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
//        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(generic.getMessage_title());
        alertDialogBuilder.setMessage(generic.getMsg());
        alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.create();
        alertDialogBuilder.show();
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public String saveImage(Bitmap bitmap, int quality) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        String imagePath = Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name) + File.separator + System.currentTimeMillis() + "smartwashr.jpg";
        File f = new File(imagePath);
        f.createNewFile();
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagePath;
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(ContactUs.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
