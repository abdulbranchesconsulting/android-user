package user.com.smartwashr.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.newActivities.NewHomeActivity;
import user.com.smartwashr.activities.newActivities.NewLogReg;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.settings.SettingsResponse;
import user.com.smartwashr.models.settings.Translations;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 5/31/17.
 */

public class Splash extends AppCompatActivity implements ResponseHandler {

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();

    private SessionManager sessionManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String token = "";
    private boolean isCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_scrn);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.getAppInstanceId();

        token = FirebaseInstanceId.getInstance().getToken();
        Log.d("MYTAG", "This is your Firebase token : " + token);

        TextView title = findViewById(R.id.title);
//        FontUtils.setMARegular(title);

        function();

    }

    private void function() {
        isCalled = true;
        sessionManager = new SessionManager(Splash.this);
        sessionManager.put(Constants.DEVICE_TOKEN, token);

        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english") || sessionManager.get(Constants.LANG).equalsIgnoreCase("arabic")) {
            sessionManager.clearSession();
            startActivity(new Intent(Splash.this, Splash.class));
            finish();
        } else {
            if (Internet.isAvailable(this)) {
                callApi(sessionManager.get(Constants.LANG));
            } else {
                isCalled = false;
                if (Constants.TRANSLATIONS.getLabelPleaseCheckYourInternetConnection() == null) {
                    Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, Constants.TRANSLATIONS.getLabelPleaseCheckYourInternetConnection(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!isCalled) {
            function();
        }
    }

    private void callApi(String lang) {
        if (!Loading.isVisible) {
            if (sessionManager.get(Constants.LANG).length() > 0) {
                Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(this, false, "Please wait...");
            }
        }
        sessionManager.put(Constants.LANG, lang);
        new RestCaller(Splash.this, SmartWashr.getRestClient().fetchSettings(lang), 1);
    }

    private void setLocality() {

        Locale locale = null;
        if (sessionManager.get(Constants.LANG).length() > 0) {
            locale = new Locale(sessionManager.get(Constants.LANG));
        } else {
            locale = new Locale("en");
        }

        // Create a new configuration object
        Configuration config = new Configuration();
        // Set the locale of the new configuration
        config.locale = locale;
        // Update the configuration of the Accplication context
        getResources().updateConfiguration(
                config,
                getResources().getDisplayMetrics()
        );
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        isCalled = false;
        Loading.cancel();
        SettingsResponse settingsResponse = (SettingsResponse) response.body();
        Translations translations = settingsResponse.getTranslations();
        Constants.TRANSLATIONS = translations;
        Gson gson = new Gson();
        String json = gson.toJson(settingsResponse);
        sessionManager.put(Constants.TRANS, json);

        sessionManager.put(Constants.SORT_FEE, settingsResponse.getSortingFee() + "");
        sessionManager.put(Constants.DELIVERY_CHARGES, settingsResponse.getDeliveryCharges() + "");

        if (sessionManager.checkLogin() && sessionManager.get(Constants.LANG).length() > 0) {
            setLocality();
//            Runnable task = new Runnable() {
//                public void run() {
            startActivity(new Intent(Splash.this, NewHomeActivity.class));
            finish();
//                }
//            };
//            worker.schedule(task, 3000, TimeUnit.MILLISECONDS);
        } else if (sessionManager.get(Constants.LANG).length() > 0) {

            setLocality();

//            Runnable task = new Runnable() {
//                public void run() {
            startActivity(new Intent(Splash.this, NewLogReg.class));
            finish();
//                }
//            };
//            worker.schedule(task, 3000, TimeUnit.MILLISECONDS);

        } else {
//            Runnable task = new Runnable() {
//                public void run() {
            startActivity(new Intent(Splash.this, LanguageSelector.class));
            finish();
//                }
//            };
//            worker.schedule(task, 3000, TimeUnit.MILLISECONDS);
        }


    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        isCalled = false;
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        isCalled = false;
        Loading.cancel();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        isCalled = false;
        Loading.cancel();
        if (error.getError() != null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Splash.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();
        }
    }

}