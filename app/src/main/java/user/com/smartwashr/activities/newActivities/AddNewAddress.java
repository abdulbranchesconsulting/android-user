package user.com.smartwashr.activities.newActivities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.MapsActivity;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.saveAddress.SaveAddress;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class AddNewAddress extends AppCompatActivity implements ResponseHandler {

    private String TAG = "ADDRESS";
    private ImageView back;
    private TextView tvSave;
    private EditText etName;
    private TextView lable1;
    private RelativeLayout rlAddress;
    private TextView tvAddress;
    private RelativeLayout rlPhone;
    private EditText etNumber;
    private TextView lable2;
    private TextView opn;
    private RelativeLayout rlNotes;
    private EditText etNotes;
    private SessionManager sessionManager;

    private void findViews() {
        back = (ImageView) findViewById(R.id.back);
        tvSave = (TextView) findViewById(R.id.tv_save);
        etName = (EditText) findViewById(R.id.et_name);
        lable1 = (TextView) findViewById(R.id.lable1);
        rlAddress = (RelativeLayout) findViewById(R.id.rl_address);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        rlPhone = (RelativeLayout) findViewById(R.id.rl_phone);
        etNumber = (EditText) findViewById(R.id.et_number);
        lable2 = (TextView) findViewById(R.id.lable2);
        opn = (TextView) findViewById(R.id.opn);
        rlNotes = (RelativeLayout) findViewById(R.id.rl_notes);
        etNotes = (EditText) findViewById(R.id.et_notes);
        TextView title = (TextView) findViewById(R.id.title);
        TextView tv_nick = (TextView) findViewById(R.id.tv_nick);

        TextView tv_phone = (TextView) findViewById(R.id.tv_phone);

        tv_nick.setText(Constants.TRANSLATIONS.getPlaceNicknameLabel());
        lable1.setText(Constants.TRANSLATIONS.getNicknameInstructions());
        etNumber.setHint(Constants.TRANSLATIONS.getPhoneLabel());
        tv_phone.setText(Constants.TRANSLATIONS.getPhoneNumberLabel());
        etNotes.setHint(Constants.TRANSLATIONS.getOptionalDeliveryNotesInstructions());
        etName.setHint(Constants.TRANSLATIONS.getNicknameLabel());
        opn.setText(Constants.TRANSLATIONS.getOptionalDeleiveryNotesLabel());
        tvAddress.setText(Constants.TRANSLATIONS.getLocationMapLable());
        lable2.setText(Constants.TRANSLATIONS.getPhoneNumberInstructions());
        title.setText(Constants.TRANSLATIONS.getMyPlacesLabel());
        tvSave.setText(Constants.TRANSLATIONS.getSaveLabel());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);

        sessionManager = new SessionManager(this);

        findViews();

        if (getIntent().getStringExtra("edit") != null) {
            etName.setText(Constants.ADDRESS.getBuildingName());
            tvAddress.setText(Constants.ADDRESS.getAddress());
            etNumber.setText(Constants.ADDRESS.getPhone());
//            etNotes.setText(Constants.ADDRESS.get);
        }

        tvAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AddNewAddress.this, MapsActivity.class);
                i.putExtra("add_place", "add_place");
                startActivityForResult(i, 1);
            }
        });

        if (getIntent().getStringExtra("maps") != null) {
            tvAddress.setText(sessionManager.get(Constants.ADDRESS_ONE));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAddress();
            }
        });

    }


    private void saveAddress() {
        if (Internet.isAvailable(this)) {
            Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            if (getIntent().getStringExtra("edit") == null) {
                new RestCaller(AddNewAddress.this, SmartWashr.getRestClient().saveAddress(
                        sessionManager.get(Constants.ACCESS_TOKEN),
                        etName.getText().toString(),
                        sessionManager.get(Constants.LATITUDE),
                        sessionManager.get(Constants.LONGITUDE),
                        sessionManager.get(Constants.ADDRESS_ONE),
                        sessionManager.get(Constants.LATITUDE),
                        sessionManager.get(Constants.CITY),
                        "12345",
                        etNumber.getText().toString()), 1);
            } else {

                new RestCaller(AddNewAddress.this, SmartWashr.getRestClient().editAddress(
                        sessionManager.get(Constants.ACCESS_TOKEN),
                        Constants.ADDRESS.getId() + "",
                        etName.getText().toString(),
                        Constants.ADDRESS.getLocation().getCoordinates().get(1) + "",
                        Constants.ADDRESS.getLocation().getCoordinates().get(0) + "",
                        Constants.ADDRESS.getAddress(),
                        Constants.ADDRESS.getAddress(),
                        Constants.ADDRESS.getAddress(),
                        "12345",
                        etNumber.getText().toString()), 1);
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();

        SaveAddress saveAddress = (SaveAddress) response.body();
        if (getIntent().getStringExtra("maps") != null) {
            startActivity(new Intent(this, NewSelectItems.class));
            finish();
        } else {
            finish();
        }

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                tvAddress.setText(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(this, "Result cancelled", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddNewAddress.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
