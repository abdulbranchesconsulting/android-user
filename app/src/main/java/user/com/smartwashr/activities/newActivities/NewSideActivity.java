package user.com.smartwashr.activities.newActivities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.AboutActivity;
import user.com.smartwashr.activities.ChangePassword;
import user.com.smartwashr.activities.ProfileActivity;
import user.com.smartwashr.activities.Splash;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.settings.SettingsResponse;
import user.com.smartwashr.models.settings.Translations;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewSideActivity extends AppCompatActivity implements ResponseHandler {
    private RelativeLayout top;
    private ImageView ivMenu;
    private TextView tvAbout;
    private TextView tvProfile;
    private TextView tvChngePass;
    private TextView tvChngeLang;
    private TextView tvShare, title, tv_logout, tv_del;
    private RelativeLayout rlLogout;
    private RelativeLayout rlDelAccount;
    SessionManager sessionManager;
    private AlertDialog.Builder alertDialogBuilder;
    private int clicked_item;


    private void findViews() {
        top = (RelativeLayout) findViewById(R.id.top);
        ivMenu = (ImageView) findViewById(R.id.iv_menu);
        tvAbout = (TextView) findViewById(R.id.tv_about);
        tvProfile = (TextView) findViewById(R.id.tv_profile);
        tvChngePass = (TextView) findViewById(R.id.tv_chnge_pass);
        tvChngeLang = (TextView) findViewById(R.id.tv_chnge_lang);
        tvShare = (TextView) findViewById(R.id.tv_share);
        rlLogout = (RelativeLayout) findViewById(R.id.rl_logout);
        rlDelAccount = (RelativeLayout) findViewById(R.id.rl_del_account);
        title = findViewById(R.id.title);
        tv_logout = findViewById(R.id.tv_logout);
        tv_del = findViewById(R.id.tv_del);

        tvAbout.setText(Constants.TRANSLATIONS.getAboutLabel());
        tvProfile.setText(Constants.TRANSLATIONS.getProfileLabel());
        tvChngePass.setText(Constants.TRANSLATIONS.getChangePasswordLabel());
        tvChngeLang.setText(Constants.TRANSLATIONS.getChangeLanguageLabel());
        tvShare.setText(Constants.TRANSLATIONS.getTellAFriedLabel());
        tv_logout.setText(Constants.TRANSLATIONS.getLogoutLabel());
        tv_del.setText(Constants.TRANSLATIONS.getDeleteAccountLabel());
        title.setText(Constants.TRANSLATIONS.getSettingLabel());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_side);

        sessionManager = new SessionManager(this);

        alertDialogBuilder = new AlertDialog.Builder(this);
        findViews();

        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rlLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogBuilder.setTitle(Constants.TRANSLATIONS.getAttentionLable());
                alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getLogoutMsg());
                alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getYesLable(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        sessionManager.clearSession();
                        Intent intent = new Intent(NewSideActivity.this, Splash.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                alertDialogBuilder.setNegativeButton(Constants.TRANSLATIONS.getNoLable(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialogBuilder.create();
                alertDialogBuilder.show();
            }
        });

        rlDelAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogBuilder.setTitle(Constants.TRANSLATIONS.getAttentionLable());
                alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getDeactivateMsg());
                alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getYesLable(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        sessionManager.clearSession();
                        Intent intent = new Intent(NewSideActivity.this, Splash.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Toast.makeText(NewSideActivity.this, "Done", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialogBuilder.setNegativeButton(Constants.TRANSLATIONS.getNoLable(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialogBuilder.create();
                alertDialogBuilder.show();
            }
        });

        tvAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewSideActivity.this, AboutActivity.class));
            }
        });

        tvChngePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewSideActivity.this, ChangePassword.class));
            }
        });

        tvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewSideActivity.this, ProfileActivity.class));
            }
        });

        tvChngeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title, ok = "";
                clicked_item = 0;
                title = Constants.TRANSLATIONS.getChangeLanguageLabel();
                ok = Constants.TRANSLATIONS.getOkLabel();
                showDialog(NewSideActivity.this, title, new String[]{ok}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == -1) {
                            if (clicked_item == 1) {
                                callApi("en");
//                                alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getRestartAppLabel());
//                                alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getContinueLable(), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                alertDialogBuilder.setNegativeButton(Constants.TRANSLATIONS.getNoLable(), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                alertDialogBuilder.create();
//                                alertDialogBuilder.show();
                            } else if (clicked_item == 2) {
                                callApi("ar");
//
//                                alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getRestartAppLabel());
//                                alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getContinueLable(), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                alertDialogBuilder.setNegativeButton(Constants.TRANSLATIONS.getNoLable(), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                alertDialogBuilder.create();
//                                alertDialogBuilder.show();
                            }
                        }
                    }
                });
            }
        });

        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "Download Smart Washr and let us take care of your laundry! \n\nhttps://play.google.com/store/apps/details?id=user.com.smartwashr";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
    }

    private void callApi(String lang) {
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
        SessionManager.put(Constants.LANG, lang);
        new RestCaller(NewSideActivity.this, SmartWashr.getRestClient().fetchSettings(lang), 1);
    }

    public void showDialog(Context context, String title, String[] btnText,
                           DialogInterface.OnClickListener listener) {
        CharSequence[] items;
        items = new CharSequence[]{"English", "Arabic"};

        if (listener == null)
            listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface,
                                    int paramInt) {
                    paramDialogInterface.dismiss();
                }
            };

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setTitle(title);

        builder.setSingleChoiceItems(items, -1,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            clicked_item = 1;
                        } else {
                            clicked_item = 2;
                        }
                    }
                });
        builder.setPositiveButton(btnText[0], listener);
        if (btnText.length != 1) {
            builder.setNegativeButton(btnText[1], listener);
        }
        builder.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 1) {
            SettingsResponse settingsResponse = (SettingsResponse) response.body();
            Translations translations = settingsResponse.getTranslations();
            Constants.TRANSLATIONS = translations;
            Gson gson = new Gson();
            sessionManager.put(Constants.SORT_FEE, settingsResponse.getSortingFee() + "");
            sessionManager.put(Constants.DELIVERY_CHARGES, settingsResponse.getDeliveryCharges() + "");
            String json = gson.toJson(settingsResponse);
            sessionManager.put(Constants.TRANS, json);

            Intent i = new Intent(NewSideActivity.this, Splash.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);


        } else {

        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(NewSideActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
