package user.com.smartwashr.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.TimePasser;
import user.com.smartwashr.adapter.OrderAdapterNew;
import user.com.smartwashr.adapter.ProductsOrder;
import user.com.smartwashr.adapter.TimeAdapter;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.ProductOrder;
import user.com.smartwashr.models.couponResponse.CouponResponse;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 12/7/17.
 */

public class ZeplinOrderDone extends AppCompatActivity implements
        ResponseHandler,
        TimePasser,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    TextView setting_title,
            items_payment,
            del_txt,
            pickup_at,
            dropoff_at,
            total_txt,
            option_txt,
            change1,
            change2,
            txt,
            col_date_time,
            del_date_time,
            delivery_charges,
            total,
            view_title,
            done,
            submit, ar_submit;
    RecyclerView recyclerView, time_view;
    Button order_place;
    EditText instructions, coupon_code, ar_et_coupon;
    private ArrayList<ProductsOrder> list;
    private ArrayList<Integer> total_list;
    private OrderAdapterNew adapter;
    private int clicked_item;
    private Gson gson;
    private SessionManager sessionManager;
    private AlertDialog alertDialog;
    private int t;
    private int selected_date = 0;
    private String col_date, del_date;
    private int selected_time = 0;
    private String col_time, del_time;
    private ImageView bback, back;
    private ArrayList<String> time_list = new ArrayList<>();
    private TimeAdapter time_adapter;
    private ArrayList<String> full_time_list;
    private TimePasser timePasser;
    private int col_del = 0;
    private LinearLayout time_container;
    private String collection_time;
    private int hour, hour24;
    private String coll_time;
    private String current_date, current_time;
    public boolean couponApplied = false;
    private String total_price;
    String discount_price;

    RelativeLayout ar_coupon, en_coupon;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zeplin_order_parent);

        list = new ArrayList<>();
        total_list = new ArrayList<>();
        gson = new Gson();
        sessionManager = new SessionManager(ZeplinOrderDone.this);

        timePasser = (TimePasser) this;

        time_list.add("01 pm - 02 pm");
        time_list.add("02 pm - 03 pm");
        time_list.add("03 pm - 04 pm");
        time_list.add("04 pm - 05 pm");
        time_list.add("05 pm - 06 pm");
        time_list.add("06 pm - 07 pm");
        time_list.add("07 pm - 08 pm");
        time_list.add("08 pm - 09 pm");
        time_list.add("09 pm - 10 pm");
        time_list.add("10 pm - 11 pm");

        setting_title = findViewById(R.id.setting_title);
        col_date_time = findViewById(R.id.collection_date_time);
        del_date_time = findViewById(R.id.delivery_date_time);
        coupon_code = findViewById(R.id.coupon_code);
        ar_et_coupon = findViewById(R.id.arabic_coupon_code);

        total = (TextView) findViewById(R.id.total_price);
        change1 = findViewById(R.id.change1);
        change2 = findViewById(R.id.change2);
        delivery_charges = findViewById(R.id.delivery_charges);
        order_place = findViewById(R.id.order_place);
        instructions = findViewById(R.id.instructions);
        txt = findViewById(R.id.txt);
        items_payment = findViewById(R.id.items_payment);
        option_txt = findViewById(R.id.optional_txt);
        del_txt = findViewById(R.id.del_txt);
        total_txt = findViewById(R.id.total_txt);
        dropoff_at = findViewById(R.id.dropoff_at);
        pickup_at = findViewById(R.id.pick_up);
        time_container = findViewById(R.id.time_container);

        submit = findViewById(R.id.submit);
        ar_submit = findViewById(R.id.ar_submit);

        ar_coupon = findViewById(R.id.arabic_coupon);
        en_coupon = findViewById(R.id.english_coupon);

        view_title = findViewById(R.id.view_title);
        done = findViewById(R.id.done);
        time_view = findViewById(R.id.time_view);

        time_view = findViewById(R.id.time_view);
        recyclerView = findViewById(R.id.zeplin_recylerView);

        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            en_coupon.setVisibility(View.VISIBLE);
            ar_coupon.setVisibility(View.GONE);
            FontUtils.setRobotoMediumFont(setting_title);
            FontUtils.setRobotoMediumFont(col_date_time);
            FontUtils.setRobotoMediumFont(del_date_time);
            FontUtils.setRobotoMediumFont(change1);
            FontUtils.setRobotoMediumFont(change2);
            FontUtils.setRobotoMediumFont(delivery_charges);
            FontUtils.setRobotoMediumFont(order_place);
            FontUtils.setRobotoMediumFont(instructions);
            FontUtils.setRobotoMediumFont(items_payment);
            FontUtils.setRobotoMediumFont(option_txt);
            FontUtils.setRobotoMediumFont(del_txt);
            FontUtils.setRobotoMediumFont(order_place);
            FontUtils.setRobotoMediumFont(total);
            FontUtils.setRobotoMediumFont(total_txt);
            FontUtils.setRobotoMediumFont(dropoff_at);
            FontUtils.setRobotoMediumFont(pickup_at);

        } else {
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            FontUtils.setArabic(setting_title);
            en_coupon.setVisibility(View.GONE);
            ar_coupon.setVisibility(View.VISIBLE);
            FontUtils.setRobotoMediumFont(col_date_time);
            FontUtils.setRobotoMediumFont(del_date_time);
            FontUtils.setArabic(change1);
            FontUtils.setArabic(change2);
            FontUtils.setArabic(delivery_charges);
            FontUtils.setArabic(order_place);
            FontUtils.setArabic(instructions);
            FontUtils.setArabic(items_payment);
            FontUtils.setArabic(option_txt);
            FontUtils.setArabic(del_txt);
            FontUtils.setArabic(total_txt);
            FontUtils.setArabic(order_place);
            FontUtils.setArabic(pickup_at);
            FontUtils.setArabic(dropoff_at);
            FontUtils.setArabic(ar_submit);
            pickup_at.setGravity(Gravity.END);
            pickup_at.setText("وقت الاستلام");
            dropoff_at.setGravity(Gravity.END);
            dropoff_at.setText("وقت التوصيل");
            setting_title.setText("راجع و أكد الطلب");
            change1.setText("تغير");
            change2.setText("تغير");
            items_payment.setText("الأصناف والدفع");
            option_txt.setText("تعليمات الغسيل [اختياري]");
            instructions.setHint("تعليمات اخرا للغسيل");
            del_txt.setText("رسوم التوصيل");
            total_txt.setText("المجموع");
//            col_date_time.setGravity(Gravity.END);
//            del_date_time.setGravity(Gravity.END);
            del_date_time.setText("اختار التاريخ و الوقت");
            col_date_time.setText("اختار التاريخ و الوقت");
            delivery_charges.setGravity(Gravity.START);
            delivery_charges.setText(SessionManager.get(Constants.DELIVERY_CHARGES) + " ريال ");
            order_place.setText("نفذ الطلب");
            ar_et_coupon.setHint("رمز ترويجي");
            ar_submit.setText("تم");

        }

        ar_et_coupon.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            applyCoupon(ar_et_coupon.getText().toString());
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });

        coupon_code.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            applyCoupon(coupon_code.getText().toString());
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });


        current_date = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        current_time = new SimpleDateFormat("HH", Locale.getDefault()).format(new Date());

        if (current_time.equalsIgnoreCase("00")) {
            current_time = "13";
        }
        Log.d("CURRENT_DATE_TIME", current_date + " " + get12hour(current_time) + " pm" + " - " + (Integer.parseInt(get12hour(current_time)) + 1) + "pm");

        col_date = current_date;
        hour24 = Integer.parseInt(current_time);
        coll_time = (Integer.parseInt(current_time) + 1) + "";

        Calendar calendar = Calendar.getInstance();
        Calendar d_calendar = Calendar.getInstance();

        if (Integer.parseInt(current_time) >= 22) {
            d_calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        Date d_tomorrow = d_calendar.getTime();
        DateFormat date_Format = new SimpleDateFormat("yyyy/MM/dd");
        String dd_date = date_Format.format(d_tomorrow);

        if (Integer.parseInt(current_time) > 21) {
            calendar.add(Calendar.DAY_OF_YEAR, 2);
        } else {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String ddate = dateFormat.format(tomorrow);
        Log.d("tomorrow", ddate);
        del_date = ddate;
        del_time = (Integer.parseInt(coll_time) + 1) + "";
        if (Integer.parseInt(current_time) <= 21) {
            col_date_time.setText(current_date + " " + get12hour(current_time) + "pm" + " - " + (Integer.parseInt(get12hour(current_time)) + 1) + "pm");
        } else {
            col_date_time.setText(dd_date + " " + 01 + "pm" + " - " + 02 + "pm");
        }
        if (Integer.parseInt(current_time) > 21) {
            del_date_time.setText(ddate + " " + 02 + "pm" + " - " + 02 + "pm");
        } else {
            del_date_time.setText(ddate + " " + (Integer.parseInt(get12hour(current_time)) + 1) + "pm" + " - " + (Integer.parseInt(get12hour(current_time)) + 2) + "pm");
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(ZeplinOrderDone.this));
        time_view.setLayoutManager(new LinearLayoutManager(ZeplinOrderDone.this));

        list = (ArrayList<ProductsOrder>) getIntent().getSerializableExtra("products");

        adapter = new OrderAdapterNew(ZeplinOrderDone.this, list, txt, total, couponApplied);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        order_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title, ok = "";
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    title = "Select Payment Method";
                    ok = "Ok";
                } else {
                    title = "اختر طريقة الدفع";
                    ok = "تم";
                }

                if (containsDigit(col_date_time.getText().toString()) && containsDigit(del_date_time.getText().toString())) {
                    showDialog(ZeplinOrderDone.this, title, new String[]{ok},
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == -1) {
                                        if (clicked_item == 1) {
                                            if (Internet.isAvailable(ZeplinOrderDone.this)) {
                                                ProductOrder productOrder = new ProductOrder();
                                                productOrder.setPostCode("0");
                                                productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                                                productOrder.setLaundryId(SessionManager.get(Constants.LAUNDRY_ID));
                                                productOrder.setAddressOne(SessionManager.get(Constants.LANE1));
                                                productOrder.setAddressTwo("");
                                                productOrder.setDiscount("");
                                                productOrder.setDiscountType("");
                                                productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                                                productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                                                productOrder.setDeliveryInstruction(instructions.getText().toString());
                                                if (coupon_code.getText().length() > 0 && couponApplied) {
                                                    productOrder.setCoupon_code(coupon_code.getText().toString());
                                                    productOrder.setDiscounted_price(discount_price);
                                                    productOrder.setTotalPrice(total_price);

                                                } else {
                                                    productOrder.setCoupon_code("");
                                                    productOrder.setDiscounted_price("");
                                                    productOrder.setTotalPrice(total.getText().toString());

                                                }
//                                                productOrder.setCollectionDateTime(col_date_time.getText().toString());
//                                                productOrder.setDeliveryDateTime(del_date_time.getText().toString());
                                                productOrder.setCollectionDateTime(col_date + " " + coll_time + ":00");
                                                Log.d("ABC", col_date + " " + coll_time + ":00");
                                                productOrder.setDeliveryDateTime(del_date + " " + (Integer.parseInt(coll_time) + 1) + ":00");
                                                Log.d("DEF", del_date + " " + (Integer.parseInt(coll_time) + 1) + ":00");
                                                productOrder.setCollection_date_time_to(col_date + " " + coll_time + ":00");
                                                Log.d("GHI", col_date + " " + coll_time + ":00");
                                                productOrder.setProductsOrder(list);
                                                String json = gson.toJson(productOrder);
                                                new RestCaller(ZeplinOrderDone.this, SmartWashr.getRestClient().order("application/json", "applicaton/json", "Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), productOrder), 1);
                                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                    Loading.show(ZeplinOrderDone.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                                                } else {
                                                    Loading.show(ZeplinOrderDone.this, false, "الرجاء الانتظار");
                                                }
                                            } else {
                                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                    Toast.makeText(ZeplinOrderDone.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(ZeplinOrderDone.this, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                        } else if (clicked_item == 2) {
                                            Intent i = new Intent(ZeplinOrderDone.this, CreditCard.class);
                                            ProductOrder productOrder = new ProductOrder();
                                            productOrder.setPostCode("0");
                                            productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                                            productOrder.setLaundryId(SessionManager.get(Constants.LAUNDRY_ID));
                                            productOrder.setTotalPrice(total.getText().toString());
                                            productOrder.setAddressOne(SessionManager.get(Constants.LANE1));
                                            productOrder.setAddressTwo("");
                                            productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                                            productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                                            productOrder.setDiscount("");
                                            productOrder.setDiscountType("");
                                            productOrder.setDeliveryInstruction(instructions.getText().toString());
//                                            productOrder.setCollectionDateTime(col_date_time.getText().toString());
//                                            productOrder.setDeliveryDateTime(del_date_time.getText().toString());
                                            productOrder.setCollectionDateTime(col_date + " " + hour24 + ":00");
                                            productOrder.setDeliveryDateTime(del_date + " " + (Integer.parseInt(coll_time) + 1) + ":00");
                                            productOrder.setCollection_date_time_to(col_date + " " + coll_time + ":00");
                                            productOrder.setProductsOrder(list);
                                            i.putExtra("total", total.getText().toString());
                                            i.putExtra("products", productOrder);
                                            startActivity(i);

                                        }
                                    }
                                }
                            });

                } else {
                    Toast.makeText(ZeplinOrderDone.this, "Please select pickup and dropoff time", Toast.LENGTH_SHORT).show();
                }
            }
        });

        col_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                t = 1;
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ZeplinOrderDone.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DATE)
                );
                dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE)));
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                    dpd.setTitle(getResources().getString(R.string.time_date_msg));
                else
                    dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 1 ظهرا - 11 مساءا ]");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        del_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selected_date != 0) {
                    Calendar now = Calendar.getInstance();
                    t = 2;
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            ZeplinOrderDone.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            selected_date
                    );
                    dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), selected_date + 1));
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                        dpd.setTitle(getResources().getString(R.string.time_date_msg));
                    else
                        dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 1 ظهرا - 11 مساءا ]");
                    dpd.show(getFragmentManager(), "Datepickerdialog");

                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(ZeplinOrderDone.this, "Please set collection date first.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ZeplinOrderDone.this, "يرجى تعيين تاريخ المجموعة أولا", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        change1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change2.setEnabled(false);
                Calendar now = Calendar.getInstance();
                t = 1;
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ZeplinOrderDone.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DATE)
                );
                dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE)));
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                    dpd.setTitle(getResources().getString(R.string.time_date_msg));
                else
                    dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 3 ظهرا - 11 مساءا ]");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        change2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selected_date != 0) {
                    Calendar now = Calendar.getInstance();
                    t = 2;
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            ZeplinOrderDone.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            selected_date
                    );
                    dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), selected_date + 1));
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                        dpd.setTitle(getResources().getString(R.string.time_date_msg));
                    else
                        dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 3 ظهرا - 11 مساءا ]");
                    dpd.show(getFragmentManager(), "Datepickerdialog");

                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(ZeplinOrderDone.this, "Please set collection date first.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ZeplinOrderDone.this, "يرجى تعيين تاريخ المجموعة أولا", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (coupon_code.getText().toString().length() > 0) {
                    applyCoupon(coupon_code.getText().toString());
                }
            }
        });

        ar_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ar_et_coupon.getText().toString().length() > 0) {
                    applyCoupon(ar_et_coupon.getText().toString());
                }
            }
        });

    }

    private void applyCoupon(String coupon) {
        closeKeyboard();
        if (Internet.isAvailable(ZeplinOrderDone.this)) {
            new RestCaller(ZeplinOrderDone.this, SmartWashr.getRestClient()
                    .applyCoupon("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN),
                            "application/json",
                            txt.getText().toString(),
                            coupon), 5);
        } else {
            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    public final boolean containsDigit(String s) {
        boolean containsDigit = false;

        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }

        return containsDigit;
    }

    public void showDialog(Context context, String title, String[] btnText,
                           DialogInterface.OnClickListener listener) {
        CharSequence[] items;
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            items = new CharSequence[]{"Cash on delivery"};
        } else {
            items = new CharSequence[]{" نقدا عند التوصيل"};
        }

        if (listener == null)
            listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface,
                                    int paramInt) {
                    paramDialogInterface.dismiss();
                }
            };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        builder.setSingleChoiceItems(items, -1,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            clicked_item = 1;
                        } else {
                            clicked_item = 2;
                        }
                    }
                });
        builder.setPositiveButton(btnText[0], listener);
        if (btnText.length != 1) {
            builder.setNegativeButton(btnText[1], listener);
        }
        builder.show();
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (t == 1) {
            col_date = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
            selected_date = dayOfMonth;
            col_date_time.setText(col_date);
            Calendar now = Calendar.getInstance();
            t = 1;

            hour = now.get(Calendar.HOUR);
            hour24 = now.get(Calendar.HOUR_OF_DAY);
            Log.d("hour12", hour + "");
            Log.d("hour24", hour24 + "");
            col_del = 0;

            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                view_title.setText("Select Collection time");
                FontUtils.setRobotoMediumFont(view_title);
                FontUtils.setRobotoBold(done);
            } else {
                view_title.setText("اختار وقت الاستلام");
                done.setText("تم");
                FontUtils.setArabic(view_title);
                FontUtils.setArabic(done);
            }

            if (hour24 > 22 || hour24 < 13) {
                if (hour24 > 22) {
                    selected_date = dayOfMonth;
                    selected_date = dayOfMonth + 1;
                    col_date = year + "/" + (monthOfYear + 1) + "/" + selected_date;

                    col_date_time.setText(col_date);
                    full_time_list = time_list;
                    time_adapter = new TimeAdapter(full_time_list, ZeplinOrderDone.this, done, timePasser, col_del, view_title);
                    time_view.setAdapter(time_adapter);
                    time_container.setVisibility(View.VISIBLE);
                    time_adapter.notifyDataSetChanged();
                }
                if (hour24 > 0 || hour24 < 13) {
                    full_time_list = time_list;
                    time_adapter = new TimeAdapter(full_time_list, ZeplinOrderDone.this, done, timePasser, col_del, view_title);
                    time_view.setAdapter(time_adapter);
                    time_container.setVisibility(View.VISIBLE);
                    time_adapter.notifyDataSetChanged();
                }
            } else {
                full_time_list = new ArrayList<String>(time_list.subList(hour, time_list.size()));
                time_adapter = new TimeAdapter(full_time_list, ZeplinOrderDone.this, done, timePasser, col_del, view_title);
                time_view.setAdapter(time_adapter);
                time_container.setVisibility(View.VISIBLE);
                time_adapter.notifyDataSetChanged();
            }


        } else if (t == 2) {
            del_date = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;

            col_del = 1;

            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                view_title.setText("Select Delivery Time");
                FontUtils.setRobotoMediumFont(view_title);
                FontUtils.setRobotoBold(done);
            } else {
                view_title.setText("اختار وقت التوصيل");
                done.setText("تم");
                FontUtils.setArabic(view_title);
                FontUtils.setArabic(done);
            }


            int position = time_list.indexOf(collection_time);
            coll_time = time_list.get(position).substring(0, 2);

            Log.d("coll_time", coll_time);


            if (dayOfMonth == selected_date + 1) {
                full_time_list = new ArrayList<String>(time_list.subList(position + 1, time_list.size()));
                time_adapter = new TimeAdapter(full_time_list, ZeplinOrderDone.this, done, timePasser, col_del, view_title);
                time_view.setAdapter(time_adapter);
                time_container.setVisibility(View.VISIBLE);
                time_adapter.notifyDataSetChanged();
            } else {
                full_time_list = time_list;
                time_adapter = new TimeAdapter(full_time_list, ZeplinOrderDone.this, done, timePasser, col_del, view_title);
                time_view.setAdapter(time_adapter);
                time_container.setVisibility(View.VISIBLE);
                time_adapter.notifyDataSetChanged();

            }

        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String time = "You picked the following time: " + hourOfDay + "h" + minute + "m" + second;
        Log.d("time", time);
        Calendar c = Calendar.getInstance();
        int today = c.get(Calendar.DATE);
        if (t == 1) {
            if (col_date_time.getText().length() != 0) {
                if ((hourOfDay >= 13 && (hourOfDay <= 22 && minute <= 59)) && selected_date == today) {
                    col_time = hourOfDay + ":" + minute;
                    selected_time = hourOfDay;
                    col_date_time.setText(col_date + " " + col_time);
//                    col_date_time.setGravity(Gravity.START);
//                    selected_date = today;
//                    col_time = "13:30";
//                    col_date = c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + selected_date;
//                    col_date_time.setText(col_date + " " + col_time);
//                    col_date_time.setGravity(Gravity.START);
                }
//                else if () {
//                    col_time = hourOfDay + ":" + minute;
//                    selected_time = hourOfDay;
//                    col_date_time.setText(col_date + " " + col_time);
//                    col_date_time.setGravity(Gravity.START);
//                }
                else {
//                    if (today == selected_date) {
//                    selected_date = selected_date + 1;
                    col_time = "01:30";
                    selected_time = 01;
                    col_date = c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + selected_date;
                    col_date_time.setText(col_date + " " + col_time);
//                    col_date_time.setGravity(Gravity.START);
//                    } else {
//                        selected_date = selected_date + 1;
//                        col_time = "13:30";
//                        selected_time = 13;
//                        col_date = c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + selected_date;
//                        col_date_time.setText(col_date + " " + col_time);
//                        col_date_time.setGravity(Gravity.START);
//                    }
                }
            } else {
                alertDialog = new AlertDialog.Builder(
                        ZeplinOrderDone.this).create();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please enter collection date first.");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                } else {
                    alertDialog.setTitle("خطأ");
                    alertDialog.setMessage("الرجاء إدخال تاريخ المجموعة أولا");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "أوكي", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                }
                alertDialog.show();
            }

        } else if (t == 2) {
            del_time = hourOfDay + ":" + minute;
            del_date_time.setText(del_date + " " + del_time);
//            del_date_time.setGravity(Gravity.START);
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        String json = gson.toJson(response.body());
        Log.d("Response", response.body().toString());

        if (reqCode == 5) {
            CouponResponse couponResponse = (CouponResponse) response.body();
//            if (couponResponse.getSuccess()) {
//                couponApplied = true;
//                findViewById(R.id.coupon).setEnabled(false);
//                total_price = total.getText().toString();
//                discount_price = couponResponse.getNewPrice();
//                total.setText("SAR " + (Double.parseDouble(couponResponse.getNewPrice()) + Double.parseDouble(SessionManager.get(Constants.DELIVERY_CHARGES))));
//                Toast.makeText(this, couponResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                adapter.notifyDataSetChanged();
//            } else {
////                Toast.makeText(this, couponResponse.getMessage(), Toast.LENGTH_SHORT).show();
//            }

        } else {
            alertDialog = new AlertDialog.Builder(
                    ZeplinOrderDone.this).create();
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                alertDialog.setTitle("Success");
                alertDialog.setMessage("Thank you for placing order with Smart Washr");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        startActivity(new Intent(ZeplinOrderDone.this, HomeActivity.class));
                        ZeplinOrderDone.this.finish();
                    }
                });
            } else {
                alertDialog.setTitle("تم بنجاح");
                alertDialog.setMessage("شكرا لكم على طلبكم من سمارت ووشر");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "تم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        startActivity(new Intent(ZeplinOrderDone.this, HomeActivity.class));
                        ZeplinOrderDone.this.finish();
                    }
                });
            }
            alertDialog.show();
            OrderResponse orderResponse = (OrderResponse) response.body();
            CategoriesHandler ordersHandler = new CategoriesHandler(ZeplinOrderDone.this);
            if (ordersHandler.getAllOrders().size() > 0) {
                ordersHandler.deleteAllOrders();
                ordersHandler.close();
            }
            sessionManager.put(Constants.ACTIVE_ORDER, orderResponse.getActiveorders());
            ordersHandler.insertOrder(orderResponse);
            ordersHandler.close();
            sessionManager.remove(Constants.PRODUCTS_ORDER);
            if (sessionManager.get(Constants.LANE1) != null) {
                sessionManager.remove(Constants.LANE1);
            }
            if (sessionManager.get(Constants.LANE2) != null) {
                sessionManager.remove(Constants.LANE2);
            }
            if (sessionManager.get(Constants.ZIP) != null) {
                sessionManager.remove(Constants.ZIP);
            }
            if (sessionManager.get(Constants.COL_DATE) != null) {
                sessionManager.remove(Constants.COL_DATE);
            }
            if (sessionManager.get(Constants.COL_TIME) != null) {
                sessionManager.remove(Constants.COL_TIME);
            }
            if (sessionManager.get(Constants.DEL_DATE) != null) {
                sessionManager.remove(Constants.DEL_DATE);
            }
            if (sessionManager.get(Constants.DEL_TIME) != null) {
                sessionManager.remove(Constants.DEL_TIME);
            }
            if (sessionManager.getInt(Constants.SELECTED_TIME) != 0) {
                sessionManager.remove(Constants.SELECTED_TIME);
            }
            if (sessionManager.getInt(Constants.SELECTED_DATE) != 0) {
                sessionManager.remove(Constants.SELECTED_DATE);
            }
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        if (reqCode != 5) {
            Toast.makeText(this, "Order not placed, something went wrong", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onTimePass(String value, int i) {
        if (i == 1) {
            col_date_time.setText(col_date + " " + value);
            time_container.setVisibility(View.GONE);
            collection_time = value;
            change2.setEnabled(true);
        } else {
            del_time = value.substring(0, 2);
            del_date_time.setText(del_date + " " + value);
            time_container.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public static String get24FormattedTime(String time) {
        String displayValue = time;
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm a");
            Date date = dateFormatter.parse(time);

// Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
            displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayValue;
    }

    public static String get12hour(String time) {
        String displayValue = time;
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("HH");
            Date date = dateFormatter.parse(time);

// Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("hh");
            displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayValue;
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ZeplinOrderDone.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

}
