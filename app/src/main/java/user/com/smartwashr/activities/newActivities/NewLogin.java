package user.com.smartwashr.activities.newActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.StringConverterFactory;
import user.com.smartwashr.activities.ForgotPassword;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.login.LoginResult;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestApis;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewLogin extends AppCompatActivity implements ResponseHandler {

    SessionManager sessionManager;
    private EditText etPass;
    private EditText etEmail;
    private Button btnLogin;
    private TextView tv_forgot, tvprofile3, terms3;
    Context context;

    private void findViews() {
        etPass = (EditText) findViewById(R.id.epass6);
        etEmail = (EditText) findViewById(R.id.epass4);
        btnLogin = (Button) findViewById(R.id.save3);
        tv_forgot = findViewById(R.id.terms2);
        tvprofile3 = findViewById(R.id.tvprofile3);
        terms3 = findViewById(R.id.terms3);
//        etEmail.setText("danish@h.com");
//        etPass.setText("123455");

        etPass.setHint(Constants.TRANSLATIONS.getPasswordLabel());
        etEmail.setHint(Constants.TRANSLATIONS.getEmailLabel());
        btnLogin.setText(Constants.TRANSLATIONS.getLetGetWashingLabel());
        tv_forgot.setText(Constants.TRANSLATIONS.getForgotPasswordLabel());
        tvprofile3.setText(Constants.TRANSLATIONS.getLoginLabel());
        terms3.setText(Constants.TRANSLATIONS.getResendConfirmationLabel());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_login);

        context = this;

        sessionManager = new SessionManager(this);

        findViews();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etEmail.getText().toString().length() > 0 && etPass.getText().toString().length() > 0) {
                    if (etEmail.getText().toString().contains("@") && etEmail.getText().toString().contains(".")) {
                        login();
                    } else {
                        Toast.makeText(context, "Please enter a valid email", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Please fill both fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewLogin.this, ForgotPassword.class));
            }
        });

        findViewById(R.id.backtohome3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void login() {

        if (Internet.isAvailable(context)) {
            Loading.show(context, false, Constants.TRANSLATIONS.getPleaseWaitLable());
//            new RestCaller(NewLogin.this, SmartWashr.getRestClient().newLogin(etEmail.getText().toString(), etPass.getText().toString(), sessionManager.get(Constants.DEVICE_TOKEN)), 1);

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(60, TimeUnit.SECONDS);
            httpClient.connectTimeout(60, TimeUnit.SECONDS);

            httpClient.addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY));

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_IP)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(StringConverterFactory.create())
                    .client(httpClient.build())
                    .build();


            RestApis retrofitInterface = retrofit.create(RestApis.class);


            Call<LoginResult> call = retrofitInterface.newLogin(etEmail.getText().toString(), etPass.getText().toString(), sessionManager.get(Constants.DEVICE_TOKEN));
            call.enqueue(new Callback<LoginResult>() {
                @Override
                public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {
                        LoginResult loginResponse = (LoginResult) response.body();
                        Log.d("response", new Gson().toJson(loginResponse));
                        sessionManager.createLoginSession();
                        SessionManager.put(Constants.ACCESS_TOKEN, "Bearer " + loginResponse.getToken());
//        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getData().getUser().);
                        SessionManager.put(Constants.USER_ID, loginResponse.getUser().getId().toString());
                        SessionManager.put(Constants.USER_NAME, loginResponse.getUser().getFirstName() + " " + loginResponse.getUser().getLastName());
                        SessionManager.put(Constants.EMAIL, loginResponse.getUser().getEmail());
                        SessionManager.put(Constants.USER_IMG, loginResponse.getUser().getProfilePic());
                        SessionManager.put(Constants.USER_PHONE, loginResponse.getUser().getPhone());
                        Loading.cancel();
                        Intent i = new Intent(NewLogin.this, NewHomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    } else {
                        Loading.cancel();
                        ERRORSO error = null;
                        Converter<ResponseBody, ERRORSO> errorConverter =
                                SmartWashr.getRetrofit().responseBodyConverter(ERRORSO.class, new Annotation[0]);
                        try {
                            error = errorConverter.convert(response.errorBody());
                        } catch (IOException e) {
                        }
                        Toast.makeText(SmartWashr.getAppContext(), error.getError().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<LoginResult> call, Throwable throwable) {
                    Loading.cancel();
                }
            });

        } else {
            Toast.makeText(context, "No internet Connection available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();

        LoginResult loginResponse = (LoginResult) response.body();
        Log.d("response", new Gson().toJson(loginResponse));
        sessionManager.createLoginSession();
        SessionManager.put(Constants.ACCESS_TOKEN, "Bearer " + loginResponse.getToken());
//        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getData().getUser().);
        SessionManager.put(Constants.USER_ID, loginResponse.getUser().getId().toString());
        SessionManager.put(Constants.USER_NAME, loginResponse.getUser().getFirstName() + " " + loginResponse.getUser().getLastName());
        SessionManager.put(Constants.EMAIL, loginResponse.getUser().getEmail());
        SessionManager.put(Constants.USER_IMG, loginResponse.getUser().getProfilePic());
        SessionManager.put(Constants.USER_PHONE, loginResponse.getUser().getPhone());
        Loading.cancel();
        Intent i = new Intent(NewLogin.this, NewHomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewLogin.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
