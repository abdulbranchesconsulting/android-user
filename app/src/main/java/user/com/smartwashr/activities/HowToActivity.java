package user.com.smartwashr.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.paolorotolo.appintro.AppIntro;

import user.com.smartwashr.R;
import user.com.smartwashr.fragments.HowToFragment;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/1/17.
 */

public class HowToActivity extends AppIntro implements HowToFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            addSlide(HowToFragment.newInstance("", "", "", R.drawable.how1_eng));
            addSlide(HowToFragment.newInstance("", "", "", R.drawable.how2_eng));
            addSlide(HowToFragment.newInstance("", "", "", R.drawable.how3_eng));
            setDoneText("Done");
        } else {
            addSlide(HowToFragment.newInstance("", "", "", R.drawable.how1_ar));
            addSlide(HowToFragment.newInstance("", "", "", R.drawable.how2_ar));
            addSlide(HowToFragment.newInstance("", "", "", R.drawable.how3_ar));
            setDoneText("تم");
        }
        setBarColor(0);
        setSeparatorColor(0);
        nextButton.setEnabled(true);
        skipButton.setEnabled(true);
        setCustomIndicator(mController);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(HowToActivity.this, HomeActivity.class);
                startActivity(in);
                finish();
            }
        });
        setIndicatorColor(getResources().getColor(R.color.white), getResources().getColor(R.color.black));
        setFlowAnimation();
        showSkipButton(false);

//        Drawable drawable = new BitmapDrawable(getResources(), getMarkerBitmapFromView());
//        setImageNextButton(drawable);
        showDoneButton(true);
//        }

    }

    private Bitmap getMarkerBitmapFromView() {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.skip_btn, null);
        TextView skip = (TextView) customMarkerView.findViewById(R.id.skip);
        if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            skip.setText("تخطي");
        }
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        FontUtils.setFont(customMarkerView);
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }


    @Override
    public void onSkipPressed(Fragment currentFragment) {
        Intent in = new Intent(this, HomeActivity.class);
        startActivity(in);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        Intent in = new Intent(this, HomeActivity.class);
        startActivity(in);
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

