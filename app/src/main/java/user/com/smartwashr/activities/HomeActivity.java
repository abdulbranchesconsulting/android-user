package user.com.smartwashr.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.HttpDataHandler;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.fragments.ActiveOrderFrag;
import user.com.smartwashr.fragments.HomeFragment;
import user.com.smartwashr.fragments.MyPlaces;
import user.com.smartwashr.fragments.OrderHistoryFrag;
import user.com.smartwashr.fragments.VoteFragment;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.UpdateResponse;
import user.com.smartwashr.models.User;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;
import user.com.smartwashr.utils.SingleShotLocationProvider;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, ResponseHandler {

    private FragmentManager fragmentManager;
    private HomeFragment homeFragment;
    private SessionManager sessionManager;
    private PermissionManager permissionsManager;
    private Toolbar toolbar;
    TextView home, order_his, pricing, menu, change_pas, how, log_out, settings, my_places;
    private TextView mTitle;
    private AlertDialog alertDialog;
    boolean location_received;
    private String address = "test";
    private String latitude, longitude;
    private boolean locationSent;
    private boolean called;
    public int val = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        homeFragment = new HomeFragment();
        setDefaultFragment(homeFragment);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            toolbar.setTitle("Home");
            FontUtils.setRobotoBold(toolbar);
        } else {
            toolbar.setTitle("الرئيسية");
            FontUtils.setArabic(toolbar);
        }

        sessionManager = new SessionManager(HomeActivity.this);
        sessionManager.createLoginSession();

        if (Internet.isAvailable(this)) {
            new RestCaller(HomeActivity.this, SmartWashr.getRestClient().update_deviceToken("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), SessionManager.get(Constants.DEVICE_TOKEN)), 2);
        }

        permissionsManager = PermissionManager.getInstance(HomeActivity.this);
        permissionsManager.getPermissionifNotAvailble(new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.ACCESS_FINE_LOCATION
                , Manifest.permission.ACCESS_COARSE_LOCATION}, 111);

        RippleEffect.applyRippleEffect(findViewById(R.id.home), "#bdbdbd");
        RippleEffect.applyRippleEffect(findViewById(R.id.order_history), "#bdbdbd");
        RippleEffect.applyRippleEffect(findViewById(R.id.my_location), "#bdbdbd");
//        RippleEffect.applyRippleEffect(findViewById(R.id.pricing), "#bdbdbd");
        RippleEffect.applyRippleEffect(findViewById(R.id.settings), "#bdbdbd");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setClickListeners();

        home = (TextView) findViewById(R.id.home_txt);
        order_his = (TextView) findViewById(R.id.orders_his);
        pricing = (TextView) findViewById(R.id.priceing_txt);
        settings = (TextView) findViewById(R.id.settings_txt);
        change_pas = (TextView) findViewById(R.id.chnge_pass_txt);
        how = (TextView) findViewById(R.id.how_txt);
        menu = (TextView) findViewById(R.id.menu);
        log_out = (TextView) findViewById(R.id.logout_txt);
        my_places = (TextView) findViewById(R.id.my_loc);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setRobotoBold(home);
            FontUtils.setRobotoBold(order_his);
            FontUtils.setRobotoBold(findViewById(R.id.my_loc));
            FontUtils.setRobotoBold(pricing);
            FontUtils.setRobotoBold(settings);
            FontUtils.setMABold(menu);
            FontUtils.setRobotoBold(toolbar);
        } else {
            FontUtils.setArabic(menu);
            FontUtils.setArabic(home);
            FontUtils.setArabic(order_his);
            FontUtils.setArabic(my_places);
            FontUtils.setArabic(settings);
            FontUtils.setArabic(toolbar);
            menu.setText("القائمة");
            home.setText(getString(R.string.home_ar));
            order_his.setText(getString(R.string.order_his_ar));
            my_places.setText(getString(R.string.ar_places));
            pricing.setText(getString(R.string.pricing_ar));
            settings.setText(getString(R.string.ar_settings));
            log_out.setText(getString(R.string.logout));
        }


    }

    void setClickListeners() {
        findViewById(R.id.home).setOnClickListener(this);
//        findViewById(R.id.active_order).setOnClickListener(this);
        findViewById(R.id.order_history).setOnClickListener(this);
        findViewById(R.id.my_location).setOnClickListener(this);
        findViewById(R.id.pricing).setOnClickListener(this);
        findViewById(R.id.settings).setOnClickListener(this);
        findViewById(R.id.change_password).setOnClickListener(this);
        findViewById(R.id.how_it_wrk).setOnClickListener(this);
        findViewById(R.id.logout).setOnClickListener(this);
        findViewById(R.id.back_arrow).setOnClickListener(this);
    }

    public void setDefaultFragment(Fragment fragment) {
        if (fragmentManager.getBackStackEntryCount() < 1) {
            Log.d("fragment", "set Default_  Count:" + fragmentManager.getBackStackEntryCount());
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.mainframe, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void openFragment(final Fragment fragment) {
        Log.d("fragment", "Count: " + fragmentManager.getBackStackEntryCount());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                if (fragmentManager.getBackStackEntryCount() > 1)
                    fragmentManager.popBackStack();
                transaction.add(R.id.mainframe, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadTitles();
    }

    public void loadTitles() {
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            if (val == 0) {
                toolbar.setTitle("Home");
            } else if (val == 1) {
                toolbar.setTitle("Order History");
            } else if (val == 2) {
                toolbar.setTitle("My Places");
            }
        } else {
            if (val == 0) {
                toolbar.setTitle("الرئيسية");
            } else if (val == 1) {
                toolbar.setTitle(getString(R.string.order_his_ar));
            } else if (val == 2) {
                toolbar.setTitle(getString(R.string.ar_places));
            }
        }
    }

    private void openVote() {
        location_received = true;
        VoteFragment voteFragment = new VoteFragment();
        voteFragment.show(fragmentManager, "");
        voteFragment.setOnItemClickListener(new VoteFragment.MyClickListener() {
            @Override
            public void onYes() {
                voteArea();
            }
        });
    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            permissionsManager.getPermissionifNotAvailble(new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE
                    , Manifest.permission.INTERNET
                    , Manifest.permission.ACCESS_FINE_LOCATION
                    , Manifest.permission.ACCESS_COARSE_LOCATION}, 111);
            return;
        } else {
            LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean enabled = service
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!enabled) {
                alertDialog = new AlertDialog.Builder(
                        HomeActivity.this).create();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    alertDialog.setTitle("Attention");
                    alertDialog.setMessage("Please enable GPS from settings");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    });
                } else {
                    alertDialog.setTitle("انتباه");
                    alertDialog.setMessage("الرجاء تمكين غس من الإعدادات");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    });
                }
                alertDialog.show();
            } else {
                called = true;
                sendLocation();
            }
        }

    }

    private void sendLocation() {
        SingleShotLocationProvider.requestSingleUpdate(HomeActivity.this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Log.d("Location", "my location is " + location.toString() + location);
                        if (location != null) {
                            location_received = true;
                            Log.d("Location", location.latitude + " \n" + location.longitude);
                            latitude = location.latitude + "";
                            longitude = location.longitude + "";
                            if (Internet.isAvailable(HomeActivity.this)) {
//                                        new GetAddress().execute(String.format("%.4f,%.4f", location.latitude, location.longitude));
                                new RestCaller(HomeActivity.this, SmartWashr.getRestClient().nearest_laundry("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), location.latitude + "", location.longitude + ""), 1);
                            }
                        }
                    }
                });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finishAffinity();
        } else {
            val = 0;
            super.onBackPressed();
            loadTitles();
        }
    }

    void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  Toast.makeText(ParentActivity.this, "Request Code => "+requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 111:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!location_received && !called)
                        getLocation();
                } else {
                    Toast.makeText(this, "Please allow the location permission. Thanks", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home:
                if (fragmentManager.getBackStackEntryCount() == 1) {
                    val = 0;
                    closeDrawer();
                } else {
                    val = 0;
                    fragmentManager.popBackStack();
                    closeDrawer();
                }
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    toolbar.setTitle("Home");
                } else {
                    toolbar.setTitle("الرئيسية");
                }
                closeDrawer();
                break;
            case R.id.active_order:
                openFragment(new ActiveOrderFrag());
                closeDrawer();
                break;
            case R.id.order_history:
                val = 1;
                openFragment(new OrderHistoryFrag());
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    toolbar.setTitle("Order History");
                } else {
                    toolbar.setTitle(getString(R.string.order_his_ar));
                }
                closeDrawer();
                break;
            case R.id.my_location:
                val = 2;
                openFragment(new MyPlaces());
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    toolbar.setTitle("My Places");
                } else {
                    toolbar.setTitle(getString(R.string.ar_places));
                }
//                closeDrawer();
                closeDrawer();
                break;
            case R.id.pricing:
//                openFragment(new PricingFrag());
//                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
//                    toolbar.setTitle("Pricing");
//                } else {
//                    toolbar.setTitle(getString(R.string.pricing_ar));
//                }
                closeDrawer();
                break;
            case R.id.settings:
                startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
                closeDrawer();
                break;
            case R.id.how_it_wrk:
                Intent i = new Intent(HomeActivity.this, HowToActivity.class);
                startActivity(i);
                closeDrawer();
                break;
            case R.id.logout:
                sessionManager.clearSession();
                sessionManager.remove(Constants.LANG);
                startActivity(new Intent(HomeActivity.this, Splash.class));
                CategoriesHandler categoriesHandler = new CategoriesHandler(HomeActivity.this);
                categoriesHandler.deleteAllOrders();
                categoriesHandler.deleteAllCategories();
                categoriesHandler.deleteLabels();
                categoriesHandler.close();
                finish();
                closeDrawer();
                break;
            case R.id.back_arrow:
                closeDrawer();
                break;
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
//        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
//            Loading.show(HomeActivity.this, false, "Getting Location...");
//        } else {
//            Loading.show(HomeActivity.this, false, "الرجاء الانتظار");
//        }
        if (reqCode == 1) {
            GenericResponse genericResponse = (GenericResponse) response.body();
            if (genericResponse.getDriver_id() != null && genericResponse.getLaundry_id() != null) {
//            Toast.makeText(HomeActivity.this, "WORKING", Toast.LENGTH_SHORT).show();
                locationSent = true;

            } else {
                locationSent = true;
                openVote();
//                alertDialog = new AlertDialog.Builder(
//                        HomeActivity.this).create();
//                alertDialog.setTitle("Failure");
//                alertDialog.setMessage(genericResponse.getError().toString());
//                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        alertDialog.dismiss();
////                        getfinish();
//                    }
//                });
//                alertDialog.show();

            }
        } else if (reqCode == 5) {
            GenericResponse genericResponse = (GenericResponse) response.body();
            Toast.makeText(this, genericResponse.getSuccess(), Toast.LENGTH_SHORT).show();
        } else {
            UpdateResponse updateResponse = (UpdateResponse) response.body();
            User user = (User) updateResponse.getUser();
        }
    }

    private void voteArea() {
        if (Internet.isAvailable(HomeActivity.this)) {
            new RestCaller(HomeActivity.this, SmartWashr.getRestClient().voteArea("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), latitude, longitude, address), 5);
        }

    }

    private class GetAddress extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                double lat = Double.parseDouble(strings[0].split(",")[0]);
                double lng = Double.parseDouble(strings[0].split(",")[1]);
                String response;
                HttpDataHandler http = new HttpDataHandler();
                String url = String.format("https://maps.googleapis.com/maps/api/geocode/json?latlng=%.4f,%.4f&sensor=false", lat, lng);
                response = http.GetHTTPData(url);
                return response;
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                try {
                    JSONObject jsonObject = new JSONObject(s);

                    address = ((JSONArray) jsonObject.get("results")).getJSONObject(0).get("formatted_address").toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
        }
    }


    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {

    }

    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if (error.getError() != null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
