package user.com.smartwashr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.adapter.ProductsOrder;
import user.com.smartwashr.models.ProductOrder;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

public class OrderPlacement extends AppCompatActivity {

    private TextView dollar_1, dollar_2, dollar_3, category, product, deal1, deal2, deal3, total_price, txt1, txt2, txt3, quantity, proceed, service;
    private EditText order_num;
    private View parent;
    private ProductsOrder productsOrder;

    private LinearLayout d1, d2, d3, checkout;
    private ImageView btn_neg, btn_pos, deal_img_1, deal_img_2, deal_img_3, dress;
    private String dealPrice = "";
    private String sadaPrice = "";
    private int j;
    private String position;
    private String sw_dry_price, sw_wash_price, sw_press, name, url, dry_price, wash_price, press_price;
    private int id;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().getSerializableExtra("products_edit") != null) {
            productsOrder = (ProductsOrder) getIntent().getSerializableExtra("products_edit");
            position = getIntent().getStringExtra("list_position");
        } else if (getIntent().getSerializableExtra("product") != null) {
            productsOrder = (ProductsOrder) getIntent().getSerializableExtra("product");
        }

        ImageView back = (ImageView) findViewById(R.id.back);
        ImageView bback = (ImageView) findViewById(R.id.bback);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        parent = findViewById(R.id.parent);
        j = 0;
        title = (TextView) findViewById(R.id.title);
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            title.setText("Place Order");
        } else {
            title.setText("مكان الامر");
        }
        category = (TextView) findViewById(R.id.cat_txt);
        product = (TextView) findViewById(R.id.product_txt);
        deal1 = (TextView) findViewById(R.id.deal1);
        deal2 = (TextView) findViewById(R.id.deal2);
        deal3 = (TextView) findViewById(R.id.deal3);
        proceed = (TextView) findViewById(R.id.proceed_check);
        quantity = (TextView) findViewById(R.id.quantity_txt);
        total_price = (TextView) findViewById(R.id.tot_price);

        dollar_1 = (TextView) findViewById(R.id.dollar_1);
        dollar_2 = (TextView) findViewById(R.id.dollar_2);
        dollar_3 = (TextView) findViewById(R.id.dollar_3);

        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);

        service = (TextView) findViewById(R.id.service);

        dress = (ImageView) findViewById(R.id.img_back_url);
        deal_img_1 = (ImageView) findViewById(R.id.image_d1);
        deal_img_2 = (ImageView) findViewById(R.id.image_d2);
        deal_img_3 = (ImageView) findViewById(R.id.image_d3);

        d1 = (LinearLayout) findViewById(R.id.d1_con);
        d2 = (LinearLayout) findViewById(R.id.d2_con);
        d3 = (LinearLayout) findViewById(R.id.d3_con);
        checkout = (LinearLayout) findViewById(R.id.checkout);
        order_num = (EditText) findViewById(R.id.order_num);
        order_num.setEnabled(false);

        btn_neg = (ImageView) findViewById(R.id.btn_neg);
        btn_pos = (ImageView) findViewById(R.id.btn_pos);

        if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            service.setText("إختر الخدمة");
            bback.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
            txt1.setText("غسيل جاف مع الكي");
            txt2.setText("غسيل عادي مع الكي");
            txt3.setText("كي فقط");
            quantity.setText("عدد القطع");
            proceed.setText("استمر لإجراء الطلب");
            dollar_1.setText(" ريال");
            dollar_2.setText(" ريال");
            dollar_3.setText(" ريال");
            total_price.setText("0  ريال");
        } else {

            bback.setVisibility(View.GONE);
            back.setVisibility(View.VISIBLE);
            FontUtils.setFont(dollar_1);
            FontUtils.setFont(dollar_2);
            FontUtils.setFont(dollar_3);
            FontUtils.setFont(txt1);
            FontUtils.setFont(txt2);
            FontUtils.setFont(txt3);
            FontUtils.setFont(total_price);
            FontUtils.setFont(proceed);
            FontUtils.setFont(quantity);
            FontUtils.setFont(service);
            FontUtils.setFont(category);
            FontUtils.setFont(product);
        }


        RippleEffect.applyRippleEffect(d1, "#bdbdbd");
        RippleEffect.applyRippleEffect(d2, "#bdbdbd");
        RippleEffect.applyRippleEffect(d3, "#bdbdbd");
        RippleEffect.applyRippleEffect(btn_pos, "#bdbdbd");
        RippleEffect.applyRippleEffect(btn_neg, "#bdbdbd");
        RippleEffect.applyRippleEffect(checkout, "#bdbdbd");

        if (productsOrder == getIntent().getSerializableExtra("products_edit") && productsOrder != null) {
            id = productsOrder.getProductId();
            name = productsOrder.getName();
            url = productsOrder.getUrl();
            sw_dry_price = productsOrder.getSw_dryclean();
            sw_wash_price = productsOrder.getSw_wash_press();
            sw_press = productsOrder.getSw_press();
            dry_price = productsOrder.getDry_price();
            wash_price = productsOrder.getWash_price();
            press_price = productsOrder.getPress_price();
            category.setText(productsOrder.getCategory());
            product.setText(productsOrder.getName());
            deal1.setText(productsOrder.getSw_dryclean());
            deal2.setText(productsOrder.getSw_wash_press());
            deal3.setText(productsOrder.getSw_press());
            Picasso.with(OrderPlacement.this).load(productsOrder.getUrl()).fit().into(dress);
            order_num.setText(productsOrder.getQuantity());

            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                total_price.setText("SAR " + productsOrder.getTotal());
            } else {
                total_price.setText(productsOrder.getTotal() + "ريال ");
            }
            if (productsOrder.getService().equalsIgnoreCase(txt1.getText().toString())) {
                container("1");
            } else if (productsOrder.getService().equalsIgnoreCase(txt2.getText().toString())) {
                container("2");
            } else if (productsOrder.getService().equalsIgnoreCase(txt3.getText().toString())) {
                container("3");
            }
        }

        if (productsOrder == getIntent().getSerializableExtra("product") && productsOrder != null) {
            id = productsOrder.getProductId();
            name = productsOrder.getName();
            url = productsOrder.getUrl();
            sw_dry_price = productsOrder.getSw_dryclean();
            sw_wash_price = productsOrder.getSw_wash_press();
            sw_press = productsOrder.getSw_press();
            dry_price = productsOrder.getDry_price();
            wash_price = productsOrder.getWash_price();
            press_price = productsOrder.getPress_price();
            category.setText(productsOrder.getCategory());
            product.setText(productsOrder.getName());
            deal1.setText(productsOrder.getSw_dryclean());
            deal2.setText(productsOrder.getSw_wash_press());
            deal3.setText(productsOrder.getSw_press());
            Picasso.with(OrderPlacement.this).load(productsOrder.getUrl()).fit().into(dress);

        }


        if (productsOrder != getIntent().getSerializableExtra("products_edit")) {

            DrawableCompat.setTint(deal_img_1.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));
            DrawableCompat.setTint(deal_img_2.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));
            DrawableCompat.setTint(deal_img_3.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));

            dollar_1.setTextColor(getResources().getColor(R.color.colorPrimary));
            dollar_2.setTextColor(getResources().getColor(R.color.colorPrimary));
            dollar_3.setTextColor(getResources().getColor(R.color.colorPrimary));

            txt1.setTextColor(getResources().getColor(R.color.colorPrimary));
            txt2.setTextColor(getResources().getColor(R.color.colorPrimary));
            txt3.setTextColor(getResources().getColor(R.color.colorPrimary));
        }


        d1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container("1");
                order_num.setText("1");
                dealPrice = deal1.getText().toString();
                sadaPrice = productsOrder.getDry_price();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    total_price.setText("SAR " + deal1.getText().toString());
                } else {
                    total_price.setText(deal1.getText().toString() + " ريال ");
                }
            }
        });

        d2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container("2");
                order_num.setText("1");
                dealPrice = deal2.getText().toString();
                sadaPrice = productsOrder.getWash_price();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    total_price.setText("SAR " + deal2.getText().toString());
                } else {
                    total_price.setText(deal2.getText().toString() + " ريال ");

                }
            }
        });

        d3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container("3");
                order_num.setText("1");
                dealPrice = deal3.getText().toString();
                sadaPrice = productsOrder.getPress_price();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    total_price.setText("SAR " + deal3.getText().toString());
                } else {
                    total_price.setText(deal3.getText().toString() + " ريال ");
                }
            }
        });


        btn_pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (order_num.getText().toString().length() < 1) {
                    Toast.makeText(OrderPlacement.this, "Please select a service first.", Toast.LENGTH_SHORT).show();
                } else {
                    String text = order_num.getText().toString();
                    int i = Integer.parseInt(text) + 1;
                    order_num.setText("" + i);

                    if (dealPrice.equalsIgnoreCase("-")) {
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            Toast.makeText(OrderPlacement.this, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderPlacement.this, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            total_price.setText("SAR " + i * Integer.parseInt(dealPrice));
                        } else {
                            total_price.setText(i * Integer.parseInt(dealPrice) + " ريال ");
                        }
                    }
                }
            }
        });

        btn_neg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (order_num.getText().toString().length() < 1 || order_num.getText().toString().equalsIgnoreCase("1")) {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(OrderPlacement.this, "Cannot proceed", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderPlacement.this, "لا يمكن المتابعة", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String text = order_num.getText().toString();
                    int i = Integer.parseInt(text);
                    if (i != 0) {
                        int j = i - 1;
                        order_num.setText("" + j);
                        if (dealPrice.equalsIgnoreCase("-")) {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Toast.makeText(OrderPlacement.this, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OrderPlacement.this, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                total_price.setText("SAR " + j * Integer.parseInt(dealPrice));
                            } else {
                                total_price.setText(j * Integer.parseInt(dealPrice) + " ريال ");
                            }
                        }
                    }
                }
            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (j == 0) {
                    Snackbar.make(parent, "Please select service to proceed. Thank you", Snackbar.LENGTH_SHORT).show();
                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        String total = total_price.getText().toString();
                        total = total.substring(4, total_price.length());
                        Intent i = new Intent(OrderPlacement.this, OrderDone.class);
                        ProductOrder productOrder = new ProductOrder();
                        ProductsOrder productsOrder = new ProductsOrder();
                        ArrayList<ProductsOrder> products = new ArrayList<>();
                        productsOrder.setProductId(id);
                        productsOrder.setCategory(category.getText().toString());
                        productsOrder.setName(name);
                        productsOrder.setSw_dryclean(sw_dry_price);
                        productsOrder.setSw_wash_press(sw_wash_price);
                        productsOrder.setSw_press(sw_press);
                        productsOrder.setSwPrice(dealPrice);
                        productsOrder.setDry_price(dry_price);
                        productsOrder.setWash_price(wash_price);
                        productsOrder.setPress_price(press_price);
                        productsOrder.setPrice(sadaPrice);
                        if (j == 1) {
                            productsOrder.setService(txt1.getText().toString());
                        } else if (j == 2) {
                            productsOrder.setService(txt2.getText().toString());
                        } else if (j == 3) {
                            productsOrder.setService(txt3.getText().toString());
                        }
                        productsOrder.setQuantity(order_num.getText().toString());
                        productsOrder.setTotal(total);
                        productsOrder.setUrl(url);
                        products.add(productsOrder);
                        productOrder.setProductsOrder(products);
                        i.putExtra("products", productOrder);
                        if (position != null) {
                            i.putExtra("list_position", position);
                        }
                        startActivity(i);
                        finish();
                    } else {
                        String total = total_price.getText().toString();
                        total = total.substring(0, total_price.length() - 5).trim();
                        Intent i = new Intent(OrderPlacement.this, OrderDone.class);
                        ProductOrder productOrder = new ProductOrder();
                        ProductsOrder productsOrder = new ProductsOrder();
                        ArrayList<ProductsOrder> products = new ArrayList<>();
                        productsOrder.setProductId(id);
                        productsOrder.setCategory(category.getText().toString());
                        productsOrder.setName(name);
                        productsOrder.setSw_dryclean(sw_dry_price);
                        productsOrder.setSw_wash_press(sw_wash_price);
                        productsOrder.setSw_press(sw_press);
                        productsOrder.setSwPrice(dealPrice);
                        productsOrder.setDry_price(dry_price);
                        productsOrder.setWash_price(wash_price);
                        productsOrder.setPress_price(press_price);
                        productsOrder.setPrice(sadaPrice);
                        if (j == 1) {
                            productsOrder.setService(txt1.getText().toString());
                        } else if (j == 2) {
                            productsOrder.setService(txt2.getText().toString());
                        } else if (j == 3) {
                            productsOrder.setService(txt3.getText().toString());
                        }
                        productsOrder.setQuantity(order_num.getText().toString());
                        productsOrder.setTotal(total);
                        productsOrder.setUrl(url);
                        products.add(productsOrder);
                        productOrder.setProductsOrder(products);
                        i.putExtra("products", productOrder);
                        if (position != null) {
                            i.putExtra("list_position", position);
                        }
                        startActivity(i);
                        finish();
                    }
                }
            }
        });
    }

    private void container(String i) {
        if (i.equalsIgnoreCase("1")) {
            j = 1;
            DrawableCompat.setTint(deal_img_1.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.white));
            DrawableCompat.setTint(deal_img_2.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));
            DrawableCompat.setTint(deal_img_3.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));

            dollar_1.setTextColor(getResources().getColor(R.color.white));
            dollar_2.setTextColor(getResources().getColor(R.color.colorPrimary));
            dollar_3.setTextColor(getResources().getColor(R.color.colorPrimary));

            txt1.setTextColor(getResources().getColor(R.color.white));
            txt2.setTextColor(getResources().getColor(R.color.colorPrimary));
            txt3.setTextColor(getResources().getColor(R.color.colorPrimary));

            deal1.setTextColor(getResources().getColor(R.color.white));
            d1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            deal2.setTextColor(getResources().getColor(R.color.colorPrimary));
            d2.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            deal3.setTextColor(getResources().getColor(R.color.colorPrimary));
            d3.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            dealPrice = deal1.getText().toString();

        } else if (i.equalsIgnoreCase("2")) {
            j = 2;
            DrawableCompat.setTint(deal_img_1.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));
            DrawableCompat.setTint(deal_img_2.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.white));
            DrawableCompat.setTint(deal_img_3.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));

            dollar_1.setTextColor(getResources().getColor(R.color.colorPrimary));
            dollar_2.setTextColor(getResources().getColor(R.color.white));
            dollar_3.setTextColor(getResources().getColor(R.color.colorPrimary));

            txt1.setTextColor(getResources().getColor(R.color.colorPrimary));
            txt2.setTextColor(getResources().getColor(R.color.white));
            txt3.setTextColor(getResources().getColor(R.color.colorPrimary));

            deal1.setTextColor(getResources().getColor(R.color.colorPrimary));
            d1.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            deal2.setTextColor(getResources().getColor(R.color.white));
            d2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            deal3.setTextColor(getResources().getColor(R.color.colorPrimary));
            d3.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            dealPrice = deal2.getText().toString();

        } else if (i.equalsIgnoreCase("3")) {
            j = 3;
            DrawableCompat.setTint(deal_img_3.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.white));
            DrawableCompat.setTint(deal_img_2.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));
            DrawableCompat.setTint(deal_img_1.getDrawable(), ContextCompat.getColor(OrderPlacement.this, R.color.colorPrimary));
            deal1.setTextColor(getResources().getColor(R.color.colorPrimary));

            dollar_1.setTextColor(getResources().getColor(R.color.colorPrimary));
            dollar_2.setTextColor(getResources().getColor(R.color.colorPrimary));
            dollar_3.setTextColor(getResources().getColor(R.color.white));

            txt1.setTextColor(getResources().getColor(R.color.colorPrimary));
            txt2.setTextColor(getResources().getColor(R.color.colorPrimary));
            txt3.setTextColor(getResources().getColor(R.color.white));
            d1.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            deal2.setTextColor(getResources().getColor(R.color.colorPrimary));
            d2.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            deal3.setTextColor(getResources().getColor(R.color.white));
            d3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            dealPrice = deal3.getText().toString();

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        OrderPlacement.this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
