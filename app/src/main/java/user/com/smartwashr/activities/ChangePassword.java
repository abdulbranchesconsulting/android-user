package user.com.smartwashr.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 5/31/17.
 */

public class ChangePassword extends AppCompatActivity implements ResponseHandler {

    private EditText old_pass, new_pass, cpass;
    private Button submit;
    private SessionManager sessionManager;
    private AlertDialog alertDialog;
    private View parent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass);

        old_pass = (EditText) findViewById(R.id.old_pass);
        new_pass = (EditText) findViewById(R.id.new_pass);
        cpass = (EditText) findViewById(R.id.cpass);

        parent = findViewById(R.id.parent);
        sessionManager = new SessionManager(ChangePassword.this);
        submit = (Button) findViewById(R.id.update);
        TextView title = findViewById(R.id.title);
        title.setText(Constants.TRANSLATIONS.getChangePasswordLabel());

        old_pass.setHint(Constants.TRANSLATIONS.getCurrentPasswordLabel());
        new_pass.setHint(Constants.TRANSLATIONS.getNewPasswordLabel());
        cpass.setHint(Constants.TRANSLATIONS.getConfirmPasswordLabel());
        submit.setText(Constants.TRANSLATIONS.getSaveLabel());

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (old_pass.getText().length() > 0 && new_pass.getText().length() > 0 && cpass.getText().length() > 0) {
                    if (Internet.isAvailable(ChangePassword.this)) {
                        if (new_pass.getText().toString().length() >= 6) {
                            if (!new_pass.getText().toString().equalsIgnoreCase(cpass.getText().toString())) {
                                Snackbar.make(parent, Constants.TRANSLATIONS.getPasswordAndConfirmPasswordMustSameLabel(), Snackbar.LENGTH_LONG).show();
                            } else {
                                new RestCaller(ChangePassword.this, SmartWashr.getRestClient().changePassword(sessionManager.get(Constants.ACCESS_TOKEN), old_pass.getText().toString(), new_pass.getText().toString()), 1);
                                Loading.show(ChangePassword.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());

                            }
                        } else {
                            alertDialog = new AlertDialog.Builder(
                                    ChangePassword.this).create();

                            alertDialog.setTitle(Constants.TRANSLATIONS.getPasswordLabel());
                            alertDialog.setMessage(Constants.TRANSLATIONS.getPasswordInstructions());
                            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });

                            alertDialog.show();
                        }
                    }
                } else {
                    Toast.makeText(ChangePassword.this, Constants.TRANSLATIONS.getLabelPleaseFillAllFields(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        GenericResponse genericResponse = (GenericResponse) response.body();
        String title, msg = "";
        if (genericResponse.getMsg() != null) {
            title = "Success";
            msg = genericResponse.getMsg().toString();
        } else {
            title = "Failure";
//            msg = genericResponse.getError().toString();
        }
        alertDialog = new AlertDialog.Builder(
                ChangePassword.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
