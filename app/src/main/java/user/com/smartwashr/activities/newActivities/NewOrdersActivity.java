package user.com.smartwashr.activities.newActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.MapsActivity;
import user.com.smartwashr.adapter.newAdapters.NewOrderAdapter;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.orders.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewOrdersActivity extends AppCompatActivity implements ResponseHandler {

    private ImageView ivBack;
    private ImageView imgOrder;
    private TextView active;
    private TextView completed, title;
    private RecyclerView recyclerView;
    private LinearLayout llPlaceOrder;
    SessionManager sessionManager;
    NewOrderAdapter adapter;
    private ArrayList<OrderResponse> list = new ArrayList<>();
    private TextView tv_text, tv_placeorder;

    private void findViews() {
        ivBack = (ImageView) findViewById(R.id.iv_back);
        imgOrder = (ImageView) findViewById(R.id.img_order);
        active = (TextView) findViewById(R.id.active);
        completed = (TextView) findViewById(R.id.completed);

        title = (TextView) findViewById(R.id.title);
        tv_text = findViewById(R.id.tv_text);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        llPlaceOrder = (LinearLayout) findViewById(R.id.ll_place_order);
        tv_placeorder = findViewById(R.id.tv_placeorder);

        title.setText(Constants.TRANSLATIONS.getMyOrderLabel());
        active.setText(Constants.TRANSLATIONS.getActiveLabel());
        completed.setText(Constants.TRANSLATIONS.getCompletedLabel());
        tv_text.setText(Constants.TRANSLATIONS.getNoOrderFound());
        tv_placeorder.setText(Constants.TRANSLATIONS.getPlaceNewOrderLabel());

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_orders_screen);
        sessionManager = new SessionManager(this);

        findViews();

        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
                    imgOrder.setImageResource(R.drawable.order_bg_2);
                } else {
                    imgOrder.setImageResource(R.drawable.order_bg_1);

                }
                completed.setTextColor(Color.WHITE);
                active.setTextColor(Color.GRAY);

                getOrders("completed");

            }
        });

        active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
                    imgOrder.setImageResource(R.drawable.order_bg_1);
                } else {
                    imgOrder.setImageResource(R.drawable.order_bg_2);
                }
                completed.setTextColor(Color.GRAY);
                active.setTextColor(Color.WHITE);
                getOrders("active");

            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        adapter = new NewOrderAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        getOrders("active");

        llPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewOrdersActivity.this, MapsActivity.class));
            }
        });

    }

    private void getOrders(String status) {
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
        new RestCaller(NewOrdersActivity.this, SmartWashr.getRestClient().getOrders(sessionManager.get(Constants.ACCESS_TOKEN), status), 1);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        list.clear();
        ArrayList<OrderResponse> orderResponses = (ArrayList<OrderResponse>) response.body();
        Log.e("RESPONSE", new Gson().toJson(orderResponses));
        if (orderResponses.size() > 0) {
            list.addAll(orderResponses);
            adapter.notifyDataSetChanged();
            tv_text.setVisibility(View.GONE);
        } else {
            tv_text.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewOrdersActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
