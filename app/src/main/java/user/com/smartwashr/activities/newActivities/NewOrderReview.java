package user.com.smartwashr.activities.newActivities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.StringConverterFactory;
import user.com.smartwashr.TimePasser;
import user.com.smartwashr.adapter.TimeAdapter;
import user.com.smartwashr.adapter.newAdapters.OrderItemsAdapter;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.ProductOrder;
import user.com.smartwashr.models.TimeSlotResponse;
import user.com.smartwashr.models.couponResponse.CouponResponse;
import user.com.smartwashr.newModels.OrderProduct;
import user.com.smartwashr.newModels.orderPlacement.OrderPlacementResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestApis;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewOrderReview extends AppCompatActivity implements ResponseHandler,
        DatePickerDialog.OnDateSetListener,
        TimePasser {

    private RelativeLayout top;
    private ImageView ivBack;
    private LinearLayout shashka;
    private RelativeLayout rlTimeCon;
    private TextView txtPick;
    private TextView tvPdate;
    private TextView tvPtimeSlot;
    private TextView txtDelivery;
    private TextView tvDdate;
    private TextView tvDtimeSlot;
    private TextView tvChange1;
    private TextView tvChange2;
    private LinearLayout llTitles;
    private RecyclerView itemRecycler;
    private View view1;
    private TextView tvPayment;
    private LinearLayout llMethods;
    private RelativeLayout rlPos;
    private ImageView ivPos;
    private ImageView ivPosSelected;
    private TextView pos;
    private RelativeLayout rlCod, rlSort;
    private ImageView ivCod;
    private ImageView ivCodSelected;
    private TextView cod;
    private LinearLayout rlNotes;
    private EditText etDescription;
    private RelativeLayout rlPromo;
    private TextView txtPromo;
    private EditText etEntercode;
    private ImageView ivApplycode, iv_cod_1, iv_pos_1;
    private View view2;
    private TextView tvDeliveryCharges;
    private TextView tvTotal;
    private LinearLayout llPlaceOrder;
    private SessionManager sessionManager;

    private OrderItemsAdapter adapter;
    private String pick_date, del_date = null;
    private boolean isPicked;
    private int DAY;
    private int col_del = 0;
    private TimePasser timePasser;

    private String original_price;


    private LinearLayout time_container;

    private ArrayList<String> time_list = new ArrayList<>();
    private ArrayList<String> full_time_list = new ArrayList<>();

    private TimeAdapter time_adapter;
    private TextView view_title, done;
    private RecyclerView time_view;
    private int hour24, hour;
    private Integer OPEN_HOUR, CLOSE_HOUR;
    private String collection_time;
    private String coll_time;
    private String del_time;
    private Gson gson = new Gson();
    private int total;


    android.app.AlertDialog.Builder alertDialogBuilder;
    private int CURRENT_DAY;
    private ArrayList<String> all_day_time_list = new ArrayList<>();
    private ArrayList<String> current_time_list = new ArrayList<>();
    private boolean isCurrentDay = false;
    private Integer coupon_id;
    private String payment_method = null;
    private LinearLayout price_con;
    private TextView title, tv_items, tv_qty, tv_cost, tv_placeorder, tv_notes, txt_sort, tv_sortFee, txt_total, txt_delivery_charges;

    public static final String URL = "http://45.63.83.178/api/v1/";

    private void findViews() {
        top = findViewById(R.id.top);
        ivBack = findViewById(R.id.iv_back);
        shashka = findViewById(R.id.shashka);
        rlTimeCon = findViewById(R.id.rl_time_con);

        txt_delivery_charges = findViewById(R.id.txt_delivery_charges);
        txt_total = findViewById(R.id.txt_total_3);
        txtPick = findViewById(R.id.txt_pick);
        tvPdate = findViewById(R.id.tv_pdate);
        tvPtimeSlot = findViewById(R.id.tv_ptimeSlot);
        txtDelivery = findViewById(R.id.txt_delivery);
        tvDdate = findViewById(R.id.tv_ddate);
        tvDtimeSlot = findViewById(R.id.tv_dtimeSlot);
        tvChange1 = findViewById(R.id.tv_change1);
        tvChange2 = findViewById(R.id.tv_change2);
        llTitles = findViewById(R.id.ll_titles);
        itemRecycler = findViewById(R.id.item_recycler);
        view1 = findViewById(R.id.view1);
        tvPayment = findViewById(R.id.tv_payment);
        llMethods = findViewById(R.id.ll_methods);
        rlPos = findViewById(R.id.rl_pos);
        ivPos = findViewById(R.id.iv_pos);
        iv_cod_1 = findViewById(R.id.iv_cod_1);
        iv_pos_1 = findViewById(R.id.iv_pos_1);
        title = findViewById(R.id.title);
        ivPosSelected = findViewById(R.id.iv_pos_selected);
        pos = findViewById(R.id.pos);
        rlCod = findViewById(R.id.rl_cod);
        ivCod = findViewById(R.id.iv_cod);
        ivCodSelected = findViewById(R.id.iv_cod_selected);
        cod = findViewById(R.id.cod);
        rlNotes = findViewById(R.id.rl_notes);
        etDescription = findViewById(R.id.et_description);
        rlPromo = findViewById(R.id.rl_promo);
        txtPromo = findViewById(R.id.txt_promo);
        etEntercode = findViewById(R.id.et_entercode);
        ivApplycode = findViewById(R.id.iv_applycode);
        view2 = findViewById(R.id.view2);
        tvDeliveryCharges = findViewById(R.id.tv_delivery_charges);
        tvTotal = findViewById(R.id.tv_total);
        llPlaceOrder = findViewById(R.id.ll_place_order);
        price_con = findViewById(R.id.price_con);
        rlSort = findViewById(R.id.rl_sort);

        time_container = findViewById(R.id.time_container);
        view_title = findViewById(R.id.view_title);
        done = findViewById(R.id.done);
        time_view = findViewById(R.id.time_view);

        tv_sortFee = findViewById(R.id.tv_sort);
        txt_sort = findViewById(R.id.txt_sort);

        ivCodSelected.setImageResource(R.drawable.ic_unselected_tick);
        ivPosSelected.setImageResource(R.drawable.ic_unselected_tick);

        tv_items = findViewById(R.id.tv_items);
        tv_qty = findViewById(R.id.tv_qty);
        tv_cost = findViewById(R.id.tv_cost);
        tv_notes = findViewById(R.id.tv_notes);

        tv_placeorder = findViewById(R.id.tv_placeorder);

        txt_sort.setText(Constants.TRANSLATIONS.getSortingFeeLabel());
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
            tv_sortFee.setText(Constants.TRANSLATIONS.getCurrency() + " " + SessionManager.get(Constants.SORT_FEE));
        } else {
            tv_sortFee.setText(SessionManager.get(Constants.SORT_FEE) + " " + Constants.TRANSLATIONS.getCurrency());
        }
        tvPayment.setText(Constants.TRANSLATIONS.getPaymentMethodLabel());
        tv_items.setText(Constants.TRANSLATIONS.getItemsLabel());
        tv_qty.setText(Constants.TRANSLATIONS.getQuantityLabel());
        tv_cost.setText(Constants.TRANSLATIONS.getCostLabel());

        txt_total.setText(Constants.TRANSLATIONS.getTotalLabel());
        txt_delivery_charges.setText(Constants.TRANSLATIONS.getDeliveryChargesLabel());
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
            tvDeliveryCharges.setText(Constants.TRANSLATIONS.getCurrency() + " " + SessionManager.get(Constants.DELIVERY_CHARGES));
        } else {
            tvDeliveryCharges.setText(SessionManager.get(Constants.DELIVERY_CHARGES) + " " + Constants.TRANSLATIONS.getCurrency());
        }
        tv_placeorder.setText(Constants.TRANSLATIONS.getPlaceNewOrderLabel());
        tv_notes.setText(Constants.TRANSLATIONS.getNotesLabel());
        etDescription.setHint(Constants.TRANSLATIONS.getAdd_description());
        txtPromo.setText(Constants.TRANSLATIONS.getPromoCodeFieldLabel());
        etEntercode.setHint(Constants.TRANSLATIONS.getEnterCodeLabel());
        time_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        tvChange2.setEnabled(false);
        timePasser = this;

        title.setText(Constants.TRANSLATIONS.getReviewAndOrderLabel());
        tvChange1.setText(Constants.TRANSLATIONS.getChangeLabel());
        tvChange2.setText(Constants.TRANSLATIONS.getChangeLabel());
        txtPick.setText(Constants.TRANSLATIONS.getPickupDateLabel());
        txtDelivery.setText(Constants.TRANSLATIONS.getDropoffDateLabel());

        cod.setText(Constants.TRANSLATIONS.getCashOnDeliveryLabel());
        pos.setText(Constants.TRANSLATIONS.getPosLabel());
        txt_sort.setText(Constants.TRANSLATIONS.getSortingFeeLabel());
//        tv_sortFee.setText();

        if (getIntent().getStringExtra("skip") == null) {
            rlSort.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_review);

        sessionManager = new SessionManager(this);
        alertDialogBuilder = new AlertDialog.Builder(this);

        findViews();

        if (getIntent().getStringExtra("skip") == null) {
            itemRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            adapter = new OrderItemsAdapter(this, Constants.orderList);
            itemRecycler.setAdapter(adapter);
        } else {
            itemRecycler.setVisibility(View.GONE);
//            price_con.setVisibility(View.GONE);
            txt_total.setVisibility(View.GONE);
            tvTotal.setVisibility(View.GONE);
            llTitles.setVisibility(View.INVISIBLE);
        }

        llPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });

        calculateTotal(tvTotal);

        int HOUR = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (HOUR == 0) {
            HOUR = 1;
        }
        getTimeSlots(HOUR);

        tvChange1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPicked = true;

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        NewOrderReview.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DATE)
                );
                dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE)));
                dpd.setTitle(getResources().getString(R.string.time_date_msg));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        tvChange2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPicked = false;
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        NewOrderReview.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        DAY
                );
                dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), DAY + 1));
                dpd.setTitle(getResources().getString(R.string.time_date_msg));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ivApplycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etEntercode.getText().toString().length() > 0) {
                    loading();
                    applyCode();
                }
            }
        });

        rlCod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPayment(1);
            }
        });

        rlPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPayment(0);
            }
        });

    }

    private void applyCode() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);

        httpClient.addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY));
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(StringConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        RestApis retrofitInterface = retrofit.create(RestApis.class);
        Call<CouponResponse> call = null;

//        new RestCaller(NewOrderReview.this, SmartWashr.getRestClient().applyCoupon1(sessionManager.get(Constants.ACCESS_TOKEN), sessionManager.get(Constants.LANG), original_price, etEntercode.getText().toString()), 3);
//        new RestCaller(NewOrderReview.this, SmartWashr.getRestClient().applyCoupon1(sessionManager.get(Constants.ACCESS_TOKEN), sessionManager.get(Constants.LANG), "10000", etEntercode.getText().toString()), 3);

        if (getIntent().getStringExtra("skip") == null) {
            call = retrofitInterface.applyCoupon1(SessionManager.get(Constants.ACCESS_TOKEN), SessionManager.get(Constants.LANG), original_price, etEntercode.getText().toString());

            call.enqueue(new Callback<CouponResponse>() {
                @Override
                public void onResponse(Call<CouponResponse> call, Response<CouponResponse> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {
                        CouponResponse couponResponse = response.body();
                        coupon_id = couponResponse.getCouponId();
                        etEntercode.setTextColor(Color.GREEN);
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
                            tvTotal.setText(Constants.TRANSLATIONS.getCurrency() + " " + (couponResponse.getDiscountedPrice() + Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES))));
                        } else {
                            tvTotal.setText((couponResponse.getDiscountedPrice() + +Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES))) + " " + Constants.TRANSLATIONS.getCurrency());
                        }
                        alertDialogBuilder.setTitle(couponResponse.getMessageTitle());
                        alertDialogBuilder.setMessage(couponResponse.getMessage());
                        alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();
                    } else {
                        ERRORSO error = null;
                        Converter<ResponseBody, ERRORSO> errorConverter =
                                SmartWashr.getRetrofit().responseBodyConverter(ERRORSO.class, new Annotation[0]);
                        try {
                            error = errorConverter.convert(response.errorBody());
                        } catch (IOException e) {
                        }

                        alertDialogBuilder.setTitle(error.getError().getErrorTitle());
                        alertDialogBuilder.setMessage(error.getError().getMessage());
                        alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();
                    }

                }

                @Override
                public void onFailure(Call<CouponResponse> call, Throwable throwable) {

                }
            });

        } else {
            call = retrofitInterface.applyCoupon1(SessionManager.get(Constants.ACCESS_TOKEN), SessionManager.get(Constants.LANG), "10000", etEntercode.getText().toString());

            call.enqueue(new Callback<CouponResponse>() {
                @Override
                public void onResponse(Call<CouponResponse> call, Response<CouponResponse> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {

                        CouponResponse couponResponse = response.body();
                        coupon_id = couponResponse.getCouponId();
                        etEntercode.setTextColor(Color.GREEN);
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
                            tvTotal.setText(Constants.TRANSLATIONS.getCurrency() + " " + (couponResponse.getDiscountedPrice() + Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES))));
                        } else {
                            tvTotal.setText((couponResponse.getDiscountedPrice() + +Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES))) + " " + Constants.TRANSLATIONS.getCurrency());
                        }
                        alertDialogBuilder.setTitle(couponResponse.getMessageTitle());
                        alertDialogBuilder.setMessage(couponResponse.getMessage());
                        alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();
                    } else {
                        ERRORSO error = null;
                        Converter<ResponseBody, ERRORSO> errorConverter =
                                SmartWashr.getRetrofit().responseBodyConverter(ERRORSO.class, new Annotation[0]);
                        try {
                            error = errorConverter.convert(response.errorBody());
                        } catch (IOException e) {
                        }

                        alertDialogBuilder.setTitle(error.getError().getErrorTitle());
                        alertDialogBuilder.setMessage(error.getError().getMessage());
                        alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();
                    }

                }

                @Override
                public void onFailure(Call<CouponResponse> call, Throwable throwable) {

                }
            });

        }


    }

    private void selectPayment(int i) {
        if (i == 0) {
            ivPosSelected.setImageResource(R.drawable.ic_tick_selected);
            iv_pos_1.setImageResource(R.drawable.ic_pos);
            pos.setTextColor(getResources().getColor(R.color.blue));

            ivCodSelected.setImageResource(R.drawable.ic_unselected_tick);
            iv_cod_1.setImageResource(R.drawable.ic_cod);
            cod.setTextColor(getResources().getColor(R.color.grey));

            payment_method = "pos";

        } else {
            ivPosSelected.setImageResource(R.drawable.ic_unselected_tick);
            iv_pos_1.setImageResource(R.drawable.ic_pos_unselected);
            pos.setTextColor(getResources().getColor(R.color.grey));

            ivCodSelected.setImageResource(R.drawable.ic_tick_selected);
            iv_cod_1.setImageResource(R.drawable.ic_cod_select);
            cod.setTextColor(getResources().getColor(R.color.blue));
            payment_method = "cod";


        }
    }

    private void getTimeSlots(int hour) {
        loading();
        new RestCaller(NewOrderReview.this, SmartWashr.getRestClient().getTimeSlots(hour), 1);

    }

    private void placeOrder() {
        if (pick_date != null && del_date != null) {
            if (payment_method != null) {
                loading();
                if (getIntent().getStringExtra("skip") == null) {
                    ProductOrder productOrder = new ProductOrder();
                    productOrder.setPayment_method(payment_method);
                    productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                    productOrder.setAddressOne(SessionManager.get(Constants.ADDRESS_ONE));
                    productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                    productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                    productOrder.setUser_comments(etDescription.getText().toString());
                    productOrder.setCouponId(coupon_id);
                    productOrder.setCollectionDateTime(getFormattedTime(pick_date + " " + coll_time + ":00"));
                    productOrder.setDeliveryDateTime(getFormattedTime(del_date + " " + del_time + ":00"));
                    productOrder.setOrderProducts(Constants.orderList);
                    String json = gson.toJson(productOrder);
                    Log.e("ORDER", json);
                    new RestCaller(NewOrderReview.this, SmartWashr.getRestClient().orderReview(SessionManager.get(Constants.ACCESS_TOKEN),
                            SessionManager.get(Constants.LANG),
                            "application/json",
                            productOrder), 2);
                } else {
                    if (coupon_id != null) {
                        loading();
                        new RestCaller(NewOrderReview.this, SmartWashr.getRestClient().skipOrder(
                                SessionManager.get(Constants.ACCESS_TOKEN),
                                SessionManager.get(Constants.LANG),
                                "application/json",
                                SessionManager.get(Constants.LATITUDE),
                                SessionManager.get(Constants.LONGITUDE),
                                SessionManager.get(Constants.DRIVER_ID),
                                coupon_id + "",
                                SessionManager.get(Constants.ADDRESS_ONE),
                                pick_date + " " + coll_time + ":00",
                                del_date + " " + del_time + ":00",
                                payment_method,
                                etDescription.getText().toString()), 2);
                    } else {
                        loading();
                        new RestCaller(NewOrderReview.this, SmartWashr.getRestClient().skipOrder(
                                SessionManager.get(Constants.ACCESS_TOKEN),
                                SessionManager.get(Constants.LANG),
                                "application/json",
                                SessionManager.get(Constants.LATITUDE),
                                SessionManager.get(Constants.LONGITUDE),
                                SessionManager.get(Constants.DRIVER_ID),
                                SessionManager.get(Constants.ADDRESS_ONE),
                                pick_date + " " + coll_time + ":00",
                                del_date + " " + coll_time + ":00",
                                payment_method,
                                etDescription.getText().toString()), 2);
                    }
                }
            } else {
                Toast.makeText(this, "Please select the Payment Method first", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please select the pickup/delivery date and time", Toast.LENGTH_SHORT).show();
        }
    }

    private String getFormattedTime(String time) {
        String displayValue = time + "PM";
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy h:mma", Locale.US);
            Date date = dateFormatter.parse(displayValue);

// Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("dd-MM-yyy HH:mm", Locale.US);
            displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayValue;

    }

    private void loading() {
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 1) {
            TimeSlotResponse timeSlotResponse = (TimeSlotResponse) response.body();

            SimpleDateFormat currentDate = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.forLanguageTag("en"));
            }
            SimpleDateFormat currentDate1 = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                currentDate1 = new SimpleDateFormat("dd-MM-yyyy", Locale.forLanguageTag("en"));
            }

            if (timeSlotResponse.getCurrentDayTime() != null) {

                time_list = timeSlotResponse.getCurrentDayTime();
                full_time_list = timeSlotResponse.getAllDaysTime();
                all_day_time_list = timeSlotResponse.getAllDaysTime();
                tvPtimeSlot.setText(timeSlotResponse.getCurrentDayTime().get(0));

                if (timeSlotResponse.getCurrentDayTime().size() > 1) {
                    tvDtimeSlot.setText(timeSlotResponse.getCurrentDayTime().get(1));
                    coll_time = time_list.get(0).substring(0, 2);
                    del_time = time_list.get(1).substring(0, 2);
                } else {
                    tvDtimeSlot.setText(timeSlotResponse.getAllDaysTime().get(0));
                    coll_time = time_list.get(0).substring(0, 2);
                    del_time = full_time_list.get(0).substring(0, 2);
                }

            } else {

                time_list = timeSlotResponse.getAllDaysTime();
                full_time_list = timeSlotResponse.getAllDaysTime();
                all_day_time_list = timeSlotResponse.getAllDaysTime();

                tvPtimeSlot.setText(timeSlotResponse.getAllDaysTime().get(0));
                tvDtimeSlot.setText(timeSlotResponse.getAllDaysTime().get(1));
                coll_time = time_list.get(0).substring(0, 2);
                del_time = time_list.get(1).substring(0, 2);


            }


            Date todayDate = new Date();
            String today = currentDate.format(todayDate);
            pick_date = currentDate1.format(todayDate);

            Date tomorrow = new Date(todayDate.getTime() + (1000 * 60 * 60 * 24));
            String tom1 = currentDate.format(tomorrow);
            del_date = currentDate1.format(tomorrow);
            tvPdate.setText(today + " ;");
            tvDdate.setText(tom1 + " ;");


            CLOSE_HOUR = timeSlotResponse.getCloseTime();
            OPEN_HOUR = timeSlotResponse.getOpenTime();

        } else if (reqCode == 2) {

            OrderPlacementResponse orderPlacementResponse = (OrderPlacementResponse) response.body();

            alertDialogBuilder.setTitle(orderPlacementResponse.getMessageTitle());
            alertDialogBuilder.setMessage(orderPlacementResponse.getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent i = new Intent(NewOrderReview.this, NewHomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        } else {
            CouponResponse couponResponse = (CouponResponse) response.body();
            coupon_id = couponResponse.getCouponId();
            etEntercode.setTextColor(Color.GREEN);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
                tvTotal.setText(Constants.TRANSLATIONS.getCurrency() + " " + (couponResponse.getDiscountedPrice() + Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES))));
            } else {
                tvTotal.setText((couponResponse.getDiscountedPrice() + +Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES))) + " " + Constants.TRANSLATIONS.getCurrency());
            }
            alertDialogBuilder.setTitle(couponResponse.getMessageTitle());
            alertDialogBuilder.setMessage(couponResponse.getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if (error.getError() != null) {
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    private void calculateTotal(TextView txt) {
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < Constants.orderList.size(); i++) {
            OrderProduct product = Constants.orderList.get(i);
            double price = Double.parseDouble((String) product.getServicePrice()) * Integer.parseInt(product.getProductQty());
            prices.add(price);
        }

        int sum = 0;
        for (Double i : prices) {
            sum += i;
        }
        original_price = sum + "";
        total = sum + Integer.parseInt(SessionManager.get(Constants.DELIVERY_CHARGES));
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
            txt.setText(Constants.TRANSLATIONS.getCurrency() + " " + total);
        } else {
            txt.setText(total + " " + Constants.TRANSLATIONS.getCurrency());
        }

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        String day, month;

        if (isPicked) {
            col_del = 0;

            DAY = dayOfMonth;
            day = dayOfMonth + "";
            month = (monthOfYear + 1) + "";

            if (day.length() == 1) {
                day = "0" + day;
            } else {
                day = dayOfMonth + "";
            }
            if (month.length() == 1) {
                month = "0" + (monthOfYear + 1);
            } else {
                month = (monthOfYear + 1) + "";
            }
            pick_date = day + "-" + month + "-" + year;

            if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
                try {
                    String englishNumerals = new BigDecimal(pick_date).toString();
                    Log.e("num", englishNumerals);
                } catch (Exception e) {

                }
            }
            tvPdate.setText(day + "/" + month + "/" + year + " ;");

            hour24 = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            hour = Calendar.getInstance().get(Calendar.HOUR);
            CURRENT_DAY = Calendar.getInstance().get(Calendar.DATE);
            Log.e("DATE", CURRENT_DAY + "");
            col_del = 0;
            isPicked = false;


            if (CURRENT_DAY != DAY) {
                isCurrentDay = false;
                time_adapter = new TimeAdapter(all_day_time_list, NewOrderReview.this, done, timePasser, col_del, view_title);
                time_view.setAdapter(time_adapter);
                time_container.setVisibility(View.VISIBLE);
                time_adapter.notifyDataSetChanged();
            } else {
                isCurrentDay = true;
                if (hour24 > CLOSE_HOUR || hour24 < OPEN_HOUR) {
                    if (hour24 > CLOSE_HOUR) {
                        full_time_list = time_list;
                        time_adapter = new TimeAdapter(full_time_list, NewOrderReview.this, done, timePasser, col_del, view_title);
                        time_view.setAdapter(time_adapter);
                        time_container.setVisibility(View.VISIBLE);
                        time_adapter.notifyDataSetChanged();
                    }
                    if (hour24 > 0 || hour24 < OPEN_HOUR) {
                        full_time_list = time_list;
                        time_adapter = new TimeAdapter(full_time_list, NewOrderReview.this, done, timePasser, col_del, view_title);
                        time_view.setAdapter(time_adapter);
                        time_container.setVisibility(View.VISIBLE);
                        time_adapter.notifyDataSetChanged();
                    }
                } else {
//                full_time_list = new ArrayList<String>(time_list.subList(hour, time_list.size()));
                    full_time_list = time_list;
                    time_adapter = new TimeAdapter(full_time_list, NewOrderReview.this, done, timePasser, col_del, view_title);
                    time_view.setAdapter(time_adapter);
                    time_container.setVisibility(View.VISIBLE);
                    time_adapter.notifyDataSetChanged();
                }
            }
        } else {
            day = dayOfMonth + "";
            month = (monthOfYear + 1) + "";
            if (day.length() == 1) {
                day = "0" + day;
            } else {
                day = dayOfMonth + "";
            }
            if (month.length() == 1) {
                month = "0" + (monthOfYear + 1);
            } else {
                month = (monthOfYear + 1) + "";
            }
            del_date = day + "-" + month + "-" + year;
            tvDdate.setText(day + "/" + month + "/" + year + " ;");
            col_del = 1;
            int position;
            if (isCurrentDay) {
                position = time_list.indexOf(collection_time);
                coll_time = time_list.get(position).substring(0, 2);

            } else {
                position = all_day_time_list.indexOf(collection_time);
                coll_time = all_day_time_list.get(position).substring(0, 2);

            }
            Log.d("coll_time", coll_time);

            if (dayOfMonth == DAY + 1) {
                if (isCurrentDay) {
                    full_time_list = new ArrayList<String>(time_list.subList(position + 1, time_list.size()));
                } else {
                    full_time_list = new ArrayList<String>(all_day_time_list.subList(position + 1, all_day_time_list.size()));
                }
                time_adapter = new TimeAdapter(full_time_list, NewOrderReview.this, done, timePasser, col_del, view_title);
                time_view.setAdapter(time_adapter);
                time_container.setVisibility(View.VISIBLE);
                time_adapter.notifyDataSetChanged();
            } else {
                full_time_list = time_list;
                time_adapter = new TimeAdapter(full_time_list, NewOrderReview.this, done, timePasser, col_del, view_title);
                time_view.setAdapter(time_adapter);
                time_container.setVisibility(View.VISIBLE);
                time_adapter.notifyDataSetChanged();
            }

        }
    }

    @Override
    public void onTimePass(String value, int i) {
        if (i == 1) {
            tvPtimeSlot.setText(value);
            time_container.setVisibility(View.GONE);
            collection_time = value;
            tvChange2.setEnabled(true);
            Log.e("PDATE", pick_date + " " + coll_time + ":00");
        } else {
            del_time = value.substring(0, 2);
            tvDtimeSlot.setText(value);
            time_container.setVisibility(View.GONE);
            Log.e("DDATE", del_date + " " + del_time + ":00");

        }
    }

}
