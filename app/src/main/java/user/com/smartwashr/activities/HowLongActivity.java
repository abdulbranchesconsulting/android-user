package user.com.smartwashr.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

public class HowLongActivity extends AppCompatActivity {
    TextView title, inner;
    ImageView back, bback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_long);

        title = (TextView) findViewById(R.id.title);
        inner = (TextView) findViewById(R.id.inner);
        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            FontUtils.setRobotoMediumFont(findViewById(R.id.inner));
            FontUtils.setRobotoBold(title);
        } else {
            bback.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
            title.setText("كم فترة الانتظار ؟");
            inner.setText("متوسط وقت الانتظار يعتمد على توفر السائق وعلى المسافة بينك وبين أقرب مغسلة.");
            FontUtils.setArabic(title);
            FontUtils.setArabic(inner);
        }

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
