package user.com.smartwashr.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

public class SettingsActivity extends AppCompatActivity implements ResponseHandler {

    TextView settings_title, about, profile, change_pass, change_lang, tell_a, add_promo, add_payment, logout, delete_account;
    private SessionManager sessionManager;
    ImageView back, bback;
    AlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        FacebookSdk.sdkInitialize(getApplicationContext());

        sessionManager = new SessionManager(SettingsActivity.this);

        settings_title = (TextView) findViewById(R.id.setting_title);
        about = (TextView) findViewById(R.id.about);
        profile = (TextView) findViewById(R.id.profile);
        change_pass = (TextView) findViewById(R.id.change_pass);
        change_lang = (TextView) findViewById(R.id.change_lang);
        tell_a = (TextView) findViewById(R.id.tell_friend);
        add_promo = (TextView) findViewById(R.id.add_promo);
        add_payment = (TextView) findViewById(R.id.add_payment);
        logout = (TextView) findViewById(R.id.logout);
        delete_account = (TextView) findViewById(R.id.delete_account);
        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            FontUtils.setRobotoBold(settings_title);
            FontUtils.setRobotoMediumFont(about);
            FontUtils.setRobotoMediumFont(profile);
            FontUtils.setRobotoMediumFont(change_pass);
            FontUtils.setRobotoMediumFont(change_lang);
            FontUtils.setRobotoMediumFont(tell_a);
            FontUtils.setRobotoMediumFont(add_promo);
            FontUtils.setRobotoMediumFont(add_payment);
            FontUtils.setRobotoMediumFont(logout);
            FontUtils.setRobotoBold(delete_account);
        } else {
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            FontUtils.setArabic(settings_title);
            FontUtils.setArabic(about);
            FontUtils.setArabic(profile);
            FontUtils.setArabic(change_pass);
            FontUtils.setArabic(change_lang);
            FontUtils.setArabic(tell_a);
            FontUtils.setArabic(add_promo);
            FontUtils.setArabic(add_payment);
            FontUtils.setArabic(logout);
            FontUtils.setArabic(delete_account);
            settings_title.setText(getString(R.string.ar_settings));
            about.setText(getString(R.string.ar_about));
            profile.setText(getString(R.string.profile_ar));
            change_pass.setText(getString(R.string.chnge_pass));
            change_lang.setText(getString(R.string.ar_lang_sett));
            tell_a.setText(getString(R.string.ar_tell_a_frnd));
            add_promo.setText(getString(R.string.ar_promo));
            add_payment.setText(getString(R.string.ar_add_pay));
            logout.setText(getString(R.string.logout));
            delete_account.setText(getString(R.string.ar_del_account));
        }

        RippleEffect.applyRippleEffect(about, "#bdbdbd");
        RippleEffect.applyRippleEffect(profile, "#bdbdbd");
        RippleEffect.applyRippleEffect(change_pass, "#bdbdbd");
        RippleEffect.applyRippleEffect(change_lang, "#bdbdbd");
        RippleEffect.applyRippleEffect(tell_a, "#bdbdbd");
//        RippleEffect.applyRippleEffect(add_promo, "#bdbdbd");
//        RippleEffect.applyRippleEffect(add_payment, "#bdbdbd");
        RippleEffect.applyRippleEffect(logout, "#bdbdbd");
        RippleEffect.applyRippleEffect(delete_account, "#bdbdbd");

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        alertDialogBuilder = new AlertDialog.Builder(this);

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, AboutActivity.class));
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ProfileActivity.class));
            }
        });

        change_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ChangePassword.class));
            }
        });

        change_lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingsActivity.this, LanguageSelector.class);
                i.putExtra("val", "val");
                startActivity(i);
            }
        });

        tell_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Download Smart Washr and let us take care of your laundry! \n\nhttps://play.google.com/store/apps/details?id=user.com.smartwashr";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        add_promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, PromoActivity.class));
            }
        });

        add_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.clearSession();
                sessionManager.remove(Constants.LANG);
                LoginManager.getInstance().logOut();
                startActivity(new Intent(SettingsActivity.this, Splash.class));
                CategoriesHandler categoriesHandler = new CategoriesHandler(SettingsActivity.this);
                categoriesHandler.deleteAllOrders();
                categoriesHandler.deleteAllCategories();
                categoriesHandler.deleteLabels();
                categoriesHandler.close();
                finish();
            }
        });

        delete_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    alertDialogBuilder.setMessage("Are you sure you want to delete your account?");
                    alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Internet.isAvailable(SettingsActivity.this)) {
                                new RestCaller(SettingsActivity.this, SmartWashr.getRestClient().delete("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN)), 1);
                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                    Loading.show(SettingsActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                                } else {
                                    Loading.show(SettingsActivity.this, false, "الرجاء الانتظار");
                                }
                            } else {
                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                    Toast.makeText(SettingsActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(SettingsActivity.this, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                } else {
                    alertDialogBuilder.setMessage("هل انت متأكد انك تريد حذف حسابك؟");
                    alertDialogBuilder.setPositiveButton("نعم فعلا", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Internet.isAvailable(SettingsActivity.this)) {
                                new RestCaller(SettingsActivity.this, SmartWashr.getRestClient().delete("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN)), 1);
                                Loading.show(SettingsActivity.this, false, "الرجاء الانتظار");
                            } else {
                                Toast.makeText(SettingsActivity.this, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }


            }
        });

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        GenericResponse genericResponse = (GenericResponse) response.body();
        if (genericResponse.getSuccess() != null) {
            alertDialog = new AlertDialog.Builder(
                    SettingsActivity.this).create();
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                alertDialog.setTitle("Success");
                alertDialog.setMessage("Account deleted successfully");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();

                        sessionManager.clearSession();
                        sessionManager.remove(Constants.LANG);
                        startActivity(new Intent(SettingsActivity.this, Splash.class));
                        CategoriesHandler categoriesHandler = new CategoriesHandler(SettingsActivity.this);
                        categoriesHandler.deleteAllOrders();
                        categoriesHandler.deleteAllCategories();
                        categoriesHandler.deleteLabels();
                        categoriesHandler.close();
                        finish();

                    }
                });
            } else {
                alertDialog.setTitle("نجاح");
                alertDialog.setMessage("تم حذف الحساب بنجاح");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "أوكي", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
            }

            alertDialog.show();
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

}
