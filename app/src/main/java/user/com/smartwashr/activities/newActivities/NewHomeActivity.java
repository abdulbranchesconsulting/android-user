package user.com.smartwashr.activities.newActivities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.newAdapters.HomeAdapter;
import user.com.smartwashr.adapter.newAdapters.HomeModel;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.orders.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.SessionManager;

public class NewHomeActivity extends AppCompatActivity implements ResponseHandler {

    HomeAdapter adapter;
    RecyclerView recyclerView;
    ImageView iv_menu, iv_priceList;
    RelativeLayout btn_activeOrders;
    TextView tv_orders, title, tv_lable1;
    ArrayList<HomeModel> list = new ArrayList<>();
    SessionManager sessionManager;
    private PermissionManager permissionsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);

        sessionManager = new SessionManager(this);

        adapter = new HomeAdapter(this, list);
        recyclerView = findViewById(R.id.recyclerView);
        iv_menu = findViewById(R.id.iv_menu);
        iv_priceList = findViewById(R.id.iv_price);
        btn_activeOrders = findViewById(R.id.rl_btn);
        tv_orders = findViewById(R.id.tv_orders);
        title = findViewById(R.id.title);
        tv_lable1 = findViewById(R.id.tv_lable1);

        permissionsManager = PermissionManager.getInstance(NewHomeActivity.this);
        permissionsManager.getPermissionifNotAvailble(new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.ACCESS_FINE_LOCATION
                , Manifest.permission.ACCESS_COARSE_LOCATION}, 111);

        title.setText(Constants.TRANSLATIONS.getHomeLabel());
        tv_lable1.setText(Constants.TRANSLATIONS.getYourActiveOrderLabel());

        list.add(new HomeModel(Constants.TRANSLATIONS.getWashOrderLabel(), Constants.TRANSLATIONS.getWashOrderDescriptionLabel(), R.drawable.washrder));
        list.add(new HomeModel(Constants.TRANSLATIONS.getCarpetCurtainsLabel(), Constants.TRANSLATIONS.getCarpetCurtainsDescriptionLabel(), R.drawable.curtains));
        list.add(new HomeModel(Constants.TRANSLATIONS.getPremiumCareLabel(), Constants.TRANSLATIONS.getPremiumCareDescriptionLabel(), R.drawable.premium));
        list.add(new HomeModel(Constants.TRANSLATIONS.getMyPlacesLabel(), Constants.TRANSLATIONS.getMyPlacesDescriptionLabel(), R.drawable.places));

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);

        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewHomeActivity.this, NewSideActivity.class));
            }
        });

        iv_priceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewHomeActivity.this, NewPriceList.class));
            }
        });

        btn_activeOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewHomeActivity.this, NewOrdersActivity.class));
            }
        });

        getOrders("active");
    }

    private void getOrders(String status) {
        if (Internet.isAvailable(this)) {
            Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            new RestCaller(NewHomeActivity.this, SmartWashr.getRestClient().getOrders(SessionManager.get(Constants.ACCESS_TOKEN), status), 1);
        } else {
            Toast.makeText(this, Constants.TRANSLATIONS.getLabelPleaseCheckYourInternetConnection(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        ArrayList<OrderResponse> orderResponses = (ArrayList<OrderResponse>) response.body();
        if (orderResponses.size() > 0) {
            tv_orders.setText(orderResponses.size() + "");
        } else {
            tv_orders.setText("0");
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  Toast.makeText(ParentActivity.this, "Request Code => "+requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 111:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, "Please allow the location permission. Thanks", Toast.LENGTH_SHORT).show();
                    permissionsManager = PermissionManager.getInstance(NewHomeActivity.this);
                    permissionsManager.getPermissionifNotAvailble(new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE
                            , Manifest.permission.INTERNET
                            , Manifest.permission.ACCESS_FINE_LOCATION
                            , Manifest.permission.ACCESS_COARSE_LOCATION}, 111);
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewHomeActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

}
