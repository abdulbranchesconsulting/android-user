package user.com.smartwashr.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.payfort.start.Card;
import com.payfort.start.Start;
import com.payfort.start.Token;
import com.payfort.start.TokenCallback;
import com.payfort.start.error.CardVerificationException;
import com.payfort.start.error.StartApiException;

import java.util.EnumSet;

import user.com.smartwashr.R;
import user.com.smartwashr.models.ProductOrder;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

import static user.com.smartwashr.R.id.month;
import static user.com.smartwashr.R.id.year;

public class CreditCard extends AppCompatActivity implements TokenCallback {

    private ProductOrder productOrder;

    private static final String API_OPEN_KEY = "live_open_k_55e06cde7fe8d3141a7e";
    private static final String LOG_TAG = CreditCard.class.getSimpleName();

    private EditText name, num, cmonth, cyear, cvv;
    private TextView total_text;
    private Button payButton, cancel;
    String total = "";
    int i;
    Start start = new Start(API_OPEN_KEY);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);

        productOrder = (ProductOrder) getIntent().getSerializableExtra("products");
        String total = getIntent().getStringExtra("total");
        total = total.substring(1, total.length());
        i = Integer.parseInt(total);

        name = (EditText) findViewById(R.id.cname);
        num = (EditText) findViewById(R.id.cnum);
        cmonth = (EditText) findViewById(month);
        cyear = (EditText) findViewById(year);
        cvv = (EditText) findViewById(R.id.cvv);
        total_text = (TextView) findViewById(R.id.total_txt);

        payButton = (Button) findViewById(R.id.pay);
        cancel = (Button) findViewById(R.id.cancel);

        total_text.setText("SAR" + total);


    }

    private Card unbindCard() throws CardVerificationException {
        clearErrors();
        String number = unbindString(num);
        int year = Integer.parseInt(cyear.getText().toString());
        int month = Integer.parseInt(cmonth.getText().toString());
        String cvc = unbindString(cvv);
        String owner = unbindString(name);
        return new Card(number, cvc, month, year, owner);
    }

    private void clearErrors() {
        num.setError(null);
        cmonth.setError(null);
        cyear.setError(null);
        cvv.setError(null);
        name.setError(null);
    }

    private void setErrors(EnumSet<Card.Field> errors) {
        String error = getString(R.string.edit_text_invalid);

        if (errors.contains(Card.Field.NUMBER)) {
            num.setError(error);
        }
        if (errors.contains(Card.Field.EXPIRATION_YEAR)) {
            cyear.setError(error);
        }
        if (errors.contains(Card.Field.EXPIRATION_MONTH)) {
            cmonth.setError(error);
        }
        if (errors.contains(Card.Field.CVC)) {
            cvv.setError(error);
        }
        if (errors.contains(Card.Field.OWNER)) {
            name.setError(error);
        }
    }

    public void pay(View view) {
        try {
            Card card = unbindCard();

            hideKeyboard();
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(CreditCard.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            }else {
                Loading.show(CreditCard.this, false, "الرجاء الانتظار");
            }
            payButton.setEnabled(false);
            start.createToken(this, card, this, i, "USD");
        } catch (CardVerificationException e) {
            setErrors(e.getErrorFields());
        }
    }

    private String unbindString(EditText editText) {
        return editText.getText().toString().trim();
    }


    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public void onSuccess(Token token) {
        Log.d(LOG_TAG, "Token is received: " + token);
        Toast.makeText(this, getString(R.string.congrats, token.getId()), Toast.LENGTH_LONG).show();
        Loading.cancel();
        payButton.setEnabled(true);


    }

    @Override
    public void onError(StartApiException error) {
        Log.e(LOG_TAG, "Error getting token", error);
        Loading.cancel();
        payButton.setEnabled(true);
    }

    @Override
    public void onCancel() {
        Log.e(LOG_TAG, "Getting token is canceled by user");
        Loading.cancel();
        payButton.setEnabled(true);
    }
}
