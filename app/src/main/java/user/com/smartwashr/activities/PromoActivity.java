package user.com.smartwashr.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 10/21/17.
 */

public class PromoActivity extends AppCompatActivity {

    TextView title, text;
    Button submit;
    EditText promo_code;
    private ImageView back, bback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promo_layout);

        title = (TextView) findViewById(R.id.title);
        text = (TextView) findViewById(R.id.text);
        submit = (Button) findViewById(R.id.submit);

        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        promo_code = (EditText) findViewById(R.id.promo_code);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            FontUtils.setRobotoBold(title);
            FontUtils.setRobotoMediumFont(text);
            FontUtils.setRobotoBold(submit);
        } else {
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            title.setText("إضافة رمز ترويجي");
            promo_code.setHint("عندك رمز ترويجي ؟  ادخله هنا");
            submit.setText("تأكيد");
        }

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
