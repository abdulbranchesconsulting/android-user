package user.com.smartwashr.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.TermsCondition;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.Errors;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.loginresponse.LoginResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 5/31/17.
 */

public class SignUp extends AppCompatActivity implements ResponseHandler {

    private TextView sign_up, welcome, setup, conditions;
    private ImageView back_btn;
    private View parent;
    private AlertDialog alertDialog;
    private EditText name, email, phone, password, cpassword;
    private TextView terms;
    ImageView display_pic;
    private String profilePic;
    private MultipartBody.Part dp;
    private PermissionManager permissionsManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.sign_up_scrn);

        permissionsManager = PermissionManager.getInstance(SignUp.this);
        permissionsManager.getPermissionifNotAvailble(new String[]{Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.READ_EXTERNAL_STORAGE}, 111);

        parent = findViewById(R.id.parent);
        sign_up = (TextView) findViewById(R.id.sign_up);
        setup = (TextView) findViewById(R.id.setup);
        welcome = (TextView) findViewById(R.id.welcome);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        display_pic = findViewById(R.id.display_pic);
        conditions = findViewById(R.id.conditions);

        name = (EditText) findViewById(R.id.name);
//        name.setText("Zeeshan");
        email = (EditText) findViewById(R.id.email);
//        email.setText("gamithonfantasyworld@gmail.com");
        phone = (EditText) findViewById(R.id.phone);
//        phone.setText("963214148671");
        password = (EditText) findViewById(R.id.password);
        cpassword = (EditText) findViewById(R.id.confirm_pass);
//        password.setText("Zee1234#");
//        cpassword.setText("Zee1234#");
        terms = (TextView) findViewById(R.id.terms);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setRobotoRegular(name);
            FontUtils.setRobotoRegular(email);
            FontUtils.setRobotoRegular(phone);
            FontUtils.setRobotoRegular(password);
            FontUtils.setRobotoRegular(cpassword);
            FontUtils.setMABold(sign_up);
            FontUtils.setRobotoMediumFont(setup);
            FontUtils.setMABold(welcome);
            FontUtils.setRobotoMediumFont(terms);
            FontUtils.setRobotoMediumFont(conditions);
        } else {
            FontUtils.setRobotoRegular(name);
            FontUtils.setRobotoRegular(email);
            FontUtils.setRobotoRegular(password);
            FontUtils.setRobotoRegular(cpassword);
            FontUtils.setArabic(sign_up);
            FontUtils.setArabic(terms);
            FontUtils.setArabic(welcome);
            FontUtils.setArabic(setup);
            conditions.setVisibility(View.GONE);
            name.setHint(getString(R.string.name_ar));
            email.setHint(getString(R.string.email_ar));
            password.setHint(getString(R.string.pass_ar));
            cpassword.setHint(getString(R.string.cpass_ar));
            sign_up.setText(getString(R.string.signup_ar));
            terms.setText("* تسجيلك يعتبر كموافقة على الشروط والأحكام ");
            welcome.setText("أهلا بكم في سمارت ووشر");
            setup.setText("الرجاء إنشاء حسابك");
        }

        RippleEffect.applyRippleEffect(name, "#bdbdbd");
        RippleEffect.applyRippleEffect(back_btn, "#bdbdbd");
        RippleEffect.applyRippleEffect(cpassword, "#bdbdbd");
        RippleEffect.applyRippleEffect(phone, "#bdbdbd");
        RippleEffect.applyRippleEffect(email, "#bdbdbd");
        RippleEffect.applyRippleEffect(password, "#bdbdbd");
        RippleEffect.applyRippleEffect(sign_up, "#bdbdbd");

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().length() != 0 && email.getText().toString().length() != 0 && phone.getText().toString().length() != 0 && password.getText().toString().length() != 0 && cpassword.getText().toString().length() != 0) {
                    if (phone.getText().length() == 12) {
                        if (isValidEmail(email.getText().toString())) {
                            if (password.getText().toString().length() >= 6) {
                                if (!password.getText().toString().equalsIgnoreCase(cpassword.getText().toString())) {
                                    Snackbar.make(parent, "Password not match", Snackbar.LENGTH_LONG).show();
                                } else {
                                    makeRestCall();
                                }
                            } else {
                                alertDialog = new AlertDialog.Builder(
                                        SignUp.this).create();
                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {

                                    alertDialog.setTitle("Password requirements");
                                    alertDialog.setMessage("* At least 6 characters");
                                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            alertDialog.dismiss();
                                        }
                                    });

                                } else {
                                    alertDialog.setTitle("شروط كلمة المرور");
                                    alertDialog.setMessage("* 6 خانات على الأقل");
                                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            alertDialog.dismiss();
                                        }
                                    });

                                }
                                alertDialog.show();
                            }

                        } else {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Snackbar.make(parent, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
                            } else {
                                Snackbar.make(parent, "الإيجار إدخال عنوان بريد إلكتروني صالح", Snackbar.LENGTH_LONG).show();
                            }
                        }
                    } else {

                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            Snackbar.make(parent, "Please enter a valid phone number", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(parent, "يرجى إدخال رقم هاتف صالح", Snackbar.LENGTH_LONG).show();
                        }
                    }

                } else {

                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Snackbar.make(parent, "Please fill all fields", Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(parent, "لو سمحت أملأ كل الحقول", Snackbar.LENGTH_LONG).show();
                    }
                }

            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUp.this, TermsCondition.class));
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        display_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePopup(v, 0);
            }
        });

    }

    public void showImagePopup(View view, int i) {
        if (ActivityCompat.checkSelfPermission(SignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(SignUp.this, "Please grant storage permission", Toast.LENGTH_SHORT).show();
            return;
        } else {
            // File System.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_PICK);
            // Chooser of file system options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose image");
            switch (i) {
                case 0:
                    startActivityForResult(chooserIntent, 0);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(SignUp.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                return;
            }
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = SignUp.this.getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                switch (requestCode) {
                    case 0:
                        profilePic = cursor.getString(columnIndex);
                        display_pic.setBackgroundResource(android.R.color.transparent);
                        display_pic.setBackgroundDrawable(null);
                        Picasso.with(SignUp.this).load(new File(profilePic))
                                .into(display_pic);
                        break;
                }
                cursor.close();
            } else {
                Toast.makeText(SignUp.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(SignUp.this, "CANCELLED", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  Toast.makeText(ParentActivity.this, "Request Code => "+requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 111:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }

                break;
            default:
                break;
        }
    }


    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void makeRestCall() {
        if (Internet.isAvailable(SignUp.this)) {
            if (profilePic != null) {
                File file = new File(profilePic);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                dp =
                        MultipartBody.Part.createFormData("pic", file.getName(), requestFile);
                new RestCaller(SignUp.this, SmartWashr.getRestClient().register("application/json",
                        name.getText().toString(),
                        email.getText().toString(),
                        password.getText().toString(),
                        phone.getText().toString(),
                        SessionManager.get(Constants.DEVICE_TOKEN),
                        dp), 1);
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    Loading.show(SignUp.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                } else {
                    Loading.show(SignUp.this, false, "الرجاء الانتظار");
                }
            } else {
                new RestCaller(SignUp.this, SmartWashr.getRestClient().register("application/json",
                        name.getText().toString(),
                        email.getText().toString(),
                        password.getText().toString(),
                        phone.getText().toString(), SessionManager.get(Constants.DEVICE_TOKEN)), 1);
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    Loading.show(SignUp.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                } else {
                    Loading.show(SignUp.this, false, "الرجاء الانتظار");
                }
            }
        } else {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Snackbar.make(parent, "Check your Internet Connection", Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(parent, "تأكد من اتصالك بالانترنت", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        LoginResponse loginResponse = (LoginResponse) response.body();
        Log.d("response", new Gson().toJson(loginResponse));
        SessionManager.put(Constants.ACCESS_TOKEN, loginResponse.getToken());
        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getActiveorders());
        SessionManager.put(Constants.USER_ID, loginResponse.getUser().getId().toString());
        SessionManager.put(Constants.USER_NAME, loginResponse.getUser().getName());
        SessionManager.put(Constants.EMAIL, loginResponse.getUser().getEmail());
        SessionManager.put(Constants.USER_IMG, loginResponse.getUser().getProfilePic());
        SessionManager.put(Constants.USER_PHONE, loginResponse.getUser().getPhoneNumber());
        Loading.cancel();

        Intent i = new Intent(SignUp.this, HowToActivity.class);
        startActivity(i);
        SignUp.this.finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SignUp.this.finish();
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        if (error.getErrors().size() > 0) {
            for (int i = 0; i <= error.getErrors().size(); i++) {
                Errors errors = error.getErrors().get(i);
                if (errors.getField().equalsIgnoreCase("email")) {
                    alertDialog = new AlertDialog.Builder(
                            SignUp.this).create();
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("Email already exist");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });
                    } else {

                        alertDialog.setTitle("خطأ");
                        alertDialog.setMessage("البريد الالكتروني موجود بالفعل");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "أوكي", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });
                    }

                    alertDialog.show();
                }
                break;
            }
        }
        Toast.makeText(this, "" + error.getErrors(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUp.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
