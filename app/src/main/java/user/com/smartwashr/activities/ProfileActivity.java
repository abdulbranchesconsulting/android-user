package user.com.smartwashr.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.login.LoginResult;
import user.com.smartwashr.newModels.login.User;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.SessionManager;

public class
ProfileActivity extends AppCompatActivity implements ResponseHandler {

    private EditText name, phone, email;
    private CircleImageView imageView;
    private Button update;
    private SessionManager sessionManager;
    private PermissionManager permissionsManager;
    private String profilePic;
    private MultipartBody.Part dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sessionManager = new SessionManager(ProfileActivity.this);

        permissionsManager = PermissionManager.getInstance(ProfileActivity.this);
        permissionsManager.getPermissionifNotAvailble(new String[]{Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.READ_EXTERNAL_STORAGE}, 111);

        name = (EditText) findViewById(R.id.name);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        update = (Button) findViewById(R.id.update);

        email.setEnabled(false);

        TextView title = (TextView) findViewById(R.id.title);
        title.setText(Constants.TRANSLATIONS.getProfileLabel());
        name.setHint(Constants.TRANSLATIONS.getNameLabel());
        phone.setHint(Constants.TRANSLATIONS.getPhoneLabel());
        update.setText(Constants.TRANSLATIONS.getSaveLabel());
        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        imageView = findViewById(R.id.profile_image);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePopup(v, 0);
            }
        });

        if (sessionManager.get(Constants.USER_IMG).length() > 0) {
            Picasso.with(ProfileActivity.this).load(sessionManager.get(Constants.USER_IMG)).placeholder(R.drawable.ic_pic)
                    .into(imageView);
        } else {
            Picasso.with(ProfileActivity.this).load(R.drawable.ic_pic).placeholder(R.drawable.ic_pic)
                    .into(imageView);

        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Internet.isAvailable(ProfileActivity.this)) {
                    if (profilePic != null) {
                        File file = new File(profilePic);
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        dp =
                                MultipartBody.Part.createFormData("profile_pic", file.getName(), requestFile);
                        new RestCaller(ProfileActivity.this, SmartWashr.getRestClient().updateUser(sessionManager.get(Constants.ACCESS_TOKEN),
                                "application/json",
                                name.getText().toString(),
                                phone.getText().toString(),
                                dp), 1);
                        Loading.show(ProfileActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        new RestCaller(ProfileActivity.this, SmartWashr.getRestClient().updateUserWithoutPic(sessionManager.get(Constants.ACCESS_TOKEN),
                                "application/json",
                                name.getText().toString(),
                                phone.getText().toString()
                        ), 2);
                        Loading.show(ProfileActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        name.setText(sessionManager.get(Constants.USER_NAME));
        phone.setText(sessionManager.get(Constants.USER_PHONE));
        email.setText(sessionManager.get(Constants.EMAIL));

    }

    public void showImagePopup(View view, int i) {
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(ProfileActivity.this, "Please grant storage permission", Toast.LENGTH_SHORT).show();
            return;
        } else {
            // File System.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_PICK);
            // Chooser of file system options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose Image");
            switch (i) {
                case 0:
                    startActivityForResult(chooserIntent, 0);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(ProfileActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                return;
            }
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = ProfileActivity.this.getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                switch (requestCode) {
                    case 0:
                        profilePic =  cursor.getString(columnIndex);
                        imageView.setBackgroundDrawable(null);
                        Picasso.with(ProfileActivity.this).load(new File(profilePic)).placeholder(R.drawable.ic_pic)
                                .into(imageView);
                        break;
                }
                cursor.close();
            } else {
                Toast.makeText(ProfileActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  Toast.makeText(ParentActivity.this, "Request Code => "+requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 111:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 2) {
            LoginResult updateResponse = (LoginResult) response.body();
            User user = (User) updateResponse.getUser();
            SessionManager.put(Constants.USER_NAME, user.getFirstName() + user.getLastName());
            SessionManager.put(Constants.USER_PHONE, user.getPhone());
            Toast.makeText(this, updateResponse.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            LoginResult updateResponse = (LoginResult) response.body();
            User user = (User) updateResponse.getUser();
            String username = user.getFirstName() + user.getLastName();
            SessionManager.put(Constants.USER_NAME, username);
            String s = user.getPhone();
            s = s.substring(1, s.length() - 1);
            SessionManager.put(Constants.USER_PHONE, s);
            SessionManager.put(Constants.USER_IMG, user.getProfilePic());
            Toast.makeText(this, updateResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();
        }
    }

}
