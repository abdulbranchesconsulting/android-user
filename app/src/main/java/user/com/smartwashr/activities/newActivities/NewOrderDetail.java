package user.com.smartwashr.activities.newActivities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.newAdapters.OrderItemsAdapter;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.OrderProduct;
import user.com.smartwashr.newModels.orderPlacement.OrderPlacementResponse;
import user.com.smartwashr.newModels.orders.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewOrderDetail extends AppCompatActivity implements ResponseHandler {

    private RelativeLayout top;
    private ImageView ivBack;
    private ImageView btnStatus;
    private TextView tvOrderNum;
    private TextView tvPickup;
    private TextView tvDropoff;
    private TextView tvAddress;
    private RecyclerView itemRecycler;
    private TextView tvTotal;
    private ImageView img;
    private TextView tvCall, txt_items, txt_qty, txt_cost, txt_total, tv_status;
    private TextView tvCancel, txt_order, title, txt_pick, txt_drop, txt_delivered_to;
    private LinearLayout price_con;

    private SessionManager sessionManager;
    private OrderResponse orderResponse;

    ArrayList<OrderProduct> list = new ArrayList<>();
    OrderItemsAdapter adapter;
    private LinearLayout ll_items;

    private void findViews() {
        top = (RelativeLayout) findViewById(R.id.top);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        btnStatus = (ImageView) findViewById(R.id.btn_status);
        tvOrderNum = (TextView) findViewById(R.id.tv_order_num);
        tvPickup = (TextView) findViewById(R.id.tv_pickup);
        tvDropoff = (TextView) findViewById(R.id.tv_dropoff);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        itemRecycler = (RecyclerView) findViewById(R.id.item_recycler);
        tvTotal = (TextView) findViewById(R.id.tv_total);
        img = (ImageView) findViewById(R.id.img);
        tvCall = (TextView) findViewById(R.id.tv_call);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        price_con = findViewById(R.id.price_con);
        title = findViewById(R.id.title);
        txt_order = findViewById(R.id.txt_order);
        txt_pick = findViewById(R.id.txt_pick);
        txt_drop = findViewById(R.id.txt_drop);
        txt_items = findViewById(R.id.txt_items);
        txt_qty = findViewById(R.id.txt_qty);
        txt_cost = findViewById(R.id.txt_cost);
        txt_total = findViewById(R.id.txt_total);
        txt_delivered_to = findViewById(R.id.txt_delivered_to);
        tv_status = findViewById(R.id.tv_status);
        ll_items = findViewById(R.id.ll_items);

        title.setText(Constants.TRANSLATIONS.getOrderStatusLabel());
        txt_pick.setText(Constants.TRANSLATIONS.getPickupDateLabel());
        txt_drop.setText(Constants.TRANSLATIONS.getDropoffDateLabel());
        tvCall.setText(Constants.TRANSLATIONS.getCallUsLabel());
        tvCancel.setText(Constants.TRANSLATIONS.getCancelOrderLabel());
        txt_order.setText(Constants.TRANSLATIONS.getOrderNumberLabel());
        txt_delivered_to.setText(Constants.TRANSLATIONS.getDeliveredToLabel());
        txt_cost.setText(Constants.TRANSLATIONS.getCostLabel());
        txt_qty.setText(Constants.TRANSLATIONS.getQuantityLabel());
        txt_items.setText(Constants.TRANSLATIONS.getItemsLabel());
        txt_total.setText(Constants.TRANSLATIONS.getTotalLabel());

        itemRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_details);

        sessionManager = new SessionManager(this);
        orderResponse = Constants.orderResponse;

        findViews();

        tvOrderNum.setText(orderResponse.getInvoiceNum());
        tvPickup.setText(orderResponse.getPickupTime());
        tvDropoff.setText(orderResponse.getDeliveryTime());
        tvAddress.setText(orderResponse.getAddress());

        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
            tvTotal.setText(Constants.TRANSLATIONS.getCurrency() + " " + orderResponse.getTotal());
        } else {
            tvTotal.setText(orderResponse.getTotal() + " " + Constants.TRANSLATIONS.getCurrency());
        }


        if (orderResponse.getOrderstatusId() == 1 || orderResponse.getOrderstatusId() == 2) {
            btnStatus.setImageResource(R.drawable.pick_green_bg);
            tv_status.setText(Constants.TRANSLATIONS.getReadyForPickupLabel());
        } else if (orderResponse.getOrderstatusId() == 4) {
            btnStatus.setImageResource(R.drawable.washed_green_bg);
            tv_status.setText(Constants.TRANSLATIONS.getGettingWashedLabel());
        }
        if (orderResponse.getOrderstatusId() == 8) {
            btnStatus.setImageResource(R.drawable.completed_gold_bg);
            tv_status.setText(Constants.TRANSLATIONS.getCompletedLabel());
            tvCancel.setVisibility(View.GONE);
        }


        if (orderResponse.getDetail().size() > 0) {
            for (int i = 0; i < orderResponse.getDetail().size(); i++) {
                if (orderResponse.getDetail().get(i).getProduct() != null)
                    list.add(new OrderProduct(orderResponse.getDetail().get(i).getProductId() + "",
                            orderResponse.getDetail().get(i).getProduct().getName(),
                            orderResponse.getDetail().get(i).getQty() + "",
                            orderResponse.getDetail().get(i).getServiceId() + "",
                            orderResponse.getDetail().get(i).getPrice() + ""));

            }
        } else {
            ll_items.setVisibility(View.GONE);
            itemRecycler.setVisibility(View.GONE);
            txt_total.setVisibility(View.GONE);
            tvTotal.setVisibility(View.GONE);
        }

        adapter = new OrderItemsAdapter(this, list);
        itemRecycler.setAdapter(adapter);

        tvCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+966547771807"));
                if (android.support.v4.app.ActivityCompat.checkSelfPermission(NewOrderDetail.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                startActivity(callIntent);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading();
                new RestCaller(NewOrderDetail.this, SmartWashr.getRestClient().cancelOrder(sessionManager.get(Constants.ACCESS_TOKEN),
                        "application/json",
                        orderResponse.getId() + "",
                        "7"),
                        1);
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void loading() {
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
    }

    public static String getFormattedTime(String time) {
        String displayValue = time;
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormatter.parse(time);

// Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("EEE dd MMM, yyyy");
            displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayValue;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        OrderPlacementResponse orderPlacementResponse = (OrderPlacementResponse) response.body();
        Toast.makeText(this, orderPlacementResponse.getMessage(), Toast.LENGTH_SHORT).show();
        Intent i = new Intent(NewOrderDetail.this, NewHomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewOrderDetail.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
