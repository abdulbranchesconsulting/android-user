package user.com.smartwashr.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.fragments.GResponse;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

public class ComplaintActivity extends AppCompatActivity implements ResponseHandler {

    EditText description;
    ImageView img;
    TextView cancel, send, title, txt;
    private PermissionManager permissionsManager;

    ArrayList<String> files; //These are the uris for the files to be uploaded
    MediaType mediaType = MediaType.parse("");
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint);

        sessionManager = new SessionManager(this);

        permissionsManager = PermissionManager.getInstance(this);
        permissionsManager.getPermissionifNotAvailble(new String[]{Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.READ_EXTERNAL_STORAGE}, 111);

        cancel = (TextView) findViewById(R.id.cancel);
        title = (TextView) findViewById(R.id.title);
        send = (TextView) findViewById(R.id.send);
        txt = (TextView) findViewById(R.id.text);

        files = new ArrayList<>();

        description = (EditText) findViewById(R.id.description);

        img = (ImageView) findViewById(R.id.img);

        RippleEffect.applyRippleEffect(img, "#bdbdbd");
        RippleEffect.applyRippleEffect(send, "#bdbdbd");
        RippleEffect.applyRippleEffect(cancel, "#bdbdbd");

        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setRobotoMediumFont(cancel);
            FontUtils.setRobotoMediumFont(send);
            FontUtils.setRobotoBold(title);
            FontUtils.setRobotoMediumFont(description);
            FontUtils.setRobotoMediumFont(txt);
        } else {
            title.setText("شكوى");
            cancel.setText("إلغاء");
            send.setText("إرسال");
            description.setHint("اكتب ايش المشكلة اللي واجهته");
            txt.setText("فضلاً ارفق صوره مع الشكوى");
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Internet.isAvailable(ComplaintActivity.this)) {
//                    if (files.size() > 0) {
                    final MultipartBody.Part[] fileParts = new MultipartBody.Part[1];
//                        for (int i = 0; i < files.size(); i++) {
//                            File file = new File(files.get(i));

                    RequestBody fileBody = RequestBody.create(mediaType, complaint_img);
                    //Setting the file name as an empty string here causes the same issue, which is sending the request successfully without saving the files in the backend, so don't neglect the file name parameter.
                    fileParts[0] = MultipartBody.Part.createFormData(String.format(Locale.ENGLISH, "complain_pic[%d]", 0), complaint_img.getName(), fileBody);

//                 final RequestBody _accessKey = RequestBody.create(MediaType.parse("text/plain"), accessKey);
//                 final RequestBody _accessKey = RequestBody.create(MediaType.parse("text/plain"), accessKey);
//                 final RequestBody _accessKey = RequestBody.create(MediaType.parse("text/plain"), accessKey);
                    new RestCaller(ComplaintActivity.this, SmartWashr.getRestClient().complaint("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), getIntent().getStringExtra("orderId"), description.getText().toString(), fileParts), 1);

                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(ComplaintActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(ComplaintActivity.this, false, "الرجاء الانتظار");
                    }
                }
//                }
            }
        });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage(101);
            }
        });

    }

    void selectImage(final int code) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            builder.setMessage("Select Image");
            builder.setPositiveButton("Gallery",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            EasyImage.openGallery(ComplaintActivity.this, code);
                            dialog.cancel();
                        }
                    });

            builder.setNeutralButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

            builder.setNegativeButton("Camera",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            EasyImage.openCamera(ComplaintActivity.this, code);
                            dialog.cancel();
                        }
                    });

        }else {
            builder.setMessage("اختر صورة");
            builder.setPositiveButton("الصور",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            EasyImage.openGallery(ComplaintActivity.this, code);
                            dialog.cancel();
                        }
                    });

            builder.setNeutralButton("إلغاء",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

            builder.setNegativeButton("خذ صورة",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            EasyImage.openCamera(ComplaintActivity.this, code);
                            dialog.cancel();
                        }
                    });

        }
        builder.create().show();

    }


    File complaint_img = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                if (type == 101) {
                    complaint_img = new File(imageFile.getPath());
                    Picasso.with(ComplaintActivity.this).load(complaint_img).fit().into(img);
                }
            }

        });
    }


    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();

        GResponse generic = (GResponse) response.body();
        if (generic.isSuccess()) {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                Toast.makeText(this, "Your complaint has been raised successfully.", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, getResources().getString(R.string.complaint_msg), Toast.LENGTH_SHORT).show();
            finish();

        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ComplaintActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

}
