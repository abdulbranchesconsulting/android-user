package user.com.smartwashr.activities.newActivities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.Login;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.loginFB.ErrorResponse;
import user.com.smartwashr.newModels.loginFB.NewLoginResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewSignUp extends AppCompatActivity implements ResponseHandler {
    private ImageView ivBack;
    private CircleImageView ivImg;
    private EditText name;
    private EditText email;
    private EditText ephone;
    private EditText epass;
    private EditText econfirm;
    private Button btnRegister;
    private TextView terms;
    private SessionManager sessionManager;

    private Context context;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String actualPath;
    private MultipartBody.Part dp;
    private TextView titletext, profilesetup, passwordCond;

    private void findViews() {
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivImg = (CircleImageView) findViewById(R.id.iv_img);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        ephone = (EditText) findViewById(R.id.ephone);
        epass = (EditText) findViewById(R.id.epass);
        econfirm = (EditText) findViewById(R.id.econfirm);
        btnRegister = (Button) findViewById(R.id.btn_register);
        terms = (TextView) findViewById(R.id.terms);
        titletext = findViewById(R.id.titletext);
        profilesetup = findViewById(R.id.profilesetup);
        passwordCond = findViewById(R.id.passwordCond);
//        name.setText("Zeeshan Kahn");
//        email.setText("zeeshan@gmail.com");
//        ephone.setText("12235423");
//        epass.setText("123456");
//        econfirm.setText("123456");
        titletext.setText(Constants.TRANSLATIONS.getWelcomeSmartwashrLabel());
        name.setHint(Constants.TRANSLATIONS.getNameLabel());
        email.setHint(Constants.TRANSLATIONS.getEmailLabel());
        ephone.setHint(Constants.TRANSLATIONS.getPhoneLabel());
        epass.setHint(Constants.TRANSLATIONS.getPasswordLabel());
        econfirm.setHint(Constants.TRANSLATIONS.getConfirmPasswordLabel());
        btnRegister.setText(Constants.TRANSLATIONS.getLetGetWashingLabel());
        terms.setText(Constants.TRANSLATIONS.getTermsAndConditionsLabel());
        profilesetup.setText(Constants.TRANSLATIONS.getSetupProfileInstructions());
        passwordCond.setText(Constants.TRANSLATIONS.getPasswordInstructions());


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sign_up);

        sessionManager = new SessionManager(this);
        context = this;

        findViews();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().length() > 0 && name.getText().toString().length() > 0 && epass.getText().toString().length() > 0 && ephone.getText().toString().length() > 0 && econfirm.getText().toString().length() > 0) {
                    if (name.getText().toString().contains(" ")) {
                        if (email.getText().toString().contains("@") && email.getText().toString().contains(".")) {
                            if (epass.getText().toString().length() >= 6) {
                                signup();
                            } else {
                                epass.setError(Constants.TRANSLATIONS.getPasswordInstructions());
                            }
                        } else {
                            email.setError(Constants.TRANSLATIONS.getLabelPleaseEnterAValidEmail());
                        }
                    } else {
                        name.setError(Constants.TRANSLATIONS.getLabelPleaseEnterYourFullName());
                    }
                } else {
                    Toast.makeText(NewSignUp.this, Constants.TRANSLATIONS.getLabelPleaseFillAllFields(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ivImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

    }


    private void signup() {
        if (Internet.isAvailable(NewSignUp.this)) {
            Loading.show(NewSignUp.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());

            if (actualPath != null) {
                File file = new File(actualPath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                dp =
                        MultipartBody.Part.createFormData("profile_pic", file.getName(), requestFile);
                new RestCaller(NewSignUp.this, SmartWashr.getRestClient().newRegister(name.getText().toString(), email.getText().toString(), ephone.getText().toString(), epass.getText().toString(), epass.getText().toString(), SessionManager.get(Constants.DEVICE_TOKEN), dp), 1);
            } else {
                new RestCaller(NewSignUp.this, SmartWashr.getRestClient().newRegister(name.getText().toString(), email.getText().toString(), ephone.getText().toString(), epass.getText().toString(), epass.getText().toString(), SessionManager.get(Constants.DEVICE_TOKEN)), 1);
            }
        } else {
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();

        NewLoginResponse loginResponse = (NewLoginResponse) response.body();
        Log.d("response", new Gson().toJson(loginResponse));
        sessionManager.createLoginSession();
        SessionManager.put(Constants.ACCESS_TOKEN, "Bearer " + loginResponse.getData().getToken());
//        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getData().getUser().);
        SessionManager.put(Constants.USER_ID, loginResponse.getData().getUser().getId().toString());
        SessionManager.put(Constants.USER_NAME, loginResponse.getData().getUser().getFirstName() + " " + loginResponse.getData().getUser().getLastName());
        SessionManager.put(Constants.EMAIL, loginResponse.getData().getUser().getEmail());
        SessionManager.put(Constants.USER_IMG, loginResponse.getData().getUser().getProfilePic());
        SessionManager.put(Constants.USER_PHONE, loginResponse.getData().getUser().getPhone());
        Loading.cancel();
        Intent i = new Intent(NewSignUp.this, NewHomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        NewSignUp.this.finish();

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        ErrorResponse errorResponse = (ErrorResponse) error.getError();
        Toast.makeText(context, errorResponse.getMessage().getEmail().get(0), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
                Uri SelectedImage = data.getData();


            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
                Uri SelectedImage1 = data.getData();

            }

        }
    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        ivImg.setImageBitmap(thumbnail);
        try {
            actualPath = saveImage(thumbnail, 100);
            //list.set(currentPosition, actualPath);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ivImg.setImageBitmap(bm);
        try {
            actualPath = saveImage(bm, 100);
            //list.set(currentPosition, actualPath);
            Log.d("PIC", "" + actualPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void selectImage() {
        final String[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //  builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                //Utility.test(MainActivity.this);
                //boolean result=true
                boolean result = Constants.checkPermission(context);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public String saveImage(Bitmap bitmap, int quality) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        String imagePath = Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name) + File.separator + "smartwashr.jpg";
        File f = new File(imagePath);
        f.createNewFile();
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagePath;
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(NewSignUp.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

}
