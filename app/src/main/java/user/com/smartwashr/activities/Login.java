package user.com.smartwashr.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.loginresponse.LoginResponse;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 5/31/17.
 */

public class Login extends AppCompatActivity implements ResponseHandler {

    private TextView sign_in;
    private ImageView back_btn;
    private TextView forgot_password, resend_c_email;
    private SessionManager sessionManager;
    private EditText email, password;
    private View parent;
    private AlertDialog alertDialog;
    private String lang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.baaba_g);

        sessionManager = new SessionManager(Login.this);
        parent = findViewById(R.id.parent);
        sign_in = (TextView) findViewById(R.id.sign_in);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        resend_c_email = (TextView) findViewById(R.id.reset_link);
        email = (EditText) findViewById(R.id.email);
        email.setText("zeeshansardar71@gmail.com");
        password = (EditText) findViewById(R.id.password);
        password.setText("123456");

        TextView title = (TextView) findViewById(R.id.title);


        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setMARegular(title);
            FontUtils.setRobotoRegular(email);
            FontUtils.setRobotoRegular(password);
            FontUtils.setRobotoMediumFont(resend_c_email);
            FontUtils.setRobotoMediumFont(forgot_password);
            FontUtils.setMABold(sign_in);
        } else {
            FontUtils.setMARegular(title);
            FontUtils.setRobotoRegular(email);
            FontUtils.setRobotoRegular(password);
            FontUtils.setArabic(forgot_password);
            FontUtils.setArabic(resend_c_email);
            FontUtils.setArabic(sign_in);
            email.setHint(getString(R.string.email_ar));
            password.setHint(getString(R.string.pass_ar));
            forgot_password.setText(getString(R.string.forgotpass_ar));
            resend_c_email.setText(getString(R.string.resend_ar));
            sign_in.setText(getString(R.string.signin_ar));
        }

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().length() != 0 && password.getText().length() != 0) {
                    if (isValidEmail(email.getText().toString())) {
                        if (Internet.isAvailable(Login.this)) {
                            new RestCaller(Login.this, SmartWashr.getRestClient().login(email.getText().toString(), password.getText().toString(), sessionManager.get(Constants.DEVICE_TOKEN)), 1);
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Loading.show(Login.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                            } else {
                                Loading.show(Login.this, false, "الرجاء الانتظار");
                            }
                        } else {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Snackbar.make(parent, "Check your Internet Connection", Snackbar.LENGTH_LONG).show();
                            } else {
                                Snackbar.make(parent, "تأكد من اتصالك بالانترنت", Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }
                } else {
                    Snackbar.make(parent, "Please fill all fields", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, ForgotPassword.class);
                startActivity(i);
            }
        });

        resend_c_email.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, ForgotPassword.class);
                i.putExtra("resend", "resend");
                startActivity(i);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RippleEffect.applyRippleEffect(sign_in, "#bdbdbd");
        RippleEffect.applyRippleEffect(back_btn, "#bdbdbd");
        RippleEffect.applyRippleEffect(forgot_password, "#bdbdbd");
        RippleEffect.applyRippleEffect(email, "#bdbdbd");
        RippleEffect.applyRippleEffect(password, "#bdbdbd");

    }

    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        LoginResponse loginResponse = (LoginResponse) response.body();
        Log.d("response", new Gson().toJson(loginResponse));
        sessionManager.putAccessToken(loginResponse.getToken());
        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getActiveorders());
        SessionManager.put(Constants.USER_ID, loginResponse.getUser().getId().toString());
        SessionManager.put(Constants.USER_NAME, loginResponse.getUser().getName());
        SessionManager.put(Constants.EMAIL, loginResponse.getUser().getEmail());
        SessionManager.put(Constants.USER_IMG, loginResponse.getUser().getProfilePic());
        SessionManager.put(Constants.USER_PHONE, loginResponse.getUser().getPhoneNumber());
        Loading.cancel();
        Intent i = new Intent(Login.this, HomeActivity.class);
        startActivity(i);
        Login.this.finish();
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
//        Log.d("Response", error.getError().getMessage());
        alertDialog = new AlertDialog.Builder(
                Login.this).create();
        alertDialog.setTitle("Error");
//        alertDialog.setMessage(error.getError().getMessage());
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        Loading.cancel();
    }

    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Login.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }


    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
        Snackbar.make(parent, "Api Crashed", Snackbar.LENGTH_LONG).show();
    }
}
