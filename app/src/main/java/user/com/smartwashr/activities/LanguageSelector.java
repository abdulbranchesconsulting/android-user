package user.com.smartwashr.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.newActivities.NewLogReg;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.settings.SettingsResponse;
import user.com.smartwashr.models.settings.Translations;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 9/5/17.
 */

public class LanguageSelector extends AppCompatActivity implements ResponseHandler {

    ImageView arabic, english;
    TextView title;
    AlertDialog.Builder alertDialogBuilder;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lang_selector);

        sessionManager = new SessionManager(this);

        title = (TextView) findViewById(R.id.title);
        arabic = (ImageView) findViewById(R.id.arabic);
        english = (ImageView) findViewById(R.id.english);

        alertDialogBuilder = new AlertDialog.Builder(this);
        FontUtils.setMABold(title);

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApi("ar");
            }
        });

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApi("en");
            }
        });


    }


    private void callApi(String lang) {
        Loading.show(this, false, "Please wait...");
        SessionManager.put(Constants.LANG, lang);
        new RestCaller(LanguageSelector.this, SmartWashr.getRestClient().fetchSettings(lang), 1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        SettingsResponse settingsResponse = (SettingsResponse) response.body();
        Translations translations = settingsResponse.getTranslations();
        Constants.TRANSLATIONS = translations;
        Gson gson = new Gson();
        String json = gson.toJson(settingsResponse);
        sessionManager.put(Constants.TRANS, json);

        sessionManager.put(Constants.SORT_FEE, settingsResponse.getSortingFee()+"");
        sessionManager.put(Constants.DELIVERY_CHARGES, settingsResponse.getDeliveryCharges()+"");

        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
            Intent i = new Intent(LanguageSelector.this, NewLogReg.class);
            startActivity(i);
        } else {
            Intent i = new Intent(LanguageSelector.this, Splash.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(LanguageSelector.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
