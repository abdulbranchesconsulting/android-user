package user.com.smartwashr.activities.newActivities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.PlaceAdapter;
import user.com.smartwashr.adapter.PlaceVH;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.address.AddressResponse;
import user.com.smartwashr.newModels.address.Datum;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.RecyclerItemTouchHelper;
import user.com.smartwashr.utils.SessionManager;

public class NewPlacesActivity extends AppCompatActivity implements ResponseHandler, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    ImageView back;
    RecyclerView recyclerView;
    RelativeLayout rl_new_add;
    PlaceAdapter adapter;
    ArrayList<Datum> list = new ArrayList<>();
    SessionManager sessionManager;
    TextView no_data;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_place_list);

        sessionManager = new SessionManager(this);
        alertDialogBuilder = new AlertDialog.Builder(this);

        back = findViewById(R.id.back);
        recyclerView = findViewById(R.id.recyclerView);
        rl_new_add = findViewById(R.id.rl_add_new);
        no_data = findViewById(R.id.no_data);
        TextView title = (TextView) findViewById(R.id.title);
        TextView tv_add = findViewById(R.id.tv_add_address);
        tv_add.setText(Constants.TRANSLATIONS.getAddNewAddressLabel());
        title.setText(Constants.TRANSLATIONS.getMyPlacesLabel());
        no_data.setText(Constants.TRANSLATIONS.getNoAddressFound());
        adapter = new PlaceAdapter(this, list);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        rl_new_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewPlacesActivity.this, AddNewAddress.class));
            }
        });

        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("en")) {
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        } else {
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.RIGHT, this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        }

    }

    private void getAddress() {
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
        new RestCaller(NewPlacesActivity.this, SmartWashr.getRestClient().getAddresses(sessionManager.get(Constants.ACCESS_TOKEN)), 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAddress();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 1) {
            list.clear();
            AddressResponse addressResponse = (AddressResponse) response.body();
            if (addressResponse.getData().size() > 0) {
                no_data.setVisibility(View.GONE);
                list.addAll(addressResponse.getData());
                adapter.notifyDataSetChanged();
            } else {
                no_data.setVisibility(View.VISIBLE);
            }
        } else {
            GenericResponse genericResponse = (GenericResponse)response.body();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(genericResponse.getMessage_title());
            alertDialogBuilder.setMessage(genericResponse.getMsg());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();
        }

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof PlaceVH) {
            // get the removed item name to display it in snack bar
            final int id = list.get(viewHolder.getAdapterPosition()).getId();
            final Datum deletedItem = list.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            alertDialogBuilder.setTitle(Constants.TRANSLATIONS.getYesLable());
            alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getDeleteMsg());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getYesLable(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
//                    Toast.makeText(NewPlacesActivity.this, "Removed", Toast.LENGTH_SHORT).show();
                    adapter.removeItem(viewHolder.getAdapterPosition());
                    new RestCaller(NewPlacesActivity.this, SmartWashr.getRestClient().deleteNewAddress(sessionManager.get(Constants.ACCESS_TOKEN),
                            sessionManager.get(Constants.LANG),
                            id+""),2);
                }
            });
            alertDialogBuilder.setNegativeButton(Constants.TRANSLATIONS.getNoLable(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    adapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(NewPlacesActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
