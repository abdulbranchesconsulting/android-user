package user.com.smartwashr.activities.newActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.newAdapters.PriceAdapter;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.pricing.Product;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;

public class NewPlacesList extends AppCompatActivity implements ResponseHandler {

    RecyclerView recyclerView;
    ArrayList<Product> list = new ArrayList<>();
    PriceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_place_list);


    }

    private void getPrices() {
        if (Internet.isAvailable(NewPlacesList.this)) {
            Loading.show(NewPlacesList.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            new RestCaller(NewPlacesList.this, SmartWashr.getRestClient().newCategories(), 2);
        } else {
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
       }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewPlacesList.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
