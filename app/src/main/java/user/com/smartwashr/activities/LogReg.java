package user.com.smartwashr.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.Errors;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.loginresponse.LoginResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 5/31/17.
 */

public class LogReg extends AppCompatActivity implements ResponseHandler {

    private Button login, signup;
    private TextView fb, sw_title;
    private String lang;
    private ProfileTracker mProfileTracker;
    private CallbackManager callbackManager;
    private LoginButton login_fb;
    private String email_fb, name, pic;
    private ProgressDialog alertDialog;
    private SessionManager sessionManager;

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.log_reg_scrn);

        sessionManager = new SessionManager(this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "user.com.smartwashr",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                Toast.makeText(this, ""+Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_SHORT).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        login = (Button) findViewById(R.id.sign_in);
        signup = (Button) findViewById(R.id.sign_up);
        fb = (TextView) findViewById(R.id.facebook_login);
        sw_title = (TextView) findViewById(R.id.title);
        login_fb = (LoginButton) findViewById(R.id.login_button);

        login_fb.setReadPermissions(Arrays.asList(
                "public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setMABold(login);
            FontUtils.setMABold(signup);
            FontUtils.setMABold(fb);
            FontUtils.setMARegular(sw_title);
        } else {
            FontUtils.setMARegular(sw_title);
            FontUtils.setArabic(login);
            FontUtils.setArabic(signup);
            FontUtils.setArabic(fb);
            login.setText(getString(R.string.signin_ar));
            signup.setText(getString(R.string.signup_ar));
            fb.setText("التسجيل عن طريق فيسبوك");
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogReg.this, Login.class);
                startActivity(i);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogReg.this, SignUp.class);
                startActivity(i);
            }
        });


        RippleEffect.applyRippleEffect(login, "#bdbdbd");
        RippleEffect.applyRippleEffect(signup, "#bdbdbd");
        RippleEffect.applyRippleEffect(fb, "#bdbdbd");

        login_fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginResult.getAccessToken().getUserId();
                loginResult.getAccessToken().getToken();
                final AccessToken accessToken = loginResult.getAccessToken();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    email_fb = object.getString("email");
                                    Log.v("facebook - profile=data", email_fb);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");
                request.setParameters(parameters);
                request.executeAsync();


                if (Profile.getCurrentProfile() == null) {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(LogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(LogReg.this, false, "الرجاء الانتظار");
                    }
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                            // profile2 is the new profile
                            pic = profile2.getProfilePictureUri(500, 500) + "";
                            name = profile2.getName();
                            SessionManager.put(Constants.USER_NAME, name);
                            SessionManager.put(Constants.USER_PIC, pic);
                            Log.v("facebook - profile", name + "\n" + pic);
                            mProfileTracker.stopTracking();
                            Log.d("login_success_1", "successful");
                            Runnable task = new Runnable() {
                                public void run() {
                                    loginFb(email_fb, name, pic);
                                }
                            };
                            worker.schedule(task, 3000, TimeUnit.MILLISECONDS);
                        }
                    };
                    // no need to call startTracking() on mProfileTracker
                    // because it is called by its constructor, internally.
                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(LogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(LogReg.this, false, "الرجاء الانتظار");
                    }
                    Profile profile = Profile.getCurrentProfile();
                    String id = profile.getId();
                    pic = profile.getProfilePictureUri(500, 500) + "";
                    name = profile.getName();
                    SessionManager.put(Constants.USER_NAME, name);
                    SessionManager.put(Constants.USER_PIC, pic);
                    Log.d("login_success_2", "successful");
                    Log.v("facebook", profile.getName() + "\n" + id + "\n" + pic);
                    Runnable task = new Runnable() {
                        public void run() {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Loading.show(LogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                            } else {
                                Loading.show(LogReg.this, false, "الرجاء الانتظار");
                            }
                            loginFb(email_fb, name, pic);
                        }
                    };
                    worker.schedule(task, 3000, TimeUnit.MILLISECONDS);
                }
            }

            @Override
            public void onCancel() {
                Toast.makeText(LogReg.this, "Login Canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LogReg.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_fb.performClick();
            }
        });
    }

    private void loginFb(String email, String name, String profile_pic) {
        if (Internet.isAvailable(LogReg.this)) {
            new RestCaller(LogReg.this, SmartWashr.getRestClient().login_with_fb(email, name, profile_pic, SessionManager.get(Constants.DEVICE_TOKEN)), 2);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(LogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(LogReg.this, false, "الرجاء الانتظار");
            }
        } else {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();

        LoginResponse loginResponse = (LoginResponse) response.body();
        Log.d("response", new Gson().toJson(loginResponse));
        SessionManager.put(Constants.ACCESS_TOKEN, "Bearer "+loginResponse.getToken());
        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getActiveorders());
        SessionManager.put(Constants.USER_ID, loginResponse.getUser().getId().toString());
        SessionManager.put(Constants.USER_NAME, loginResponse.getUser().getName());
        SessionManager.put(Constants.EMAIL, loginResponse.getUser().getEmail());
        SessionManager.put(Constants.USER_IMG, loginResponse.getUser().getProfilePic());
        SessionManager.put(Constants.USER_PHONE, loginResponse.getUser().getPhoneNumber());
        Loading.cancel();
        Intent i = new Intent(LogReg.this, HowToActivity.class);
        startActivity(i);
        LogReg.this.finish();
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        if (error.getErrors().size() > 0) {
            for (int i = 0; i <= error.getErrors().size(); i++) {
                Errors errors = error.getErrors().get(i);
                if (errors.getField().equalsIgnoreCase("email")) {

                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("Email already exist");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });
                    } else {

                        alertDialog.setTitle("خطأ");
                        alertDialog.setMessage("البريد الالكتروني موجود بالفعل");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "أوكي", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });
                    }

                    alertDialog.show();
                }
                break;
            }
        }
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LogReg.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
