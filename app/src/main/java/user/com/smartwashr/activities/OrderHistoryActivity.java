package user.com.smartwashr.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.OrderDBAdapter;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.models.orderresponse.UserOrder;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 10/24/17.
 */

public class OrderHistoryActivity extends AppCompatActivity implements ResponseHandler {
    
    private CategoriesHandler ordersHandler;
    private RecyclerView recyclerView;
    private TextView no_orders;
    private OrderDBAdapter adapter;
    private ArrayList<OrderResponse> orderResponses;
    private ArrayList<UserOrder> orders;
    private Gson gson;
    String val = "0";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_history);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        no_orders = (TextView) findViewById(R.id.no_items);
        orderResponses = new ArrayList<>();
        orders = new ArrayList<>();
        gson = new Gson();
        recyclerView.setLayoutManager(new LinearLayoutManager(OrderHistoryActivity.this));
        ordersHandler = new CategoriesHandler(OrderHistoryActivity.this);
        if (ordersHandler.getAllOrders().size() > 0) {
            orderResponses.addAll(ordersHandler.getAllOrders());
            ordersHandler.close();
            for (int i = 0; i < orderResponses.size(); i++) {
                OrderResponse orderResponse = orderResponses.get(i);
                for (int j = 0; j < orderResponses.get(i).getUserOrder().size(); j++) {
                    UserOrder userOrder = orderResponse.getUserOrder().get(j);
                    orders.add(userOrder);
                }
            }
        }
//        adapter = new OrderDBAdapter(OrderHistoryActivity.this, orders, val);
//        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Internet.isAvailable(OrderHistoryActivity.this)) {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(OrderHistoryActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            }else {
                Loading.show(OrderHistoryActivity.this, false, "الرجاء الانتظار");
            }
            SessionManager sessionManager = new SessionManager(OrderHistoryActivity.this);
            new RestCaller(OrderHistoryActivity.this, SmartWashr.getRestClient().fetchOrders("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN)), 5);
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        SessionManager sessionManager = new SessionManager(OrderHistoryActivity.this);
        orders.clear();
        orderResponses.clear();
        adapter.notifyDataSetChanged();
        String json = gson.toJson(response.body());
        Log.d("Response", json);
        OrderResponse orderResponse = (OrderResponse) response.body();
        sessionManager.put(Constants.ACTIVE_ORDER, orderResponse.getActiveorders());


        if (orderResponse.getUserOrder().size() > 0) {
            no_orders.setVisibility(View.GONE);
        } else {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                no_orders.setText("Currently you have no orders.");
            } else {
                no_orders.setText("لا يوجد لديك طلبات حاليا");
            }
            no_orders.setVisibility(View.VISIBLE);
        }
        ordersHandler.deleteAllOrders();
        ordersHandler.insertOrder(orderResponse);
        ordersHandler.close();
        orderResponses.addAll(ordersHandler.getAllOrders());
        ordersHandler.close();

        for (int i = 0; i < orderResponses.size(); i++) {
            orderResponse = orderResponses.get(i);
            for (int j = 0; j < orderResponses.get(i).getUserOrder().size(); j++) {
                UserOrder userOrder = orderResponse.getUserOrder().get(j);
                orders.add(userOrder);
            }
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderHistoryActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}

