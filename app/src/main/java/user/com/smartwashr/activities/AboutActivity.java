package user.com.smartwashr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.TermsCondition;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.RippleEffect;

public class AboutActivity extends AppCompatActivity {

    TextView faq, contact, tv_version, version, terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        faq = (TextView) findViewById(R.id.faq);
        contact = (TextView) findViewById(R.id.contact_us);
        version = (TextView) findViewById(R.id.version);
        tv_version = (TextView) findViewById(R.id.tv_version);
        terms = (TextView) findViewById(R.id.terms);

        TextView title = findViewById(R.id.title);
        title.setText(Constants.TRANSLATIONS.getAboutLabel());
        tv_version.setText(Constants.TRANSLATIONS.getVersionLabel());
        version.setText(Constants.VERSION);
        faq.setText(Constants.TRANSLATIONS.getFaqHelpLabel());
        contact.setText(Constants.TRANSLATIONS.getContactUsLabel());
        terms.setText(Constants.TRANSLATIONS.getTermsAndConditionsLabel());

        RippleEffect.applyRippleEffect(version, "#bdbdbd");
        RippleEffect.applyRippleEffect(contact, "#bdbdbd");
        RippleEffect.applyRippleEffect(faq, "#bdbdbd");
//        RippleEffect.applyRippleEffect(terms, "#bdbdbd");

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, FAQActivity.class));
            }
        });

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, ContactUs.class));
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, TermsCondition.class));
            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
