package user.com.smartwashr.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.newActivities.AddNewAddress;
import user.com.smartwashr.activities.newActivities.NewOrderReview;
import user.com.smartwashr.activities.newActivities.NewSelectItems;
import user.com.smartwashr.adapter.PlaceAdapter;
import user.com.smartwashr.fragments.VoteFragment;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.address.AddressResponse;
import user.com.smartwashr.newModels.address.Datum;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResponseHandler,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMapLoadedCallback {

    private GoogleMap mMap;
    public static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;
    private Marker marker;
    private LatLng latLng;
    private AlertDialog alertDialog;
    private TextView map_address;
    private ImageView iv_down, iv_back;
    private Button btn_order;

    private SessionManager sessionManager;
    private MarkerOptions options;

    private FragmentManager fragmentManager;
    private LatLng mCenterLatLong;
    private boolean changed = false;

    RelativeLayout rl_addresses;
    RecyclerView address_recycler;

    PlaceAdapter adapter;
    ArrayList<Datum> list = new ArrayList<>();
    TextView no_data;
    private boolean clicked = false;
    ImageView iv_add_address;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maps_screen);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sessionManager = new SessionManager(MapsActivity.this);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.getView().setVisibility(View.GONE);


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                Log.d("POSITION", place.getLatLng() + "");

                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15.0f));
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });


        final View root = autocompleteFragment.getView();

        fragmentManager = getSupportFragmentManager();
        map_address = findViewById(R.id.map_address);
        iv_down = findViewById(R.id.iv_down);
        iv_back = findViewById(R.id.iv_back);
        no_data = findViewById(R.id.no_data);
        title = findViewById(R.id.title);
        iv_add_address = findViewById(R.id.iv_add_address);
        address_recycler = findViewById(R.id.address_recycler);
        btn_order = findViewById(R.id.order_place);

        title.setText(Constants.TRANSLATIONS.getSelectAddressLabel());
        btn_order.setText(Constants.TRANSLATIONS.getConfirmPickupLabel());
        no_data.setText(Constants.TRANSLATIONS.getNoAddressFound());

        iv_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MapsActivity.this, AddNewAddress.class);
                i.putExtra("maps", "maps");
                startActivity(i);
            }
        });

        address_recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rl_addresses = findViewById(R.id.rl_addresses);
        adapter = new PlaceAdapter(this, list, map_address, rl_addresses, iv_down, true);
        address_recycler.setAdapter(adapter);

        map_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicked = true;
                root.findViewById(R.id.place_autocomplete_search_input)
                        .performClick();
            }
        });


        if (getIntent().getStringExtra("add_place") != null) {
            iv_add_address.setVisibility(View.GONE);
            btn_order.setText("SAVE");
        } else {
            iv_add_address.setVisibility(View.VISIBLE);
        }


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_down.setImageResource(R.drawable.ic_down_arrow);

        iv_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rl_addresses.getVisibility() == View.VISIBLE) {
                    rl_addresses.setVisibility(View.GONE);
                    iv_down.setImageResource(R.drawable.ic_down_arrow);
                } else {
                    rl_addresses.setVisibility(View.VISIBLE);
                    iv_down.setImageResource(R.drawable.ic_cancel);
                }
            }
        });

        View locationButton = mapFragment.getView().findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000)        // 10 seconds, in milliseconds
                .setFastestInterval(5000); // 5 second, in milliseconds

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            alertDialog = new AlertDialog.Builder(
                    MapsActivity.this).create();
            alertDialog.setTitle(Constants.TRANSLATIONS.getAttentionLable());
            alertDialog.setMessage(Constants.TRANSLATIONS.getEnableLocationDescriptionLabel());
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }


        getAddress();


        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (map_address.getText().toString().length() > 0) {
//                    if (getIntent().getExtras().getString("add_place") != null) {
//                Intent i = new Intent(MapsActivity.this, NewSelectItems.class);
//                        i.putExtra("address", map_address.getText().toString());
//                startActivity(i);
//                    } else {

                if (Internet.isAvailable(MapsActivity.this)) {
                    new RestCaller(MapsActivity.this, SmartWashr.getRestClient().nearest_laundry(SessionManager.get(Constants.ACCESS_TOKEN),
                            SessionManager.get(Constants.LATITUDE),
                            SessionManager.get(Constants.LONGITUDE)), 2);
//                    new RestCaller(MapsActivity.this, SmartWashr.getRestClient().nearest_laundry("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), Constants.SAUDI_LNG, Constants.SAUDI_LAT), 1);
                    Loading.show(MapsActivity.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());

                } else {
                    Toast.makeText(MapsActivity.this, "Enter your address first", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void getAddress() {
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
        new RestCaller(MapsActivity.this, SmartWashr.getRestClient().getAddresses(SessionManager.get(Constants.ACCESS_TOKEN)), 1);
    }


    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 1) {
            AddressResponse addressResponse = (AddressResponse) response.body();
            if (addressResponse.getData().size() > 0) {
                no_data.setVisibility(View.GONE);
                list.addAll(addressResponse.getData());
                adapter.notifyDataSetChanged();
            } else {
                no_data.setVisibility(View.VISIBLE);
            }

        } else if (reqCode == 2) {
            GenericResponse genericResponse = (GenericResponse) response.body();
            if (genericResponse.getDriver() != null) {
                SessionManager.put(Constants.DRIVER_ID, String.valueOf(genericResponse.getDriver().getUserId()));
                if (getIntent().getStringExtra("add_place") != null) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", SessionManager.get(Constants.ADDRESS_ONE));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else if (getIntent().getStringExtra("skip") != null) {
                    Intent i = new Intent(MapsActivity.this, NewOrderReview.class);
                    i.putExtra("skip", "skip");
                    startActivity(i);
                } else {
                    startActivity(new Intent(MapsActivity.this, NewSelectItems.class));
                }

            } else {
                alertDialog = new AlertDialog.Builder(
                        MapsActivity.this).create();
                alertDialog.setTitle("Failure");
//                alertDialog.setMessage(genericResponse.getError().toString());
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        finish();
                    }
                });
                alertDialog.show();
                openVote();
            }
        } else {
            GenericResponse genericResponse = (GenericResponse) response.body();
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Toast.makeText(this, "Area Voted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "تم تحديد حي جديد", Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(MapsActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        Loading.cancel();
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        changed = true;
        mMap.setOnMapLoadedCallback(this);
    }

    @Override
    public void onMapLoaded() {
        LatLng position = mMap.getCameraPosition().target;
        double Lat = position.latitude;
        double Long = position.longitude;

        SessionManager.put(Constants.LATITUDE, position.latitude + "");
        SessionManager.put(Constants.LONGITUDE, position.longitude + "");

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(Lat, Long, 1);
            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String address11 = addresses.get(0).getAddressLine(1);
                String city = addresses.get(0).getLocality();
                SessionManager.put(Constants.CITY, city);
                Log.e("Address>>", address);
                map_address.setText(address);
                SessionManager.put(Constants.ADDRESS_ONE, address);
                clicked = false;
                iv_add_address.setClickable(true);
                btn_order.setClickable(true);
            }
        } catch (IOException e) {
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setPadding(0, 0, 30, 105);
        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_SATELLITE)
                .compassEnabled(false)
                .rotateGesturesEnabled(true)
                .tiltGesturesEnabled(false);

        mMap.setOnCameraChangeListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    public void moveMaps(double lat, double lng) {
        double currentLatitude = lat;
        double currentLongitude = lng;
        LatLng latLong = new LatLng(currentLatitude, currentLongitude);
        latLng = latLong;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latLng.latitude, latLng.longitude), 15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLong = new LatLng(currentLatitude, currentLongitude);
        latLng = latLong;
        mMap.clear();
        if (clicked) {
            mMap.animateCamera(CameraUpdateFactory.zoomIn());
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomIn());
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }
        options = new MarkerOptions()
                .position(latLng)
                .title("Current Location");

        if (marker != null) {
            marker.remove();
        }
//        marker = mMap.addMarker(options);
//        marker = mMap.addMarker(options
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
//        if (map_address.getText().length() < 1)
//        new GetAddress().execute(String.format("%.4f,%.4f", currentLatitude, currentLongitude));
    }

    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }

    private void openVote() {
        VoteFragment voteFragment = new VoteFragment();
        voteFragment.show(fragmentManager, "");
        voteFragment.setOnItemClickListener(new VoteFragment.MyClickListener() {
            @Override
            public void onYes() {
                voteArea();
            }
        });
    }

    private void voteArea() {
        if (Internet.isAvailable(MapsActivity.this)) {
            new RestCaller(MapsActivity.this, SmartWashr.getRestClient().voteArea("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), SessionManager.get(Constants.LATITUDE), SessionManager.get(Constants.LONGITUDE), map_address.getText().toString()), 5);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onBackPressed() {
        if (Constants.address != null) {
            Constants.address = null;
        }
        super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapsActivity.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
