package user.com.smartwashr.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.OrderAdapterNew;
import user.com.smartwashr.adapter.ProductsOrder;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.ProductOrder;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

import static user.com.smartwashr.R.id.del_price;
import static user.com.smartwashr.R.id.delivr_time;

/**
 * Created by zeeshan on 6/9/17.
 */

public class OrderDoneNew extends AppCompatActivity implements
        ResponseHandler,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    private ArrayList<ProductsOrder> list;
    private ProductOrder productOrder;
    private RecyclerView recyclerView;
    private OrderAdapterNew adapter;
    private Button order_now;
    private TextView txt_address, add_item, txt_collection, txt_delivery, txt_items, txt_checkout, delivery_txt, final_total_txt, final_total, del_charges;
    private EditText collect_date, collect_time, deliver_date, delivery_time, lane1, zip, lane2, order_instructions;
    private TextView total_txt;
    public static TextView total_price;
    private ImageView collection_date, collection_time, delivery_date, delivery_tme;
    private LinearLayout add_btn;
    private String list_string = "";
    private String string_list = "";
    private AlertDialog alertDialog;
    private View parent;
    private SessionManager sessionManager;
    private Calendar myCalendar;
    private Gson gson;
    private ArrayList<Integer> total_list;
    private DatePickerDialog.OnDateSetListener date;
    private int clicked_item = 0;
    private static final String API_OPEN_KEY = "test_open_k_15fa56ef35f880f9c2ca";
    private static final String LOG_TAG = OrderDoneNew.class.getSimpleName();
    private int t = 0;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private String pos;
    private int selected_date = 0;
    private int selected_time = 0;
    private Toolbar toolbar;
    private TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_done);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView back = (ImageView) findViewById(R.id.back);
        ImageView bback = (ImageView) findViewById(R.id.bback);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        list = new ArrayList<>();
        total_list = new ArrayList<>();
        gson = new Gson();
        sessionManager = new SessionManager(OrderDoneNew.this);

        recyclerView = (RecyclerView) findViewById(R.id.recylerView);

        title = (TextView) findViewById(R.id.title);
        order_now = (Button) findViewById(R.id.order_now);
        parent = findViewById(R.id.parent);
        total_price = (TextView) findViewById(R.id.total_price);
        total_txt = (TextView) findViewById(R.id.total_txt);
        txt_address = (TextView) findViewById(R.id.txt_address);
        add_item = (TextView) findViewById(R.id.add_item);
        txt_collection = (TextView) findViewById(R.id.txt_collection);
        txt_delivery = (TextView) findViewById(R.id.txt_delivery);
        txt_items = (TextView) findViewById(R.id.txt_items);
        txt_checkout = (TextView) findViewById(R.id.txt_checkout);
        delivery_txt = (TextView) findViewById(R.id.delivery_txt);
        final_total_txt = (TextView) findViewById(R.id.final_total_txt);
        del_charges = (TextView) findViewById(del_price);
        final_total = (TextView) findViewById(R.id.final_total_price);
        lane1 = (EditText) findViewById(R.id.lane1);
        lane2 = (EditText) findViewById(R.id.lane2);
        order_instructions = (EditText) findViewById(R.id.order_ins);
        add_btn = (LinearLayout) findViewById(R.id.add_btn);
        zip = (EditText) findViewById(R.id.zip);
        collect_date = (EditText) findViewById(R.id.collec_date);
        collect_time = (EditText) findViewById(R.id.collec_time);
        deliver_date = (EditText) findViewById(R.id.delivery_date);
        delivery_time = (EditText) findViewById(R.id.delivery_time);
        collection_date = (ImageView) findViewById(R.id.collection_date);
        collection_time = (ImageView) findViewById(R.id.collection_time);
        delivery_date = (ImageView) findViewById(R.id.delivr_date);
        delivery_tme = (ImageView) findViewById(delivr_time);

        lane1.setText(SessionManager.get(Constants.ADDRESS_ONE));

        RippleEffect.applyRippleEffect(add_btn, "#bdbdbd");
        RippleEffect.applyRippleEffect(collection_date, "#bdbdbd");
        RippleEffect.applyRippleEffect(collection_time, "#bdbdbd");
        RippleEffect.applyRippleEffect(delivery_date, "#bdbdbd");
        RippleEffect.applyRippleEffect(delivery_tme, "#bdbdbd");
        RippleEffect.applyRippleEffect(lane2, "#bdbdbd");
        RippleEffect.applyRippleEffect(zip, "#bdbdbd");
        RippleEffect.applyRippleEffect(order_now, "#bdbdbd");

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lane1.getText().toString().length() > 1) {
                    sessionManager.put(Constants.LANE1, lane1.getText().toString());
                }
                if (lane2.getText().toString().length() > 1) {
                    sessionManager.put(Constants.LANE2, lane2.getText().toString());
                }
                if (zip.getText().toString().length() > 1) {
                    sessionManager.put(Constants.ZIP, zip.getText().toString());
                }
                if (collect_date.getText().toString().length() > 1) {
                    sessionManager.put(Constants.COL_DATE, collect_date.getText().toString());
                }
                if (collect_time.getText().toString().length() > 1) {
                    sessionManager.put(Constants.COL_TIME, collect_time.getText().toString());
                }
                if (deliver_date.getText().toString().length() > 1) {
                    sessionManager.put(Constants.DEL_DATE, deliver_date.getText().toString());
                }
                if (delivery_time.getText().toString().length() > 1) {
                    sessionManager.put(Constants.DEL_TIME, delivery_time.getText().toString());
                }
                onBackPressed();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(OrderDoneNew.this));

        list = (ArrayList<ProductsOrder>) getIntent().getSerializableExtra("products");

        adapter = new OrderAdapterNew(OrderDoneNew.this, list, total_price, final_total, false);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            title.setText("Order");
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            FontUtils.setNewFont(txt_address);
            FontUtils.setNewFont(txt_collection);
            FontUtils.setNewFont(txt_delivery);
            FontUtils.setNewFont(txt_items);
            FontUtils.setNewFont(txt_checkout);
            FontUtils.setFont(total_price);
            FontUtils.setFont(total_txt);
            FontUtils.setFont(delivery_txt);
            FontUtils.setFont(del_charges);
            FontUtils.setFont(final_total);
            FontUtils.setFont(final_total_txt);
            FontUtils.setFont(lane1);
            FontUtils.setFont(zip);
            FontUtils.setNewFont(order_now);

            FontUtils.setNewFont(txt_collection);
            FontUtils.setNewFont(txt_delivery);
            FontUtils.setFont(lane2);

            FontUtils.setFont(collect_date);
            FontUtils.setFont(collect_time);
            FontUtils.setFont(deliver_date);
            FontUtils.setFont(delivery_time);
            FontUtils.setNewFont(add_item);
            FontUtils.setFont(order_instructions);
        } else {
            title.setText("الطلب");
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            txt_address.setText("أدخل عنوانك");
            txt_collection.setText("تاريخ و وقت الاستلام");
            txt_delivery.setText(" تاريخ و وقت");
            txt_items.setText("القطع المختارة");
            txt_checkout.setText("إجراء الطلب");
            final_total_txt.setText("الإجمالي");
            total_txt.setText("إجمالي القطع");
            delivery_txt.setText("رسوم التوصيل");
            add_item.setText("إضافة قطعة");
            del_charges.setText("10" + " ريال ");
            order_now.setText("أطلب الآن");

            lane1.setHint("مسار 1");
            lane2.setHint("مسار 2");
            zip.setHint("الرمز البريدي");
            order_instructions.setHint("تعليمات الطلب");
            collect_date.setHint("تاريخ");
            collect_time.setHint("زمن");
            deliver_date.setHint("تاريخ");
            delivery_time.setHint("زمن");
        }


        Log.d("listCreate", "" + list.size());
        adapter.notifyDataSetChanged();

        collection_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                t = 1;
                now.add(Calendar.MINUTE, 120);
                TimePickerDialog dpd = TimePickerDialog.newInstance(
                        OrderDoneNew.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        true
                );
                dpd.setMinTime(
                        now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), 0);
                dpd.setMaxTime(23, 0, 0);
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                    dpd.setTitle(getResources().getString(R.string.time_date_msg));
                else
                    dpd.setTitle("نحن منفتحون 7 أيام في الأسبوع 1pm - 11pm");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        collection_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                t = 1;
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        OrderDoneNew.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DATE)
                );
                dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DATE)));
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                    dpd.setTitle(getResources().getString(R.string.time_date_msg));
                else
                    dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 1 ظهرا - 11 مساءا ]");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        delivery_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_date != 0) {
                    Calendar now = Calendar.getInstance();
                    t = 2;
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            OrderDoneNew.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            selected_date
                    );
                    dpd.setMinDate(new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), selected_date + 1));
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                        dpd.setTitle(getResources().getString(R.string.time_date_msg));
                    else
                        dpd.setTitle("في خدمتكم 7 أيام في الأسبوع [ 1 ظهرا - 11 مساءا ]");
                    dpd.show(getFragmentManager(), "Datepickerdialog");

                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(OrderDoneNew.this, "Please set collection date first.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderDoneNew.this, "يرجى تعيين تاريخ المجموعة أولا", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        delivery_tme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_time != 0) {
                    Calendar now = Calendar.getInstance();
                    t = 2;
                    TimePickerDialog dpd = TimePickerDialog.newInstance(
                            OrderDoneNew.this,
                            selected_time,
                            0,
                            true
                    );
                    if (selected_time == 13) {
                        dpd.setMinTime(
                                selected_time, 30, 0);
                    } else {
                        dpd.setMinTime(
                                selected_time, now.get(Calendar.MINUTE), 0);
                    }
                    dpd.setMaxTime(23, 0, 0);
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                        dpd.setTitle(getResources().getString(R.string.time_date_msg));
                    else
                        dpd.setTitle("نحن منفتحون 7 أيام في الأسبوع 1pm - 11pm");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(OrderDoneNew.this, "Please set collection time first.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderDoneNew.this, "الرجاء تعيين وقت التجميع أولا", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        order_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    showDialog(OrderDoneNew.this, "Select payment method", new String[]{"Ok"},
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == -1) {
                                        if (clicked_item == 1) {
                                            if (Internet.isAvailable(OrderDoneNew.this)) {
                                                if (lane1.getText().length() > 0) {
                                                    if (collect_time.getText().length() > 0 && collect_date.getText().length() > 0 && delivery_time.getText().length() > 0 && deliver_date.getText().length() > 0) {
                                                        ProductOrder productOrder = new ProductOrder();
                                                        productOrder.setPostCode(zip.getText().toString());
                                                        productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                                                        productOrder.setLaundryId(SessionManager.get(Constants.LAUNDRY_ID));
                                                        productOrder.setTotalPrice(total_price.getText().toString());
                                                        productOrder.setAddressOne(lane1.getText().toString());
                                                        productOrder.setAddressTwo(lane2.getText().toString());
                                                        productOrder.setDiscount("");
                                                        productOrder.setDiscountType("");
                                                        productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                                                        productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                                                        productOrder.setDeliveryInstruction(order_instructions.getText().toString());
                                                        productOrder.setCollectionDateTime(collect_date.getText().toString()+ " "+ collect_time.getText().toString());
                                                        productOrder.setDeliveryDateTime(deliver_date.getText().toString()+ " "+ delivery_time.getText().toString());
                                                        productOrder.setProductsOrder(list);
                                                        String json = gson.toJson(productOrder);
                                                        new RestCaller(OrderDoneNew.this, SmartWashr.getRestClient().order("application/json", "application/json","Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), productOrder), 1);
                                                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                            Loading.show(OrderDoneNew.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                                                        } else {
                                                            Loading.show(OrderDoneNew.this, false, "الرجاء الانتظار");
                                                        }
                                                    } else {
                                                        collect_time.requestFocus();
                                                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                            Toast.makeText(OrderDoneNew.this, "Please fill all date and time fields", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Toast.makeText(OrderDoneNew.this, "يرجى ملء جميع حقول التاريخ والوقت", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                } else {
                                                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                        lane1.setError("Please fill this field");
                                                    } else {
                                                        lane1.setError("يرجى ملء هذا الحقل");
                                                    }
                                                }
                                            } else {
                                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                    Snackbar.make(parent, "Check your Internet Connection", Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    Snackbar.make(parent, "تأكد من اتصالك بالانترنت", Snackbar.LENGTH_LONG).show();
                                                }

                                            }
                                        } else if (clicked_item == 2) {
                                            Intent i = new Intent(OrderDoneNew.this, CreditCard.class);
                                            ProductOrder productOrder = new ProductOrder();
                                            productOrder.setPostCode(zip.getText().toString());
                                            productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                                            productOrder.setLaundryId(SessionManager.get(Constants.LAUNDRY_ID));
                                            productOrder.setTotalPrice(total_price.getText().toString());
                                            productOrder.setAddressOne(lane1.getText().toString());
                                            productOrder.setAddressTwo(lane2.getText().toString());
                                            productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                                            productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                                            productOrder.setDiscount("");
                                            productOrder.setDiscountType("");
                                            productOrder.setDeliveryInstruction(order_instructions.getText().toString());
                                            productOrder.setCollectionDateTime(collect_date.getText().toString()+ " "+ collect_time.getText().toString());
                                            productOrder.setDeliveryDateTime(deliver_date.getText().toString()+ " "+ delivery_time.getText().toString());
                                            productOrder.setProductsOrder(list);
                                            i.putExtra("total", total_price.getText().toString());
                                            i.putExtra("products", productOrder);
                                            startActivity(i);
                                        }
                                    }

                                }
                            });

                } else {
                    showDialog(OrderDoneNew.this, " اختر طريقة الدفع", new String[]{"أوكي"},
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == -1) {
                                        if (clicked_item == 1) {
                                            if (Internet.isAvailable(OrderDoneNew.this)) {
                                                if (lane1.getText().length() > 0) {
                                                    if (collect_time.getText().length() > 0 && collect_date.getText().length() > 0 && delivery_time.getText().length() > 0 && deliver_date.getText().length() > 0) {
                                                        ProductOrder productOrder = new ProductOrder();
                                                        productOrder.setPostCode(zip.getText().toString());
                                                        productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                                                        productOrder.setLaundryId(SessionManager.get(Constants.LAUNDRY_ID));
                                                        productOrder.setTotalPrice(total_price.getText().toString());
                                                        productOrder.setAddressOne(lane1.getText().toString());
                                                        productOrder.setAddressTwo(lane2.getText().toString());
                                                        productOrder.setDiscount("");
                                                        productOrder.setDiscountType("");
                                                        productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                                                        productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                                                        productOrder.setDeliveryInstruction(order_instructions.getText().toString());
                                                        productOrder.setCollectionDateTime(collect_date.getText().toString()+ " "+ collect_time.getText().toString());
                                                        productOrder.setDeliveryDateTime(deliver_date.getText().toString()+ " "+ delivery_time.getText().toString());
                                                        productOrder.setProductsOrder(list);
                                                        String json = gson.toJson(productOrder);
                                                        new RestCaller(OrderDoneNew.this, SmartWashr.getRestClient().order("application/json","application/json", "Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), productOrder), 1);
                                                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                            Loading.show(OrderDoneNew.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                                                        } else {
                                                            Loading.show(OrderDoneNew.this, false, "الرجاء الانتظار");
                                                        }
                                                    } else {
                                                        collect_time.requestFocus();
                                                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                            Toast.makeText(OrderDoneNew.this, "Please fill all date and time fields", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Toast.makeText(OrderDoneNew.this, "يرجى ملء جميع حقول التاريخ والوقت", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                } else {
                                                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                        lane1.setError("Please fill this field");
                                                    } else {
                                                        lane1.setError("يرجى ملء هذا الحقل");
                                                    }
                                                }
                                            } else {
                                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                                    Snackbar.make(parent, "Check your Internet Connection", Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    Snackbar.make(parent, "تأكد من اتصالك بالانترنت", Snackbar.LENGTH_LONG).show();
                                                }

                                            }
                                        } else if (clicked_item == 2) {
                                            Intent i = new Intent(OrderDoneNew.this, CreditCard.class);
                                            ProductOrder productOrder = new ProductOrder();
                                            productOrder.setPostCode(zip.getText().toString());
                                            productOrder.setDriverId(Integer.valueOf(SessionManager.get(Constants.DRIVER_ID)));
                                            productOrder.setLaundryId(SessionManager.get(Constants.LAUNDRY_ID));
                                            productOrder.setTotalPrice(total_price.getText().toString());
                                            productOrder.setAddressOne(lane1.getText().toString());
                                            productOrder.setAddressTwo(lane2.getText().toString());
                                            productOrder.setLatitude(SessionManager.get(Constants.LATITUDE));
                                            productOrder.setLongitude(SessionManager.get(Constants.LONGITUDE));
                                            productOrder.setDiscount("");
                                            productOrder.setDiscountType("");
                                            productOrder.setDeliveryInstruction(order_instructions.getText().toString());
                                            productOrder.setCollectionDateTime(collect_date.getText().toString()+ " "+ collect_time.getText().toString());
                                            productOrder.setDeliveryDateTime(deliver_date.getText().toString()+ " "+ delivery_time.getText().toString());
                                            productOrder.setProductsOrder(list);
                                            i.putExtra("total", total_price.getText().toString());
                                            i.putExtra("products", productOrder);
                                            startActivity(i);
                                        }
                                    }

                                }
                            });
                }

            }
        });

        recyclerView.setAdapter(adapter);

    }


    public void showDialog(Context context, String title, String[] btnText,
                           DialogInterface.OnClickListener listener) {
        CharSequence[] items;
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            items = new CharSequence[]{"Cash on delivery"};
        } else {
            items = new CharSequence[]{" نقدا عند التوصيل"};
        }

        if (listener == null)
            listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface,
                                    int paramInt) {
                    paramDialogInterface.dismiss();
                }
            };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        builder.setSingleChoiceItems(items, -1,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            clicked_item = 1;
                        } else {
                            clicked_item = 2;
                        }
                    }
                });
        builder.setPositiveButton(btnText[0], listener);
        if (btnText.length != 1) {
            builder.setNegativeButton(btnText[1], listener);
        }
        builder.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sessionManager.get(Constants.LANE1) != null) {
            lane1.setText(sessionManager.get(Constants.LANE1));
        }
        if (sessionManager.get(Constants.LANE2) != null) {
            lane2.setText(sessionManager.get(Constants.LANE2));
        }
        if (sessionManager.get(Constants.ZIP) != null) {
            zip.setText(sessionManager.get(Constants.ZIP));
        }
        if (sessionManager.get(Constants.COL_DATE) != null) {
            collect_date.setText(sessionManager.get(Constants.COL_DATE));
        }
        if (sessionManager.get(Constants.COL_TIME) != null) {
            collect_time.setText(sessionManager.get(Constants.COL_TIME));
        }
        if (sessionManager.get(Constants.DEL_DATE) != null) {
            deliver_date.setText(sessionManager.get(Constants.DEL_DATE));
        }
        if (sessionManager.get(Constants.DEL_TIME) != null) {
            delivery_time.setText(sessionManager.get(Constants.DEL_TIME));
        }
        if (sessionManager.getInt(Constants.SELECTED_TIME) != 0) {
            selected_time = sessionManager.getInt(Constants.SELECTED_TIME);
        }
        if (sessionManager.getInt(Constants.SELECTED_DATE) != 0) {
            selected_date = sessionManager.getInt(Constants.SELECTED_DATE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (selected_time != 0) {
            sessionManager.put(Constants.SELECTED_TIME, selected_time);
        }
        if (selected_date != 0) {
            sessionManager.put(Constants.SELECTED_DATE, selected_date);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        String json = gson.toJson(response.body());
        Log.d("Response", response.body().toString());
        alertDialog = new AlertDialog.Builder(
                OrderDoneNew.this).create();
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            alertDialog.setTitle("Success");
            alertDialog.setMessage("Thank you for placing order with SmartWashr");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    startActivity(new Intent(OrderDoneNew.this, HomeActivity.class));
                    OrderDoneNew.this.finish();
                }
            });
        } else {
            alertDialog.setTitle("تم بنجاح");
            alertDialog.setMessage("شكرا لكم على طلبكم من سمارت ووشر");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "أوكي", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    startActivity(new Intent(OrderDoneNew.this, HomeActivity.class));
                    OrderDoneNew.this.finish();
                }
            });
        }
        alertDialog.show();
        OrderResponse orderResponse = (OrderResponse) response.body();
        CategoriesHandler ordersHandler = new CategoriesHandler(OrderDoneNew.this);
        if (ordersHandler.getAllOrders().size() > 0) {
            ordersHandler.deleteAllOrders();
            ordersHandler.close();
        }
        sessionManager.put(Constants.ACTIVE_ORDER, orderResponse.getActiveorders());
        ordersHandler.insertOrder(orderResponse);
        ordersHandler.close();
        sessionManager.remove(Constants.PRODUCTS_ORDER);
        if (sessionManager.get(Constants.LANE1) != null) {
            sessionManager.remove(Constants.LANE1);
        }
        if (sessionManager.get(Constants.LANE2) != null) {
            sessionManager.remove(Constants.LANE2);
        }
        if (sessionManager.get(Constants.ZIP) != null) {
            sessionManager.remove(Constants.ZIP);
        }
        if (sessionManager.get(Constants.COL_DATE) != null) {
            sessionManager.remove(Constants.COL_DATE);
        }
        if (sessionManager.get(Constants.COL_TIME) != null) {
            sessionManager.remove(Constants.COL_TIME);
        }
        if (sessionManager.get(Constants.DEL_DATE) != null) {
            sessionManager.remove(Constants.DEL_DATE);
        }
        if (sessionManager.get(Constants.DEL_TIME) != null) {
            sessionManager.remove(Constants.DEL_TIME);
        }
        if (sessionManager.getInt(Constants.SELECTED_TIME) != 0) {
            sessionManager.remove(Constants.SELECTED_TIME);
        }
        if (sessionManager.getInt(Constants.SELECTED_DATE) != 0) {
            sessionManager.remove(Constants.SELECTED_DATE);
        }

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        Toast.makeText(this, "Order not placed, something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
        Toast.makeText(this, "Order not placed, something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String time = "You picked the following time: " + hourOfDay + "h" + minute + "m" + second;
        Log.d("time", time);
        Calendar c = Calendar.getInstance();
        int today = c.get(Calendar.DATE);
        if (t == 1) {
            if (collect_date.getText().length() != 0) {
                if ((hourOfDay < 13 && hourOfDay > 23) && selected_date == today) {
                    selected_date = today;
                    collect_time.setText("13:30");
                    collect_date.setText(c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + selected_date);
                } else if (hourOfDay <= 22 && minute <= 59) {
                    collect_time.setText(hourOfDay + ":" + minute);
                    selected_time = hourOfDay;
                } else {
                    if (today == selected_date) {
                        selected_date = selected_date + 1;
                        collect_time.setText("13:30");
                        selected_time = 13;
                        collect_date.setText(c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + selected_date);
                    } else {
                        selected_date = selected_date + 1;
                        collect_time.setText("13:30");
                        selected_time = 13;
                        collect_date.setText(c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + selected_date);
                    }
                }
            } else {
                alertDialog = new AlertDialog.Builder(
                        OrderDoneNew.this).create();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please enter collection date first.");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                } else {
                    alertDialog.setTitle("خطأ");
                    alertDialog.setMessage("الرجاء إدخال تاريخ المجموعة أولا");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "أوكي", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                }
                alertDialog.show();
            }

        } else if (t == 2) {
            delivery_time.setText(hourOfDay + ":" + minute);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (t == 1) {
            collect_date.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
            selected_date = dayOfMonth;
        } else if (t == 2) {
            deliver_date.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
        }
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderDoneNew.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}