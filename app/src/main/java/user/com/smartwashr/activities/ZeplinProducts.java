package user.com.smartwashr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import user.com.smartwashr.OnDataPass;
import user.com.smartwashr.R;
import user.com.smartwashr.adapter.MainPageAdapter;
import user.com.smartwashr.adapter.ProductsOrder;
import user.com.smartwashr.fragments.MensProductFragment;
import user.com.smartwashr.fragments.MiscProductFragment;
import user.com.smartwashr.fragments.WomenProductsFragment;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

public class ZeplinProducts extends AppCompatActivity implements OnDataPass {

    private TabLayout tabLayout;
    MensProductFragment mensProductFragment;
    WomenProductsFragment womenProductsFragment;
    MiscProductFragment miscProductFragment;
    private ViewPager mViewPager;
    private MainPageAdapter mainPageAdapter;
    LinearLayout container;
    TextView total_txt, title, proceed_check;
    ArrayList<ProductsOrder> selected_products = new ArrayList<>();
    private int position_array;
    private ImageView bback, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zeplin_products);

        mensProductFragment = new MensProductFragment();
        womenProductsFragment = new WomenProductsFragment();
        miscProductFragment = new MiscProductFragment();

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        title = findViewById(R.id.title);
        total_txt = (TextView) findViewById(R.id.total);
        proceed_check = findViewById(R.id.proceed_check);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);

            tabLayout.addTab(tabLayout.newTab().setText("Male"));
            tabLayout.addTab(tabLayout.newTab().setText("Female"));
            tabLayout.addTab(tabLayout.newTab().setText("Misc."));
            FontUtils.setRobotoMediumFont(tabLayout);
            FontUtils.setRobotoMediumFont(title);

        } else {
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            FontUtils.setArabic(title);
            title.setText("منتجات");
            tabLayout.addTab(tabLayout.newTab().setText("ذكر"));
            tabLayout.addTab(tabLayout.newTab().setText("أنثى"));
            tabLayout.addTab(tabLayout.newTab().setText("انواع اخر"));

        }

        changeTabsFont();

//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            //noinspection ConstantConditions
//            TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
//            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
//                tabLayout.getTabAt(i).setCustomView(tv);
//                FontUtils.setRobotoMediumFont(tv);
//            } else {
//                tabLayout.getTabAt(i).setCustomView(tv);
//                FontUtils.setArabic(tv);
//            }
//
//        }

        mViewPager = (ViewPager) findViewById(R.id.vpcontainer);

        container = (LinearLayout) findViewById(R.id.checkout_con);

        mainPageAdapter = new MainPageAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), mensProductFragment, womenProductsFragment, miscProductFragment);
        mViewPager.setAdapter(mainPageAdapter);
        mViewPager.setOffscreenPageLimit(3);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        tabLayout.getTabAt(0).select();
                        break;
                    case 1:
                        tabLayout.getTabAt(1).select();
                        break;
                    case 2:
                        tabLayout.getTabAt(2).select();
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ZeplinProducts.this, ZeplinOrderDone.class);
                intent.putExtra("products", selected_products);
                startActivity(intent);
            }
        });


    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout;
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        FontUtils.setRobotoMediumFont((TextView) tabViewChild);
                    } else {
                        FontUtils.setArabic((TextView) tabViewChild);
                    }
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onDataPass(ProductsOrder productsOrder) {

        if (selected_products.size() == 0) {
            selected_products.add(productsOrder);
        } else {
            if (containsObject(selected_products, productsOrder)) {
                selected_products.remove(position_array);
            }
            selected_products.add(productsOrder);
        }


        if (selected_products.size() > 0) {
            container.setVisibility(View.VISIBLE);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                FontUtils.setRobotoMediumFont(total_txt);
                FontUtils.setRobotoMediumFont(proceed_check);
                total_txt.setText("Total SAR " + total(selected_products));
            } else {
                total_txt.setGravity(Gravity.END);
//                proceed_check.setGravity(Gravity.END);
                FontUtils.setArabic(proceed_check);
                FontUtils.setArabic(total_txt);
                proceed_check.setText("استمر");
                total_txt.setText(total(selected_products) + " ريال ");

            }
        } else {
            container.setVisibility(View.GONE);
        }

//        if (selected_products.size() > 0) {
//            container.setVisibility(View.VISIBLE);
//            total_txt.setText("TOTAL  s.r." + total(selected_products));
//            selectedProducts = selected_products;
//        } else {
//            container.setVisibility(View.GONE);
//        }

    }

    @Override
    public void onDataPass(int product_id) {
        if (selected_products.size() > 0) {
            if (removeObject(selected_products, product_id)) {
                selected_products.remove(position_array);
            }
            if (selected_products.size() > 0) {
                container.setVisibility(View.VISIBLE);
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    FontUtils.setRobotoMediumFont(total_txt);
                    FontUtils.setRobotoMediumFont(proceed_check);
                    total_txt.setText("Total SAR " + total(selected_products));
                } else {
                    total_txt.setGravity(Gravity.END);
//                proceed_check.setGravity(Gravity.END);
                    FontUtils.setArabic(proceed_check);
                    FontUtils.setArabic(total_txt);
                    proceed_check.setText("استمر");
                    total_txt.setText(" المجموع " + total(selected_products) + " ريال ");

                }
            } else {

                container.setVisibility(View.GONE);
            }
        }
    }

    private boolean containsObject(ArrayList<ProductsOrder> collection, ProductsOrder product) {
        for (ProductsOrder proList : collection) {
            if (proList.getProductId().equals(product.getProductId())) {
                position_array = collection.indexOf(proList);
                Log.d("size", position_array + " position");
                return true;
            }
        }
        return false;
    }

    private boolean removeObject(ArrayList<ProductsOrder> collection, int product) {
        for (ProductsOrder proList : collection) {
            if (proList.getProductId().equals(product)) {
                position_array = collection.indexOf(proList);
                Log.d("size", position_array + " position");
                return true;
            }
        }
        return false;
    }

    private double total(ArrayList<ProductsOrder> proList) {
        double i = 0;
        for (ProductsOrder pro : proList) {
            i = i + (Integer.parseInt(pro.getQuantity()) * Double.parseDouble(pro.getSwPrice()));
        }
        return i;
    }

    private String getSWPrice(Product product) {
        String price = "";
        if (product.isSeriver1()) {
            if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                price = product.getSwDrycleanPrice();
            } else {

            }
        } else if (product.isSeriver2()) {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                price = product.getSwWashingPrice();
            } else {

            }
        } else if (product.isSeriver3()) {
            if (!product.getSwPress().equalsIgnoreCase("-")) {
                price = product.getSwPress();
            } else {

            }
        }

        return price;
    }

    private String getPrice(Product product) {
        String price = "";
        if (product.isSeriver1()) {
            if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                price = product.getDrycleanPrice();
            } else {

            }
        } else if (product.isSeriver2()) {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                price = product.getWashingPrice();
            } else {

            }
        } else if (product.isSeriver3()) {
            if (!product.getSwPress().equalsIgnoreCase("-")) {
                price = product.getPress();
            } else {

            }
        }
        return price;
    }


    private String getService(Product product) {
        String service = "";
        if (product.isSeriver1()) {
            service = "DryClean & Press";
        } else if (product.isSeriver2()) {
            service = "Wash & Press";
        } else if (product.isSeriver3()) {
            service = "Only Press";
        }
        return service;
    }


}
