package user.com.smartwashr.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 1/3/18.
 */

public class HowPayActivity  extends AppCompatActivity {
    TextView title, inner;
    ImageView back, bback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.how_pay);

        title = (TextView) findViewById(R.id.title);
        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            FontUtils.setRobotoBold(title);
        } else {
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            title.setText("كيف أدفع للسائق ؟");
            FontUtils.setArabic(title);
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}

