package user.com.smartwashr.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.NewProductsAdapter;
import user.com.smartwashr.models.Category;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.models.productsresponse.ProductsResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewProducts extends AppCompatActivity implements ResponseHandler {

    private RecyclerView listView;
    private TextView title, total;
    private NewProductsAdapter adapter;
    private ArrayList<Product> products;
    private ArrayList<Integer> positionss;
    private ProductsResponse productsResponse;
    private ArrayList<Category> categories;
    private LinearLayout checkout_con;
    private EditText search_bar;
    private TextView proceed_check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_products);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (RecyclerView) findViewById(R.id.listView);
        title = (TextView) findViewById(R.id.title);
        total = (TextView) findViewById(R.id.total);
        search_bar = (EditText) findViewById(R.id.search);
        checkout_con = (LinearLayout) findViewById(R.id.checkout_con);
        proceed_check = (TextView) findViewById(R.id.proceed_check);
        ImageView back = (ImageView) findViewById(R.id.back);
        ImageView bback = (ImageView) findViewById(R.id.bback);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            title.setText("Products");
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
        } else {
            title.setText("منتجات");
            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            proceed_check.setText("استمر لإجراء الطلب");
        }

        products = new ArrayList<>();
        positionss = new ArrayList<>();
        categories = new ArrayList<>();
        listView.setLayoutManager(new LinearLayoutManager(NewProducts.this));
        makeCall();
        FontUtils.setFont(search_bar);
        adapter = new NewProductsAdapter(categories, products, positionss, NewProducts.this, total, checkout_con, search_bar);
        listView.setAdapter(adapter);

    }

    private void makeCall() {
        if (Internet.isAvailable(NewProducts.this)) {
            new RestCaller(NewProducts.this, SmartWashr.getRestClient().fetchProducts(), 1);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(NewProducts.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(NewProducts.this, false, "الرجاء الانتظار");
            }
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        productsResponse = (ProductsResponse) response.body();
        int pos = 0;
        positionss.add(pos);
        for (int i = 0; i < productsResponse.getCategories().size(); i++) {
            Category obj = productsResponse.getCategories().get(i);
            for (int j = 0; j < obj.getProduct().size(); j++) {
                Product pro = obj.getProduct().get(j);
                products.add(pro);
            }
            positionss.add(products.size());
            categories.add(obj);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewProducts.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
