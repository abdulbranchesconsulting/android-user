package user.com.smartwashr.activities.newActivities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.newAdapters.NewProductsAdapter;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.pricing.PricingResponse;
import user.com.smartwashr.newModels.pricing.Product;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;

public class NewSelectItems extends AppCompatActivity implements ResponseHandler {

    RecyclerView recyclerView;
    ArrayList<Product> list = new ArrayList<>();
    RelativeLayout rl_bill;
    NewProductsAdapter adapter;
    TextView tv_bill, tv_skip, title, txt_total;
    ImageView iv_forward;
    EditText et_search;

    android.app.AlertDialog.Builder alertDialogBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_selected_items);

        Constants.orderList.clear();

        alertDialogBuilder = new AlertDialog.Builder(this);

        recyclerView = findViewById(R.id.recyclerView);
        rl_bill = findViewById(R.id.rl_bill);
        title = findViewById(R.id.title);
        tv_skip = findViewById(R.id.tv_skip);
        iv_forward = findViewById(R.id.iv_forward);
        tv_bill = findViewById(R.id.tv_bill);
        et_search = findViewById(R.id.et_search);
        txt_total = findViewById(R.id.txt_total);
        adapter = new NewProductsAdapter(this, list, rl_bill, tv_bill, tv_skip);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        et_search.setHint(Constants.TRANSLATIONS.getSearchLabel());
        title.setText(Constants.TRANSLATIONS.getSelectItemLabel());
        tv_skip.setText(Constants.TRANSLATIONS.getOrderSkipButton());
        txt_total.setText(Constants.TRANSLATIONS.getTotalLabel());

        getPrices();

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    adapter.getFilter().filter(et_search.getText().toString());
                    return true;
                }
                return false;
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        iv_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewSelectItems.this, NewOrderReview.class));
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialogBuilder.setTitle(Constants.TRANSLATIONS.getAttentionLable());
                alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getPromptOrderSkipButton());
                alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent i = new Intent(NewSelectItems.this, NewOrderReview.class);
                        i.putExtra("skip", "skip");
                        startActivity(i);
                    }
                });
                alertDialogBuilder.create();
                alertDialogBuilder.show();


            }
        });

    }

    private void getPrices() {
        if (Internet.isAvailable(NewSelectItems.this)) {
            Loading.show(NewSelectItems.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            new RestCaller(NewSelectItems.this, SmartWashr.getRestClient().newProducts("Android"), 2);
        } else {
            Toast.makeText(this, Constants.TRANSLATIONS.getLabelPleaseCheckYourInternetConnection(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        list.clear();
        PricingResponse pricingResponse = (PricingResponse) response.body();
        for (int i = 0; i < pricingResponse.getData().size(); i++) {
            list.addAll(pricingResponse.getData().get(i).getProducts());
            Constants.products.addAll(list);
        }
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(NewSelectItems.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}