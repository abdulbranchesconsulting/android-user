package user.com.smartwashr.activities.newActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.loginFB.NewLoginResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

public class NewLogReg extends AppCompatActivity implements ResponseHandler {

    private Button login;
    private Button signup;
    private ImageView fbButton;
    private LoginButton login_fb;

    private ProfileTracker mProfileTracker;
    private CallbackManager callbackManager;
    private SessionManager sessionManager;

    private String email_fb, name, pic;
    private TextView continuewithfb;

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();

    private void findViews() {
        login = (Button) findViewById(R.id.login);
        signup = (Button) findViewById(R.id.signup);
        fbButton = (ImageView) findViewById(R.id.fbButton);
        login_fb = (LoginButton) findViewById(R.id.login_button);
        continuewithfb = findViewById(R.id.continuewithfb);

        login.setText(Constants.TRANSLATIONS.getLoginLabel());
        signup.setText(Constants.TRANSLATIONS.getSignupLabel());
        continuewithfb.setText(Constants.TRANSLATIONS.getContinueFacebook());

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_new_log_reg);

        sessionManager = new SessionManager(this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "user.com.smartwashr",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                Toast.makeText(this, ""+Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_SHORT).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        findViews();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(NewLogReg.this, NewLogin.class);
                startActivity(i);

            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(NewLogReg.this, NewSignUp.class);
                startActivity(i);

            }
        });


        login_fb.setReadPermissions(Arrays.asList(
                "public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();


        login_fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginResult.getAccessToken().getUserId();
                loginResult.getAccessToken().getToken();
                final AccessToken accessToken = loginResult.getAccessToken();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    email_fb = object.getString("email");
                                    Log.v("facebook - profile=data", email_fb);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");
                request.setParameters(parameters);
                request.executeAsync();

                if (Profile.getCurrentProfile() == null) {
                    Loading.show(NewLogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                            // profile2 is the new profile
                            pic = profile2.getProfilePictureUri(500, 500) + "";
                            name = profile2.getName();
                            SessionManager.put(Constants.USER_NAME, name);
                            SessionManager.put(Constants.USER_PIC, pic);
                            Log.v("facebook - profile", name + "\n" + pic);
                            mProfileTracker.stopTracking();
                            Log.d("login_success_1", "successful");
                            Runnable task = new Runnable() {
                                public void run() {
                                    loginFb(email_fb, name, pic);
                                }
                            };
                            worker.schedule(task, 3000, TimeUnit.MILLISECONDS);
                        }
                    };

                } else {
                    Loading.show(NewLogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    Profile profile = Profile.getCurrentProfile();
                    String id = profile.getId();
                    pic = profile.getProfilePictureUri(500, 500) + "";
                    name = profile.getName();
                    SessionManager.put(Constants.USER_NAME, name);
                    SessionManager.put(Constants.USER_PIC, pic);
                    Log.d("login_success_2", "successful");
                    Log.v("facebook", profile.getName() + "\n" + id + "\n" + pic);
                    Runnable task = new Runnable() {
                        public void run() {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Loading.show(NewLogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                            } else {
                                Loading.show(NewLogReg.this, false, "الرجاء الانتظار");
                            }
                            loginFb(email_fb, name, pic);
                        }
                    };
                    worker.schedule(task, 3000, TimeUnit.MILLISECONDS);

                }
            }


            @Override
            public void onCancel() {
                Toast.makeText(NewLogReg.this, "Login Canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(NewLogReg.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        fbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login_fb.performClick();
            }
        });

    }

    private void loginFb(String email, String name, String profile_pic) {
        if (Internet.isAvailable(NewLogReg.this)) {
            Loading.show(NewLogReg.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
            new RestCaller(NewLogReg.this, SmartWashr.getRestClient().newFbLogin(email, email, name, profile_pic, SessionManager.get(Constants.DEVICE_TOKEN)), 2);
        } else {
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();

        NewLoginResponse loginResponse = (NewLoginResponse) response.body();
        Log.d("response", new Gson().toJson(loginResponse));
        sessionManager.createLoginSession();
        SessionManager.put(Constants.ACCESS_TOKEN, "Bearer " + loginResponse.getData().getToken());
//        SessionManager.put(Constants.ACTIVE_ORDER, loginResponse.getData().getUser().);
        SessionManager.put(Constants.USER_ID, loginResponse.getData().getUser().getId().toString());
        SessionManager.put(Constants.USER_NAME, loginResponse.getData().getUser().getFirstName() + " " + loginResponse.getData().getUser().getLastName());
        SessionManager.put(Constants.EMAIL, loginResponse.getData().getUser().getEmail());
        SessionManager.put(Constants.USER_IMG, loginResponse.getData().getUser().getProfilePic());
        SessionManager.put(Constants.USER_PHONE, loginResponse.getData().getUser().getPhone());
        LoginManager.getInstance().logOut();
        Intent i = new Intent(NewLogReg.this, NewHomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        NewLogReg.this.finish();


    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewLogReg.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoginManager.getInstance().logOut();
    }
}
