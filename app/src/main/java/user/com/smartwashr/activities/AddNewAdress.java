package user.com.smartwashr.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.saveAddressRespnse.SaveResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by farazqureshi on 27/04/2018.
 */

public class AddNewAdress extends AppCompatActivity implements ResponseHandler {

    EditText et_nickName, et_address, et_phone, et_flat, et_delivery;
    TextView txt1, txt2;
    Button save;
    TextView title;
    ImageView back, bback;
    private SessionManager sessionManager;
    private boolean reviewed = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_address);

        sessionManager = new SessionManager(this);

        et_nickName = findViewById(R.id.et_nickname);
        et_address = findViewById(R.id.et_address);
        et_phone = findViewById(R.id.et_phone);
        et_flat = findViewById(R.id.et_flat);
        et_delivery = findViewById(R.id.et_del_ins);
        back = findViewById(R.id.back);
        title = findViewById(R.id.title);
        txt1 = findViewById(R.id.txt1);
        txt2 = findViewById(R.id.txt2);

        save = findViewById(R.id.save);

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        back = (ImageView) findViewById(R.id.back);
        bback = (ImageView) findViewById(R.id.bback);


        if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {

            back.setVisibility(View.GONE);
            bback.setVisibility(View.VISIBLE);
            et_nickName.setHint("اختار اسم");
//            txt1.setText("");
            et_flat.setHint("رقم الشقه / اسم المبنى");
            et_address.setHint("العنوان");
            et_phone.setHint("رقم الجوال");
            txt1.setText("حدد نوع العنوان مثل البيت، المكتب، فندق");
            txt2.setText("راح نستخدم رقمك عند الاستلام و التسليم");
            et_delivery.setHint("تعلیمات اخرى");
            FontUtils.setArabic(title);
            FontUtils.setArabic(et_nickName);
            FontUtils.setArabic(txt1);
            FontUtils.setArabic(et_flat);
            FontUtils.setArabic(et_address);
            FontUtils.setArabic(txt2);
            FontUtils.setArabic(et_phone);
            FontUtils.setArabic(et_delivery);

            if (Constants.address.getName() == null) {
                title.setText("ضيف موقع جديد");
            } else {
                title.setText("عدل موقعك");
                try {
                    et_nickName.setText(Constants.address.getName());
                    et_flat.setText(Constants.address.getBuildingName());
                    et_address.setText(Constants.address.getAddressLine1());
                    et_phone.setText(Constants.address.getPhone());
                    et_delivery.setText(Constants.address.getDeliveryInstructions());

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
            save.setText("احفظ");

        } else {
            back.setVisibility(View.VISIBLE);
            bback.setVisibility(View.GONE);
            FontUtils.setRobotoMediumFont(title);
            FontUtils.setRobotoMediumFont(et_nickName);
            FontUtils.setRobotoMediumFont(txt1);
            FontUtils.setRobotoMediumFont(et_flat);
            FontUtils.setRobotoMediumFont(et_address);
            FontUtils.setRobotoMediumFont(txt2);
            FontUtils.setRobotoMediumFont(et_phone);
            FontUtils.setRobotoMediumFont(et_delivery);
            FontUtils.setRobotoMediumFont(save);

            try {
                if (Constants.address.getName() != null) {
                    title.setText("Edit Address");

                    et_nickName.setText(Constants.address.getName());
                    et_flat.setText(Constants.address.getBuildingName());
                    et_address.setText(Constants.address.getAddressLine1());
                    et_phone.setText(Constants.address.getPhone());
                    et_delivery.setText(Constants.address.getDeliveryInstructions());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        et_address.setText(sessionManager.get(Constants.LANE1));

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_nickName.getText().toString().length() != 0) {
                    if (et_nickName.getText().toString().length() > 3) {
                        if (et_address.getText().toString().length() != 0) {
                            if (et_phone.getText().length() != 0) {
                                if (!reviewed) {
                                    disableView();
                                } else {
//                                    saveAddress();
                                }
                            } else {
                                if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                                    et_phone.setError("Please enter a Phone number");
                                else
                                    et_phone.setError("فضلاً ادخل رقم جوال");
                            }
                        } else {
                            if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                                et_address.setError("Please Enter your address");
                            else
                                et_address.setError("فضلاً ادخل عنوانك");
                        }
                    } else {
                        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                            et_nickName.setError("Please Enter a valid name");
                        else
                            et_nickName.setError("فضلاً ادخل اسمك صح");
                    }
                } else {
                    if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                        et_nickName.setError("Please Enter a name");
                    else
                        et_nickName.setError("فضلاً ادخل اسم صحيح");

                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        et_nickName.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || actionId == EditorInfo.IME_ACTION_NEXT
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            et_nickName.clearFocus();
                            et_flat.requestFocus();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });


        et_address.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || actionId == EditorInfo.IME_ACTION_NEXT
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            et_address.clearFocus();
                            et_phone.requestFocus();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });


        et_flat.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || actionId == EditorInfo.IME_ACTION_NEXT
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            et_flat.clearFocus();
                            et_address.requestFocus();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });


        et_phone.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || actionId == EditorInfo.IME_ACTION_NEXT
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            et_phone.clearFocus();
                            et_delivery.requestFocus();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });


        et_delivery.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || actionId == EditorInfo.IME_ACTION_NEXT
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            hideKeyboard(AddNewAdress.this);
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });
    }

    private void disableView() {
        reviewed = true;
        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            title.setText("Confirm Address");
            save.setText("Confirm");
        } else {
            title.setText("أكد موقعك");
            save.setText("أكد");
        }
        et_nickName.setEnabled(false);
        et_flat.setEnabled(false);
        et_address.setEnabled(false);
        et_phone.setEnabled(false);
        et_delivery.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        if (reviewed) {
            enableViews();
        } else {
            super.onBackPressed();
        }
    }

    private void enableViews() {
        reviewed = false;
        if (sessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            save.setText("Save");
            if (Constants.address.getName() != null)
                title.setText("Edit Address");
            else
                title.setText("Add Address");

        } else {
            if (Constants.address.getName() == null)
                title.setText("ضيف موقع جديد");
            else
                title.setText("عدل موقعك");
            save.setText("احفظ");

        }
        et_nickName.setEnabled(true);
        et_flat.setEnabled(true);
        et_address.setEnabled(true);
        et_phone.setEnabled(true);
        et_delivery.setEnabled(true);
    }

//    private void saveAddress() {
//
//        if (Internet.isAvailable(AddNewAdress.this)) {
//            if (Constants.address.getName() == null) {
//                new RestCaller(AddNewAdress.this, SmartWashr.getRestClient().saveAddress("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN),
//                        "application/json",
//                        et_nickName.getText().toString(),
//                        et_flat.getText().toString(),
//                        et_address.getText().toString(),
//                        "",
//                        et_phone.getText().toString(),
//                        et_delivery.getText().toString(),
//                        sessionManager.get(Constants.LATITUDE),
//                        sessionManager.get(Constants.LONGITUDE)), 1);
//            } else {
//                new RestCaller(AddNewAdress.this, SmartWashr.getRestClient().updateAddress("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN),
//                        "application/json",
//                        Constants.address.getId() + "",
//                        et_nickName.getText().toString(),
//                        et_flat.getText().toString(),
//                        et_address.getText().toString(),
//                        et_phone.getText().toString(),
//                        et_delivery.getText().toString(),
//                        sessionManager.get(Constants.LATITUDE),
//                        sessionManager.get(Constants.LONGITUDE)), 2);
//            }
////                    new RestCaller(AddNewAdress.this, SmartWashr.getRestClient().nearest_laundry("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), Constants.SAUDI_LNG, Constants.SAUDI_LAT), 1);
//            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
//                Loading.show(AddNewAdress.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
//            } else {
//                Loading.show(AddNewAdress.this, false, "الرجاء الانتظار");
//            }
//        } else {
//            Toast.makeText(this, "No Internet Connection Available", Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        SaveResponse saveResponse = (SaveResponse) response.body();
        if (saveResponse.getSuccess()) {

            if (reqCode == 1) {
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                    Toast.makeText(this, "Your address saved successfully", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "تم تسجيل الموقع بنجاح", Toast.LENGTH_SHORT).show();
            } else {
                Constants.address = null;
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english"))
                    Toast.makeText(this, "Your address updated successfully", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "تم حذف الموقعك بنجاح", Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(AddNewAdress.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        Toast.makeText(this, error.getErrors().get(0).getMessage(), Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddNewAdress.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
