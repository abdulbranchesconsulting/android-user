package user.com.smartwashr.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;

/**
 * Created by zeeshan on 5/31/17.
 */

public class ForgotPassword extends AppCompatActivity implements ResponseHandler {

    private EditText email;
    private Button submit;
    private ImageView back_btn;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.forget_pass);

        email = (EditText) findViewById(R.id.email);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        submit = (Button) findViewById(R.id.submit);

        email.setHint(Constants.TRANSLATIONS.getEmailLabel());
        submit.setText(Constants.TRANSLATIONS.getConfirmLabel());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().length() > 0) {
                    if (Internet.isAvailable(ForgotPassword.this)) {
                        if (getIntent().getStringExtra("resend") == null) {
                            new RestCaller(ForgotPassword.this, SmartWashr.getRestClient().forgetPass(email.getText().toString()), 1);
                            Loading.show(ForgotPassword.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                        } else {
                            new RestCaller(ForgotPassword.this, SmartWashr.getRestClient().resendEmail(email.getText().toString()), 2);
                            Loading.show(ForgotPassword.this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                        }
                    } else {
                        Toast.makeText(ForgotPassword.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ForgotPassword.this, "Please enter you email address", Toast.LENGTH_SHORT).show();
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        GenericResponse success = (GenericResponse) response.body();
        String msg = "";
        String title = "";

        if (reqCode == 1) {
            if (success.getError() != null) {
//                msg = success.getError().toString();
                msg = "Error";
                title = "Failure";
            } else {
                msg = success.getMsg().toString();
                title = "Success";
            }
        } else {
            if (success.getError() != null) {
                msg = "Error";
//                msg = success.getError().toString();
                title = "Failure";
            } else {
                msg = "We have sent an email to your account. Please check your email to verify your email";
                title = "Success";
            }
        }

        alertDialog = new AlertDialog.Builder(
                ForgotPassword.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
        alertDialog = new AlertDialog.Builder(
                ForgotPassword.this).create();
        alertDialog.setTitle("Failure");
        alertDialog.setMessage("Something went wrong, please try again in a while");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
