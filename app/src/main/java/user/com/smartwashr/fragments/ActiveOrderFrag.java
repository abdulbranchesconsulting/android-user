package user.com.smartwashr.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import user.com.smartwashr.R;

/**
 * Created by zeeshan on 6/1/17.
 */

public class ActiveOrderFrag extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.active_order, container, false);
        return set;
    }
}

