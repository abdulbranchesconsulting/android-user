package user.com.smartwashr.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import user.com.smartwashr.R;

/**
 * Created by zeeshan on 6/23/17.
 */

public class PricingURL extends Fragment {

    private WebView web;
    private String url = "https://www.smartwashr.com/#pricing";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.pricing_url, container, false);
        web = (WebView) set.findViewById(R.id.webView);
        final ProgressDialog progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Loading");
        progressBar.show();
        web.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressBar != null && progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }
        });
        web.getSettings().setLoadsImagesAutomatically(true);
        web.getSettings().setJavaScriptEnabled(true);
        web.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        web.loadUrl(url);

        return set;
    }
}
