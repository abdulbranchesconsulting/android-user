package user.com.smartwashr.fragments;

/**
 * Created by farazqureshi on 24/04/2018.
 */


import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import user.com.smartwashr.R;
import user.com.smartwashr.utils.AnimationUtility;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;


public class VoteFragment extends DialogFragment {
    private static MyClickListener myClickListener;

    public interface MyClickListener {
        public void onYes();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    View view;
    Activity activity;
    Handler handler;
    TextView tvYes;
    TextView tvNo, txt,txt1;
    SessionManager sessionManager;

    public VoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            view = inflater.inflate(R.layout.vote_fragment, container, false);
            handler = new Handler();
            sessionManager = new SessionManager(getActivity());
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);

            tvYes = (TextView) view.findViewById(R.id.tvYes);
            txt = view.findViewById(R.id.txt);
            txt1 = view.findViewById(R.id.txt1);
            tvNo = (TextView) view.findViewById(R.id.tvNo);

            if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                txt1.setText("معليش لسى ما وصلنه هنا");
                txt.setText("فين ساكن؟");
                tvNo.setText("لا");
                tvYes.setText("اختار حي جديد");
            }

                tvYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    myClickListener.onYes();
                }
            });

            tvNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismissWithAnimation();
                }
            });

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.findViewById(R.id.root).setVisibility(View.VISIBLE);
                    AnimationUtility.slideInDown(view.findViewById(R.id.root));
                }
            }, 300);
        } catch (Exception e) {
            Log.e("oncreate ", e.toString());
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                dismissWithAnimation();
            }
        };
    }

    private void dismissWithAnimation() {
        try {
            AnimationUtility.slideOutUp(view.findViewById(R.id.root));
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getDialog().dismiss();
                }
            }, 500);
        } catch (Exception e) {
            Log.e("oncreate ", e.toString());
            e.printStackTrace();
        }
    }

}
