package user.com.smartwashr.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/1/17.
 */

public class ChngPassFrag extends Fragment implements ResponseHandler {

    private EditText current, new_pass, confirm_pass;
    private Button submit;
    private SessionManager sessionManager;
    private AlertDialog alertDialog;
    private View parent;
    String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.change_pass, container, false);

        current = (EditText) set.findViewById(R.id.old_pass);
        new_pass = (EditText) set.findViewById(R.id.new_pass);
        confirm_pass = (EditText) set.findViewById(R.id.cpass);
        submit = (Button) set.findViewById(R.id.submit);
        parent = set.findViewById(R.id.parent);
        sessionManager = new SessionManager(getActivity());

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setFont(current);
            FontUtils.setFont(new_pass);
            FontUtils.setFont(confirm_pass);
            FontUtils.setFont(submit);
        } else {
            current.setHint("كلمة المرور الحالية");
            new_pass.setHint("كلمة المرور الجديدة");
            confirm_pass.setHint(getString(R.string.cpass_ar));
            submit.setText(getString(R.string.submit_ar));
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current.getText().length() > 0 && new_pass.getText().length() > 0 && confirm_pass.getText().length() > 0) {
                    if (Internet.isAvailable(getContext())) {
                        if (new_pass.getText().toString().matches(pattern)) {
                            if (!new_pass.getText().toString().equalsIgnoreCase(confirm_pass.getText().toString())) {
                                Snackbar.make(parent, "Password not match", Snackbar.LENGTH_LONG).show();
                            } else {
                                new RestCaller(ChngPassFrag.this, SmartWashr.getRestClient().changePassword("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), current.getText().toString(), new_pass.getText().toString()), 1);
                                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                    Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
                                } else {
                                    Loading.show(getActivity(), false, "الرجاء الانتظار");
                                }
                            }
                        } else {
                            alertDialog = new AlertDialog.Builder(
                                    getActivity()).create();
                            alertDialog.setTitle("Password requirements");
                            alertDialog.setMessage("* A digit must"
                                    + "\n* A lower case letter must"
                                    + "\n* An upper case letter must"
                                    + "\n* At least 8 characters"
                                    + "\n* No whitespace allowed in password");
                            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                            alertDialog.show();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return set;

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        GenericResponse genericResponse = (GenericResponse) response.body();
        String title, msg = "";
        if (genericResponse.getMsg() != null) {
            title = "Success";
            msg = genericResponse.getMsg().toString();
        } else {
            title = "Failure";
//            msg = genericResponse.getError().toString();
            msg = "Password not correct";
        }

        alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}

