package user.com.smartwashr.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.OnDataPass;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.ProductsOrder;
import user.com.smartwashr.adapter.ZeplinProductsAdapter;
import user.com.smartwashr.models.Category;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.models.productsresponse.ProductsResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;

/**
 * Created by zeeshan on 11/24/17.
 */

public class WomenProductsFragment extends Fragment implements ResponseHandler, OnDataPass {


    LinearLayoutManager linearLayoutManager = null;
    RecyclerView recyclerView;
    private ArrayList<Product> products;
    private ProductsResponse productsResponse;
    private ArrayList<Category> categories;
    private ZeplinProductsAdapter adapter;
    private TextView total;
    private LinearLayout checkout_con;
    private OnDataPass dataPasser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.frame, container, false);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) set.findViewById(R.id.recyler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        checkout_con = (LinearLayout) set.findViewById(R.id.checkout_con);

        products = new ArrayList<>();
        makeCall();

        adapter = new ZeplinProductsAdapter(products, getActivity(), dataPasser);
        recyclerView.setAdapter(adapter);

        return set;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    private void makeCall() {
        if (Internet.isAvailable(getActivity())) {
            new RestCaller(WomenProductsFragment.this, SmartWashr.getRestClient().fetchProducts(), 1);
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        productsResponse = (ProductsResponse) response.body();

        for (int i = 0; i < productsResponse.getCategories().get(2).getProduct().size(); i++) {
            Product pro = productsResponse.getCategories().get(2).getProduct().get(i);
            products.add(pro);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onDataPass(ProductsOrder selected_products) {
        dataPasser.onDataPass(selected_products);
    }

    @Override
    public void onDataPass(int product_id) {
        dataPasser.onDataPass(product_id);
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
