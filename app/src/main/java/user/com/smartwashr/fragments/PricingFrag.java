package user.com.smartwashr.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.ProductsAdapter;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.Category;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.models.productsresponse.ProductsResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/1/17.
 */

public class PricingFrag extends Fragment implements ResponseHandler {

    private CategoriesHandler categoriesHandler;
    private RecyclerView recyclerView;
    private ProductsAdapter adapter;
    private ArrayList<Category> categories;
    private ArrayList<Product> products;
    public static String cat_name = "";
    private Spinner mySpinner;
    private TextView swipe_txt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.pricing_frag, container, false);

        recyclerView = (RecyclerView) set.findViewById(R.id.recylerView);
        mySpinner = (Spinner) set.findViewById(R.id.category_selector);
        swipe_txt = (TextView) set.findViewById(R.id.swipe_txt);
        FontUtils.setFont(mySpinner);
        categories = new ArrayList<>();
        products = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoriesHandler = new CategoriesHandler(getContext());
//        if (categoriesHandler.getAllCategories().size() == 0) {
//            makeCall();
//        } else {
//            mySpinner.setVisibility(View.GONE);
//        }
        adapter = new ProductsAdapter(products, getActivity(), "", "yes");

        recyclerView.setAdapter(adapter);

        return set;
    }


    private void makeCall() {
        if (Internet.isAvailable(getActivity())) {
            new RestCaller(PricingFrag.this, SmartWashr.getRestClient().fetchProducts(), 1);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(getActivity(), false, "الرجاء الانتظار");
            }
        } else {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Toast.makeText(getActivity(), "No internet available", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
//        if (categories.size() == 0)
//            categories.addAll(categoriesHandler.getAllCategories());
        makeCall();
    }

    private void categorySelector(String str) {

        for (int i = 0; i < categories.size(); i++) {
            Category category = categories.get(i);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                if (category.getName().equalsIgnoreCase(str)) {
                    for (int j = 0; j < categories.get(i).getProduct().size(); j++) {
                        Product product = category.getProduct().get(j);
                        products.add(product);
                    }
                }
                adapter.notifyDataSetChanged();
            } else {
                if (category.getNameAr().equalsIgnoreCase(str)) {
                    for (int j = 0; j < categories.get(i).getProduct().size(); j++) {
                        Product product = category.getProduct().get(j);
                        products.add(product);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }

    }

    private void loadSpinnerData() {
        mySpinner.setVisibility(View.VISIBLE);
        List<String> lables = categoriesHandler.getAllLabels();
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item);
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            dataAdapter.add("Select Product");
        } else {
            dataAdapter.add("اختر منتج");
        }
        dataAdapter.addAll(lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mySpinner.setAdapter(dataAdapter);

        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                products.clear();
                adapter.notifyDataSetChanged();
                categorySelector(parent.getItemAtPosition(pos).toString());
                cat_name = parent.getItemAtPosition(pos).toString();
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    if (cat_name.contains("Select")) {
                        swipe_txt.setVisibility(View.VISIBLE);
                    } else {
                        swipe_txt.setVisibility(View.GONE);
                    }
                } else {
                    if (cat_name.equalsIgnoreCase("اختر منتج")) {
                        swipe_txt.setVisibility(View.VISIBLE);
                        swipe_txt.setText("اختر التصنيف لرؤية المنتجات");
                    } else {
                        swipe_txt.setVisibility(View.GONE);
                    }
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (categoriesHandler.getAllCategories().size() > 0) {
            categoriesHandler.deleteAllCategories();
            categoriesHandler.deleteLabels();
            categories.clear();
            adapter.notifyDataSetChanged();
        }
        ProductsResponse productsResponse = (ProductsResponse) response.body();
        for (int i = 0; i < productsResponse.getCategories().size(); i++) {
            Category obj = productsResponse.getCategories().get(i);
            categories.add(obj);
            categoriesHandler.insertCategory(obj);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                categoriesHandler.insertLabel(obj.getName());
            } else {
                categoriesHandler.insertLabel(obj.getNameAr());
            }
        }
        adapter.notifyDataSetChanged();
        loadSpinnerData();

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}