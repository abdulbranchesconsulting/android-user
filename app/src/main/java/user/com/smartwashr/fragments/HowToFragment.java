package user.com.smartwashr.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;

/**
 * Created by zeeshan on 6/1/17.
 */

public class HowToFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_Title = "title";
    private static final String ARG_SUB_Title = "sub_title";
    private static final String ARG_Descriptiom = "desc";
    private static final String ARG_Img = "img";

    // TODO: Rename and change types of parameters
    private String f_subTitle;
    private String f_title;
    private String fDescription;
    private int fImage;

    private OnFragmentInteractionListener mListener;

    public HowToFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param t_title     Parameter 1.
     * @param description Parameter 2.
     * @return A new instance of fragment IntroFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HowToFragment newInstance(String t_title, String subtitle, String description, int center_Icon) {
        HowToFragment fragment = new HowToFragment();
        Bundle args = new Bundle();
        args.putString(ARG_Title, t_title);
        args.putString(ARG_SUB_Title, subtitle);
        args.putString(ARG_Descriptiom, description);
        args.putInt(ARG_Img, center_Icon);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            f_title = getArguments().getString(ARG_Title);
            f_subTitle = getArguments().getString(ARG_SUB_Title);
            fDescription = getArguments().getString(ARG_Descriptiom);
            fImage = getArguments().getInt(ARG_Img);
        }
    }

    ImageView img;
    TextView title, subtitle, txtdescription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.intro_base, container, false);

        img = (ImageView) v.findViewById(R.id.back_img);
//        subtitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf"));
//        txtdescription.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf"));

        try {
            img.setImageResource(fImage);
        } catch (Exception e) {


        }


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
