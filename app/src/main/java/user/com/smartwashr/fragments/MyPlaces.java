package user.com.smartwashr.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.AddressAdapter;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.MapsActivity;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.addressesResponse.Address;
import user.com.smartwashr.models.addressesResponse.AddressesResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;


/**
 * Created by farazqureshi on 27/04/2018.
 */

public class MyPlaces extends Fragment implements ResponseHandler {

    public TextView no_data;
    RecyclerView recyclerView;
    FloatingActionButton fab;
    AddressAdapter adapter;
    ArrayList<Address> list;
    private boolean called = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.my_places, container, false);

        no_data = set.findViewById(R.id.no_data);
        recyclerView = set.findViewById(R.id.recyclerView);
        fab = set.findViewById(R.id.fab);
        list = new ArrayList<>();


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        adapter = new AddressAdapter(getActivity(), list, no_data);
        recyclerView.setAdapter(adapter);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), MapsActivity.class);
                i.putExtra("add_place", "1");
                startActivity(i);
            }
        });

        if (!called) {
            fetchAddresses();
        }

        return set;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void fetchAddresses() {
        called = true;
        if (Internet.isAvailable(getActivity())) {
            new RestCaller(MyPlaces.this, SmartWashr.getRestClient().getAddresses("Bearer " + SessionManager.get(Constants.ACCESS_TOKEN), "application/json"), 1);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(getActivity(), true, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(getActivity(), true, "الرجاء الانتظار");
            }
        } else {
            Toast.makeText(getActivity(), "No internet available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        called = false;
        Loading.cancel();

        list.clear();
        AddressesResponse res = (AddressesResponse) response.body();
        if (res.getSuccess()) {
            if (res.getAddresses().size() > 0) {
                no_data.setVisibility(View.GONE);
                list.addAll(res.getAddresses());
                adapter.notifyDataSetChanged();
            } else {
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.VISIBLE);
                    no_data.setText("لا يوجد مواقع مسجل سابقاً");
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();

    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }

}
