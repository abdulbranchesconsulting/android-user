package user.com.smartwashr.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.adapter.OrderDBAdapter;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.models.orderresponse.UserOrder;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/1/17.
 */

public class OrderHistoryFrag extends Fragment implements ResponseHandler {

    private CategoriesHandler ordersHandler;
    private RecyclerView recyclerView;
    private TextView no_orders, a_orders, o_history;
    private OrderDBAdapter adapter;
    private ArrayList<OrderResponse> orderResponses;
    private ArrayList<UserOrder> orders;
    private Gson gson;
    private View active_o, order_h;
    private String val = "0";
    private boolean called = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.order_history, container, false);
        recyclerView = set.findViewById(R.id.recyclerView);
        no_orders = set.findViewById(R.id.no_items);
        a_orders = set.findViewById(R.id.active_o);
        o_history = set.findViewById(R.id.order_h);
        active_o = set.findViewById(R.id.a_orders);
        order_h = set.findViewById(R.id.o_history);
        orderResponses = new ArrayList<>();
        orders = new ArrayList<>();
        gson = new Gson();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ordersHandler = new CategoriesHandler(getContext());

        adapter = new OrderDBAdapter(getActivity(), orders);
        recyclerView.setAdapter(adapter);

        if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            a_orders.setText("طلبات فاعلة");
            o_history.setText("طلبات سابقة");
            FontUtils.setArabic(a_orders);
            FontUtils.setArabic(o_history);
            FontUtils.setArabic(no_orders);
        } else {
            FontUtils.setRobotoBold(o_history);
            FontUtils.setRobotoBold(a_orders);
            FontUtils.setRobotoMediumFont(no_orders);
        }
        a_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                active_o.setVisibility(View.VISIBLE);
                order_h.setVisibility(View.GONE);
                if (Internet.isAvailable(getContext())) {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(getActivity(), false, "الرجاء الانتظار");
                    }
                    SessionManager sessionManager = new SessionManager(getActivity());
                    new RestCaller(OrderHistoryFrag.this, SmartWashr.getRestClient().fetchOrders("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN)), 1);
                }
            }
        });

        o_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                active_o.setVisibility(View.GONE);
                order_h.setVisibility(View.VISIBLE);
                if (Internet.isAvailable(getContext())) {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(getActivity(), false, "الرجاء الانتظار");
                    }
                    SessionManager sessionManager = new SessionManager(getActivity());
                    new RestCaller(OrderHistoryFrag.this, SmartWashr.getRestClient().deliveredOrders("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN)), 2);
                }
            }
        });

        return set;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!called) {
            fetchOrders();
        }
    }

    private void fetchOrders() {
        called = true;
        if (Internet.isAvailable(getContext())) {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
            } else {
                Loading.show(getActivity(), false, "الرجاء الانتظار");
            }
            SessionManager sessionManager = new SessionManager(getActivity());
            new RestCaller(OrderHistoryFrag.this, SmartWashr.getRestClient().fetchOrders("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN)), 1);
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        called=false;
        Loading.cancel();
        SessionManager sessionManager = new SessionManager(getActivity());
        orders.clear();
        orderResponses.clear();
        adapter.notifyDataSetChanged();

        OrderResponse orderResponse = (OrderResponse) response.body();
        String json = gson.toJson(orderResponse);
        Log.d("Response", json);
        sessionManager.put(Constants.ACTIVE_ORDER, orderResponse.getActiveorders());


        if (orderResponse.getUserOrder().size() > 0) {
            no_orders.setVisibility(View.GONE);
        } else {
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                FontUtils.setRobotoMediumFont(no_orders);
                no_orders.setText("Currently you have no orders.");
            } else {
                FontUtils.setArabic(no_orders);
                no_orders.setText("لا يوجد لديك طلبات حاليا");
            }
            no_orders.setVisibility(View.VISIBLE);
        }
        ordersHandler.deleteAllOrders();
        ordersHandler.insertOrder(orderResponse);
        ordersHandler.close();
        orderResponses.addAll(ordersHandler.getAllOrders());
        ordersHandler.close();

        for (int i = 0; i < orderResponses.size(); i++) {
            orderResponse = orderResponses.get(i);
            for (int j = 0; j < orderResponses.get(i).getUserOrder().size(); j++) {
                UserOrder userOrder = orderResponse.getUserOrder().get(j);
                orders.add(userOrder);
            }
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}

