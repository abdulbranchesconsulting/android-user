package user.com.smartwashr.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.UpdateResponse;
import user.com.smartwashr.models.User;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.PermissionManager;
import user.com.smartwashr.utils.SessionManager;

import static android.app.Activity.RESULT_OK;

/**
 * Created by zeeshan on 6/1/17.
 */

public class SettingsFrag extends Fragment implements ResponseHandler {
    private EditText name, phone;
    private ImageView imageView;
    private Button update;
    private SessionManager sessionManager;
    private PermissionManager permissionsManager;
    private String profilePic;
    private MultipartBody.Part dp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.settings_frag, container, false);

        sessionManager = new SessionManager(getActivity());

        permissionsManager = PermissionManager.getInstance(getActivity());
        permissionsManager.getPermissionifNotAvailble(new String[]{Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.READ_EXTERNAL_STORAGE}, 111);

        name = (EditText) set.findViewById(R.id.name);
        phone = (EditText) set.findViewById(R.id.phone);
        name.setText(sessionManager.get(Constants.USER_NAME));
        phone.setText(sessionManager.get(Constants.USER_PHONE));

        imageView = set.findViewById(R.id.profile_image);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePopup(v, 0);
            }
        });

        if (sessionManager.get(Constants.USER_IMG).length() > 0) {
            Picasso.with(getActivity()).load(sessionManager.get(Constants.USER_IMG))
                    .into(imageView);
        }

        update = (Button) set.findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Internet.isAvailable(getActivity())) {
                    Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    if (profilePic != null) {
                        File file = new File(profilePic);
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        dp =
                                MultipartBody.Part.createFormData("pic", file.getName(), requestFile);
//                        new RestCaller(SettingsFrag.this, SmartWashr.getRestClient().updateUser("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), name.getText().toString(), phone.getText().toString(), dp), 1);
                    } else {
//                        new RestCaller(SettingsFrag.this, SmartWashr.getRestClient().updateUserWithoutPic("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), name.getText().toString(), phone.getText().toString()
//                        ), 2);
                    }
                }
            }
        });

        if (!SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            update.setText("تحديث");
            name.setHint(getString(R.string.name_ar));
        }

        return set;
    }

    public void showImagePopup(View view, int i) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Please grant storage permission", Toast.LENGTH_SHORT).show();
            return;
        } else {
            // File System.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_PICK);
            // Chooser of file system options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose image");
            switch (i) {
                case 0:
                    startActivityForResult(chooserIntent, 0);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                return;
            }
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                switch (requestCode) {
                    case 0:
                        profilePic = cursor.getString(columnIndex);
                        Picasso.with(getActivity()).load(new File(profilePic))
                                .into(imageView);
                        break;
                }
                cursor.close();
            } else {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  Toast.makeText(ParentActivity.this, "Request Code => "+requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 111:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 2) {
            UpdateResponse updateResponse = (UpdateResponse) response.body();
            User user = (User) updateResponse.getUser();
            SessionManager.put(Constants.USER_NAME, user.getName());
            SessionManager.put(Constants.USER_PHONE, user.getPhoneNumber());
        } else {
            UpdateResponse updateResponse = (UpdateResponse) response.body();
            User user = (User) updateResponse.getUser();
            String username = user.getName();
            username = username.substring(1, username.length() - 1);

            SessionManager.put(Constants.USER_NAME, username);
            String s = user.getPhoneNumber();
            s = s.substring(1, s.length() - 1);
            SessionManager.put(Constants.USER_PHONE, s);
            SessionManager.put(Constants.USER_IMG, user.getProfilePic());
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}

