package user.com.smartwashr.fragments;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.HomeActivity;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.orderresponse.OrderResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.RippleEffect;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 5/31/17.
 */

public class HomeFragment extends Fragment implements ResponseHandler {

    private TextView title, activeOrder, youhave, activeOrders;
    private SessionManager sessionManager;
    private Button btn_order;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View set = inflater.inflate(R.layout.home_orders, container, false);

        sessionManager = new SessionManager(getActivity());
        title = (TextView) set.findViewById(R.id.title);
        activeOrder = (TextView) set.findViewById(R.id.active_order);
        youhave = (TextView) set.findViewById(R.id.you_have);
        activeOrders = (TextView) set.findViewById(R.id.active_orders);
        activeOrder.setText("" + sessionManager.getInt(Constants.ACTIVE_ORDER));
        btn_order = (Button) set.findViewById(R.id.order_now);
//        Calendar calendar = Calendar.getInstance();
//        if (Integer.parseInt(current_time) > 21) {
//            calendar.add(Calendar.DAY_OF_YEAR, 1);
//        } else {
//        calendar.add(Calendar.DAY_OF_YEAR, 2);
//        }
//        Date tomorrow = calendar.getTime();
//
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//        String ddate = dateFormat.format(tomorrow);
//        Log.d("tomorrow", ddate);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {

            FontUtils.setMARegular(title);
            FontUtils.setRobotoBold(youhave);
            FontUtils.setRobotoBold(activeOrders);
            FontUtils.setMARegular(btn_order);
        } else {
            FontUtils.setMARegular(title);
            FontUtils.setArabic(youhave);
            FontUtils.setArabic(activeOrders);
            FontUtils.setArabic(btn_order);
            youhave.setText(getString(R.string.youhave_ar));
            activeOrders.setText(getString(R.string.active_ar));
            btn_order.setText(getString(R.string.order_now_ar));
        }

        RippleEffect.applyRippleEffect(btn_order, "#bdbdbd");

        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (SessionManager.get(Constants.USER_PHONE).length() > 0 && SessionManager.get(Constants.USER_PHONE) != null) {
//                    getActivity().startActivity(new Intent(getActivity(), MapsActivity.class));
//                } else {
//                    getActivity().startActivity(new Intent(getActivity(), ProfileActivity.class));
//                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
//                        Toast.makeText(getActivity(), "Please enter phone number first", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getActivity(), "الرجاء إدخال رقم الهاتف أولا", Toast.LENGTH_SHORT).show();
//                    }
//                }
                ((HomeActivity) getActivity()).openFragment(new MyPlaces());
                ((HomeActivity) getActivity()).val = 2;
                ((HomeActivity) getActivity()).loadTitles();

            }
        });

        return set;
    }

    @Override
    public void onResume() {
        super.onResume();
//        makeCall();
    }

    private void makeCall() {
        if (Internet.isAvailable(getActivity())) {
            new RestCaller(HomeFragment.this, SmartWashr.getRestClient().fetchOrders("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN)), 5);
            Loading.show(getActivity(), false, Constants.TRANSLATIONS.getPleaseWaitLable());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  Toast.makeText(ParentActivity.this, "Request Code => "+requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 111:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        if (reqCode == 5) {
            OrderResponse orderResponse = (OrderResponse) response.body();
            sessionManager.put(Constants.ACTIVE_ORDER, orderResponse.getActiveorders());
            activeOrder.setText("" + sessionManager.getInt(Constants.ACTIVE_ORDER));
        }
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }



    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        Loading.cancel();
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}


