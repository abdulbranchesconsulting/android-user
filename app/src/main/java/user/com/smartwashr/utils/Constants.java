package user.com.smartwashr.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;

import user.com.smartwashr.models.addressesResponse.Address;
import user.com.smartwashr.models.settings.Translations;
import user.com.smartwashr.newModels.OrderProduct;
import user.com.smartwashr.newModels.address.Datum;
import user.com.smartwashr.newModels.orders.OrderResponse;
import user.com.smartwashr.newModels.pricing.Product;

/**
 * Created by zeeshan on 5/31/17.
 */

public class Constants {

    public static final boolean isDevelopment = false;

    public static final String ACCESS_TOKEN = "access_token";
    public static final String ACTIVE_ORDER = "active_order";
    public static final String USER_ID = "user_id";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
//    public static final String SERVER_IP = "http://181.214.204.190/api/v1/";

    public static final String SERVER_IP = "http://45.63.83.178/api/v1/";

    //    public static final String SERVER_IP = "http://45.77.5.116/public/api/v1/";
    public static final String DEV_IP = "http://development.smartwashr.com/api/v1/";
    public static final String PRODUCTS_ORDER = "productsorder";
    public static final String LANE1 = "lane1";
    public static final String LANE2 = "lane2";
    public static final String ZIP = "zipcode";
    public static final String COL_DATE = "col_date";
    public static final String COL_TIME = "col_time";
    public static final String DEL_TIME = "del_time";
    public static final String DEL_DATE = "del_date";

    public static final String SAUDI_LAT = "39.15531199999998";
    public static final String SAUDI_LNG = "21.5152891";
    public static final String DRIVER_ID = "driver_id";
    public static final String LAUNDRY_ID = "laundry_id";
    public static final String SELECTED_TIME = "0";
    public static final String SELECTED_DATE = "0";
    public static final String USER_NAME = "user_name";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_IMG = "user_img";
    public static final String LANG = "language";
    public static final String EMAIL = "email";
    public static final String USER_PIC = "user_pic";
    public static final String ORDER_ID = "order_id";
    public static final String DELIVERY_CHARGES = "del_charges";
    public static final String CITY = "city";
    public static final String TRANS = "trans";
    public static final String SORT_FEE = "sortfee";
    public static Datum ADDRESS = new Datum();

    public static Translations TRANSLATIONS = new Translations();

    public static String DBName = "smartwashr";
    public static String ORDERDB = "orders";
    public static int DBVersion = 1;

    public static String ADDRESS_ONE = "address_one";
    public static String VERSION = " 1.4.0";

    public static String DEVICE_TOKEN = "device_token";

    public static Address address = new Address();

    public static ArrayList<OrderProduct> orderList = new ArrayList<>();
    public static ArrayList<Product> products = new ArrayList<>();
    public static OrderResponse orderResponse = new OrderResponse();

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)
                        && ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{android.Manifest.permission.CAMERA,
                                            android.Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{android.Manifest.permission.CAMERA,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}
