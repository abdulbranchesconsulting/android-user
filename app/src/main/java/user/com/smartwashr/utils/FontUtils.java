package user.com.smartwashr.utils;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import user.com.smartwashr.SmartWashr;

/**
 * Created by zeeshan on 6/5/17.
 */

public class FontUtils {

    public static void applyTypeface(ViewGroup v, Typeface f) {

        if (v != null) {

            int vgCount = v.getChildCount();

            for (int i = 0; i < vgCount; i++) {

                if (v.getChildAt(i) == null) continue;

                if (v.getChildAt(i) instanceof ViewGroup) {

                    applyTypeface((ViewGroup) v.getChildAt(i), f);
                } else {
                    View view = v.getChildAt(i);

                    if (view instanceof TextView) {

                        ((TextView) (view)).setTypeface(f);

                    } else if (view instanceof EditText) {

                        ((EditText) (view)).setTypeface(f);

                    } else if (view instanceof Button) {

                        ((Button) (view)).setTypeface(f);
                    }
                }
            }
        }
    }


    public static void setFont(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/Raleway-Regular.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }

    public static void setRobotoMediumFont(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/RobotoMedium.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }

    public static void setRobotoBold(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/Roboto-Bold.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }


    public static void setArabic(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/Atrissi-sans-Book.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }

    public static void setRobotoRegular(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/RobotoRegular.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }


    public static void setNewFont(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/NewFont.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }

    public static void setMARegular(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/MontserratAlternates-Regular.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }


    public static void setAvenir(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/Avenir.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }

    public static void setMABold(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/MontserratAlternates-Bold.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }

    public static void setNewRegularFont(View v) {
        Typeface tf = Typeface.createFromAsset(SmartWashr.getInstance().getAssets(), "fonts/NewRegular.ttf");

        if (v instanceof TextView) {
            ((TextView) v).setTypeface(tf);
        }

    }


}
