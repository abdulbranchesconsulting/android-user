package user.com.smartwashr;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import user.com.smartwashr.activities.Login;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.newModels.HTMLResponse;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 1/3/18.
 */

public class TermsCondition extends AppCompatActivity implements ResponseHandler {
    TextView text, text_t, title;
    ImageView back;
    private boolean called = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms);

        back = (ImageView) findViewById(R.id.back);
        text = findViewById(R.id.text);
        text_t = findViewById(R.id.text_t);
        title = findViewById(R.id.title);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        title.setText(Constants.TRANSLATIONS.getTermsAndConditionsLabel());


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!called) {
            fetchHtml();
        }
    }

    private void fetchHtml() {
        called = true;
        Loading.show(this, false, Constants.TRANSLATIONS.getPleaseWaitLable());
        new RestCaller(TermsCondition.this, SmartWashr.getRestClient().fetchTerms(new SessionManager(this).get(Constants.LANG)), 1);
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        called = false;
        Loading.cancel();
        HTMLResponse html = (HTMLResponse) response.body();
        text_t.setText(html.getData().getTitle());
        text.setText(Html.fromHtml(html.getData().getDescription()));
    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        called = false;
        Loading.cancel();

    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        called = false;
        Loading.cancel();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TermsCondition.this);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
