package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

public class HomeVH extends RecyclerView.ViewHolder {

    ImageView img;
    TextView title, description;
    LinearLayout ll_parent;

    public HomeVH(View itemView) {
        super(itemView);

        ll_parent = itemView.findViewById(R.id.ll_parent);
        img = itemView.findViewById(R.id.mainImage);
        title = itemView.findViewById(R.id.heading1);
        description = itemView.findViewById(R.id.descriptor);

    }
}
