package user.com.smartwashr.adapter.newAdapters;

public class HomeModel {

    String title, description;
    int img;

    public HomeModel(String title, String description, int img) {
        this.title = title;
        this.description = description;
        this.img = img;
    }
}
