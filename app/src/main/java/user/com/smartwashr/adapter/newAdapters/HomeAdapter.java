package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.MapsActivity;
import user.com.smartwashr.activities.newActivities.NewPlacesActivity;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

public class HomeAdapter extends RecyclerView.Adapter<HomeVH> {

    Activity activity;
    ArrayList<HomeModel> list;

    public HomeAdapter(Activity activity, ArrayList<HomeModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public HomeVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        return new HomeVH(v);
    }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    @Override
    public void onBindViewHolder(HomeVH holder, final int position) {
        HomeModel item = list.get(position);

        holder.title.setText(item.title);
        holder.description.setText(item.description);
        holder.img.setImageResource(item.img);

        if (position == 0) {
            if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
                setMargins(holder.ll_parent, 50, 0, 0, 0);
            } else {
                setMargins(holder.ll_parent, 0, 0, 50, 0);
            }
        } else if (position == 1) {
            if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en"))
                holder.description.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
        } else if (position == 3) {
            if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
                setMargins(holder.ll_parent, 0, 0, 50, 0);
            } else {
                setMargins(holder.ll_parent, 50, 0, 0, 0);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 0) {
                    activity.startActivity(new Intent(activity, MapsActivity.class));
                } else if (position == 1) {
                    Intent i = new Intent(activity, MapsActivity.class);
                    i.putExtra("skip", "skip");
                    activity.startActivity(i);
                } else if (position == 2) {
                    Intent i = new Intent(activity, MapsActivity.class);
                    i.putExtra("skip", "skip");
                    activity.startActivity(i);
                } else if (position == 3) {
                    activity.startActivity(new Intent(activity, NewPlacesActivity.class));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
