package user.com.smartwashr.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.OrderPlacement;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/1/17.
 */

public class OrderAdapter extends RecyclerView.Adapter<ProductView> {

    private ArrayList<ProductsOrder> lst;
    private Activity activity;
    private View.OnClickListener listener;
    private ArrayList<Integer> total_list;
    private TextView textView_total;
    private TextView textView_subTotal;

    public OrderAdapter(Activity activity, ArrayList<ProductsOrder> list, View.OnClickListener clicklistener, TextView textView, TextView total) {
        lst = list;
        this.activity = activity;
        listener = clicklistener;
        this.textView_total = textView;
        this.textView_subTotal = total;
    }

    @Override
    public ProductView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_product_view, parent, false);
        total_list = new ArrayList<>();
        return new ProductView(itemView);
    }

    private ProductsOrder getObject(int i) {
        return (lst.get(i));
    }

    @Override
    public void onBindViewHolder(final ProductView holder, final int position) {
        FontUtils.setFont(holder.getTotal_num());
        FontUtils.setFont(holder.getTxt_title());
        FontUtils.setFont(holder.getTotal());
        holder.getTotal_num().setText(getObject(position).getQuantity().toString() + "x");
        try {
            holder.getTxt_title().setText(getObject(position).getName().toString());
        } catch (NullPointerException e) {

        }
        holder.getTotal().setText(getObject(position).getTotal().toString());
//        try {
//            Picasso.with(activity).load(getObject(position).getUrl().toString()).fit().into(holder.getImg());
//        } catch (NullPointerException e) {
//
//        }

        holder.getProduct().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager sessionManager = new SessionManager(activity);
                Intent i = new Intent(activity, OrderPlacement.class);
                i.putExtra("products_edit", getObject(position));
                i.putExtra("list_position", position);
                lst.remove(position);
                String list_string = new Gson().toJson(lst);
                sessionManager.put(Constants.PRODUCTS_ORDER, list_string);
                activity.startActivity(i);
            }
        });

//        holder.getCross().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SessionManager sessionManager = new SessionManager(activity);
//                lst.remove(position);
//                if (total_list.size() != 0) {
//                    total_list.clear();
//                }
//                String list_string = new Gson().toJson(lst);
//                sessionManager.put(Constants.PRODUCTS_ORDER, list_string);
//                notifyDataSetChanged();
//                if (lst.size() == 0) {
//                    total_list.add(0);
//                } else {
//                    for (int i = 0; i < lst.size(); i++) {
//                        total_list.add(Integer.parseInt(lst.get(i).getTotal()));
//                    }
//                }

//                doButtonOneClickActions(OrderDone.total_price, sum(total_list));
//                textView_total.setText("SAR " + sum(total_list));
//                if (!textView_total.getText().toString().equalsIgnoreCase("SAR " + 0)) {
//                    textView_subTotal.setText("SAR " + (sum(total_list) + 15));
//                } else {
//                    textView_subTotal.setText("SAR " + 0);
//                }
//            }
//        });
    }


    public static int sum(List<Integer> list) {
        int sum = 0;
        for (int i : list) {
            sum += i;
        }
        return sum;
    }

    @Override
    public int getItemCount() {
        return lst.size();
    }


    private void editDialogue(final ProductView holder, final String quantity, final String price, final int position) {
        final EditText taskEditText = new EditText(activity);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10, 0, 10, 0);
        taskEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        taskEditText.setText(quantity);
        taskEditText.setLayoutParams(lp);

        AlertDialog dialog = new AlertDialog.Builder(activity)
                .setTitle("Add a new quantity")
                .setMessage("Edit quantity")
                .setView(taskEditText)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int nums = Integer.parseInt(taskEditText.getText().toString());
                        int price_value = Integer.parseInt(price);
                        lst.get(position).setQuantity(nums + "");
                        lst.get(position).setTotal(nums * price_value + "");
                        holder.getTotal_num().setText(lst.get(position).getQuantity());
                        holder.getTotal().setText(lst.get(position).getTotal());
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

}