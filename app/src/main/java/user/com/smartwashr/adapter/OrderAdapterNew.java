package user.com.smartwashr.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.ZeplinOrderDone;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/1/17.
 */

public class OrderAdapterNew extends RecyclerView.Adapter<ProductView> {

    private ArrayList<ProductsOrder> lst;
    private Activity activity;
    private ArrayList<Integer> total_list;
    TextView sub_total, final_total;
    boolean applied;

    public OrderAdapterNew(Activity activity, ArrayList<ProductsOrder> list, TextView s_total, TextView f_total, boolean applied) {
        lst = list;
        this.activity = activity;
        this.sub_total = s_total;
        this.final_total = f_total;
        this.applied = applied;
    }

    @Override
    public ProductView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_product_view, parent, false);
        total_list = new ArrayList<>();
        return new ProductView(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductView holder, final int position) {
        ProductsOrder productsOrder = lst.get(position);
        holder.getTotal_num().setText(productsOrder.getQuantity() + "x");
        try {
            holder.getTxt_title().setText(productsOrder.getName().toString());
        } catch (NullPointerException e) {

        }
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            holder.getTotal().setText("SAR " + productsOrder.getTotal());
            if (!((ZeplinOrderDone)activity).couponApplied) {
                sub_total.setText(total(lst) + "");
                final_total.setText("SAR " + (total(lst) + Double.parseDouble(SessionManager.get(Constants.DELIVERY_CHARGES))));
            }
            FontUtils.setRobotoMediumFont(holder.getTotal_num());
            FontUtils.setRobotoMediumFont(holder.getTxt_title());
            FontUtils.setRobotoMediumFont(holder.getTotal());
        } else {
            FontUtils.setArabic(holder.getTotal_num());
            FontUtils.setArabic(holder.getTxt_title());
            FontUtils.setArabic(holder.getTotal());
            FontUtils.setArabic(sub_total);
            FontUtils.setArabic(final_total);
            final_total.setGravity(Gravity.START);
            holder.getTotal().setText(productsOrder.getTotal() + " ريال ");
            if (!((ZeplinOrderDone)activity).couponApplied) {
                sub_total.setText(total(lst) + "");
                final_total.setText((total(lst) + Double.parseDouble(SessionManager.get(Constants.DELIVERY_CHARGES))) + " ريال ");
            }
        }

    }

    private double total(ArrayList<ProductsOrder> proList) {
        double i = 0;
        for (ProductsOrder pro : proList) {
            i = i + (Integer.parseInt(pro.getQuantity()) * Double.parseDouble(pro.getSwPrice()));
        }
        return i;
    }

    @Override
    public int getItemCount() {
        return lst.size();
    }

}