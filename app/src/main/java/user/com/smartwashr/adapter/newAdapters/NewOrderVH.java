package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;

public class NewOrderVH extends RecyclerView.ViewHolder {

    public TextView tvOrderid, tv_order_lable,tv_pickup, tv_cost;
    public ImageView ivOrderCompleted;
    public TextView tvDate;
    public TextView tvPrice;

    public NewOrderVH(View itemView) {
        super(itemView);
        tvOrderid = (TextView) itemView.findViewById(R.id.tv_orderid);
        ivOrderCompleted = (ImageView) itemView.findViewById(R.id.iv_order_completed);
        tvDate = (TextView) itemView.findViewById(R.id.tv_date);
        tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
        tv_cost = itemView.findViewById(R.id.tv_cost);
        tv_pickup = itemView.findViewById(R.id.tv_pickup);
        tv_order_lable =itemView.findViewById(R.id.tv_order_lable);
    }
}
