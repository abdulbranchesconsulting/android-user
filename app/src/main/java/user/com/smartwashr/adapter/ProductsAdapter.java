package user.com.smartwashr.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.OrderPlacement;
import user.com.smartwashr.db.CategoriesHandler;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/6/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<CategoryView> {

    private ArrayList<Product> lst;
    private View.OnClickListener listener;
    private Activity activity;
    private String cat;
    private String visible;
    private CategoriesHandler categoriesHandler;

    public ProductsAdapter(ArrayList<Product> lst, Activity activity, String value, String visible) {
        this.lst = lst;
        this.activity = activity;
        this.cat = value;
        this.visible = visible;
    }

    @Override
    public CategoryView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_view, parent, false);
        return new CategoryView(itemView);
    }

    private Product getObject(int i) {
        return (lst.get(i));
    }

    @Override
    public void onBindViewHolder(CategoryView holder, final int position) {
        holder.getNew_parent().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, OrderPlacement.class);
                ProductsOrder productsOrder = new ProductsOrder();
                productsOrder.setCategory(SessionManager.get("CAT"));
                productsOrder.setProductId(Integer.parseInt(getObject(position).getId().toString()));
                if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                    productsOrder.setName(getObject(position).getName());
                } else {
                    productsOrder.setName(getObject(position).getNameAr());
                }
                productsOrder.setSw_dryclean(getObject(position).getSwDrycleanPrice());
                productsOrder.setSw_wash_press(getObject(position).getSwWashingPrice());
                productsOrder.setSw_press(getObject(position).getSwPress());
                productsOrder.setDry_price(getObject(position).getDrycleanPrice());
                productsOrder.setWash_price(getObject(position).getWashingPrice());
                productsOrder.setPress_price(getObject(position).getPress());
//                productsOrder.set
//                productsOrder.setPrice(getObject(position).);
                productsOrder.setUrl(getObject(position).getPicture().toString());
                i.putExtra("product", productsOrder);
                activity.startActivity(i);
            }
        });
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            holder.getTxt_title().setText(getObject(position).getName());
            FontUtils.setFont(holder.getTxt_title());
            FontUtils.setFont(holder.getTxt_Desc());
        } else {
            holder.getTxt_title().setText(getObject(position).getNameAr());
        }
        if (visible.length() != 0) {
            holder.getPricing_con().setVisibility(View.VISIBLE);
            holder.getTxt_Desc().setVisibility(View.GONE);
            holder.getNew_parent().setClickable(false);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                holder.getDry_p().setText("SAR " + getObject(position).getSwDrycleanPrice());
                holder.getWash_p().setText("SAR " + getObject(position).getSwWashingPrice());
                holder.getPress_p().setText("SAR " + getObject(position).getSwPress());
                holder.getTxt_Desc().setText("SAR " + getObject(position).getSwWashingPrice());
            } else {
                holder.dryclean.setText("غسيل جاف مع الكي");
                holder.washing.setText("غسيل عادي");
                holder.press.setText("كي");
                holder.getDry_p().setText(getObject(position).getSwDrycleanPrice() + " ريال ");
                holder.getWash_p().setText(getObject(position).getSwWashingPrice() + " ريال ");
                holder.getPress_p().setText(getObject(position).getSwPress() + " ريال ");
                holder.getTxt_Desc().setText(getObject(position).getSwWashingPrice() + " ريال");
            }
        } else {
            holder.getPricing_con().setVisibility(View.GONE);
            holder.getNew_parent().setClickable(true);
            holder.getTxt_Desc().setVisibility(View.GONE);
        }
//        holder.getImg().setImageBitmap(BitmapFactory.decodeFile(getObject(position).getProduct().get(position).getPicture().toString()));
        if (getObject(position).getPicture().toString().equalsIgnoreCase("https://www.smartwashr.com/img/smartwashr_logo.png")) {
            Picasso.with(activity).load(R.drawable.logo_icon).into(holder.getImg());
        } else {
            Picasso.with(activity).load(getObject(position).getPicture().toString()).fit().into(holder.getImg());
        }
    }

    @Override
    public int getItemCount() {
        return lst.size();
    }

}
