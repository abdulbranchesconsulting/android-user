package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

public class PlaceVH extends RecyclerView.ViewHolder {

    TextView tv_title, tv_address;
    public RelativeLayout viewBackground, viewForeground;

    public PlaceVH(View itemView) {
        super(itemView);

        tv_title = itemView.findViewById(R.id.tv_title);
        tv_address = itemView.findViewById(R.id.tv_address);
        viewBackground = itemView.findViewById(R.id.view_background);
        viewForeground = itemView.findViewById(R.id.view_foreground);
    }
}
