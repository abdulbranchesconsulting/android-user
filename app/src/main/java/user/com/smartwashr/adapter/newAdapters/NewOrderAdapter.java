package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.newActivities.NewOrderDetail;
import user.com.smartwashr.newModels.orders.OrderResponse;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

public class NewOrderAdapter extends RecyclerView.Adapter<NewOrderVH> {

    Activity activity;
    ArrayList<OrderResponse> list;

    public NewOrderAdapter(Activity activity, ArrayList<OrderResponse> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public NewOrderVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_order_item, parent, false);
        return new NewOrderVH(v);
    }

    @Override
    public void onBindViewHolder(NewOrderVH holder, int position) {
        final OrderResponse item = list.get(position);

        holder.tv_order_lable.setText(Constants.TRANSLATIONS.getOrderNumberLabel());
        holder.tv_pickup.setText(Constants.TRANSLATIONS.getPickupDateLabel());
        holder.tv_cost.setText(Constants.TRANSLATIONS.getCostLabel());
        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
            holder.tvOrderid.setText(" "+item.getInvoiceNum());
            holder.tvPrice.setText(Constants.TRANSLATIONS.getCurrency() + " " + item.getTotal());
        } else {
            holder.tvOrderid.setText(item.getInvoiceNum() + " # ");
            holder.tvPrice.setText(item.getTotal() + " " + Constants.TRANSLATIONS.getCurrency());
        }
//        holder.tvDate.setText(getFormattedTime(item.getPickupTime()));
        holder.tvDate.setText(item.getPickupTime());

        if (item.getOrderstatusId() != 8) {
            holder.ivOrderCompleted.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.orderResponse = item;
                activity.startActivity(new Intent(activity, NewOrderDetail.class));
            }
        });
    }

    public static String getFormattedTime(String time) {
        String displayValue = time;
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormatter.parse(time);

// Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("EEE dd MMM, yyyy");
            displayValue = timeFormatter.format(date);
            return displayValue;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayValue;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
