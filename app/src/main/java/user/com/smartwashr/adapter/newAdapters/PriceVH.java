package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import user.com.smartwashr.R;

public class PriceVH extends RecyclerView.ViewHolder{

    TextView tv_title;
    RecyclerView item_recycler;

    public PriceVH(View itemView) {
        super(itemView);
        tv_title = itemView.findViewById(R.id.tv_title);
        item_recycler = itemView.findViewById(R.id.item_recycler);
    }
}
