package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.pricing.Service;

public class NewPriceServiceAdapter extends RecyclerView.Adapter<ServiceVH> {

    Activity activity;
    List<Service> list;
    int p_id;

    public NewPriceServiceAdapter(Activity activity, List<Service> list, Integer id) {
        this.activity = activity;
        this.list = list;
        this.p_id = id;
    }

    @Override
    public ServiceVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_price, parent, false);
        return new ServiceVH(v);
    }

    @Override
    public void onBindViewHolder(ServiceVH holder, int position) {
        final Service item = list.get(position);

        holder.tv_serviceName.setText(item.getName());
        holder.tv_servicePrice.setText("SAR " + item.getPrice());

        if(item.isSelected()){
            holder.img.setVisibility(View.VISIBLE);
        }else {
            holder.img.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.isSelected()){
                    item.setSelected(false);
                    notifyDataSetChanged();
                }else {
                    item.setSelected(true);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
