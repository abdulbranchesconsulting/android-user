package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

/**
 * Created by zeeshan on 6/25/17.
 */

class DetailView extends RecyclerView.ViewHolder {

    TextView total_num, txt_title, total, service;
    private String img_url;
    LinearLayout product;
    TextView ar_total_num, ar_txt_title, ar_total, ar_service;
    ImageView ar_img;
    String ar_img_url;
    LinearLayout ar_parent;

    public DetailView(View itemView) {
        super(itemView);

        txt_title = (TextView) itemView.findViewById(R.id.product);
        total_num = (TextView) itemView.findViewById(R.id.quantity);
        total = (TextView) itemView.findViewById(R.id.price);
        product = (LinearLayout) itemView.findViewById(R.id.en_parent);
        img_url = "";

        ar_txt_title = (TextView) itemView.findViewById(R.id.ar_product);
        ar_total_num = (TextView) itemView.findViewById(R.id.ar_quantity);
        ar_total = (TextView) itemView.findViewById(R.id.ar_price);
        ar_parent = (LinearLayout) itemView.findViewById(R.id.arabic_parent);
        ar_img_url = "";

    }

    public LinearLayout getProduct() {
        return product;
    }

    public void setProduct(LinearLayout product) {
        this.product = product;
    }

    public LinearLayout getAr_parent() {
        return ar_parent;
    }

    public void setAr_parent(LinearLayout ar_parent) {
        this.ar_parent = ar_parent;
    }
}