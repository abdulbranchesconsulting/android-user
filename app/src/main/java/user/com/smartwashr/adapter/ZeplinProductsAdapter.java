package user.com.smartwashr.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import user.com.smartwashr.OnDataPass;
import user.com.smartwashr.R;
import user.com.smartwashr.models.Category;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 11/22/17.
 */

public class ZeplinProductsAdapter extends RecyclerView.Adapter<ZeplinProductsView> implements OnDataPass {

    ArrayList<Product> products;
    ArrayList<Category> categories;
    Activity activity;
    ArrayList<ProductsOrder> selected_products = new ArrayList<>();
    private int position_array;
    TextView total_txt;
    ArrayList<Product> original_list = new ArrayList<>();
    private OnDataPass dataPasser;

    boolean isMultiSelect = false;


    public ZeplinProductsAdapter(ArrayList<Product> products, Activity activity, OnDataPass ondataPasser) {
        this.products = products;
        this.activity = activity;
        this.original_list = products;
        this.dataPasser = ondataPasser;
    }

    @Override
    public ZeplinProductsView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.zeplin_order_placement, parent, false);
        return new ZeplinProductsView(itemView);
    }

    @Override
    public void onBindViewHolder(final ZeplinProductsView holder, final int position) {
        final int posi = position;
        final Product product = products.get(position);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            holder.product_name.setText(product.getName());
            holder.price1.setText("SAR " + product.getSwDrycleanPrice());
            holder.price2.setText("SAR " + product.getSwWashingPrice());
            holder.price3.setText("SAR " + product.getSwPress());
        } else {
            FontUtils.setArabic(holder.product_name);
            FontUtils.setArabic(holder.price1);
            FontUtils.setArabic(holder.price2);
            FontUtils.setArabic(holder.price3);
            FontUtils.setArabic(holder.service1);
            FontUtils.setArabic(holder.service2);
            FontUtils.setArabic(holder.service3);
            holder.product_name.setText(product.getNameAr());
            holder.service1.setGravity(Gravity.END);
            holder.service2.setGravity(Gravity.END);
            holder.service3.setGravity(Gravity.END);
            holder.service1.setText(product.getSwDrycleanPrice() + " ريال ");
            holder.service2.setText(product.getSwWashingPrice() + " ريال ");
            holder.service3.setText(product.getSwPress() + " ريال ");
            holder.price1.setGravity(Gravity.START);
            holder.price2.setGravity(Gravity.START);
            holder.price3.setGravity(Gravity.START);
            holder.price1.setText("غسيل جاف مع الكي");
            holder.price2.setText("غسيل عادي مع الكي");
            holder.price3.setText("كي فقط");
        }
        holder.service_con1.setSelected(product.isSelected());
        holder.service_con1.setTag(product);

        holder.service_con2.setSelected(product.isSelected());
        holder.service_con2.setTag(product);

        holder.service_con3.setSelected(product.isSelected());
        holder.service_con3.setTag(product);

        if (product.isSeriver1()) {
            setColored(posi, holder);
        } else if (product.isSeriver2()) {
            setColored(posi, holder);
        } else if (product.isSeriver3()) {
            setColored(posi, holder);
        } else {
            setColorTransparent(holder);
        }

        if (!product.isQuantity()) {
            holder.quantity.setText("0");
        } else {
            holder.quantity.setText(product.getQuantity());
        }

        holder.add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (product.isSeriver1()) {
                        String text = holder.quantity.getText().toString();
                        int i = Integer.parseInt(text) + 1;
                        holder.quantity.setText("" + i);
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else if (product.isSeriver2()) {
                        String text = holder.quantity.getText().toString();
                        int i = Integer.parseInt(text) + 1;
                        holder.quantity.setText("" + i);
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else if (product.isSeriver3()) {
                        String text = holder.quantity.getText().toString();
                        int i = Integer.parseInt(text) + 1;
                        holder.quantity.setText("" + i);
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(activity, "Please select it first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.neg_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (product.isSeriver1()) {
                        String text = holder.quantity.getText().toString();
                        if (Integer.parseInt(text) > 1) {
                            int i = Integer.parseInt(text) - 1;
                            holder.quantity.setText("" + i);
                            product.setQuantity(true);
                            product.setQuantity(holder.quantity.getText().toString());
                            add_remove(product, holder.quantity.getText().toString());
                            notifyDataSetChanged();
                        } else {

                        }
                    } else if (product.isSeriver2()) {
                        String text = holder.quantity.getText().toString();
                        if (Integer.parseInt(text) > 1) {
                            int i = Integer.parseInt(text) - 1;
                            holder.quantity.setText("" + i);
                            product.setQuantity(true);
                            product.setQuantity(holder.quantity.getText().toString());
                            add_remove(product, holder.quantity.getText().toString());
                            notifyDataSetChanged();
                        } else {

                        }
                    } else if (product.isSeriver3()) {
                        String text = holder.quantity.getText().toString();
                        if (Integer.parseInt(text) > 1) {
                            int i = Integer.parseInt(text) - 1;
                            holder.quantity.setText("" + i);
                            product.setQuantity(true);
                            product.setQuantity(holder.quantity.getText().toString());
                            add_remove(product, holder.quantity.getText().toString());
                            notifyDataSetChanged();
                        } else {

                        }
                    }
                } else {
                    Toast.makeText(activity, "Please select it first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.service_con1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                    LinearLayout cb = (LinearLayout) v;
                    Product emp = (Product) cb.getTag();
                    if (!emp.isSelected()) {
                        cb.setSelected(true);
                        emp.setSelected(cb.isSelected());
                        products.get(posi).setSelected(cb.isSelected());
                    } else {
                        cb.setSelected(false);
                        products.get(posi).setSelected(false);
                    }

                    if (cb.isSelected()) {
                        ProductsOrder productsOrder = new ProductsOrder();
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            productsOrder.setName(product.getName());
                        } else {
                            productsOrder.setName(product.getNameAr());
                        }
                        selectProduct(holder, product);
                        product.setSeriver1(true);
                        product.setSeriver2(false);
                        product.setSeriver3(false);
                        holder.quantity.setText("1");
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        productsOrder.setQuantity(holder.quantity.getText().toString());
                        productsOrder.setSwPrice(getSWPrice(product));
                        productsOrder.setPrice(getPrice(product));
                        productsOrder.setService(getService(product));
                        productsOrder.setProductId(product.getId());
                        productsOrder.setProduct_img(product.getPicture().toString());
                        productsOrder.setTotal((Integer.parseInt(holder.quantity.getText().toString()) * Double.parseDouble(getSWPrice(product))) + "");
                        Gson gson = new Gson();
                        selected_products.add(productsOrder);
                        add_remove(product, holder.quantity.getText().toString());
                        String json = gson.toJson(selected_products);
                        Log.d("size", selected_products.size() + "\n" + json);
                        Log.d("size", total(selected_products) + " TOTAL");
                        setColor(holder, 0);
                        notifyDataSetChanged();
                    } else {
                        if (containsObject(selected_products, product)) {
                            selected_products.remove(position_array);
                            onDataPass(product.getId());
                            product.setSeriver1(false);
                            product.setSeriver2(false);
                            product.setSeriver3(false);
                            product.setQuantity(false);
                            setColorTransparent(holder);
                            notifyDataSetChanged();
                            Log.d("size", selected_products.size() + "");
                        }
                    }

                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(activity, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        holder.service_con2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                    LinearLayout cb = (LinearLayout) v;
                    Product emp = (Product) cb.getTag();
                    if (!emp.isSelected()) {
                        cb.setSelected(true);
                        emp.setSelected(cb.isSelected());
                        products.get(posi).setSelected(cb.isSelected());
                    } else {
                        cb.setSelected(false);
                        products.get(posi).setSelected(false);
                    }

                    if (cb.isSelected()) {
                        ProductsOrder productsOrder = new ProductsOrder();
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            productsOrder.setName(product.getName());
                        } else {
                            productsOrder.setName(product.getNameAr());
                        }
                        selectProduct(holder, product);
                        product.setSeriver1(false);
                        product.setSeriver3(false);
                        product.setSeriver2(true);
                        holder.quantity.setText("1");
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        productsOrder.setQuantity(holder.quantity.getText().toString());
                        productsOrder.setSwPrice(getSWPrice(product));
                        productsOrder.setPrice(getPrice(product));
                        productsOrder.setService(getService(product));
                        productsOrder.setProductId(product.getId());
                        productsOrder.setProduct_img(product.getPicture().toString());
                        productsOrder.setTotal((Integer.parseInt(holder.quantity.getText().toString()) * Double.parseDouble(getSWPrice(product))) + "");
                        Gson gson = new Gson();
                        selected_products.add(productsOrder);
                        String json = gson.toJson(selected_products);
                        Log.d("size", selected_products.size() + "\n" + json);
                        Log.d("size", total(selected_products) + " TOTAL");
                        setColor(holder, 1);
                        onDataPass(productsOrder);
                        notifyDataSetChanged();
                    } else {
                        if (containsObject(selected_products, product)) {
                            selected_products.remove(position_array);
                            onDataPass(product.getId());
                            product.setSeriver1(false);
                            product.setSeriver2(false);
                            product.setSeriver3(false);
                            product.setQuantity(false);
                            setColorTransparent(holder);
                            notifyDataSetChanged();
                            Log.d("size", selected_products.size() + "");
                        }
                    }

                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(activity, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        holder.service_con3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!product.getSwPress().equalsIgnoreCase("-")) {
                    LinearLayout cb = (LinearLayout) v;
                    Product emp = (Product) cb.getTag();
                    if (!emp.isSelected()) {
                        cb.setSelected(true);
                        emp.setSelected(cb.isSelected());
                        products.get(posi).setSelected(cb.isSelected());
                    } else {
                        cb.setSelected(false);
                        products.get(posi).setSelected(false);
                    }

                    if (cb.isSelected()) {
                        ProductsOrder productsOrder = new ProductsOrder();
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            productsOrder.setName(product.getName());
                        } else {
                            productsOrder.setName(product.getNameAr());
                        }
                        selectProduct(holder, product);
                        product.setSeriver1(false);
                        product.setSeriver2(false);
                        product.setSeriver3(true);
                        holder.quantity.setText("1");
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        productsOrder.setQuantity(holder.quantity.getText().toString());
                        productsOrder.setSwPrice(getSWPrice(product));
                        productsOrder.setPrice(getPrice(product));
                        productsOrder.setService(getService(product));
                        productsOrder.setProductId(product.getId());
                        productsOrder.setProduct_img(product.getPicture().toString());
                        productsOrder.setTotal((Integer.parseInt(holder.quantity.getText().toString()) * Double.parseDouble(getSWPrice(product))) + "");
                        Gson gson = new Gson();
                        selected_products.add(productsOrder);
                        String json = gson.toJson(selected_products);
                        Log.d("size", selected_products.size() + "\n" + json);
                        Log.d("size", total(selected_products) + " TOTAL");

                        setColor(holder, 2);
                        onDataPass(productsOrder);
                        notifyDataSetChanged();
                    } else {
                        if (containsObject(selected_products, product)) {
                            selected_products.remove(position_array);
                            onDataPass(product.getId());
                            product.setSeriver1(false);
                            product.setSeriver2(false);
                            product.setSeriver3(false);
                            product.setQuantity(false);
                            setColorTransparent(holder);
                            notifyDataSetChanged();
                            Log.d("size", selected_products.size() + "");
                        }
                    }

                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(activity, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

//        search_bar.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (search_bar.getText().toString().length() > 0) {
//                    filter(s.toString());
//                } else {
//                    products = original_list;
//                    notifyDataSetChanged();
//                }
//            }
//        });


    }

    void filter(String text) {
        ArrayList<Product> temp = new ArrayList();

        for (Product d : products) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getName().toLowerCase().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        updateList(temp);
    }

    private void updateList(ArrayList<Product> temp) {
        products = temp;
        notifyDataSetChanged();
    }


    private boolean containsObject(ArrayList<ProductsOrder> collection, Product product) {
        for (ProductsOrder proList : collection) {
            if (proList.getProductId().equals(product.getId())) {
                position_array = collection.indexOf(proList);
                Log.d("size", position_array + " position");
                return true;
            }
        }
        return false;
    }

    private void add_remove(Product product, String quantity) {
        if (containsObject(selected_products, product)) {
            onDataPass(product.getId());
            selected_products.remove(position_array);

        }
        ProductsOrder productsOrder = new ProductsOrder();
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            productsOrder.setName(product.getName());
        } else {
            productsOrder.setName(product.getNameAr());
        }
        productsOrder.setQuantity(quantity);
        productsOrder.setSwPrice(getSWPrice(product));
        productsOrder.setPrice(getPrice(product));
        productsOrder.setService(getService(product));
        productsOrder.setProductId(product.getId());
        productsOrder.setProduct_img(product.getPicture().toString());
        productsOrder.setTotal((Integer.parseInt(quantity) * Double.parseDouble(getSWPrice(product))) + "");
        Gson gson = new Gson();
        selected_products.add(productsOrder);
        String json = gson.toJson(selected_products);
        Log.d("size", selected_products.size() + "\n" + json);
        Log.d("size", total(selected_products) + " TOTAL");
        onDataPass(productsOrder);
    }

    private double total(ArrayList<ProductsOrder> proList) {
        double i = 0;
        for (ProductsOrder pro : proList) {
            i = i + (Integer.parseInt(pro.getQuantity()) * Double.parseDouble(pro.getSwPrice()));
        }
        return i;
    }

    private void setColorTransparent(ZeplinProductsView holder) {
        holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
        holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
        holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
        holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
        holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
        holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
        holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
    }

    private void setColored(int posi, ZeplinProductsView holder) {
        if (products.get(posi).isSeriver1()) {
            holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.trans_color));

            holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
        } else if (products.get(posi).isSeriver2()) {
            holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.trans_color));

            holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
        } else if (products.get(posi).isSeriver3()) {
            holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
            holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.trans_color));
        }
    }

    @Override
    public int getItemCount() {
//        Log.d("VALUE", products.size() + "");
        return products.size();
    }

    public void setColor(ZeplinProductsView holder, int i) {
        switch (i) {
            case 0:
                holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.trans_color));

                holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                break;
            case 1:
                holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.trans_color));

                holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                break;
            case 2:
                holder.service1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price1.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price2.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.price3.setTextColor(activity.getResources().getColor(R.color.product_txt_color));
                holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.trans_color));
                break;
        }


    }

    private void selectProduct(ZeplinProductsView holder, Product product) {
        if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
            product.setSeriver1(true);
            product.setSeriver2(false);
            product.setSeriver3(false);
            setColor(holder, 0);
            notifyDataSetChanged();
        } else {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                product.setSeriver1(false);
                product.setSeriver2(true);
                product.setSeriver3(false);
                setColor(holder, 1);
                notifyDataSetChanged();
            } else {
                if (!product.getSwPress().equalsIgnoreCase("-")) {
                    product.setSeriver1(false);
                    product.setSeriver2(false);
                    product.setSeriver3(true);
                    setColor(holder, 2);
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(activity, "No Service available", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    private String getService(Product product) {
        String service = "";
        if (product.isSeriver1()) {
            service = "DryClean & Press";
        } else if (product.isSeriver2()) {
            service = "Wash & Press";
        } else if (product.isSeriver3()) {
            service = "Only Press";
        }
        return service;
    }

    private String getSWPrice(Product product) {
        String price = "";
        if (product.isSeriver1()) {
            if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                price = product.getSwDrycleanPrice();
            } else {

            }
        } else if (product.isSeriver2()) {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                price = product.getSwWashingPrice();
            } else {

            }
        } else if (product.isSeriver3()) {
            if (!product.getSwPress().equalsIgnoreCase("-")) {
                price = product.getSwPress();
            } else {

            }
        }

        return price;
    }

    private String getPrice(Product product) {
        String price = "";
        if (product.isSeriver1()) {
            if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                price = product.getDrycleanPrice();
            } else {

            }
        } else if (product.isSeriver2()) {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                price = product.getWashingPrice();
            } else {

            }
        } else if (product.isSeriver3()) {
            if (!product.getSwPress().equalsIgnoreCase("-")) {
                price = product.getPress();
            } else {

            }
        }
        return price;
    }

    @Override
    public void onDataPass(ProductsOrder productsOrder) {
        dataPasser.onDataPass(productsOrder);
    }

    @Override
    public void onDataPass(int product_id) {
        dataPasser.onDataPass(product_id);
    }
}

