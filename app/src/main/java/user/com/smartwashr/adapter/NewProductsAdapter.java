package user.com.smartwashr.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.OrderDoneNew;
import user.com.smartwashr.models.Category;
import user.com.smartwashr.models.productsresponse.Product;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 9/19/17.
 */

public class NewProductsAdapter extends RecyclerView.Adapter<NewProductsView> {

    ArrayList<Product> products;
    ArrayList<Integer> positiono;
    ArrayList<Category> categories;
    Activity activity;
    ArrayList<ProductsOrder> selected_products = new ArrayList<>();
    private int position_array;
    LinearLayout container;
    TextView total_txt;
    private EditText search_bar;
    ArrayList<Product> original_list = new ArrayList<>();


    public NewProductsAdapter(ArrayList<Category> cat, ArrayList<Product> products, ArrayList<Integer> pos, Activity activity, TextView total, LinearLayout checkout_con, EditText search) {
        this.categories = cat;
        this.products = products;
        this.activity = activity;
        this.positiono = pos;
        this.total_txt = total;
        this.container = checkout_con;
        this.search_bar = search;
        this.original_list = products;
    }

    @Override
    public NewProductsView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_placement, parent, false);
        return new NewProductsView(itemView);
    }

    @Override
    public void onBindViewHolder(final NewProductsView holder, final int position) {
        final int posi = position;
        final Product product = products.get(position);
        if (positiono.contains(position) && search_bar.getText().length() == 0) {
            holder.cat_con.setVisibility(View.VISIBLE);
            int i = positiono.indexOf(position);
            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                holder.cat.setText(categories.get(i).getName());
            } else {
                holder.cat.setText(categories.get(i).getNameAr());
            }
        } else {
            holder.cat_con.setVisibility(View.GONE);
        }

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            holder.product_name.setText(product.getName());
            holder.price1.setText("SAR " + product.getSwDrycleanPrice());
            holder.price2.setText("SAR " + product.getSwWashingPrice());
            holder.price3.setText("SAR " + product.getSwPress());
        } else {
            holder.product_name.setText(product.getNameAr());
            holder.price1.setText(product.getSwDrycleanPrice() + " ريال ");
            holder.price2.setText(product.getSwWashingPrice() + " ريال ");
            holder.price3.setText(product.getSwPress() + " ريال ");
            holder.service1.setText("غسيل جاف مع الكي");
            holder.service2.setText("غسيل عادي مع الكي");
            holder.service3.setText("كي فقط");
        }
        holder.checkBox.setChecked(product.isSelected());
        holder.checkBox.setTag(product);

        if (product.isSelected()) {
            holder.checkBox.setSelected(product.isSelected());
        } else {
            holder.checkBox.setSelected(!product.isSelected());
        }


        if (product.isSeriver1()) {
            setColored(posi, holder);
        } else if (product.isSeriver2()) {
            setColored(posi, holder);
        } else if (product.isSeriver3()) {
            setColored(posi, holder);
        } else {
            setColorTransparent(holder);
        }

        if (!product.isQuantity()) {
            holder.quantity.setText("0");
        } else {
            holder.quantity.setText(product.getQuantity());
        }

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                Product emp = (Product) cb.getTag();

                emp.setSelected(cb.isChecked());
                products.get(posi).setSelected(cb.isChecked());

                if (cb.isChecked() == true) {
                    ProductsOrder productsOrder = new ProductsOrder();
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        productsOrder.setName(product.getName());
                    } else {
                        productsOrder.setName(product.getNameAr());
                    }
                    selectProduct(holder, product);
                    holder.quantity.setText("1");
                    product.setQuantity(true);
                    product.setQuantity(holder.quantity.getText().toString());
                    productsOrder.setQuantity(holder.quantity.getText().toString());
                    productsOrder.setSwPrice(getSWPrice(product));
                    productsOrder.setPrice(getPrice(product));
                    productsOrder.setService(getService(product));
                    productsOrder.setProductId(product.getId());
                    productsOrder.setProduct_img(product.getPicture().toString());
                    productsOrder.setTotal((Integer.parseInt(holder.quantity.getText().toString()) * Integer.parseInt(getSWPrice(product))) + "");
                    Gson gson = new Gson();
                    selected_products.add(productsOrder);
                    String json = gson.toJson(selected_products);
                    Log.d("size", selected_products.size() + "\n" + json);
                    Log.d("size", total(selected_products) + " TOTAL");
                    if (selected_products.size() > 0) {
                        container.setVisibility(View.VISIBLE);
                        total_txt.setText("TOTAL: SAR " + total(selected_products));
                    } else {
                        container.setVisibility(View.GONE);
                    }
                } else {
                    if (containsObject(selected_products, product)) {
                        selected_products.remove(position_array);
                        product.setSeriver1(false);
                        product.setSeriver2(false);
                        product.setSeriver3(false);
                        product.setQuantity(false);
                        setColorTransparent(holder);
                        notifyDataSetChanged();
                        Log.d("size", selected_products.size() + "");
                        if (selected_products.size() > 0) {
                            container.setVisibility(View.VISIBLE);
                            total_txt.setText("TOTAL: SAR " + total(selected_products));
                        } else {
                            container.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });


        holder.add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (product.isSeriver1()) {
                        String text = holder.quantity.getText().toString();
                        int i = Integer.parseInt(text) + 1;
                        holder.quantity.setText("" + i);
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else if (product.isSeriver2()) {
                        String text = holder.quantity.getText().toString();
                        int i = Integer.parseInt(text) + 1;
                        holder.quantity.setText("" + i);
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else if (product.isSeriver3()) {
                        String text = holder.quantity.getText().toString();
                        int i = Integer.parseInt(text) + 1;
                        holder.quantity.setText("" + i);
                        product.setQuantity(true);
                        product.setQuantity(holder.quantity.getText().toString());
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(activity, "Please select it first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.neg_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (product.isSeriver1()) {
                        String text = holder.quantity.getText().toString();
                        if (Integer.parseInt(text) > 1) {
                            int i = Integer.parseInt(text) - 1;
                            holder.quantity.setText("" + i);
                            product.setQuantity(true);
                            product.setQuantity(holder.quantity.getText().toString());
                            add_remove(product, holder.quantity.getText().toString());
                            notifyDataSetChanged();
                        } else {

                        }
                    } else if (product.isSeriver2()) {
                        String text = holder.quantity.getText().toString();
                        if (Integer.parseInt(text) > 1) {
                            int i = Integer.parseInt(text) - 1;
                            holder.quantity.setText("" + i);
                            product.setQuantity(true);
                            product.setQuantity(holder.quantity.getText().toString());
                            add_remove(product, holder.quantity.getText().toString());
                            notifyDataSetChanged();
                        } else {

                        }
                    } else if (product.isSeriver3()) {
                        String text = holder.quantity.getText().toString();
                        if (Integer.parseInt(text) > 1) {
                            int i = Integer.parseInt(text) - 1;
                            holder.quantity.setText("" + i);
                            product.setQuantity(true);
                            product.setQuantity(holder.quantity.getText().toString());
                            add_remove(product, holder.quantity.getText().toString());
                            notifyDataSetChanged();
                        } else {

                        }
                    }
                } else {
                    Toast.makeText(activity, "Please select it first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.service_con1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                        product.setSeriver1(true);
                        product.setSeriver2(false);
                        product.setSeriver3(false);
                        setColor(holder, 0);
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else {
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            Toast.makeText(activity, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(activity, "Please Check it first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.service_con2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                        product.setSeriver1(false);
                        product.setSeriver3(false);
                        product.setSeriver2(true);
                        setColor(holder, 1);
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else {
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            Toast.makeText(activity, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(activity, "Please Check it first", Toast.LENGTH_SHORT).show();
                }
            }

        });

        holder.service_con3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.isSelected()) {
                    if (!product.getSwPress().equalsIgnoreCase("-")) {
                        product.setSeriver1(false);
                        product.setSeriver2(false);
                        product.setSeriver3(true);
                        setColor(holder, 2);
                        add_remove(product, holder.quantity.getText().toString());
                        notifyDataSetChanged();
                    } else {
                        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                            Toast.makeText(activity, "No price mentioned yet", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "لم يذكر السعر حتى الآن", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(activity, "Please Check it first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, OrderDoneNew.class);
                intent.putExtra("products", selected_products);
                activity.startActivity(intent);
            }
        });

        search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (search_bar.getText().toString().length() > 0) {
                    filter(s.toString());
                } else {
                    products = original_list;
                    notifyDataSetChanged();
                }
            }
        });


    }

    void filter(String text) {
        ArrayList<Product> temp = new ArrayList();

        for (Product d : products) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getName().toLowerCase().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        updateList(temp);
    }

    private void updateList(ArrayList<Product> temp) {
        products = temp;
        notifyDataSetChanged();
    }


    private boolean containsObject(ArrayList<ProductsOrder> collection, Product product) {
        for (ProductsOrder proList : collection) {
            if (proList.getProductId().equals(product.getId())) {
                position_array = collection.indexOf(proList);
                Log.d("size", position_array + " position");
                return true;
            }
        }
        return false;
    }

    private void add_remove(Product product, String quantity) {
        if (containsObject(selected_products, product)) {
            selected_products.remove(position_array);
        }
        ProductsOrder productsOrder = new ProductsOrder();
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            productsOrder.setName(product.getName());
        } else {
            productsOrder.setName(product.getNameAr());
        }
        productsOrder.setQuantity(quantity);
        productsOrder.setSwPrice(getSWPrice(product));
        productsOrder.setPrice(getPrice(product));
        productsOrder.setService(getService(product));
        productsOrder.setProductId(product.getId());
        productsOrder.setProduct_img(product.getPicture().toString());
        productsOrder.setTotal((Integer.parseInt(quantity) * Integer.parseInt(getSWPrice(product))) + "");
        Gson gson = new Gson();
        selected_products.add(productsOrder);
        String json = gson.toJson(selected_products);
        Log.d("size", selected_products.size() + "\n" + json);
        Log.d("size", total(selected_products) + " TOTAL");
        if (selected_products.size() > 0) {
            container.setVisibility(View.VISIBLE);
            total_txt.setText("TOTAL: SAR " + total(selected_products));
        } else {
            container.setVisibility(View.GONE);
        }

    }

    private int total(ArrayList<ProductsOrder> proList) {
        int i = 0;
        for (ProductsOrder pro : proList) {
            i = i + (Integer.parseInt(pro.getQuantity()) * Integer.parseInt(pro.getSwPrice()));
        }
        return i;
    }

    private void setColorTransparent(NewProductsView holder) {
        holder.service1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.price1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        holder.service2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.price2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        holder.service3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.price3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
    }

    private void setColored(int posi, NewProductsView holder) {
        if (products.get(posi).isSeriver1()) {
            holder.service1.setTextColor(activity.getResources().getColor(R.color.white));
            holder.price1.setTextColor(activity.getResources().getColor(R.color.white));
            holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));

            holder.service2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.price2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.price3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
        } else if (products.get(posi).isSeriver2()) {
            holder.service1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.price1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service2.setTextColor(activity.getResources().getColor(R.color.white));
            holder.price2.setTextColor(activity.getResources().getColor(R.color.white));
            holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));

            holder.service3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.price3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
        } else if (products.get(posi).isSeriver3()) {
            holder.service1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.price1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.price2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

            holder.service3.setTextColor(activity.getResources().getColor(R.color.white));
            holder.price3.setTextColor(activity.getResources().getColor(R.color.white));
            holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
//        Log.d("VALUE", products.size() + "");
        return products.size();
    }

    public void setColor(NewProductsView holder, int i) {
        switch (i) {
            case 0:
                holder.service1.setTextColor(activity.getResources().getColor(R.color.white));
                holder.price1.setTextColor(activity.getResources().getColor(R.color.white));
                holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));

                holder.service2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.price2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.price3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                break;
            case 1:
                holder.service1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.price1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service2.setTextColor(activity.getResources().getColor(R.color.white));
                holder.price2.setTextColor(activity.getResources().getColor(R.color.white));
                holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));

                holder.service3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.price3.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                break;
            case 2:
                holder.service1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.price1.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.service_con1.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.price2.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.service_con2.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

                holder.service3.setTextColor(activity.getResources().getColor(R.color.white));
                holder.price3.setTextColor(activity.getResources().getColor(R.color.white));
                holder.service_con3.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
                break;
        }


    }

    private void selectProduct(NewProductsView holder, Product product) {
        if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
            product.setSeriver1(true);
            product.setSeriver2(false);
            product.setSeriver3(false);
            setColor(holder, 0);
            notifyDataSetChanged();
        } else {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                product.setSeriver1(false);
                product.setSeriver2(true);
                product.setSeriver3(false);
                setColor(holder, 1);
                notifyDataSetChanged();
            } else {
                if (!product.getSwPress().equalsIgnoreCase("-")) {
                    product.setSeriver1(false);
                    product.setSeriver2(false);
                    product.setSeriver3(true);
                    setColor(holder, 2);
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(activity, "No Service available", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    private String getService(Product product) {
        String service = "";
        if (product.isSeriver1()) {
            service = "DryClean & Press";
        } else if (product.isSeriver2()) {
            service = "Wash & Press";
        } else if (product.isSeriver3()) {
            service = "Only Press";
        }
        return service;
    }

    private String getSWPrice(Product product) {
        String price = "";
        if (product.isSeriver1()) {
            if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                price = product.getSwDrycleanPrice();
            } else {

            }
        } else if (product.isSeriver2()) {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                price = product.getSwWashingPrice();
            } else {

            }
        } else if (product.isSeriver3()) {
            if (!product.getSwPress().equalsIgnoreCase("-")) {
                price = product.getSwPress();
            } else {

            }
        }

        return price;
    }

    private String getPrice(Product product) {
        String price = "";
        if (product.isSeriver1()) {
            if (!product.getSwDrycleanPrice().equalsIgnoreCase("-")) {
                price = product.getDrycleanPrice();
            } else {

            }
        } else if (product.isSeriver2()) {
            if (!product.getSwWashingPrice().equalsIgnoreCase("-")) {
                price = product.getWashingPrice();
            } else {

            }
        } else if (product.isSeriver3()) {
            if (!product.getSwPress().equalsIgnoreCase("-")) {
                price = product.getPress();
            } else {

            }
        }
        return price;
    }


}
