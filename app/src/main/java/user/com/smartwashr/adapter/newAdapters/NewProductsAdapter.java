package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.OrderProduct;
import user.com.smartwashr.newModels.pricing.Product;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

public class NewProductsAdapter extends RecyclerView.Adapter<NewPriceVH> implements Filterable {

    Activity activity;
    ArrayList<Product> list;
    RelativeLayout rl_bill;
    TextView tv_bill, tv_skip;
    private ArrayList<Product> contactList;

    public NewProductsAdapter(Activity activity, ArrayList<Product> list, RelativeLayout rl_bill, TextView tv_bill, TextView tv_skip) {
        this.activity = activity;
        this.list = list;
        this.rl_bill = rl_bill;
        this.tv_bill = tv_bill;
        this.contactList = list;
        this.tv_skip = tv_skip;
    }

    @Override
    public NewPriceVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_price_view1, parent, false);
        return new NewPriceVH(v);
    }

    @Override
    public void onBindViewHolder(final NewPriceVH holder, int position) {
        final Product item = list.get(position);

        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("ar")) {
            holder.tv_title.setText(item.getNameAr());
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            OrderItemAdapter adapter = new OrderItemAdapter(activity, item.getServices(), rl_bill, tv_bill, tv_skip, holder.tv_qty);
            holder.recyclerView.setAdapter(adapter);

        } else {
            holder.tv_title.setText(item.getName());
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            OrderItemAdapter adapter = new OrderItemAdapter(activity, item.getServices(), rl_bill, tv_bill, tv_skip, holder.tv_qty);
            holder.recyclerView.setAdapter(adapter);
        }

        if (!checkQuantity(item)) {
            holder.tv_qty.setText("0");
        }

        holder.iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkQuantity(item)) {
                    addOrder(item, holder.tv_qty);
                    calculateTotal(tv_bill);
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                    alertDialogBuilder.setTitle(Constants.TRANSLATIONS.getOopsLabel());
                    alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getSelectAnItemFirstLabel());
                    alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
//                    Toast.makeText(activity, "Please select a service please", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.iv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!holder.tv_qty.getText().toString().equalsIgnoreCase("1")) {

                    if (checkQuantity(item)) {
                        minusOrder(item, holder.tv_qty);
                        calculateTotal(tv_bill);

                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                        alertDialogBuilder.setTitle(Constants.TRANSLATIONS.getOopsLabel());
                        alertDialogBuilder.setMessage(Constants.TRANSLATIONS.getSelectAnItemFirstLabel());
                        alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();
//                        Toast.makeText(activity, "Please select a service please", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }

    private boolean checkQuantity(Product product) {
        boolean added = false;
        for (int i = 0; i < Constants.orderList.size(); i++) {
            if (Constants.orderList.get(i).getProductId().contains(String.valueOf(product.getId()))) {
                added = true;
                break;
            }
        }
        return added;
    }

    private void addOrder(Product item, TextView tv_qty) {
        for (int i = 0; i < Constants.orderList.size(); i++) {
            if (Constants.orderList.get(i).getProductId().contains(String.valueOf(item.getId()))) {
                Constants.orderList.set(i, new OrderProduct(item.getId() + "", item.getName(), (Integer.parseInt(Constants.orderList.get(i).getProductQty()) + 1) + "", Constants.orderList.get(i).getServiceId() + "", Constants.orderList.get(i).getServicePrice() + ""));
                tv_qty.setText(Constants.orderList.get(i).getProductQty());
                Log.d("LIST", new Gson().toJson(Constants.orderList));
                Log.d("LIST_SIZE", Constants.orderList.size() + "");
            }
        }
    }

    private void minusOrder(Product item, TextView tv_qty) {
        for (int i = 0; i < Constants.orderList.size(); i++) {
            if (Constants.orderList.get(i).getProductId().contains(String.valueOf(item.getId()))) {
                Constants.orderList.set(i, new OrderProduct(item.getId() + "", item.getName(), (Integer.parseInt(Constants.orderList.get(i).getProductQty()) - 1) + "", Constants.orderList.get(i).getServiceId() + "", Constants.orderList.get(i).getServicePrice() + ""));
                tv_qty.setText(Constants.orderList.get(i).getProductQty());
                Log.d("LIST", new Gson().toJson(Constants.orderList));
                Log.d("LIST_SIZE", Constants.orderList.size() + "");
            }
        }
    }

    private void calculateTotal(TextView txt) {
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < Constants.orderList.size(); i++) {
            OrderProduct product = Constants.orderList.get(i);
            double price = Double.parseDouble((String) product.getServicePrice()) * Integer.parseInt(product.getProductQty());
            prices.add(price);
        }

        int sum = 0;
        for (double i : prices) {
            sum += i;
        }
        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
            txt.setText(Constants.TRANSLATIONS.getCurrency() + " " + sum);
        } else {
            txt.setText(sum + " " + Constants.TRANSLATIONS.getCurrency());
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    list = contactList;
                } else {
                    ArrayList<Product> filteredList = new ArrayList<>();
                    for (Product row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getNameAr().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    list = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list = (ArrayList<Product>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
