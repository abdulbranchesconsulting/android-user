package user.com.smartwashr.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.MapsActivity;
import user.com.smartwashr.activities.newActivities.AddNewAddress;
import user.com.smartwashr.newModels.address.Datum;
import user.com.smartwashr.utils.Constants;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceVH> {

    boolean fromMaps;
    Activity activity;
    ArrayList<Datum> list;
    TextView textView;
    RelativeLayout rl_addresses;
    ImageView iv_down;

    public PlaceAdapter(Activity activity, ArrayList<Datum> list) {
        this.activity = activity;
        this.list = list;
    }


    public PlaceAdapter(Activity activity, ArrayList<Datum> list, TextView address, RelativeLayout rl_addresses, ImageView iv_down, boolean fromMaps) {
        this.activity = activity;
        this.list = list;
        this.textView = address;
        this.rl_addresses = rl_addresses;
        this.iv_down = iv_down;
        this.fromMaps = fromMaps;
    }

    @Override
    public PlaceVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);
        return new PlaceVH(v);
    }

    @Override
    public void onBindViewHolder(PlaceVH holder, int position) {
        final Datum item = list.get(position);

        holder.tv_title.setText(item.getBuildingName());
        holder.tv_address.setText(item.getAddress());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromMaps) {
                    if (textView != null) {
                        textView.setText(item.getAddress());
                        rl_addresses.setVisibility(View.GONE);
                        iv_down.setImageResource(R.drawable.ic_down_arrow);
                        ((MapsActivity) activity).moveMaps(item.getLocation().getCoordinates().get(1), item.getLocation().getCoordinates().get(0));
                    }
                } else {
                    Intent i = new Intent(activity, AddNewAddress.class);
                    i.putExtra("edit", "edit");
                    Constants.ADDRESS = item;
                    activity.startActivity(i);

                }
            }
        });


    }

    public void removeItem(int position) {
        list.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(Datum item, int position) {
        list.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
