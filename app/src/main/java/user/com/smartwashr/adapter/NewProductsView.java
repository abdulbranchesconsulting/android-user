package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import user.com.smartwashr.R;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 9/19/17.
 */

public class NewProductsView extends RecyclerView.ViewHolder {

    TextView product_name, quantity, service1, service2, service3, price1, price2, price3, cat;
    LinearLayout service_con1, service_con2, service_con3, cat_con;
    ImageView add_btn, neg_btn;
    CheckBox checkBox;

    public NewProductsView(View itemView) {
        super(itemView);
        product_name = (TextView) itemView.findViewById(R.id.p_name);
        quantity = (TextView) itemView.findViewById(R.id.quantity);
        service1 = (TextView) itemView.findViewById(R.id.s1);
        service2 = (TextView) itemView.findViewById(R.id.s2);
        service3 = (TextView) itemView.findViewById(R.id.s3);
        price1 = (TextView) itemView.findViewById(R.id.p1);
        price2 = (TextView) itemView.findViewById(R.id.p2);
        price3 = (TextView) itemView.findViewById(R.id.p3);
        cat = (TextView) itemView.findViewById(R.id.category);
        cat_con = (LinearLayout) itemView.findViewById(R.id.category_con);
        service_con1 = (LinearLayout) itemView.findViewById(R.id.service1);
        service_con2 = (LinearLayout) itemView.findViewById(R.id.service2);
        service_con3 = (LinearLayout) itemView.findViewById(R.id.service3);
        add_btn = (ImageView) itemView.findViewById(R.id.add_btn);
        neg_btn = (ImageView) itemView.findViewById(R.id.neg_btn);
        checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setRobotoMediumFont(service1);
            FontUtils.setRobotoMediumFont(service2);
            FontUtils.setRobotoMediumFont(service3);
            FontUtils.setRobotoBold(product_name);
            FontUtils.setNewRegularFont(cat);
        }

    }

}
