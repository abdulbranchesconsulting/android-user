package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.faq_respnose.Datum;
import user.com.smartwashr.utils.Constants;

public class FaqAdapter extends RecyclerView.Adapter<FAQView> {

    Activity activity;
    ArrayList<Datum> list;

    public FaqAdapter(Activity activity, ArrayList<Datum> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public FAQView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_item, parent, false);
        return new FAQView(v);
    }

    @Override
    public void onBindViewHolder(FAQView holder, int position) {
        final Datum item = list.get(position);

        holder.tv_how_long.setText(item.getQuestion());

        holder.tv_how_long.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                Spanned spanned = Html.fromHtml(item.getAnswer());
                alertDialogBuilder.setMessage(spanned.toString());
                alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialogBuilder.create();
                alertDialogBuilder.show();


            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
