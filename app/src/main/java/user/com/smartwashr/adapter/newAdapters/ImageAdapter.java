package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.activities.ContactUs;
import user.com.smartwashr.utils.Constants;

public class ImageAdapter extends RecyclerView.Adapter<ImageVH> {

    Activity activity;
    ArrayList<String> list;

    public ImageAdapter(Activity activity, ArrayList<String> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ImageVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.img_layout, parent, false);
        return new ImageVH(v);
    }

    @Override
    public void onBindViewHolder(ImageVH holder, final int position) {

        if (list.get(position).length() > 0) {
            Picasso.with(activity).load(new File(list.get(position))).fit().into(holder.iv_img);
        } else {
            holder.iv_img.setImageResource(R.drawable.add_img);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(position).length() == 0) {
                    if (Constants.checkPermission(activity))
                        ((ContactUs) activity).selectImage();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
