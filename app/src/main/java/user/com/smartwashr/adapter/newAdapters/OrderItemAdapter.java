package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.OrderProduct;
import user.com.smartwashr.newModels.pricing.Product;
import user.com.smartwashr.newModels.pricing.Service;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemSelectionVH> {

    Activity activity;
    List<Service> list;
    RelativeLayout rl_bill;
    TextView tv_bill, tv_skip, tv_qty;

    public OrderItemAdapter(Activity activity, List<Service> list, RelativeLayout rl_bill, TextView tv_bill, TextView tv_skip, TextView tv_qty) {
        this.activity = activity;
        this.list = list;
        this.rl_bill = rl_bill;
        this.tv_bill = tv_bill;
        this.tv_qty = tv_qty;
        this.tv_skip = tv_skip;
    }

    @Override
    public OrderItemSelectionVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item_selection, parent, false);
        return new OrderItemSelectionVH(v);
    }

    @Override
    public void onBindViewHolder(OrderItemSelectionVH holder, final int position) {
        final Service item = list.get(position);

        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
            holder.tv_service.setText(item.getName());
            holder.tv_price1.setText(Constants.TRANSLATIONS.getCurrency() + " " + item.getPrice());
        } else {
            holder.tv_service.setText(item.getNameAr());
            holder.tv_price1.setText(item.getPrice() + " " + Constants.TRANSLATIONS.getCurrency());
        }

        if (item.isSelected()) {
            holder.tv_service.setTextColor(activity.getResources().getColor(R.color.blue));
            holder.tv_price1.setTextColor(activity.getResources().getColor(R.color.blue));
            holder.iv_selected.setVisibility(View.VISIBLE);

        } else {
            holder.tv_service.setTextColor(activity.getResources().getColor(R.color.grey));
            holder.tv_price1.setTextColor(activity.getResources().getColor(R.color.grey));
            holder.iv_selected.setVisibility(View.INVISIBLE);
        }

        holder.rl_s1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.isSelected()) {
                    item.setSelected(false);
                    tv_qty.setText("0");
                    for (int i = 0; i < Constants.orderList.size(); i++) {
                        if (Constants.orderList.get(i).getProductId().contains(String.valueOf(item.getProduct_id()))) {
                            Constants.orderList.remove(i);
                            Log.d("LIST", new Gson().toJson(Constants.orderList));
                            Log.d("LIST_SIZE", Constants.orderList.size() + "");
                        }
                    }
                    if (Constants.orderList.size() == 0) {
                        rl_bill.setVisibility(View.GONE);
                        tv_skip.setVisibility(View.VISIBLE);
                    } else {
                        rl_bill.setVisibility(View.VISIBLE);
                        tv_skip.setVisibility(View.GONE);
                        calculateTotal(tv_bill);
                    }
                    notifyDataSetChanged();

                } else {

                    tv_qty.setText("1");

                    for (int i = 0; i < Constants.orderList.size(); i++) {
                        if (Constants.orderList.get(i).getProductId().contains(String.valueOf(item.getProduct_id()))) {
                            Constants.orderList.remove(i);
                            Log.d("LIST", new Gson().toJson(Constants.orderList));
                            Log.d("LIST_SIZE", Constants.orderList.size() + "");
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getProduct_id() == item.getProduct_id()) {
                            list.get(i).setSelected(false);
                        }

                    }
                    item.setSelected(true);
                    Constants.orderList.add(new OrderProduct(item.getProduct_id() + "", getProduct(item.getProduct_id()).getName(), "1", item.getId() + "", item.getPrice() + ""));
                    Log.d("LIST", new Gson().toJson(Constants.orderList));
                    Log.d("LIST_SIZE", Constants.orderList.size() + "");
                    rl_bill.setVisibility(View.VISIBLE);
                    tv_skip.setVisibility(View.GONE);
                    tv_bill.setText(Constants.TRANSLATIONS.getCurrency() + " " + item.getPrice());
                    calculateTotal(tv_bill);

                    notifyDataSetChanged();


                }
            }
        });


    }

    private void addOrder(int id, Service item, TextView tv_qty) {
        for (int i = 0; i < Constants.orderList.size(); i++) {
            if (Constants.orderList.get(i).getProductId().equalsIgnoreCase(String.valueOf(id))) {
                Constants.orderList.set(i, new OrderProduct(id + "", getProduct(id).getName(), (Integer.parseInt(Constants.orderList.get(i).getProductQty()) + 1) + "", item.getId() + "", item.getPrice() + ""));
                tv_qty.setText(Constants.orderList.get(i).getProductQty());
                Log.d("LIST", new Gson().toJson(Constants.orderList));
                Log.d("LIST_SIZE", Constants.orderList.size() + "");
                calculateTotal(tv_bill);
            }
        }
    }

//    private boolean isDouble(Object price) {
//        if (price instanceof Integer)
//            return false;
//        else
//            return true;
//    }


    private Product getProduct(int productId) {
        Product item = null;

        for (int i = 0; i < Constants.products.size(); i++) {
            if (Constants.products.get(i).getId() == productId) {
                item = Constants.products.get(i);
            }
        }

        return item;
    }

    private void minusOrder(int id, Service item, TextView tv_qty) {
        for (int i = 0; i < Constants.orderList.size(); i++) {
            if (Constants.orderList.get(i).getProductId().contains(String.valueOf(id))) {
                Constants.orderList.set(i, new OrderProduct(id + "", getProduct(id).getName(), (Integer.parseInt(Constants.orderList.get(i).getProductQty()) - 1) + "", item.getId() + "", item.getPrice() + ""));
                tv_qty.setText(Constants.orderList.get(i).getProductQty());
                Log.d("LIST", new Gson().toJson(Constants.orderList));
                Log.d("LIST_SIZE", Constants.orderList.size() + "");
                calculateTotal(tv_bill);
            }
        }
    }


    private void calculateTotal(TextView txt) {
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < Constants.orderList.size(); i++) {
            OrderProduct product = Constants.orderList.get(i);
            double price = Double.parseDouble((String) product.getServicePrice()) * Integer.parseInt(product.getProductQty());
            prices.add(price);
        }

        int sum = 0;
        for (double i : prices) {
            sum += i;
        }
        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
            txt.setText(Constants.TRANSLATIONS.getCurrency() + " " + sum);
        } else {
            txt.setText(sum + " " + Constants.TRANSLATIONS.getCurrency());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
