package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import user.com.smartwashr.R;

public class ImageVH extends RecyclerView.ViewHolder {

    ImageView iv_img;

    public ImageVH(View itemView) {
        super(itemView);
        iv_img = itemView.findViewById(R.id.iv_img);
    }
}
