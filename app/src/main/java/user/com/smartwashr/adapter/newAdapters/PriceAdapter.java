package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.pricing.Product;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

public class PriceAdapter extends RecyclerView.Adapter<PriceVH> implements Filterable {

    Activity activity;
    ArrayList<Product> list;
    ArrayList<Product> contactList;

    public PriceAdapter(Activity activity, ArrayList<Product> list) {
        this.activity = activity;
        this.list = list;
        this.contactList = list;
    }

    @Override
    public PriceVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_price_view, parent, false);
        return new PriceVH(v);
    }

    @Override
    public void onBindViewHolder(PriceVH holder, int position) {
        Product item = list.get(position);
        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
            holder.tv_title.setText(item.getName());
        } else {
            holder.tv_title.setText(item.getNameAr());
        }
        holder.item_recycler.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        PriceServiceAdapter adapter = new PriceServiceAdapter(activity, item.getServices());
        holder.item_recycler.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    list = contactList;
                } else {
                    ArrayList<Product> filteredList = new ArrayList<>();
                    for (Product row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getNameAr().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    list = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list = (ArrayList<Product>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }
}
