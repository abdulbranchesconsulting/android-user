package user.com.smartwashr.adapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zeeshan on 6/9/17.
 */

public class ProductsOrder implements Serializable {

    private String category, total, url, name;
    private String sw_dryclean, sw_wash_press, sw_press;
    private String dry_price, wash_price, press_price;
    private String product_img;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("sw_price")
    @Expose
    private String swPrice;
    @SerializedName("price")
    @Expose
    private String price;

    public ProductsOrder() {
    }

    public ProductsOrder(Integer productId, String quantity, String service, String swPrice, String price) {
        this.productId = productId;
        this.quantity = quantity;
        this.service = service;
        this.swPrice = swPrice;
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSwPrice() {
        return swPrice;
    }

    public void setSwPrice(String swPrice) {
        this.swPrice = swPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSw_dryclean() {
        return sw_dryclean;
    }

    public void setSw_dryclean(String sw_dryclean) {
        this.sw_dryclean = sw_dryclean;
    }

    public String getSw_wash_press() {
        return sw_wash_press;
    }

    public void setSw_wash_press(String sw_wash_press) {
        this.sw_wash_press = sw_wash_press;
    }

    public String getSw_press() {
        return sw_press;
    }

    public void setSw_press(String sw_press) {
        this.sw_press = sw_press;
    }

    public String getDry_price() {
        return dry_price;
    }

    public void setDry_price(String dry_price) {
        this.dry_price = dry_price;
    }

    public String getWash_price() {
        return wash_price;
    }

    public void setWash_price(String wash_price) {
        this.wash_price = wash_price;
    }

    public String getPress_price() {
        return press_price;
    }

    public void setPress_price(String press_price) {
        this.press_price = press_price;
    }

    public String getProduct_img() {
        return product_img;
    }

    public void setProduct_img(String product_img) {
        this.product_img = product_img;
    }
}