package user.com.smartwashr.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;
import user.com.smartwashr.R;
import user.com.smartwashr.SmartWashr;
import user.com.smartwashr.activities.Login;
import user.com.smartwashr.activities.OrderDetailActivity;
import user.com.smartwashr.models.ERRORSO;
import user.com.smartwashr.models.GenericResponse;
import user.com.smartwashr.models.orderDetailResponse.DetailModel;
import user.com.smartwashr.models.orderDetailResponse.OrderDetail;
import user.com.smartwashr.models.orderDetailResponse.Orders;
import user.com.smartwashr.models.orderresponse.UserOrder;
import user.com.smartwashr.restiapis.ResponseHandler;
import user.com.smartwashr.restiapis.RestCaller;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.Internet;
import user.com.smartwashr.utils.Loading;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/19/17.
 */

public class OrderDBAdapter extends RecyclerView.Adapter<DbOrderView> implements ResponseHandler {
    private ArrayList<UserOrder> lst;
    private Activity activity;
    private SessionManager sessionManager;
    private ArrayList<OrderDetail> list_product;
    private String price_total, order_id, status;

    public OrderDBAdapter(Activity activity, ArrayList<UserOrder> list) {
        lst = list;
        this.activity = activity;
    }

    @Override
    public DbOrderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_status_view, parent, false);
        list_product = new ArrayList<>();

        return new DbOrderView(itemView);
    }

    private UserOrder getObject(int i) {
        return (lst.get(i));
    }

    @Override
    public void onBindViewHolder(DbOrderView holder, final int position) {
        sessionManager = new SessionManager(activity);
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            holder.ar_parent.setVisibility(View.GONE);
            holder.getTitle_num().setText("ORDER # " + getObject(position).getOrder_id());
            if (getObject(position).getStatus().equalsIgnoreCase("0")) {
                holder.getStatus().setText("Ready for Picked Up");
                holder.getImageView().setImageBitmap(getBitmap(activity, R.drawable.ic_ready4pickup));
            } else if (getObject(position).getStatus().equalsIgnoreCase("1")) {
                holder.getStatus().setText("Getting picked up");
                holder.getImageView().setImageBitmap(getBitmap(activity, R.drawable.ic_gettingpickup));
            } else if (getObject(position).getStatus().equalsIgnoreCase("2")) {
                holder.getStatus().setText("Getting Washed");
                holder.getImageView().setImageBitmap(getBitmap(activity, R.drawable.ic_gettingwashed));
            } else if (getObject(position).getStatus().equalsIgnoreCase("3")) {
                holder.getStatus().setText("Ready For Delivery");
                holder.getImageView().setImageBitmap(getBitmap(activity, R.drawable.ic_ready4delivery));
            } else if (getObject(position).getStatus().equalsIgnoreCase("4")) {
                holder.getStatus().setText("Delivered");
                holder.getImageView().setImageBitmap(getBitmap(activity, R.drawable.ic_delivered_new));
            } else if (getObject(position).getStatus().equalsIgnoreCase("6")) {
                holder.getStatus().setText("Getting Dropped Off");
                holder.getImageView().setImageBitmap(getBitmap(activity, R.drawable.ic_gettingdroped));
            }
            holder.getP_txt().setText("SAR " + getObject(position).getSwTotalPrice());
            String start_dt = getObject(position).getCollection_date_time_to();
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = null;
            try {
                date = (Date) formatter.parse(start_dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat newFormat = new SimpleDateFormat("MMMM dd, yyyy");
            String finalString = newFormat.format(date);

            holder.getDate().setText(finalString);
        } else

        {
            holder.ar_parent.setVisibility(View.VISIBLE);
            holder.getParent().setVisibility(View.GONE);
            String start_dt = getObject(position).getCollection_date_time_to();
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = null;
            try {
                date = (Date) formatter.parse(start_dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat newFormat = new SimpleDateFormat("MMMM dd, yyyy");
            String finalString = newFormat.format(date);
            holder.ar_price.setText(finalString);
            FontUtils.setArabic(holder.ar_price);
            FontUtils.setArabic(holder.ar_title_num);
            FontUtils.setArabic(holder.ar_status);
            FontUtils.setArabic(holder.ar_price);
            holder.ar_title_num.setText("رقم الطلب" + " # " + getObject(position).getOrder_id());
            holder.getAr_p_txt().setText(getObject(position).getSwTotalPrice() + " ريال ");
            if (getObject(position).getStatus().equalsIgnoreCase("0")) {
                holder.ar_status.setText("جاهز للاستلام");
                holder.ar_imageView.setImageBitmap(getBitmap(activity, R.drawable.ic_ready4pickup));
            } else if (getObject(position).getStatus().equalsIgnoreCase("1")) {
                holder.ar_status.setText("جاري الإستلام");
                holder.ar_imageView.setImageBitmap(getBitmap(activity, R.drawable.ic_gettingpickup));
            } else if (getObject(position).getStatus().equalsIgnoreCase("2")) {
                holder.ar_status.setText("جاري الغسيل");
                holder.ar_imageView.setImageBitmap(getBitmap(activity, R.drawable.ic_gettingwashed));
            } else if (getObject(position).getStatus().equalsIgnoreCase("3")) {
                holder.ar_status.setText("جاهز للتسليم");
                holder.ar_imageView.setImageBitmap(getBitmap(activity, R.drawable.ic_ready4delivery));
            } else if (getObject(position).getStatus().equalsIgnoreCase("4")) {
                holder.ar_status.setText("تم التوصيل");
                holder.ar_imageView.setImageBitmap(getBitmap(activity, R.drawable.ic_delivered_new));
            } else if (getObject(position).getStatus().equalsIgnoreCase("6")) {
                holder.ar_status.setText("جاري التوصيل");
                holder.ar_imageView.setImageBitmap(getBitmap(activity, R.drawable.ic_gettingdroped));
            }
        }
        holder.getParent().

                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Internet.isAvailable(activity)) {
                            new RestCaller(OrderDBAdapter.this, SmartWashr.getRestClient().orderDetail("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), String.valueOf(getObject(position).getId())), 1);
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Loading.show(activity, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                            } else {
                                Loading.show(activity, false, "الرجاء الانتظار");
                            }
                            price_total = getObject(position).getSwTotalPrice();
                            order_id = getObject(position).getOrder_id();
                            status = getObject(position).getStatus();
                            SessionManager.put(Constants.ORDER_ID, order_id);
                        } else {
                            if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                                Toast.makeText(activity, "No internet available", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(activity, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

        holder.ar_parent.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                if (Internet.isAvailable(activity)) {
                    new RestCaller(OrderDBAdapter.this, SmartWashr.getRestClient().orderDetail("Bearer " + sessionManager.get(Constants.ACCESS_TOKEN), String.valueOf(getObject(position).getId())), 1);
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Loading.show(activity, false, Constants.TRANSLATIONS.getPleaseWaitLable());
                    } else {
                        Loading.show(activity, false, "الرجاء الانتظار");
                    }
                    price_total = getObject(position).getSwTotalPrice();
                    order_id = getObject(position).getOrder_id();
                    status = getObject(position).getStatus();
                    SessionManager.put(Constants.ORDER_ID, order_id);
                } else {
                    if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
                        Toast.makeText(activity, "No internet available", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, "تأكد من اتصالك بالانترنت", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    @Override
    public int getItemCount() {
        return lst.size();
    }

    @Override
    public void onSuccess(Call call, Response response, int reqCode) {
        Loading.cancel();
        DetailModel detailModel = (DetailModel) response.body();
        Orders orders = (Orders) detailModel.getOrders();

        for (int i = 0; i < orders.getOrderDetail().size(); i++) {
            OrderDetail orderDetail = (OrderDetail) orders.getOrderDetail().get(i);
            list_product.add(orderDetail);
        }

        Gson gson = new Gson();
        String product_list = gson.toJson(list_product);
        Intent i = new Intent(activity, OrderDetailActivity.class);
        i.putExtra("order_id", order_id);
        list_product.clear();
        activity.startActivity(i);

    }

    @Override
    public void onFailure(Call call, GenericResponse error, int reqCode) {
        Loading.cancel();
    }

    @Override
    public void onApiCrash(Call call, Throwable t, int reqCode) {
        Loading.cancel();
    }


    @Override
    public void onError(Call call, ERRORSO error, int reqCode) {
        if(error.getError()!=null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setTitle(error.getError().getErrorTitle());
            alertDialogBuilder.setMessage(error.getError().getMessage());
            alertDialogBuilder.setPositiveButton(Constants.TRANSLATIONS.getOkLabel(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        }
    }
}
