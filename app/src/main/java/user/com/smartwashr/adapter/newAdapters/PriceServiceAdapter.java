package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.pricing.Service;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.SessionManager;

public class PriceServiceAdapter extends RecyclerView.Adapter<ServiceVH> {

    Activity activity;
    List<Service> list;

    public PriceServiceAdapter(Activity activity, List<Service> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ServiceVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_price, parent, false);
        return new ServiceVH(v);
    }

    @Override
    public void onBindViewHolder(ServiceVH holder, int position) {
        Service item = list.get(position);

        if (new SessionManager(activity).get(Constants.LANG).equalsIgnoreCase("en")) {
            holder.tv_serviceName.setText(item.getName());
            holder.tv_servicePrice.setText(Constants.TRANSLATIONS.getCurrency() + " " + item.getPrice());
        } else {
            holder.tv_serviceName.setText(item.getNameAr());
            holder.tv_servicePrice.setText(item.getPrice() + " " + Constants.TRANSLATIONS.getCurrency());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
