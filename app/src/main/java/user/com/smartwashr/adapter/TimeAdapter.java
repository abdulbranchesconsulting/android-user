package user.com.smartwashr.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.TimePasser;

/**
 * Created by farazqureshi on 10/01/2018.
 */

public class TimeAdapter extends RecyclerView.Adapter<TimeViewHolder> implements TimePasser {

    private ArrayList<String> list;
    private Activity act;
    private ArrayList<Integer> pos = new ArrayList<>();
    private TextView done, view_title;
    private String item;
    private int col_del;

    private TimePasser timePasser;

    public TimeAdapter(ArrayList<String> list, Activity activity, TextView tv_done, TimePasser time_pass, int colDel, TextView viewTitle) {
        this.list = list;
        this.act = activity;
        this.done = tv_done;
        this.timePasser = time_pass;
        this.col_del = colDel;
        this.view_title = viewTitle;
    }

    @Override
    public TimeViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_view_holder, parent, false);
        return new TimeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TimeViewHolder holder, final int postion) {


        item = list.get(postion);
        holder.textView.setText(item);

        if(col_del==1){
            view_title.setText("Select Delivery Time");
        }

        if (pos.contains(postion)) {
            holder.tick.setVisibility(View.VISIBLE);
        } else {
            holder.tick.setVisibility(View.GONE);
        }


        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos.size() > 0)
                    pos.clear();
                pos.add(postion);
                notifyDataSetChanged();

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos.size() > 0) {
                    if (col_del == 0) {
                        onTimePass(list.get(pos.get(0)), 1);
                    } else {
                        onTimePass(list.get(pos.get(0)), 2);
                    }
                } else {
                    Toast.makeText(act, view_title.getText().toString(), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onTimePass(String value, int i) {
        timePasser.onTimePass(value, i);
    }
}
