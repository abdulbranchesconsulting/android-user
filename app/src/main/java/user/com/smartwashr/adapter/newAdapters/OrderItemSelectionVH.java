package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

public class OrderItemSelectionVH extends RecyclerView.ViewHolder {

    TextView tv_service, tv_price1;
    ImageView iv_selected;
    RelativeLayout rl_s1;


    public OrderItemSelectionVH(View itemView) {
        super(itemView);

        rl_s1 = itemView.findViewById(R.id.rl_s1);
        tv_service = itemView.findViewById(R.id.tv_labl1);
        tv_price1 = itemView.findViewById(R.id.tv_pric_p1);
        iv_selected = itemView.findViewById(R.id.img_s1);

    }
}
