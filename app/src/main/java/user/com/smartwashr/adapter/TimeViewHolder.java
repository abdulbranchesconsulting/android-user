package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;

/**
 * Created by farazqureshi on 10/01/2018.
 */

class TimeViewHolder extends RecyclerView.ViewHolder {

    public TextView textView;
    public ImageView tick;

    public TimeViewHolder(View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.time_txt_view);
        tick = itemView.findViewById(R.id.tick);

    }
}
