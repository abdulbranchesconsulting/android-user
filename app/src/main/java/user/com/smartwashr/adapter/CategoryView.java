package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

/**
 * Created by zeeshan on 6/6/17.
 */

class CategoryView extends RecyclerView.ViewHolder {

    private TextView txt_title, txt_Desc, dry_p, wash_p, press_p;
    private ImageView img;
    private RelativeLayout new_parent;
    private String img_url;
    private LinearLayout pricing_con;
    TextView dryclean, washing, press;


    public CategoryView(View itemView) {
        super(itemView);

        new_parent = (RelativeLayout) itemView.findViewById(R.id.new_parent);
        pricing_con = (LinearLayout) itemView.findViewById(R.id.prices_con);
        txt_title = (TextView) itemView.findViewById(R.id.title);
        txt_Desc = (TextView) itemView.findViewById(R.id.desc);
        img = (ImageView) itemView.findViewById(R.id.img);
        dry_p = (TextView) itemView.findViewById(R.id.dry_clean_p);
        wash_p = (TextView) itemView.findViewById(R.id.washing_p);
        press_p = (TextView) itemView.findViewById(R.id.press_p);
        dryclean = (TextView) itemView.findViewById(R.id.dryclean);
        washing = (TextView) itemView.findViewById(R.id.washing);
        press = (TextView) itemView.findViewById(R.id.press);

        img_url = "";
    }

    public TextView getTxt_title() {
        return txt_title;
    }

    public void setTxt_title(TextView txt_title) {
        this.txt_title = txt_title;
    }

    public TextView getTxt_Desc() {
        return txt_Desc;
    }

    public void setTxt_Desc(TextView txt_Desc) {
        this.txt_Desc = txt_Desc;
    }

    public ImageView getImg() {
        return img;
    }

    public void setImg(ImageView img) {
        this.img = img;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public RelativeLayout getNew_parent() {
        return new_parent;
    }

    public void setNew_parent(RelativeLayout new_parent) {
        this.new_parent = new_parent;
    }

    public TextView getDry_p() {
        return dry_p;
    }

    public void setDry_p(TextView dry_p) {
        this.dry_p = dry_p;
    }

    public TextView getWash_p() {
        return wash_p;
    }

    public void setWash_p(TextView wash_p) {
        this.wash_p = wash_p;
    }

    public TextView getPress_p() {
        return press_p;
    }

    public void setPress_p(TextView press_p) {
        this.press_p = press_p;
    }

    public LinearLayout getPricing_con() {
        return pricing_con;
    }

    public void setPricing_con(LinearLayout pricing_con) {
        this.pricing_con = pricing_con;
    }
}