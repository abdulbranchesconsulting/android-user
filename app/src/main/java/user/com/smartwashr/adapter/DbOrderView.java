package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import user.com.smartwashr.R;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/19/17.
 */

public class DbOrderView extends RecyclerView.ViewHolder {

    private ImageView imageView;
    private TextView title_num, status, date, p_txt;
    private LinearLayout parent;
    TextView ar_title_num, ar_status, ar_price, ar_p_txt;
    LinearLayout ar_parent;
    ImageView ar_imageView;

    public DbOrderView(View itemView) {
        super(itemView);

        imageView = (ImageView) itemView.findViewById(R.id.order_status_img);
        title_num = (TextView) itemView.findViewById(R.id.title_num);
        status = (TextView) itemView.findViewById(R.id.status_progress);
        date = (TextView) itemView.findViewById(R.id.date);
        p_txt = (TextView) itemView.findViewById(R.id.price_txt);
        parent = (LinearLayout) itemView.findViewById(R.id.parent);

        ar_title_num = (TextView) itemView.findViewById(R.id.ar_title_num);
        ar_status = (TextView) itemView.findViewById(R.id.ar_status_progress);
        ar_price = (TextView) itemView.findViewById(R.id.ar_price);
        ar_p_txt = (TextView) itemView.findViewById(R.id.ar_price_txt);
        ar_imageView = (ImageView) itemView.findViewById(R.id.ar_order_status_img);
        ar_parent = (LinearLayout) itemView.findViewById(R.id.ar_parent);

        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            FontUtils.setRobotoBold(title_num);
            FontUtils.setRobotoMediumFont(status);
            FontUtils.setRobotoMediumFont(date);
            FontUtils.setRobotoMediumFont(p_txt);
        } else {
            FontUtils.setArabic(ar_price);
            FontUtils.setArabic(ar_title_num);
            FontUtils.setArabic(ar_status);
            FontUtils.setArabic(ar_p_txt);
        }
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TextView getTitle_num() {
        return title_num;
    }

    public void setTitle_num(TextView title_num) {
        this.title_num = title_num;
    }

    public TextView getStatus() {
        return status;
    }

    public void setStatus(TextView status) {
        this.status = status;
    }

    public TextView getDate() {
        return date;
    }

    public void setDate(TextView date) {
        this.date = date;
    }

    public LinearLayout getParent() {
        return parent;
    }

    public void setParent(LinearLayout parent) {
        this.parent = parent;
    }

    public TextView getP_txt() {
        return p_txt;
    }

    public void setP_txt(TextView p_txt) {
        this.p_txt = p_txt;
    }

    public TextView getAr_p_txt() {
        return ar_p_txt;
    }

    public void setAr_p_txt(TextView ar_p_txt) {
        this.ar_p_txt = ar_p_txt;
    }
}
