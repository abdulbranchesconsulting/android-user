package user.com.smartwashr.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.models.orderDetailResponse.OrderDetail;
import user.com.smartwashr.utils.Constants;
import user.com.smartwashr.utils.FontUtils;
import user.com.smartwashr.utils.SessionManager;

/**
 * Created by zeeshan on 6/25/17.
 */

public class DetailAdapter extends RecyclerView.Adapter<DetailView> {

    private ArrayList<OrderDetail> lst;
    private Activity activity;

    public DetailAdapter(Activity activity, ArrayList<OrderDetail> list) {
        lst = list;
        this.activity = activity;
    }

    private OrderDetail getObject(int i) {
        return (lst.get(i));
    }

    @Override
    public DetailView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_view, parent, false);
        return new DetailView(itemView);
    }

    @Override
    public void onBindViewHolder(DetailView holder, int position) {
        if (SessionManager.get(Constants.LANG).equalsIgnoreCase("english")) {
            holder.getAr_parent().setVisibility(View.GONE);
            holder.getProduct().setVisibility(View.VISIBLE);
            FontUtils.setFont(holder.total);
            FontUtils.setFont(holder.total_num);
            FontUtils.setFont(holder.txt_title);
            holder.txt_title.setText(getObject(position).getProduct().get(0).getName());
            holder.total_num.setText(getObject(position).getQuantity() + " x ");
            holder.total.setText("SAR " + getObject(position).getSwPrice());
        } else {
            holder.getProduct().setVisibility(View.GONE);
            holder.getAr_parent().setVisibility(View.VISIBLE);
            FontUtils.setArabic(holder.ar_txt_title);
            FontUtils.setArabic(holder.total_num);
            FontUtils.setArabic(holder.ar_total);

            holder.ar_txt_title.setText(getObject(position).getProduct().get(0).getNameAr());
            holder.ar_total_num.setText("x " + getObject(position).getQuantity());
            holder.ar_total.setText(getObject(position).getSwPrice() + " ريال ");
        }
    }


    @Override
    public int getItemCount() {
        return lst.size();
    }

}
