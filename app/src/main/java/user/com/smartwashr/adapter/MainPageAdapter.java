package user.com.smartwashr.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import user.com.smartwashr.fragments.MensProductFragment;
import user.com.smartwashr.fragments.MiscProductFragment;
import user.com.smartwashr.fragments.WomenProductsFragment;

/**
 * Created by zeeshan on 11/24/17.
 */

public class MainPageAdapter  extends FragmentPagerAdapter {
    MensProductFragment mensProductFragment;
    WomenProductsFragment womenProductsFragment;
    MiscProductFragment miscProductFragment;

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public MainPageAdapter(FragmentManager fm, int tabCount, MensProductFragment men, WomenProductsFragment women, MiscProductFragment misc) {
        super(fm);
        this.tabCount = tabCount;
        mensProductFragment = men;
        womenProductsFragment = women;
        miscProductFragment = misc;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return mensProductFragment;
            case 1:
                return womenProductsFragment;
            case 2:
                return miscProductFragment;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}