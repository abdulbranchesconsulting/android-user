package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

/**
 * Created by zeeshan on 9/6/17.
 */

class ProductView extends RecyclerView.ViewHolder {

    private TextView total_num, txt_title, total;
    private String img_url;
    private LinearLayout product;

    public ProductView(View itemView) {
        super(itemView);

        txt_title = (TextView) itemView.findViewById(R.id.ordr_title);
        total_num = (TextView) itemView.findViewById(R.id.number);
        total = (TextView) itemView.findViewById(R.id.total);
        product = (LinearLayout) itemView.findViewById(R.id.product);

        img_url = "";
    }

    public TextView getTotal_num() {
        return total_num;
    }

    public void setTotal_num(TextView total_num) {
        this.total_num = total_num;
    }

    public TextView getTxt_title() {
        return txt_title;
    }

    public void setTxt_title(TextView txt_title) {
        this.txt_title = txt_title;
    }

    public TextView getTotal() {
        return total;
    }

    public void setTotal(TextView total) {
        this.total = total;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public LinearLayout getProduct() {
        return product;
    }

    public void setProduct(LinearLayout product) {
        this.product = product;
    }
}