package user.com.smartwashr.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;

/**
 * Created by farazqureshi on 09/05/2018.
 */

public class AddressView extends RecyclerView.ViewHolder {

    public TextView title, building, address;
    public ImageView delete, edit;

    public AddressView(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.address_title);
        address = itemView.findViewById(R.id.address_name);
        delete = itemView.findViewById(R.id.delete);
        edit = itemView.findViewById(R.id.edit);
    }
}
