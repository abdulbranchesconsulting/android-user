package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import user.com.smartwashr.R;

public class ServiceVH extends RecyclerView.ViewHolder {

    TextView tv_serviceName, tv_servicePrice;
    ImageView img;

    public ServiceVH(View itemView) {
        super(itemView);
        tv_serviceName = itemView.findViewById(R.id.tv_service_name);
        tv_servicePrice = itemView.findViewById(R.id.tv_price);
        img = itemView.findViewById(R.id.img);
    }
}
