package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import user.com.smartwashr.R;


public class FAQView extends RecyclerView.ViewHolder {

    TextView tv_how_long;

    public FAQView(View itemView) {
        super(itemView);
        tv_how_long = itemView.findViewById(R.id.tv_how_long);
    }
}
