package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import user.com.smartwashr.R;

public class OrderItemVH extends RecyclerView.ViewHolder {

    TextView tv_product, tv_qty, tv_price;

    public OrderItemVH(View itemView) {
        super(itemView);

        tv_product = itemView.findViewById(R.id.tv_product);
        tv_qty = itemView.findViewById(R.id.tv_qty);
        tv_price = itemView.findViewById(R.id.tv_cost);

    }
}
