package user.com.smartwashr.adapter.newAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import user.com.smartwashr.R;

public class NewPriceVH extends RecyclerView.ViewHolder {

    TextView tv_title, tv_qty;
    RecyclerView recyclerView;
    RelativeLayout iv_plus, iv_minus;

    public NewPriceVH(View itemView) {
        super(itemView);
        tv_title = itemView.findViewById(R.id.tv_title);

        tv_qty = itemView.findViewById(R.id.tv_qty);

        recyclerView = itemView.findViewById(R.id.item_recycler);

        iv_plus = itemView.findViewById(R.id.iv_plus);
        iv_minus = itemView.findViewById(R.id.iv_minus);

    }
}
