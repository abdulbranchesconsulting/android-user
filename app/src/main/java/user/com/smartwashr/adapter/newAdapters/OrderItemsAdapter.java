package user.com.smartwashr.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import user.com.smartwashr.R;
import user.com.smartwashr.newModels.OrderProduct;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemVH> {

    Activity activity;
    ArrayList<OrderProduct> list;

    public OrderItemsAdapter(Activity activity, ArrayList<OrderProduct> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_layout, parent, false);
        return new OrderItemVH(v);
    }

    @Override
    public void onBindViewHolder(OrderItemVH holder, int position) {
        OrderProduct item = list.get(position);

        holder.tv_product.setText(item.getProductName());
        holder.tv_price.setText(item.getServicePrice()+"");
        holder.tv_qty.setText("x" + item.getProductQty());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
